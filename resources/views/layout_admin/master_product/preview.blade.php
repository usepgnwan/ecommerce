@extends('template_admin.admin')

@section('title', $title . ' || Preview Produk')
@section('container')
    <div class="container bg-white p-3">
        <!-- product -->
        <div class="product-content product-wrap clearfix product-deatil">
            <div class="row">
                <div class="col-md-5 col-sm-12 col-xs-12">
                    @if (count($product->images) != 0)
                    <div id="image_utama">
                        <img class="d-block w-100" src="{{ asset('uploads/' . $product->images[0]->image_name) }}">
                    </div>
                    @else
                    NO IMAGE
                    @endif
                    <div class="row">
                        @foreach ($product->images as $image)
                            <div class="col-3">
                                <a href="#"
                                    onclick="change_image(`image`,`{{ asset('uploads/' . $image->image_name) }}`)">
                                    <img class="d-block w-100 border" src="{{ asset('uploads/' . $image->image_name) }}">
                                </a>
                            </div>
                        @endforeach
                        @if ($product->embed)
                            @foreach (json_decode($product->embed) as $link_embed)
                                <div class="col-3">
                                    <a href="#" onclick="change_image(`video`,`{{ $link_embed }}`)">
                                        {{-- <iframe width="100%" height="80" src="{{ $link_embed }}"
                                            onclick="javascript.void(0)" style="z-index: -100"></iframe> --}}
                                        <img class="d-block w-100 border" style="height: 100%"
                                            src="{{ str_replace('https://www.youtube.com/embed/', 'https://img.youtube.com/vi/', $link_embed . '/mqdefault.jpg') }}">

                                    </a>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

                <div class="col-md-6 col-md-offset-1 col-sm-12 col-xs-12">
                   <h2 class="name">
                    <a href="{{ route('products.product_details',['slug'=> $product->slug])}}" target="_blank">
                        {{ $product->nama_produk }}
                    </a>
                    </h2>
                    <small>Kategori
                        @foreach ($product->kategori as $kategori)
                            <a href="{{url('products')}}/{{ $kategori->slug }}" target="_blank">{{ $kategori->category_name }}</a>
                    </small>
                    @endforeach
                    <hr />
                    Harga: <br>
                    <h3 class="price-container">
                        @if ($product->diskon == '' || $product->diskon == null)
                            Rp . {{ number_format($product->harga_jual, 0, ',', '.') }};
                        @else
                            @php
                                $potongan = ($product->harga_jual * $product->diskon) / 100;
                                $jml = $product->harga_jual - $potongan;
                            @endphp
                            <small><del>Rp. {{ number_format($product->harga_jual, 0, ',', '.') }}</del></small> <br>
                            Rp. {{ number_format($jml, 0, ',', '.') }}
                            <br>( disc: {{ $product->diskon }}%) ;
                        @endif

                    </h3>
                    <hr />
                    Deskripsi: <br>
                    <p>{!! $product->deskripsi !!}</p>
                </div>
            </div>
        </div>
        <!-- end product -->
    </div>
    <script>
        function change_image(type, lokasi) {
            var link = "{{ asset('uploads/') }}" + lokasi;
            if (type == 'image') {
                $('#image_utama').html(`<img class="d-block w-100" id="image_utama"
                src="` + lokasi + `">`)
            } else if (type == 'video') {
                $('#image_utama').html(`<iframe width="100%" height="400" src="` + lokasi + `" title="YouTube video player"
                                    frameborder="0"
                                    allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                                    allowfullscreen></iframe>`);
            }
        }
    </script>
@endsection
