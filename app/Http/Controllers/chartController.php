<?php

namespace App\Http\Controllers;

use App\Models\MetodePembayaran;
use App\Models\Produk;
use App\Models\StoreSetting;
use App\Models\WeightOngkir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\PseudoTypes\True_;
use RajaOngkir;

class chartController extends Controller
{
    protected $produk;
    protected $metode_pembayaran;
    protected $weightOngkir;
    protected $store_setting;
    public function __construct(Produk $produk, MetodePembayaran $metode_pembayaran, WeightOngkir $weightOngkir, StoreSetting $store_setting)
    {
        $this->produk = $produk;
        $this->metode_pembayaran = $metode_pembayaran;
        $this->weightOngkir = $weightOngkir;
        $this->store_setting = $store_setting;
    }

    public function index()
    {
        $title = 'List Produk';
        return view('product', compact('title'));
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'size' => 'required|not_in:0',
            'quantity' => 'required|not_in:0',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Simpan Alamat.', 'data' => $validate->errors()]);
        }

        $find = $this->produk->find($request->id);
        // return response()->json($find);
        // die;

        $images = '';
        if (count($find->images) >= 1) {
            $images = asset('uploads/' . $find->images[0]->image_name);
        } else {
            $images = asset('assets_frontend/images/shoe-detail/3.jpg');
        }
        $total_harga = '';
        if ($request->harga_diskon != 0) {
            $total_harga = $request->quantity * $request->harga_diskon;
        } else {
            $total_harga = $request->quantity * $request->harga;
        }
        $size = explode('|', $request->size);
        $id_co = $request->id . '-' . $size[0];

        $data = [
            'id_produk' => $find->id,
            'nama_produk' => $find->nama_produk,
            'diskon' => $find->diskon,
            'images' => $images,
            'quantity' => $request->quantity,
            'harga' => $request->harga,
            'harga_diskon' => $request->harga_diskon,
            'total_harga' => $total_harga,
            'id_stok' => $size[0],
            'id_size' => $size[1],
            'size' => $size[2],
        ];

        $cart = session()->get('chart_checkout');

        if (isset($cart[$id_co])) {
            $cart[$id_co]['quantity'] += $request['quantity'];
            $cart[$id_co]['total_harga'] += $total_harga;

            session()->put('chart_checkout', $cart);
            $countQuantity =  array_sum(array_column(session()->get('chart_checkout'), 'quantity'));
            $countTotal =   array_sum(array_column(session()->get('chart_checkout'), 'total_harga'));
            return response()->json(
                [
                    'status' => True,
                    'all_checkout' => session()->get('chart_checkout'),
                    'data_co' => $data,
                    'countQuantity' => $countQuantity,
                    'countTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                    'Total' =>  $countTotal
                ]
            );
        }

        if (!$cart) {
            $cart = [
                $id_co => $data
            ];
            session()->put('chart_checkout', $cart);
        }

        // return response()->json(session()->get('chart_checkout'));
        // die;
        $cart[$id_co] = $data;
        session()->put('chart_checkout', $cart);
        $countQuantity =  array_sum(array_column(session()->get('chart_checkout'), 'quantity'));
        $countTotal =   array_sum(array_column(session()->get('chart_checkout'), 'total_harga'));
        // Session::flash('success', 'berhasil menambah data');
        return response()->json(
            [
                'status' => True,
                'all_checkout' => session()->get('chart_checkout'),
                'data_co' => $data,
                'countQuantity' => $countQuantity,
                'countTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                'Total' =>  $countTotal
            ]
        );

        // dd(session()->get('chart_checkout'));
        // dd(session('cart'));
        // return redirect()->route('chart.create', $data);
    }

    public function create(Request $request)
    {
        // if ($request->all()) {
        $data = collect(session('chart'));
        dd($data->where('id', 2)->replace('quantity', 0));
        // return dd($request->all());

        // }

        // return  redirect()->route('chart.index');

        // return is_null($request->all());
    }

    public function wilshit()
    {
        // Session::flash('success', 'berhasil menambah data');
        if (session('chart_checkout')) {
            $data = session()->get('chart_checkout');
        } else {
            $data = [];
        }


        $title = 'List Checkout';
        return view('wilshit', compact('title', 'data'))->with('success', 'berhasil menambah data');
    }

    public function show($id)
    {
        echo $id;
    }

    public function listing_checkout_cart()
    {
        // session()->forget('chart_checkout');
        // $cart = session()->get('chart_checkout');
        // if(isNull($cart)){
        //     $cart =[];
        // }
        // echo json_encode($cart);
        // $epromo = session()->get('e-promo');
        // dd($epromo);
        return view('frontend.product_checkout.checkout_cart');
    }

    public function destroy($id)
    {
        // echo json_encode($id);
        if ($id) {
            $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
            $cart = is_null(session()->get('chart_checkout')) ? [] : session()->get('chart_checkout');
            if (isset($cart[$id])) {
                unset($cart[$id]);
                session()->put('chart_checkout', $cart);
            }
            $getCart = is_null(session()->get('chart_checkout')) ? [] : session()->get('chart_checkout');
            $countQuantity =  array_sum(array_column($getCart, 'quantity'));
            $countTotal =   array_sum(array_column($getCart, 'total_harga'));

            $destroyVoucher = False;
            if(count($cart) == 0){
                if(isset($epromo)) {
                    unset($epromo);
                    $epromo = session()->put('e-promo', []);
                }
            }
            $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
            $voucherCount = [];
            if(count($epromo) != 0){
                foreach($epromo as $ix => $val){
                    $check = explode('.', $ix);
                    if($check[0] =='diskon'){
                        array_push($voucherCount, ($val->promo * $countTotal) /100);
                    }else{
                        array_push($voucherCount, $val->promo );
                    }
                }
            }else{
                $destroyVoucher = True;
            }
            $voucher = array_sum($voucherCount);

            $totalakhir = count($voucherCount)!= 0 ? $countTotal - $voucher:  $countTotal;
            // Session::flash('success', 'berhasil menambah data');
            return response()->json(
                [
                    'status' => True,
                    'all_checkout' => session()->get('chart_checkout'),
                    'countQuantity' => $countQuantity,
                    'countTotal' => 'Rp.' . number_format($totalakhir, 0, ',', '.'),
                    'subTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                    'Total' =>  $totalakhir,
                    'NormalTotal'=>  $countTotal,
                    'evocher' => $epromo,
                    'evocherstatus' => $destroyVoucher
                ]
            );
        }
    }

    public function update(Request $request, $id)
    {
        $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
        // dd($epromo);
        $cart = session()->get('chart_checkout');
        if (isset($cart[$id])) {
            $cart[$id]['quantity'] = $request['qty'];
            if ($cart[$id]['harga_diskon'] != 0 || $cart[$id]['harga_diskon'] != 0) {
                $cart[$id]['total_harga'] = $cart[$id]['harga_diskon'] *  $request['qty'];
            } else {
                $cart[$id]['total_harga'] = $cart[$id]['harga'] *  $request['qty'];
            }

            session()->put('chart_checkout', $cart);
            // echo json_encode(session()->get('chart_checkout'));
            // die;
            $countQuantity =  array_sum(array_column(session()->get('chart_checkout'), 'quantity'));
            $countTotal =   array_sum(array_column(session()->get('chart_checkout'), 'total_harga'));
            $voucherCount = [];
            if(count($epromo) != 0){
                foreach($epromo as $ix => $val){
                    $check = explode('.', $ix);
                    if($check[0] =='diskon'){
                        array_push($voucherCount, ($val->promo * $countTotal) /100);
                    }else{
                        array_push($voucherCount, $val->promo );
                    }
                }
            }
            $voucher = array_sum($voucherCount);

            $totalakhir = count($voucherCount)!= 0 ? $countTotal - $voucher:  $countTotal;
            return response()->json(
                [
                    'status' => True,
                    'all_checkout' => session()->get('chart_checkout'),
                    'totalHargaItem' => 'Rp.' . number_format(session()->get('chart_checkout')[$id]['total_harga'], 0, ',', '.'),
                    'countQuantity' => $countQuantity,
                    'countTotal' => 'Rp.' . number_format($totalakhir, 0, ',', '.'),
                    'subTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                    'Total' =>  $totalakhir,
                    'NormalTotal'=>  $countTotal,
                    'evocher' => $epromo
                ]
            );
        }
    }

    public function payment_checkout_cart(Request $request)
    {
        $rajaOngkir = new RajaOngkir;
        $metode_pembayaran = $this->metode_pembayaran->where('status', '=', 'on')->get();
        // echo json_encode($metode_pembayaran);die;
        $provinsi = $rajaOngkir->get_data_provinsi_kota('provinsi');
        return view('frontend.product_checkout.payment_cart', compact('provinsi', 'metode_pembayaran'));
    }

    public function check_ongkir(Request $request)
    {
        $store_setting = $this->store_setting->where(['id' => '1'])->first();
        // $weight = $this->weightOngkir->whereHas('detail_weight_ongkir', function ($query)  use ($request) {
        //     $query->where('jumlah',  $request->qty);
        // })->first();
        // $weight = $this->weightOngkir->whereBetween('10', ['pcs_dari','pcs_sampai'])->first();
        $weight = collect(DB::select('select * from weight_ongkirs where ' . $request->qty . ' between pcs_dari and pcs_sampai'))->first();

        // ->where('pcs_dari', '>=', $request->qty)->where('pcs_sampai', '<=', $request->qty)->get();

        $weights = 3000;
        if (!is_null($weight)) {
            $weights = (float)$weight->weight_gram;
        }
        // echo json_encode($weight);
        // die;
        $rajaOngkir = new RajaOngkir;
        $id_kota_pengirim = explode('|', $request->id_kota_pengirim);
        $provinsi = $rajaOngkir->get_ongkir($store_setting->id_kota, $id_kota_pengirim[0], 'jne', $weights);
        echo json_encode($provinsi);
    }

    public function add_promo(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'kode_voucher' => 'required',
        ]);
        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, kode voucher masih kosong.', 'data' => $validate->errors()]);
        }
        $cart = is_null(session()->get('chart_checkout')) ? [] : session()->get('chart_checkout') ;
        $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
        // if(isset($epromo)) {
        //     unset($epromo);
        //   $epromo = session()->put('e-promo', []);
        // }
        // return $epromo;
        // die;
        // $epromo = session()->get('e-promo');
        if (count($epromo) != 0) {
            // $countQuantity = !is_null($cart) ? array_sum(array_column(session()->get('chart_checkout'), 'quantity')) : 0;
            // $countTotal =  !is_null($cart) ?  array_sum(array_column(session()->get('chart_checkout'), 'total_harga')): 0;
            return response()->json(
                [
                    'status' => False,
                    // 'all_checkout' => session()->get('chart_checkout'),
                    // 'countQuantity' => $countQuantity,
                    // 'countTotal'=> 'Rp.' . number_format($countTotal, 0, ',', '.'),
                    // 'Total'=>  $countTotal,
                    // 'NormalTotal'=>  $countTotal,
                    // 'evocher' => $epromo,
                    'msg' => 'Anda telah menggunakan e-voucher.'
                ]
            );
        } else {
            if (count($cart) != 0) {
                $countQuantity =  array_sum(array_column(session()->get('chart_checkout'), 'quantity'));
                $countTotal =   array_sum(array_column(session()->get('chart_checkout'), 'total_harga'));
                $evoucher_kode = DB::select('SELECT * FROM  voucher_kodes  WHERE status = "on" And kode ="' . $request->kode_voucher . '"  AND deleted_at IS NULL AND CURDATE() BETWEEN periode_awal And periode_akhir;');
                $data = collect($evoucher_kode)->first();
                $check_login = !is_null(auth()->user()) ? 'login':'all';
                if (!is_null($data)) {
                    if($data->kuota >= 1 ){
                        if($data->type_user == 'login'){
                            if($check_login == 'login'){
                                if (!$epromo) {
                                    $epromo = [$data->type_voucher . '.'.$data->kode => $data];
                                    session()->put('e-promo', $epromo);
                                }

                                $voucherCount = [];
                                if(count($epromo) != 0){
                                    foreach($epromo as $ix => $val){
                                        $check = explode('.', $ix);
                                        if($check[0] =='diskon'){
                                            array_push($voucherCount, ($val->promo * $countTotal) /100);
                                        }else{
                                            array_push($voucherCount, $val->promo );
                                        }
                                    }
                                }
                                $voucher = array_sum($voucherCount);
                                $totalakhir = count($voucherCount)!= 0 ? $countTotal - $voucher:  $countTotal;

                                return response()->json(
                                    [
                                        'status' => True,
                                        'all_checkout' => session()->get('chart_checkout'),
                                        'countQuantity' => $countQuantity,
                                        'countTotal' => 'Rp.' . number_format($totalakhir, 0, ',', '.'),
                                        'subTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                                        'Total' =>  $totalakhir,
                                        'NormalTotal'=>  $countTotal,
                                        'evocher' => $epromo,
                                        'msg' => 'E-voucher berhasil digunakan'
                                    ]
                                );
                            }else{
                                return response()->json(
                                    [
                                        'status' => False,
                                        'msg' => 'voucher hanya dapat digunakan untuk user yang telah terdaftar / login'
                                    ]
                                );
                            }
                        }else{
                            if (!$epromo) {
                                $epromo = [$data->type_voucher . '.'.$data->kode => $data];
                                session()->put('e-promo', $epromo);
                            }
                            $voucherCount = [];
                            if(count($epromo) != 0){
                                foreach($epromo as $ix => $val){
                                    $check = explode('.', $ix);
                                    if($check[0] =='diskon'){
                                        array_push($voucherCount, ($val->promo * $countTotal) /100);
                                    }else{
                                        array_push($voucherCount, $val->promo );
                                    }
                                }
                            }
                            $voucher = array_sum($voucherCount);
                            $totalakhir = count($voucherCount)!= 0 ? $countTotal - $voucher:  $countTotal;
                            return response()->json(
                                [
                                    'status' => True,
                                    'all_checkout' => session()->get('chart_checkout'),
                                    'countQuantity' => $countQuantity,
                                    'countTotal' => 'Rp.' . number_format($totalakhir, 0, ',', '.'),
                                    'subTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                                    'Total' =>  $totalakhir,
                                    'NormalTotal'=>  $countTotal,
                                    'evocher' => $epromo,
                                    'msg' => 'e-voucher berhasil digunakan'
                                ]
                            );
                        }
                    }else{
                        return response()->json(
                            [
                                'status' => False,
                                'msg' => 'kuota voucher sudah habis'
                            ]
                        );
                    }
                } else {
                    return response()->json(
                        [
                            'status' => False,
                            // 'all_checkout' => session()->get('chart_checkout'),
                            // 'totalHargaItem' => 'Rp.' . number_format(session()->get('chart_checkout')[$id]['total_harga'], 0, ',', '.'),
                            // 'countQuantity' => $countQuantity,
                            // 'countTotal'=> 'Rp.' . number_format($countTotal, 0, ',', '.'),
                            // 'Total'=>  $countTotal
                            'msg' => 'voucher tidak tersedia / sudah tidak dapat digunakan'
                        ]
                    );
                }
            } else {
                return response()->json(
                    [
                        'status' => False,
                        // 'all_checkout' => session()->get('chart_checkout'),
                        // 'totalHargaItem' => 'Rp.' . number_format(session()->get('chart_checkout')[$id]['total_harga'], 0, ',', '.'),
                        // 'countQuantity' => $countQuantity,
                        // 'countTotal'=> 'Rp.' . number_format($countTotal, 0, ',', '.'),
                        // 'Total'=>  $countTotal
                        'msg' => 'voucher tidak dapat digunakan keranjang masih kosong'
                    ]
                );
            }
        }
    }

    public function delete_promo(Request $request){
        if ($request->id) {
            $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
            $getCart = is_null(session()->get('chart_checkout')) ? [] : session()->get('chart_checkout');
            $countQuantity =  array_sum(array_column($getCart, 'quantity'));
            $countTotal =   array_sum(array_column($getCart, 'total_harga'));

            $destroyVoucher = False;
            // if(count($epromo) == 0){
            //     if(isset($epromo)) {
            //         unset($epromo);
            //         $epromo = session()->put('e-promo', []);
            //     }
            // }
            if (isset($epromo[$request->id])) {
                unset($epromo[$request->id]);
                session()->put('e-promo', $epromo);
            }
            $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
            $voucherCount = [];
            if(count($epromo) != 0){
                foreach($epromo as $ix => $val){
                    $check = explode('.', $ix);
                    if($check[0] =='diskon'){
                        array_push($voucherCount, ($val->promo * $countTotal) /100);
                    }else{
                        array_push($voucherCount, $val->promo );
                    }
                }
            }else{
                $destroyVoucher = True;
            }

            $voucher = array_sum($voucherCount);

            $totalakhir = count($voucherCount)!= 0 ? $countTotal - $voucher:  $countTotal;
            // Session::flash('success', 'berhasil menambah data');
            return response()->json(
                [
                    'status' => True,
                    // 'all_checkout' => session()->get('chart_checkout'),
                    'countQuantity' => $countQuantity,
                    'countTotal' => 'Rp.' . number_format($totalakhir, 0, ',', '.'),
                    'subTotal' => 'Rp.' . number_format($countTotal, 0, ',', '.'),
                    'Total' =>  $totalakhir,
                    'NormalTotal'=>  $countTotal,
                    'evocher' => $epromo,
                    'evocherstatus' => $destroyVoucher
                ]
            );
        }
    }
}
