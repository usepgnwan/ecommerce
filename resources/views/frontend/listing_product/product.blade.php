@extends('frontend.template_frontend.frontend') @section('title', 'Product') @section('content')
<main class="ps-main">
    <div class="container">
        <div class="ps-products-wrap pt-80">
            <div class="@if ($routeCheck >1) ps-products_large @else ps-products @endif " data-mh="product-listing">
                @if (request()->search)
                <div class="row pb-10">
                    <div class="col-md-12">
                        Hasil Pencarian : <b>{{request()->search}}</b><hr>
                    </div>
                </div>
                @endif

                <div class="ps-product-action">
                    <div class="ps-product__filter">
                        <select class="ps-select selectpicker" name="short_by">
                            <option value="id" selected>Shortby</option>
                            <option value="name">Nama</option>
                            <option value="diskon">Diskon</option>
                            <option value="price_low">Harga Murah ke Mahal</option>
                            <option value="price_high">Harga Mahal ke Murah</option>
                        </select>
                    </div>
                    <div class="ps-pagination" style="display: flex; justify-content: flex-end; width:100%">
                        <aside class=" ps-widget--filter" style="width:300px !important">

                            <div class="ps-widget__header pb-10">
                                <h5>Price</h5>
                            </div>
                            <div class="ps-widget__content">
                                {{-- <div class="ac-slider slider-price" data-default-min="10000" data-default-max="300000"
                                    data-max="1000000" data-step="50" data-unit="$$"></div> --}}
                                <style>
                                    .price-range-slider {
                                        width: 100%;
                                        float: left;

                                        .range-bar {
                                            border: none;
                                            background: #cecece;
                                            height: 1px;
                                            width: 96%;
                                            margin-left: 8px;

                                            .ui-slider-range {
                                                background: #06b9c0;
                                            }

                                            .ui-slider-handle {
                                                border: none;
                                                border-radius: 25px;
                                                background: #cecece;
                                                border: 2px solid #50cf96;
                                                height: 17px;
                                                width: 17px;
                                                top: -0.52em;
                                                cursor: pointer;
                                            }

                                            .ui-slider-handle+span {
                                                background: #50cf96;
                                            }
                                        }
                                    }

                                    /*--- /.price-range-slider ---*/

                                </style>
                                <div class="price-range-slider">
                                    {{-- <p class="range-value">
                                        <input type="text" id="amount" readonly>
                                    </p> --}}
                                    <div id="slider-range" class="range-bar"></div>
                                </div>
                                <p class="ac-slider__meta">Price:
                                    <span class="ac-slider__value ac-slider__min min-slider-value"></span>-
                                    <span class="ac-slider__value ac-slider__max max-slider-value"></span>
                                </p>
                                <a class="ac-slider__filter ps-btn ps-btn-filter" href="javascript:void(0)">Filter</a>
                            </div>
                            <div></div>
                        </aside>
                    </div>
                </div>
                <div class="test" style="min-height: 700px; display: block !important;">
                    @include('frontend.listing_product.listing_product')
                </div>
            </div>
            @if ($routeCheck == 1)
            <div class="ps-sidebar" data-mh="product-listing"   >
                <aside class="ps-widget--sidebar ps-widget--category">
                    <div class="ps-widget__header">
                        <h3>Kategori</h3>
                    </div>
                    <div class="ps-widget__content">
                        <ul class="ps-list--checked">
                            @foreach ($kategori as $k)
                                <li class="category-filter" data-id="{{ $k->id }}"><a
                                        href="javascript:void(0)"> {{ Str::ucfirst($k->category_name) }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </aside>
            </div>
            @endif
        </div>
    </div>

</main>
@endsection
@section('js')
<script>
    $(function() {
        $("#slider-range").slider({
            range: true,
            min: 1000,
            max: 500000,
            values: [1000, 500000],
            slide: function(event, ui) {
                $('.min-slider-value').html('Rp.' + ui.values[0]);
                $('.max-slider-value').html('Rp.' + ui.values[1]);
                // $("#amount").val("$" + ui.values[0] + " - $" + ui.values[1]);
            }
        });

        $('.min-slider-value').html('Rp.' + $("#slider-range").slider("values", 0));
        $('.max-slider-value').html('Rp.' + $("#slider-range").slider("values", 1));
    });

    $(document).ready(function() {
        let $search = '{{request()->search}}';
        let $category_check = '{{$category_check}}';

        let $id = [];
        let $short_by = $('[name="short_by"]').val();

        let $min_price = $('.min-slider-value').html().split('Rp.')[1];
        let $max_price = $('.max-slider-value').html().split('Rp.')[1];

        let test = $('.ps-list--checked .current').map(function() {
            $id.push($(this).data('id'));
        });
        // $('.ps-list--checked .current').attr("data-id");;
        $(document).on('change', '[name="short_by"]', function(e) {
            $short_by = $(this).val();
            filter_data();
        });


        $(document).on('click', '.ps-btn-filter', function(e) {
            let min_price = $('.min-slider-value').html().split('Rp.')[1];
            let max_price = $('.max-slider-value').html().split('Rp.')[1];
            $min_price = min_price;
            $max_price = max_price;
            filter_data();
        });

        $(document).on('click', '.category-filter', function(e) {
            e.preventDefault();
            let id = $(this).data('id');
            let $this = $(this);
            let idx = $id.indexOf(id)
            if (idx >= 0) {
                $id.splice(idx, 1);
                $this.removeClass('current');
            } else {
                $id.push(id);
                $this.addClass('current');
            }
            filter_data();
        });
        $('.ps-btn-test').click(function() {

            $.ajax({
                url: '{{ route('products.listing_product') }}',
                data: {
                    id_kategori: $id
                },
                method: 'GET',
                beforeSend: function() {
                    $(".test").html(`
                        <div class="row mt-3 ">
                            <div class="col-md-12 d-flex justify-content-center text-center" style="color:#2ac37d">
                                <div>
                                    <div>
                                        <i class="fa fa-2x fa-spinner fa-spin"></i><h4 style="color:#2ac37d !important">loading</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `);
                },
                start_time: new Date().getTime(),
                success: function(data) {
                    setTimeout(function() {
                        $('.test').html(data);
                        //  $("#group-panel-ajax").find(res.loader).remove();
                        //  $('#group-panel-ajax').html($(data).find("#group-panel-ajax"));
                    }, (new Date().getTime() - this.start_time));
                }
            });
        });

        $(document).on('click', '.pagination a', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page);
        });

        function filter_data() {
            let id_kategori;
            if ($id.length == 0) {
                id_kategori = 0;
            } else {
                id_kategori = $id;
            }
            $.ajax({
                url: '{{ route('products.listing_product') }}',
                data: {
                    id_kategori: id_kategori,
                    short_by: $short_by,
                    min_price: $min_price,
                    max_price: $max_price,
                    search:$search,
                    category_check:$category_check
                },
                method: 'GET',
                beforeSend: function() {
                    $(".test").html(`
                        <div class="row mt-3 ">
                            <div class="col-md-12 d-flex justify-content-center text-center" style="color:#2ac37d">
                                <div>
                                    <div>
                                        <i class="fa fa-2x fa-spinner fa-spin"></i><h4 style="color:#2ac37d !important">loading</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    `);
                },
                start_time: new Date().getTime(),
                success: function(data) {
                    setTimeout(function() {
                        $('.test').html(data);
                        //  $("#group-panel-ajax").find(res.loader).remove();
                        //  $('#group-panel-ajax').html($(data).find("#group-panel-ajax"));
                    }, (new Date().getTime() - this.start_time));
                }
            });
        }

        function fetch_data(page) {
            let id_kategori;
            if ($id.length == 0) {
                id_kategori = 0;
            } else {
                id_kategori = $id;
            }
            // $.ajax({
            //     url: "/products/?page=" + page,
            //     success: function(data) {
            //         $('.test').html(data);
            //     }
            // });
            $.ajax({
                url: '{{ route('products.listing_product') }}',
                data: {
                    id_kategori: id_kategori,
                    page: page,
                    short_by: $short_by,
                    min_price: $min_price,
                    max_price: $max_price,
                    search:$search,
                    category_check:$category_check
                },
                method: 'GET',
                // beforeSend: function() {
                //     $(".test").html(`
                //         <div class="row mt-3">
                //             <div class="col-md-12 d-flex justify-content-center text-center" style="color:#2ac37d">
                //                 <div>
                //                     <div>
                //                         <i class="fa fa-2x fa-spinner fa-spin"></i><h4 style="color:#2ac37d !important">loading</h4>
                //                     </div>
                //                 </div>
                //             </div>
                //         </div>
                //     `);
                // },
                start_time: new Date().getTime(),
                success: function(data) {
                    setTimeout(function() {
                        $('.test').html(data);
                        //  $("#group-panel-ajax").find(res.loader).remove();
                        //  $('#group-panel-ajax').html($(data).find("#group-panel-ajax"));
                    }, (new Date().getTime() - this.start_time));
                }
            });
        }
    });
</script>
@endsection
