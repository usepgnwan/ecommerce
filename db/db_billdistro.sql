-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 15, 2023 at 01:59 AM
-- Server version: 5.7.36
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_billdistro`
--

-- --------------------------------------------------------

--
-- Table structure for table `adresses`
--

DROP TABLE IF EXISTS `adresses`;
CREATE TABLE IF NOT EXISTS `adresses` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `nama_penerima` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kota` int(11) NOT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_pos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `adresses_user_id_foreign` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `adresses`
--

INSERT INTO `adresses` (`id`, `user_id`, `nama_penerima`, `no_telp`, `id_provinsi`, `provinsi`, `id_kota`, `kota`, `no_pos`, `alamat`, `created_at`, `updated_at`) VALUES
(35, 1, 'Aisyah Qurani', '0896955567', 1, 'Bali', 128, 'Gianyar', '80519', 'tesstt', '2022-03-13 19:05:09', '2022-03-13 19:05:09'),
(36, 1, 'Flo Kreiger', '0797070760', 6, 'DKI Jakarta', 153, 'Jakarta Selatan', '12230', 'adafsfs', '2022-03-13 19:09:15', '2022-03-13 19:09:15'),
(37, 2, 'Aisyah Qurani', '0896955567', 9, 'Jawa Barat', 34, 'Banjar', '46311', 'lpjnkm.,', '2022-03-15 06:03:16', '2022-03-15 06:03:16'),
(38, 2, 'Mr. Arvid Keeling DVM', 'asdd', 3, 'Banten', 402, 'Serang', '42182', 'cfgvhjk', '2022-03-15 06:25:24', '2022-03-15 06:25:24');

-- --------------------------------------------------------

--
-- Table structure for table `banner_promos`
--

DROP TABLE IF EXISTS `banner_promos`;
CREATE TABLE IF NOT EXISTS `banner_promos` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `foto_banner_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'default-banner.png',
  `foto_banner_2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'default-banner.png',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_promos`
--

INSERT INTO `banner_promos` (`id`, `foto_banner_1`, `foto_banner_2`, `created_at`, `updated_at`) VALUES
(1, '32QtEEBhgF5E6nsAQefXxAXwNPPTcrap2kIgiIyk.jpg', 'XBB1b0tUtEVy0dGYIZptXv49idDHf3iQtz03QCMv.jpg', '2022-04-17 22:46:37', '2022-06-14 05:12:09');

-- --------------------------------------------------------

--
-- Table structure for table `banner_store`
--

DROP TABLE IF EXISTS `banner_store`;
CREATE TABLE IF NOT EXISTS `banner_store` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('on','off') COLLATE utf8mb4_unicode_ci DEFAULT 'on',
  `foto_banner` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '3.jpg',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banner_store`
--

INSERT INTO `banner_store` (`id`, `title`, `deskripsi`, `status`, `foto_banner`, `created_at`, `updated_at`) VALUES
(1, 'Gratis Ongkir', '<p>Promo Ramadhan<strong> Gratis Ongkir<br></strong>kesemua wilayah<strong><br></strong></p>', 'on', 'WObyrUpCh8xWFeF8IZJ3SNi6pjGAjmWbJIqC5WTb.jpg', '2022-03-27 03:35:45', '2022-05-29 05:19:15'),
(2, 'Promo Besar', '<p style=\"text-align: left;\">Segera Raih Hadianya,<br>sfsfsfsfsfs SEMANGAT!!</p>', 'on', 'wYmEl9wNTopxBmKSP5sEaEplPVA04qAz793uHJjT.jpg', '2022-03-27 03:35:45', '2022-04-09 05:04:03');

-- --------------------------------------------------------

--
-- Table structure for table `contact_us`
--

DROP TABLE IF EXISTS `contact_us`;
CREATE TABLE IF NOT EXISTS `contact_us` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_us_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact_us`
--

INSERT INTO `contact_us` (`id`, `nama`, `no_telp`, `email`, `deskripsi`, `created_at`, `updated_at`) VALUES
(1, 'Germaine Lynch', '1-458-333-3138', 'coty.konopelski@example.com', 'Itaque distinctio aut debitis corrupti culpa vero beatae sit quasi et veniam ipsum dolores provident enim aliquam sequi quia aut adipisci accusantium eum ut odio.', '2022-04-04 19:04:51', '2022-04-04 19:04:51'),
(2, 'Ms. Marina Reinger II', '364.322.5961', 'hiram25@example.com', 'Ipsa quidem eaque modi minus molestiae in perferendis autem natus maiores eius aut enim consequatur pariatur labore ratione vel et blanditiis sed rem et aut aperiam voluptas ipsa tenetur numquam repellendus libero occaecati et est.', '2022-04-04 19:04:51', '2022-04-04 19:04:51'),
(3, 'Brianne Romaguera', '731-968-9407', 'willy.wyman@example.net', 'Adipisci voluptas sint praesentium sit voluptate et et iste illum aperiam magni labore qui harum voluptatem animi voluptatem et laudantium et incidunt excepturi ducimus aut et sapiente cumque et delectus consequatur dolore officiis unde qui.', '2022-04-04 19:04:51', '2022-04-04 19:04:51'),
(4, 'Usep', '0896955567', 'jaclyn.pfannerstill@gmail.com', 'ds', '2022-04-04 20:58:57', '2022-04-04 20:58:57'),
(5, 'Aisyah Qurani', '0896955567', 'asa.dietrich@gmail.com', 'asfdggfn f nfhfdvfgd', '2022-04-04 21:02:27', '2022-04-04 21:02:27'),
(6, 'usep', '0896955567', 'cx@email.com', 'dsffsfs', '2022-04-04 21:10:02', '2022-04-04 21:10:02'),
(7, 'usep', '0896955567', 'usepgnwan@gmail.com', 'terimakasih', '2022-04-08 21:01:06', '2022-04-08 21:01:06');

-- --------------------------------------------------------

--
-- Table structure for table `detail_transaksis`
--

DROP TABLE IF EXISTS `detail_transaksis`;
CREATE TABLE IF NOT EXISTS `detail_transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `stock_id` bigint(20) UNSIGNED NOT NULL,
  `master_size_id` bigint(20) UNSIGNED NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `diskon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_diskon` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_item` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detail_transaksis_transaksi_id_foreign` (`transaksi_id`),
  KEY `detail_transaksis_product_id_foreign` (`product_id`),
  KEY `detail_transaksis_stock_id_foreign` (`stock_id`),
  KEY `detail_transaksis_master_size_id_foreign` (`master_size_id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_transaksis`
--

INSERT INTO `detail_transaksis` (`id`, `transaksi_id`, `product_id`, `stock_id`, `master_size_id`, `nama_produk`, `diskon`, `quantity`, `size`, `sub_total`, `harga_diskon`, `total_item`, `created_at`, `updated_at`) VALUES
(47, 41, 28, 26, 1, 'Produk Ke-8 Billdistro', NULL, '1', 'xl', '100000', '0', '100000', '2022-06-26 01:58:06', '2022-06-26 01:58:06'),
(48, 42, 28, 26, 1, 'Produk Ke-8 Billdistro', NULL, '1', 'xl', '100000', '0', '100000', '2022-06-26 04:37:47', '2022-06-26 04:37:47'),
(49, 42, 30, 28, 1, 'Test welah', NULL, '1', 'xl', '120000', '0', '120000', '2022-06-26 04:37:47', '2022-06-26 04:37:47'),
(50, 43, 28, 26, 1, 'Produk Ke-8 Billdistro', NULL, '2', 'xl', '100000', '0', '200000', '2022-06-26 04:48:29', '2022-06-26 04:48:29'),
(51, 43, 30, 28, 1, 'Test welah', NULL, '1', 'xl', '120000', '0', '120000', '2022-06-26 04:48:29', '2022-06-26 04:48:29'),
(52, 44, 24, 22, 1, 'Produk Ke-4 Billdistro', NULL, '3', 'xl', '100000', '0', '300000', '2022-06-26 05:14:58', '2022-06-26 05:14:58'),
(53, 44, 30, 28, 1, 'Test welah', NULL, '1', 'xl', '120000', '0', '120000', '2022-06-26 05:14:58', '2022-06-26 05:14:58'),
(54, 45, 23, 21, 2, 'Produk Ke-3 Billdistro', '15', '1', 'XXS', '120000', '102000', '102000', '2022-06-27 04:55:28', '2022-06-27 04:55:28'),
(55, 46, 24, 22, 1, 'Produk Ke-4 Billdistro', NULL, '1', 'xl', '100000', '0', '100000', '2022-06-28 04:23:42', '2022-06-28 04:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `detail_weight_ongkirs`
--

DROP TABLE IF EXISTS `detail_weight_ongkirs`;
CREATE TABLE IF NOT EXISTS `detail_weight_ongkirs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `weight_ongkir_id` bigint(20) UNSIGNED NOT NULL,
  `jumlah` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detail_weight_ongkirs_weight_ongkir_id_foreign` (`weight_ongkir_id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `detail_weight_ongkirs`
--

INSERT INTO `detail_weight_ongkirs` (`id`, `weight_ongkir_id`, `jumlah`, `created_at`, `updated_at`) VALUES
(45, 13, '6', '2022-04-17 01:51:42', '2022-04-17 01:51:42'),
(46, 13, '7', '2022-04-17 01:51:42', '2022-04-17 01:51:42'),
(47, 13, '8', '2022-04-17 01:51:42', '2022-04-17 01:51:42'),
(48, 13, '9', '2022-04-17 01:51:42', '2022-04-17 01:51:42'),
(49, 13, '10', '2022-04-17 01:51:42', '2022-04-17 01:51:42'),
(50, 13, '11', '2022-04-17 01:51:42', '2022-04-17 01:51:42'),
(51, 14, '12', '2022-04-17 01:51:59', '2022-04-17 01:51:59'),
(52, 14, '13', '2022-04-17 01:51:59', '2022-04-17 01:51:59'),
(53, 14, '14', '2022-04-17 01:51:59', '2022-04-17 01:51:59'),
(54, 14, '15', '2022-04-17 01:51:59', '2022-04-17 01:51:59'),
(55, 14, '16', '2022-04-17 01:51:59', '2022-04-17 01:51:59'),
(56, 11, '1', '2022-04-18 21:13:44', '2022-04-18 21:13:44'),
(57, 11, '2', '2022-04-18 21:13:44', '2022-04-18 21:13:44'),
(58, 11, '3', '2022-04-18 21:13:44', '2022-04-18 21:13:44'),
(59, 11, '4', '2022-04-18 21:13:44', '2022-04-18 21:13:44'),
(60, 11, '5', '2022-04-18 21:13:44', '2022-04-18 21:13:44');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faqs`
--

DROP TABLE IF EXISTS `faqs`;
CREATE TABLE IF NOT EXISTS `faqs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `question` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `most_question` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faqs`
--

INSERT INTO `faqs` (`id`, `question`, `answer`, `most_question`, `created_at`, `updated_at`) VALUES
(1, 'apaaaa', 'ahmad', 1, '2022-05-04 23:09:30', '2022-05-04 23:09:30'),
(2, 'kumaha', 'coy', 1, '2022-05-04 23:09:30', '2022-05-04 23:09:30');

-- --------------------------------------------------------

--
-- Table structure for table `master_category`
--

DROP TABLE IF EXISTS `master_category`;
CREATE TABLE IF NOT EXISTS `master_category` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `category_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `foto` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_category`
--

INSERT INTO `master_category` (`id`, `category_name`, `slug`, `created_at`, `updated_at`, `deleted_at`, `foto`) VALUES
(1, 'Baju (Koton Combat 100%)', 'baju-koton-combat-100', '2022-03-15 04:54:06', '2022-04-07 01:18:01', NULL, NULL),
(2, 'Celana Chinos', 'celana-chinos', '2022-04-03 02:54:30', '2022-04-10 02:41:32', NULL, '2mfRif7Cc2WLnmvWTvExiFLNXw3jL68igvIMDLJi.jpg'),
(3, 'Tshirt', 't-shirt', '2022-04-06 01:34:27', '2022-04-06 01:34:27', NULL, NULL),
(4, 'Jeans', 'jeans', '2022-04-06 22:23:19', '2022-04-07 01:30:47', '2022-04-07 01:30:47', NULL),
(5, 'celana dalam', 'cd', '2022-04-06 23:28:39', '2022-04-07 01:24:36', '2022-04-07 01:24:36', NULL),
(6, 'Jeans', 'jeans', '2022-04-08 21:09:19', '2022-04-10 02:40:58', NULL, 'fPzd94ViWReNilsbpc0lvlWWuEYgUAsG1U2uaVfp.jpg'),
(7, 'test', 'test', '2022-04-10 02:11:25', '2022-04-10 02:29:11', '2022-04-10 02:29:11', NULL),
(8, 'fsfs', 'fsfs', '2022-04-10 02:19:33', '2022-04-10 02:29:08', '2022-04-10 02:29:08', NULL),
(9, 'celana ddfdvdgf', 'celana-ddfdvdgf', '2022-04-10 02:20:19', '2022-04-10 02:29:04', '2022-04-10 02:29:04', NULL),
(10, 'celanacs', 'celanacs', '2022-04-10 02:22:04', '2022-04-10 02:28:59', '2022-04-10 02:28:59', 'kategori/fWS5OjwtmFFA2khQrtiXTGx6BKXLPU3WmG26eG1D.jpg'),
(11, 'Jeansdsds', 'jeansdsds', '2022-04-10 02:28:07', '2022-04-10 02:28:56', '2022-04-10 02:28:56', '5MfmSrGLJqPFGeF1WZPQtuOSEhxbFw0yOoJmXUDI.jpg'),
(12, 'bajuuuuu', 'bajuuuuu', '2022-04-10 02:30:06', '2022-04-10 02:30:06', NULL, 'mj3bhq1ycTj2a1ctc5qHNwIAC8Q63JiOVzqeUOdv.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `master_size`
--

DROP TABLE IF EXISTS `master_size`;
CREATE TABLE IF NOT EXISTS `master_size` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `size_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `master_size`
--

INSERT INTO `master_size` (`id`, `size_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'xl', '2022-03-28 02:35:46', '2022-03-27 02:35:46', NULL),
(2, 'XXS', '2022-04-05 01:33:36', '2022-04-05 02:51:51', NULL),
(3, 'XXS', '2022-04-05 01:33:36', '2022-04-05 01:33:40', '2022-04-05 01:33:40');

-- --------------------------------------------------------

--
-- Table structure for table `metode_pembayarans`
--

DROP TABLE IF EXISTS `metode_pembayarans`;
CREATE TABLE IF NOT EXISTS `metode_pembayarans` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `no_rek` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('on','off') COLLATE utf8mb4_unicode_ci DEFAULT 'on',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `metode_pembayarans`
--

INSERT INTO `metode_pembayarans` (`id`, `nama`, `kode`, `no_rek`, `status`, `created_at`, `updated_at`) VALUES
(1, 'assets_frontend/images/payment/mandiri.svg', '1213', '025281055', 'on', '2022-04-16 03:28:06', '2022-04-18 23:49:47'),
(3, 'assets_frontend/images/payment/BNI.svg', NULL, '025502535', 'on', '2022-04-16 03:42:11', '2022-04-16 04:27:38'),
(4, 'assets_frontend/images/payment/shopeepay.svg', NULL, '2323434', 'on', '2022-04-16 04:29:34', '2022-04-16 04:29:34'),
(5, 'assets_frontend/images/payment/bca.svg', '12', '123232', 'on', '2022-04-18 21:12:51', '2022-04-18 21:13:04'),
(6, 'assets_frontend/images/payment/gopay.svg', NULL, '123243435353', 'on', '2022-04-18 23:52:37', '2022-04-22 20:30:16');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_02_26_114733_create_roles_table', 1),
(6, '2022_02_28_120151_create_master_size_table', 1),
(7, '2022_03_01_115818_create_category_table', 1),
(8, '2022_03_06_115234_add_new_column_to_users_table', 1),
(9, '2022_03_09_135655_create_store_setting_table', 1),
(10, '2022_03_10_124940_create_adresses_table', 2),
(11, '2022_03_12_075001_add_type_province_city_and_postalcode_to_store_setting', 3),
(12, '2022_03_12_090356_add_province_id_to_store_setting', 4),
(13, '2022_03_13_022142_create_product_table', 5),
(14, '2022_03_13_023609_create_product_image_table', 5),
(15, '2022_03_13_023716_create_product_stock_table', 5),
(17, '2022_03_27_101921_create_banner_store_table', 6),
(18, '2022_03_27_013334_change_product_id_datatype_in_product_image', 7),
(19, '2022_04_01_011357_add_yt_embed_to_product', 7),
(20, '2022_04_02_014504_create_kategori_produk_table', 7),
(21, '2022_04_05_015418_create_contact_us_table', 8),
(22, '2022_04_07_061529_add_slug_to_master_category', 9),
(24, '2022_04_09_120519_add_new_column_to_store_setting_table', 10),
(25, '2022_04_10_084939_add_foto_to_master_category_table', 11),
(26, '2022_04_10_061819_add_background_testimoni_to_store_setting', 12),
(29, '2022_04_16_081308_create_metode_pembayarans_table', 13),
(32, '2022_04_17_033052_create_weight_ongkirs_table', 14),
(33, '2022_04_17_043310_detail_weight_ongkirs_table', 14),
(34, '2022_04_12_013808_create_reviews_table', 15),
(37, '2022_04_18_044745_create_banner_promos_table', 16),
(39, '2022_04_19_035202_create_voucher_kodes_table', 17),
(40, '2022_05_04_004030_add_kuota_to_voucher_kodes_table', 18),
(45, '2022_05_05_043830_create_faq_table', 19),
(61, '2022_05_14_005905_create_transaksis_table', 20),
(62, '2022_05_14_005929_create_detail_transaksis_table', 20),
(63, '2022_05_14_024026_create_transaksi_evouchers_table', 20),
(64, '2022_05_16_050452_add_column_bukti_transfer_to_transaksis_table', 21),
(67, '2022_05_30_120044_create_tracking_transaksis_table', 22),
(74, '2022_06_18_031518_create_preview_produks_table', 23),
(75, '2022_06_18_042408_create_preview_produk_images_table', 23);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `preview_produks`
--

DROP TABLE IF EXISTS `preview_produks`;
CREATE TABLE IF NOT EXISTS `preview_produks` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_transaksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_pemesanan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `star` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('aktif','nonaktif') COLLATE utf8mb4_unicode_ci DEFAULT 'aktif',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `preview_produks`
--

INSERT INTO `preview_produks` (`id`, `product_id`, `id_transaksi`, `id_pemesanan`, `user_id`, `nama`, `review`, `star`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(32, '28', '48', 'BDST16562434677342', NULL, 'ARIEF Gunawan', '1 gambar', '3', 'aktif', '2022-06-28 08:32:34', '2022-06-28 09:18:02', NULL),
(33, '30', '49', 'BDST16562434677342', '1', 'ARIEF Gunawan', '2 gambar', '4', 'aktif', '2022-06-28 08:32:34', '2022-06-28 09:18:06', NULL),
(34, '28', '47', 'BDST1656233886841', NULL, 'Adam levin', 'tes euyy mantrao', '4', 'aktif', '2022-06-28 08:35:37', '2022-06-28 09:17:58', NULL),
(35, '29', NULL, 'admin', NULL, 'MAMI', 'Mantap CUK', '5', 'aktif', '2022-06-28 09:08:31', '2022-06-28 09:13:23', NULL),
(36, '24', '52', 'BDST16562456984144', NULL, 'Usep gunawan', 'Bagus euy', '4', 'aktif', '2022-06-28 09:16:50', '2022-06-28 09:16:50', NULL),
(37, '30', '53', 'BDST16562456984144', NULL, 'Usep gunawan', 'Suka aing', '5', 'aktif', '2022-06-28 09:16:50', '2022-06-28 09:16:50', NULL),
(38, '28', NULL, 'admin', NULL, 'ndhfh', 'gdd', '4', 'aktif', '2022-06-28 09:20:38', '2022-06-28 09:20:38', NULL),
(39, '28', NULL, 'admin', NULL, 'ffdfd', 'vddbdb', '5', 'aktif', '2022-06-28 09:20:47', '2022-06-28 09:20:47', NULL),
(40, '28', NULL, 'admin', NULL, 'adsf', 'dgvd', '3', 'aktif', '2022-06-28 09:20:57', '2022-06-28 09:20:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `preview_produk_images`
--

DROP TABLE IF EXISTS `preview_produk_images`;
CREATE TABLE IF NOT EXISTS `preview_produk_images` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `preview_produk_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `preview_produk_images`
--

INSERT INTO `preview_produk_images` (`id`, `created_at`, `updated_at`, `preview_produk_id`, `image_name`, `deleted_at`) VALUES
(20, '2022-06-28 08:30:59', '2022-06-28 08:30:59', '30', 'image/DPtw88j2NncanDf6BQMF2qUPsUG9WaoWX3CGytY6.jpg', NULL),
(21, '2022-06-28 08:30:59', '2022-06-28 08:30:59', '31', 'image/zAnBJDhXXdjyJoN85AvmxsFar64hHO1v2JKT7rwO.jpg', NULL),
(22, '2022-06-28 08:30:59', '2022-06-28 08:30:59', '31', 'image/3xOANbIq4mr5XzFmZLcEQ7Yin8TYL1mvr2oMjx4j.jpg', NULL),
(23, '2022-06-28 08:32:34', '2022-06-28 08:32:34', '32', 'image/Wq7z9xXEVPREuwMnoFHxm3kCVG2oPXEmRzH1EDKy.jpg', NULL),
(24, '2022-06-28 08:32:34', '2022-06-28 08:32:34', '33', 'image/i1do7MieIjMiUlyxOXbCasYAwgFWBj7M4VsHjJtt.jpg', NULL),
(25, '2022-06-28 08:32:34', '2022-06-28 08:32:34', '33', 'image/Y5oVesX4gxUC9uhR0JhRaVDx8IIhghaX6t4yjDpO.jpg', NULL),
(26, '2022-06-28 08:35:37', '2022-06-28 08:35:37', '34', 'image/zbSCFHI6F76TJFrdXZhXWDuGqDwc7Gqk1OnUHpYa.jpg', NULL),
(27, '2022-06-28 09:16:50', '2022-06-28 09:16:50', '36', 'image/wrRkoo7anQ57rQF8wobCampNntRsHV1ReYOTD27y.jpg', NULL),
(28, '2022-06-28 09:16:50', '2022-06-28 09:16:50', '37', 'image/PCOA1Z61xlJVBOxZlY9X703GztQ5hZrxeuLhsWdf.jpg', NULL),
(29, '2022-06-28 09:20:47', '2022-06-28 09:20:47', '39', 'image/2KP0aNXn18oMj8BCZOirp0Di9UbjbHIRbTPGPaoV.jpg', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `harga_beli` int(11) DEFAULT NULL,
  `harga_jual` int(11) NOT NULL,
  `diskon` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `embed` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`id`, `status`, `nama_produk`, `deskripsi`, `harga_beli`, `harga_jual`, `diskon`, `created_at`, `updated_at`, `deleted_at`, `embed`, `slug`) VALUES
(1, 'Diposting', 'test', 'tes', 1200, 2200, 5, '2022-03-26 19:36:35', '2022-06-13 04:43:33', '2022-06-13 04:43:33', NULL, 'test'),
(2, 'Diposting', 'hello', 'deskripso', 200, 1200, 0, '2022-04-03 01:55:05', '2022-04-03 02:25:22', '2022-04-03 02:25:22', NULL, 'hello'),
(6, 'Diposting', 'hello', 'deskripso', 200, 1200, 0, '2022-04-03 01:57:41', '2022-04-03 02:25:18', '2022-04-03 02:25:18', NULL, 'hello'),
(7, 'Diposting', 'SGAN MAG', 'Test', 200, 1200, NULL, '2022-04-03 02:26:39', '2022-06-13 04:43:36', '2022-06-13 04:43:36', '[\"https:\\/\\/www.youtube.com\\/watch?v=0UZoNnQ04m8\"]', 'sgan-mag'),
(8, 'Diposting', 'Test welah', 'haahah', 1200, 2200, 10, '2022-04-03 03:48:15', '2022-04-03 03:50:58', '2022-04-03 03:50:58', NULL, 'test-welah'),
(9, 'Belum Diposting', 'Test welah', 'haahah', 1200, 2200, 10, '2022-04-03 03:49:11', '2022-04-03 03:50:53', '2022-04-03 03:50:53', NULL, 'test-welah'),
(10, 'Diposting', 'Test welah', 'Where does it come from?\r\n\r\nContrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.', 1200, 2200, 10, '2022-04-03 03:50:44', '2022-06-13 04:43:01', '2022-06-13 04:43:01', NULL, 'test-welah'),
(11, 'Diposting', 'Produk Unggulan 100%', 'Where can I get some?\r\n\r\nThere are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.', 9000, 10000, 15, '2022-04-05 00:38:54', '2022-06-13 04:42:57', '2022-06-13 04:42:57', NULL, 'produk-unggulan-100'),
(12, 'Diposting', 'Produk 10%', 'Foto Produk (Multiple Images/Bisa Banyak Foto)*', 1200, 10000, 0, '2022-04-05 20:32:08', '2022-06-13 04:42:54', '2022-06-13 04:42:54', NULL, 'produk-10'),
(13, 'Diposting', 'Produk 30%', 'Produk 30%', 200, 2200, 10, '2022-04-05 20:32:43', '2022-06-13 04:42:33', '2022-06-13 04:42:33', NULL, 'produk-30'),
(14, 'Diposting', 'hello', 'Produk 30%', 1200, 10000, 5, '2022-04-05 20:33:26', '2022-06-13 04:42:26', '2022-06-13 04:42:26', NULL, 'hello'),
(15, 'Diposting', 'Produk 20%', 'Produk 20%', 1200, 2200, 15, '2022-04-05 20:34:09', '2022-06-13 04:42:22', '2022-06-13 04:42:22', NULL, 'produk-20'),
(16, 'Diposting', 'Produk 301%', 'avavav', 1200, 300000, 10, '2022-04-05 20:34:44', '2022-06-13 04:42:19', '2022-06-13 04:42:19', NULL, 'produk-301'),
(17, 'Diposting', 'Produk 220%', 'Produk 220%', 1200, 100000, 10, '2022-04-05 20:35:24', '2022-06-13 04:42:16', '2022-06-13 04:42:16', NULL, 'produk-220'),
(18, 'Diposting', 'test tetzx S (100%)', 'sxnhfdhdf', 1200, 300000, 10, '2022-04-06 22:54:43', '2022-06-13 04:42:13', '2022-06-13 04:42:13', NULL, 'test-tetzx-s-100'),
(19, 'Diposting', 'Baju Baru Cotton 100%', '<p>dsfsgsb fgf fgfg</p>', 30000, 50000, 10, '2022-04-08 20:52:20', '2022-06-13 04:42:10', '2022-06-13 04:42:10', '[\"https:\\/\\/www.youtube.com\\/watch?v=0UZoNnQ04m8\"]', 'baju-baru-cotton-100'),
(20, 'Belum Diposting', 'Tshirt lengan panjang', '<p>gdgd</p>', 30000, 50000, NULL, '2022-04-09 23:37:51', '2022-06-13 04:42:06', '2022-06-13 04:42:06', NULL, 'tshirt-lengan-panjang'),
(21, 'Diposting', 'Produk Ke-1 Billdistro', '<p>Produk Ke-1 Billdistro keren banget</p>', 100000, 120000, NULL, '2022-06-13 04:21:53', '2022-06-13 04:21:53', NULL, NULL, 'produk-ke-1-billdistro'),
(22, 'Diposting', 'Produk Ke-2 Billdistro', '<p>Produk Ke-2 Billdistro Keren Banget</p>', 90000, 100000, 10, '2022-06-13 04:42:00', '2022-06-13 04:42:00', NULL, NULL, 'produk-ke-2-billdistro'),
(23, 'Diposting', 'Produk Ke-3 Billdistro', '<p>Produk Ke-3 Billdistro</p>', 100000, 120000, 15, '2022-06-13 04:46:51', '2022-06-14 05:27:54', NULL, '[\"https:\\/\\/www.youtube.com\\/watch?v=5qap5aO4i9A\"]', 'produk-ke-3-billdistro'),
(24, 'Diposting', 'Produk Ke-4 Billdistro', '<p>Produk Ke-4 Billdistro</p>', 90000, 100000, NULL, '2022-06-13 04:47:50', '2022-06-13 04:47:50', NULL, NULL, 'produk-ke-4-billdistro'),
(25, 'Diposting', 'Produk Ke-5 Billdistro', '<p>Produk Ke-5 Billdistro</p>', 100000, 120000, 10, '2022-06-13 04:48:16', '2022-06-13 04:48:16', NULL, NULL, 'produk-ke-5-billdistro'),
(26, 'Diposting', 'Produk Ke-6 Billdistro', '<p>Produk Ke-6 Billdistro</p>', 100000, 120000, 20, '2022-06-13 04:48:49', '2022-06-13 04:48:49', NULL, NULL, 'produk-ke-6-billdistro'),
(27, 'Diposting', 'Produk Ke-7 Billdistro', '<p>Produk Ke-7 Billdistro</p>', 30000, 50000, NULL, '2022-06-13 04:49:36', '2022-06-13 04:49:36', NULL, NULL, 'produk-ke-7-billdistro'),
(28, 'Diposting', 'Produk Ke-8 Billdistro', '<p>Produk Ke-8 Billdistro</p>', 90000, 100000, NULL, '2022-06-13 04:50:04', '2022-06-13 04:50:04', NULL, NULL, 'produk-ke-8-billdistro'),
(29, 'Diposting', 'Produk Ke-9 Billdistro', '<p>Produk Ke-9 Billdistro</p>', 100000, 120000, 0, '2022-06-13 04:50:54', '2022-06-19 21:48:25', NULL, NULL, 'produk-ke-9-billdistro'),
(30, 'Diposting', 'Test welah', '<p>tes</p>', 100000, 120000, NULL, '2022-06-26 04:28:22', '2022-06-26 04:28:22', NULL, NULL, 'test-welah'),
(31, 'Diposting', 'Produk Ke-2 Billdistro', '<p>h</p>', 1200, 120000, NULL, '2022-06-28 09:33:37', '2022-06-28 09:33:37', NULL, NULL, 'produk-ke-2-billdistro');

-- --------------------------------------------------------

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
CREATE TABLE IF NOT EXISTS `product_image` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `produk_id` bigint(20) UNSIGNED NOT NULL,
  `image_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `product_image_produk_id_foreign` (`produk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_image`
--

INSERT INTO `product_image` (`id`, `produk_id`, `image_name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(5, 6, 'product_images/qWI3zxpXammBCVgeuibdbBEJpF6DbkSdxiS9UaKb.jpg', '2022-04-03 01:57:41', '2022-04-03 01:57:41', NULL),
(6, 7, 'product_images/Fz4itHsiIxINXAjCLvYlLsRtzmoelaJbkKvYvtBi.jpg', '2022-04-03 02:26:39', '2022-04-03 02:26:39', NULL),
(7, 7, 'product_images/y16mRmZVuq6jDNQChjKyI6JXjH4rIw4Oyoj0TsXM.jpg', '2022-04-03 02:26:39', '2022-04-03 02:26:39', NULL),
(8, 10, 'product_images/aGfJThS60NnicbQkihhX6GyYrVooAsXphQOCb2fW.jpg', '2022-04-03 03:50:44', '2022-04-03 03:50:44', NULL),
(9, 11, 'product_images/AJPVtLxldVl5c5XPeqNx2LxkYHKWkMzalZYmyBXE.jpg', '2022-04-05 00:38:55', '2022-04-05 00:38:55', NULL),
(10, 11, 'product_images/siNeKR69Qf5MADNKYOYEtOcLhfoKYUMTCko269eZ.jpg', '2022-04-05 00:38:55', '2022-04-05 00:38:55', NULL),
(11, 11, 'product_images/EFwPYBGGoXfwhtF2OF32nQkQUQjEswekPolyCWWO.jpg', '2022-04-05 00:38:55', '2022-04-05 00:38:55', NULL),
(12, 11, 'product_images/IzViSKmZpkLWsmHPDoqGCgzbthrc8z4iOevk9Ih0.png', '2022-04-05 00:38:55', '2022-04-05 00:38:55', NULL),
(13, 12, 'product_images/SkWAtsbDvRyc9vbQHDc8EFWd4UFOC1LwEAu8eoOt.jpg', '2022-04-05 20:32:09', '2022-04-05 20:32:09', NULL),
(14, 13, 'product_images/ktY7o1b0yG557dVHPTV3SyRDAaysnUPxBiGyhoJS.jpg', '2022-04-05 20:32:43', '2022-04-05 20:32:43', NULL),
(15, 14, 'product_images/c8e7LSftDDziMTfGRIGMu6Rp0jsoBN0P9xz7dTSd.jpg', '2022-04-05 20:33:26', '2022-04-05 20:33:26', NULL),
(16, 15, 'product_images/f5Wz635qPJSzvvDUQDRc4PXPjXVHberLXqoJB3Oy.jpg', '2022-04-05 20:34:09', '2022-04-05 20:34:09', NULL),
(17, 16, 'product_images/YS7CP37ZQ7O3jxq76rvSK63XXuRKcTXI2IgXCeXA.jpg', '2022-04-05 20:34:44', '2022-04-05 20:34:44', NULL),
(18, 16, 'product_images/S2zrhKsNTvsmwvnk1tjYSHe4NNNi1aOzoH4NIbDZ.png', '2022-04-05 20:34:44', '2022-04-05 20:34:44', NULL),
(19, 17, 'product_images/WHpp9dhguLfy1wK5unh0yn0b5YOWpQ6AR1Titbhb.jpg', '2022-04-05 20:35:24', '2022-04-05 20:35:24', NULL),
(20, 18, 'product_images/qFJt9CdMNsBbVBZDiS0gwJ5JvZmjafcNiB7thUyz.jpg', '2022-04-06 22:54:43', '2022-04-06 22:54:43', NULL),
(21, 19, 'product_images/oj4z0lv2eAq7JI7zglIEJvcexDiuyvevRo07f1Ar.jpg', '2022-04-08 20:52:20', '2022-04-08 20:52:20', NULL),
(22, 19, 'product_images/DwNdmyMMgTSTZb5HkFQvoLJvXWCiLiFY6p89ewbK.jpg', '2022-04-08 20:52:20', '2022-04-08 20:52:20', NULL),
(23, 20, 'product_images/8pFr90GigD3e8NbchGuMWjvLV1sVYW3vwqmVd1dR.jpg', '2022-04-09 23:37:51', '2022-04-09 23:37:51', NULL),
(24, 21, 'product_images/qyts8y52xSVA4ZWXZXXwAGJXb5foVrRor5ZBjdTI.jpg', '2022-06-13 04:21:53', '2022-06-13 04:21:53', NULL),
(25, 21, 'product_images/x6WmSAUIUOnxcju7t1LEkTlAEgz6GgaEsKgB7U9O.jpg', '2022-06-13 04:21:53', '2022-06-13 04:21:53', NULL),
(26, 21, 'product_images/VWdf3K9snDwoR4YaF9ncUcUm4IkESDfpnSRU9TWb.jpg', '2022-06-13 04:21:53', '2022-06-13 04:21:53', NULL),
(28, 22, 'product_images/MC74zrsy7bSlfizGUeJcucITm8ii7lWy6efJZNme.jpg', '2022-06-13 04:42:00', '2022-06-13 04:42:00', NULL),
(29, 22, 'product_images/OfxOHQGRP87D78ZGndYEK4abGPgkYiyOC4VzrOLc.jpg', '2022-06-13 04:42:00', '2022-06-13 04:42:00', NULL),
(31, 24, 'product_images/xx072wV3VDs2E4juKAwls1kivxhyc5FwCBG0xZ0u.jpg', '2022-06-13 04:47:50', '2022-06-13 04:47:50', NULL),
(32, 25, 'product_images/7Wq2DnxGseCwfYOqdiMcIm1bXMgflBDNLlKRyf3t.jpg', '2022-06-13 04:48:16', '2022-06-13 04:48:16', NULL),
(33, 26, 'product_images/5WnmvQRFoGktVM7QoveLMsyRmeP3wbnS9GwkM38P.jpg', '2022-06-13 04:48:49', '2022-06-13 04:48:49', NULL),
(34, 28, 'product_images/QBOMBMhpTpdcFCSeFO1lQMLsFWU5bXIDXM3SjcTj.jpg', '2022-06-13 04:49:36', '2022-06-13 04:49:36', NULL),
(35, 28, 'product_images/ts8exdrSnarhPq7h2n3LFqilANtWHJOMPVPbJgAW.jpg', '2022-06-13 04:50:04', '2022-06-13 04:50:04', NULL),
(36, 29, 'product_images/PsjQeU13ElFCFEmaPTKQRPk2HtskXEJCNS9hw3oW.jpg', '2022-06-13 04:50:54', '2022-06-13 04:50:54', NULL),
(37, 23, 'product_images/sBQeC1VmsQ06j7CJU2YtqQL2wsZrD2fBh6zyo37J.jpg', '2022-06-14 05:27:28', '2022-06-14 05:27:28', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_stock`
--

DROP TABLE IF EXISTS `product_stock`;
CREATE TABLE IF NOT EXISTS `product_stock` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_produk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qty` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_stock`
--

INSERT INTO `product_stock` (`id`, `id_produk`, `size`, `qty`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '1', '1', '12', '2022-03-26 19:36:35', '2022-04-05 03:21:53', NULL),
(2, '6', '1', '10', '2022-04-03 01:57:41', '2022-04-03 01:57:41', NULL),
(3, '7', '1', '25', '2022-04-03 02:26:39', '2022-04-05 02:24:31', NULL),
(4, '10', '1', '10', '2022-04-03 03:50:44', '2022-04-04 23:37:59', NULL),
(5, '11', '1', '299', '2022-04-05 00:38:54', '2022-06-12 05:52:28', NULL),
(6, '11', '2', '9', '2022-04-05 01:52:13', '2022-06-12 05:52:28', NULL),
(7, '12', '1', '12', '2022-04-05 20:32:08', '2022-04-05 20:32:08', NULL),
(8, '13', '1', '12', '2022-04-05 20:32:43', '2022-04-05 20:32:43', NULL),
(9, '14', '1', '12', '2022-04-05 20:33:26', '2022-04-05 20:33:26', NULL),
(10, '15', '1', '9', '2022-04-05 20:34:09', '2022-06-12 06:25:38', NULL),
(11, '16', '1', '16', '2022-04-05 20:34:44', '2022-06-12 06:26:57', NULL),
(12, '17', '1', '12', '2022-04-05 20:35:24', '2022-04-07 07:46:02', NULL),
(13, '18', '1', '0', '2022-04-06 22:54:43', '2022-04-09 23:35:08', NULL),
(14, '19', '1', '10', '2022-04-08 20:52:20', '2022-04-09 23:36:39', NULL),
(15, '19', '2', '20', '2022-04-08 20:52:20', '2022-04-09 23:36:39', NULL),
(16, '20', '2', '10', '2022-04-09 23:37:51', '2022-04-09 23:37:51', NULL),
(17, '21', '1', '10', '2022-06-13 04:21:53', '2022-06-13 04:21:53', NULL),
(18, '21', '2', '10', '2022-06-13 04:21:53', '2022-06-13 04:21:53', NULL),
(19, '22', '1', '7', '2022-06-13 04:42:00', '2022-06-25 05:54:39', NULL),
(20, '23', '1', '12', '2022-06-13 04:46:51', '2022-06-14 05:27:54', NULL),
(21, '23', '2', '11', '2022-06-13 04:46:51', '2022-06-27 04:56:06', NULL),
(22, '24', '1', '6', '2022-06-13 04:47:50', '2022-06-28 04:25:16', NULL),
(23, '25', '1', '10', '2022-06-13 04:48:16', '2022-06-25 05:54:39', NULL),
(24, '26', '2', '9', '2022-06-13 04:48:49', '2022-06-14 04:58:03', NULL),
(25, '27', '2', '12', '2022-06-13 04:49:36', '2022-06-13 04:49:36', NULL),
(26, '28', '1', '2', '2022-06-13 04:50:04', '2022-06-26 04:49:23', NULL),
(27, '29', '1', '0', '2022-06-13 04:50:54', '2022-06-19 21:48:25', NULL),
(28, '30', '1', '9', '2022-06-26 04:28:22', '2022-06-26 05:16:09', NULL),
(29, '31', '1', '25', '2022-06-28 09:33:37', '2022-06-28 09:33:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `produk_kategori`
--

DROP TABLE IF EXISTS `produk_kategori`;
CREATE TABLE IF NOT EXISTS `produk_kategori` (
  `master_category_id` bigint(20) UNSIGNED NOT NULL,
  `produk_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`master_category_id`,`produk_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produk_kategori`
--

INSERT INTO `produk_kategori` (`master_category_id`, `produk_id`) VALUES
(1, 1),
(1, 6),
(1, 7),
(1, 11),
(1, 13),
(1, 14),
(1, 16),
(1, 19),
(1, 21),
(1, 22),
(1, 23),
(1, 25),
(1, 28),
(2, 7),
(2, 10),
(2, 11),
(2, 12),
(2, 13),
(2, 14),
(2, 15),
(2, 16),
(2, 18),
(2, 20),
(2, 28),
(2, 29),
(3, 17),
(3, 19),
(3, 21),
(3, 22),
(3, 23),
(3, 24),
(3, 25),
(3, 26),
(3, 27),
(3, 30),
(12, 22),
(12, 27),
(12, 31);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `star` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `star`, `review`, `image`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Aisyah Qurani', '5', '<p>Nice</p>', 'image/XddeWBGiXy5HB6oj4CD2ga4s2zS53Udonk5ZOu7s.jpg', '1', '2022-04-16 23:23:11', '2022-06-26 05:35:57'),
(2, 'Giri Pambayun', '5', '<p>Mantap</p>', 'image/0heUCCJ2AiGuy4vtvG5xi19cyrj8AJp4wzS0hOcM.jpg', '1', '2022-04-16 23:26:33', '2022-06-26 05:35:07'),
(3, 'Dede Jaenudi', '5', '<p>Bagus banget</p>', 'image/WHn3h9SR0615RuZMvfAH8YsRQI1UEK9dlPSN658J.jpg', '1', '2022-04-18 21:44:05', '2022-06-26 05:35:41'),
(4, NULL, NULL, '<p><br data-mce-bogus=\"1\"></p>', 'image/oWWMN8Ju41RGIB9DecNJ6zSxF9vOdc98I9pOr4KE.jpg', NULL, '2022-06-24 22:31:37', '2022-06-24 22:31:37');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2022-03-10 05:01:52', '2022-03-10 05:01:52'),
(2, 'customer', '2022-03-10 05:01:52', '2022-03-10 05:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `store_setting`
--

DROP TABLE IF EXISTS `store_setting`;
CREATE TABLE IF NOT EXISTS `store_setting` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_toko` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telepon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fax` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_provinsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_pos` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `shopee` longtext COLLATE utf8mb4_unicode_ci,
  `fb` longtext COLLATE utf8mb4_unicode_ci,
  `instagram` longtext COLLATE utf8mb4_unicode_ci,
  `tokopedia` longtext COLLATE utf8mb4_unicode_ci,
  `background_testimoni` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `store_setting`
--

INSERT INTO `store_setting` (`id`, `type`, `nama_toko`, `alamat`, `email`, `telepon`, `fax`, `id_provinsi`, `provinsi`, `id_kota`, `kota`, `kode_pos`, `deskripsi`, `shopee`, `fb`, `instagram`, `tokopedia`, `background_testimoni`) VALUES
(1, 'alamat', 'Bill Distro', '<p>asasas</p>', 'contoh@email.com', '081809300104', '12234', '9', 'Jawa Barat', '22', 'Bandung', '40311', '', 'https://shopee.co.id', 'https://fb.com', 'https://instagram.com', 'https://tokopedia.com', 'background_testimoni/EPmreezvhYUL2eSgN3YCm88s2pOxfITn4uT14USZ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tracking_transaksis`
--

DROP TABLE IF EXISTS `tracking_transaksis`;
CREATE TABLE IF NOT EXISTS `tracking_transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_tracking` timestamp NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `tracking_transaksis_transaksi_id_foreign` (`transaksi_id`)
) ENGINE=InnoDB AUTO_INCREMENT=267 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tracking_transaksis`
--

INSERT INTO `tracking_transaksis` (`id`, `transaksi_id`, `title`, `deskripsi`, `tanggal_tracking`, `created_at`, `updated_at`, `deleted_at`) VALUES
(245, 41, 'Pesanan baru', 'Pesanan baru dibuat.', '2022-06-26 01:58:06', '2022-06-26 02:06:34', '2022-06-26 02:06:34', NULL),
(246, 41, 'Pesanan Terkonfirmasi', 'Pesanan telah terkonfirmasi.', '2022-06-26 02:06:34', '2022-06-26 02:06:34', '2022-06-26 02:06:34', NULL),
(247, 41, 'sfdsfsfscscvsds sd sfsf', 'dsf', '2022-06-26 02:06:41', '2022-06-26 02:06:41', '2022-06-26 02:06:41', NULL),
(248, 42, 'Pesanan baru', 'Pesanan baru dibuat.', '2022-06-26 04:37:47', '2022-06-26 04:38:32', '2022-06-26 04:38:32', NULL),
(249, 42, 'Pesanan Terkonfirmasi', 'Pesanan telah terkonfirmasi.', '2022-06-26 04:38:32', '2022-06-26 04:38:32', '2022-06-26 04:38:32', NULL),
(250, 42, 'FSFS', 'FSFS', '2022-06-26 04:38:58', '2022-06-26 04:38:58', '2022-06-26 04:38:58', NULL),
(251, 43, 'Pesanan baru', 'Pesanan baru dibuat.', '2022-06-26 04:48:29', '2022-06-26 04:49:23', '2022-06-26 04:49:23', NULL),
(252, 43, 'Pesanan Terkonfirmasi', 'Pesanan telah terkonfirmasi.', '2022-06-26 04:49:23', '2022-06-26 04:49:23', '2022-06-26 04:49:23', NULL),
(253, 43, 'Mantap ccuk safasfdsd aefe jnl;dada sdsc', 'dsds', '2022-06-26 04:49:33', '2022-06-26 04:49:33', '2022-06-26 04:49:33', NULL),
(254, 44, 'Pesanan baru', 'Pesanan baru dibuat.', '2022-06-26 05:14:58', '2022-06-26 05:15:49', NULL, NULL),
(255, 44, 'Bukti pembayaran', 'Upload bukti pembayaran.', '2022-06-26 05:15:49', '2022-06-26 05:15:49', NULL, NULL),
(256, 44, 'Pesanan Terkonfirmasi', 'Pesanan telah terkonfirmasi.', '2022-06-26 05:16:09', '2022-06-26 05:16:09', '2022-06-26 05:16:09', NULL),
(257, 44, 'trackinh', 'trs', '2022-06-26 05:16:29', '2022-06-26 05:16:29', '2022-06-26 05:16:29', NULL),
(258, 44, 'beres', 'beres', '2022-06-26 05:16:38', '2022-06-26 05:16:38', '2022-06-26 05:16:38', NULL),
(259, 45, 'Pesanan baru', 'Pesanan baru dibuat.', '2022-06-27 04:55:28', '2022-06-27 04:55:51', NULL, NULL),
(260, 45, 'Bukti pembayaran', 'Upload bukti pembayaran.', '2022-06-27 04:55:51', '2022-06-27 04:55:51', NULL, NULL),
(261, 45, 'Pesanan Terkonfirmasi', 'Pesanan telah terkonfirmasi.', '2022-06-27 04:56:06', '2022-06-27 04:56:06', '2022-06-27 04:56:06', NULL),
(262, 45, 'tes', 'sfsd', '2022-06-27 04:56:23', '2022-06-27 04:56:23', '2022-06-27 04:56:23', NULL),
(263, 46, 'Pesanan baru', 'Pesanan baru dibuat.', '2022-06-28 04:23:42', '2022-06-28 04:25:06', NULL, NULL),
(264, 46, 'Bukti pembayaran', 'Upload bukti pembayaran.', '2022-06-28 04:25:06', '2022-06-28 04:25:06', NULL, NULL),
(265, 46, 'Pesanan Terkonfirmasi', 'Pesanan telah terkonfirmasi.', '2022-06-28 04:25:16', '2022-06-28 04:25:16', '2022-06-28 04:25:16', NULL),
(266, 46, 'BEDEGUNG', 'w', '2022-06-28 04:25:37', '2022-06-28 04:25:37', '2022-06-28 04:25:37', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksis`
--

DROP TABLE IF EXISTS `transaksis`;
CREATE TABLE IF NOT EXISTS `transaksis` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `id_pemesanan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_depan` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_belakang` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `provinsi` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_provinsi` int(11) NOT NULL,
  `kota` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kota` int(11) NOT NULL,
  `no_pos` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat_lengkap` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `catatan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kurir` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ongkir` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_bayar` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('baru','terkonfirmasi','tracking','selesai','rejected') COLLATE utf8mb4_unicode_ci DEFAULT 'baru',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `bukti_transfer` longtext COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaksis_id_pemesanan_unique` (`id_pemesanan`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transaksis`
--

INSERT INTO `transaksis` (`id`, `id_pemesanan`, `user_id`, `nama_depan`, `nama_belakang`, `no_telp`, `email`, `provinsi`, `id_provinsi`, `kota`, `id_kota`, `no_pos`, `alamat_lengkap`, `catatan`, `kurir`, `ongkir`, `total_bayar`, `status`, `created_at`, `updated_at`, `deleted_at`, `bukti_transfer`) VALUES
(41, 'BDST1656233886841', '3', 'Adam', 'levin', '0896955567', 'usepgnwan@gmail.com', 'Jawa Barat', 9, 'Bandung', 22, '40311', 'hchchc', 'bxb', 'jne REG (Layanan Reguler)', 'Rp. 8.000', 'Rp. 108.000', 'selesai', '2022-06-26 01:58:06', '2022-06-26 02:06:41', NULL, NULL),
(42, 'BDST16562434677342', '3', 'ARIEF', 'Gunawan', '0896955567', 'asa.dietrich@gmail.com', 'Bengkulu', 4, 'Bengkulu', 62, '38229', 'gdgdgdg', 'dgdg', 'jne OKE (Ongkos Kirim Ekonomis)', 'Rp. 38.000', 'Rp. 258.000', 'selesai', '2022-06-26 04:37:47', '2022-06-26 04:38:58', NULL, NULL),
(43, 'BDST16562441092543', '3', 'test', 'cxcx', 'cxcx', 'cx@email.com', 'Kalimantan Barat', 12, 'Bengkayang', 61, '79213', 'vxvxvx', 'cxcx', 'jne OKE (Ongkos Kirim Ekonomis)', 'Rp. 45.000', 'Rp. 365.000', 'selesai', '2022-06-26 04:48:29', '2022-06-26 04:49:33', NULL, NULL),
(44, 'BDST16562456984144', '3', 'Usep', 'gunawan', '0896955567', 'fggf@gmail.com', 'Jawa Tengah', 10, 'Banjarnegara', 37, '53419', 'fdsfs', 'sfs', 'jne OKE (Ongkos Kirim Ekonomis)', 'Rp. 16.000', 'Rp. 436.000', 'selesai', '2022-06-26 05:14:58', '2022-06-26 05:16:38', NULL, 'GmEAYp95jRlsbxD10SS35Q8ohyi1bAEq4psq5QPR.jpg'),
(45, 'BDST16563309289445', '3', 'aidil', 'daniel', '0896955567', 'asa.dietrich@gmail.com', 'DI Yogyakarta', 5, 'Bantul', 39, '55715', 'kh', 'ada', 'jne OKE (Ongkos Kirim Ekonomis)', 'Rp. 13.000', 'Rp. 115.000', 'selesai', '2022-06-27 04:55:28', '2022-06-27 04:56:23', NULL, 'YOeNtOu3NC9Vl9Sx4ur2ujwEZLdH10IarKZhuZc3.jpg'),
(46, 'BDST16564154222146', NULL, 'Adam', 'alis', '0896955567', 'dsd@gmail.com', 'Bangka Belitung', 2, 'Bangka', 27, '33212', 'jgjgbj', NULL, 'jne OKE (Ongkos Kirim Ekonomis)', 'Rp. 38.000', 'Rp. 138.000', 'selesai', '2022-06-28 04:23:42', '2022-06-28 04:25:37', NULL, '5aNWTYzPmXy3NlG95jdykZ7vT0ac2abjjqNMm9ZQ.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi_evouchers`
--

DROP TABLE IF EXISTS `transaksi_evouchers`;
CREATE TABLE IF NOT EXISTS `transaksi_evouchers` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `transaksi_id` bigint(20) UNSIGNED NOT NULL,
  `voucher_kode_id` bigint(20) UNSIGNED NOT NULL,
  `evocher` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaksi_evouchers_transaksi_id_foreign` (`transaksi_id`),
  KEY `transaksi_evouchers_voucher_kode_id_foreign` (`voucher_kode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `privacy_term` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `jenis_kelamin` enum('laki-laki','perempuan') COLLATE utf8mb4_unicode_ci DEFAULT 'laki-laki',
  `tanggal_lahir` date NOT NULL DEFAULT '2022-03-10',
  `default_address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'user-default.png',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `name`, `privacy_term`, `no_telp`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `jenis_kelamin`, `tanggal_lahir`, `default_address`, `foto`) VALUES
(1, 2, 'Flo Kreiger', 'on', '(650) 959-3464', 'tristin.doyle@gmail.com', '2022-03-10 05:01:52', '$2y$10$bkh6ZJMUj4/Z2X6NgwkGHO8NW/XRK/ck5UpAz3w.Zt7wUkv1lmF.S', 'v4CLHCQsgCl2n2UqohXzo3zxoo0SvNtKlSe17Mq94ih5MmbPTFfAqxdz2COg', '2022-03-10 05:01:52', '2022-03-27 04:00:59', 'laki-laki', '2022-03-10', '36', 'xTcyCIlXiAXwrntuirZzmCni3gmAEk0yqmZjeepa.jpg'),
(2, 2, 'Mr. Arvid Keeling DVM', 'on', '1-820-264-2058', 'mraz.delphine@gmail.com', '2022-03-10 05:01:52', '$2y$10$NuyCURhP/2qtxQJIZPMCyulmYSBru9UaFqUNLiVXxIOxplbmLVTga', '9PVWBsr4JCbrfw74uSEtN7cd1DoL2sbgs6xoRZm4ovIeHLiJJKksgPQ0FErv', '2022-03-10 05:01:52', '2022-03-15 06:25:46', 'perempuan', '2022-03-10', '38', 'user-default.png'),
(3, 1, 'Billdistro', 'on', '1-678-361-2245', 'billdistro99@gmail.com', '2022-03-10 05:01:52', '$2y$10$hglIMk33y27NJGEw8jsb6uhPIYVZE29NRx2nyk4ADewadRiim5uEe', 'BpnA0j57WCbdtPk5IVcitT4ZRJWZovcfMaUxeveDDAUf0ZyX4jI0mjInMU3R', '2022-03-10 05:01:52', '2022-03-10 05:01:52', 'laki-laki', '2022-03-10', NULL, 'user-default.png'),
(4, 2, 'Spencer Hackett II', 'on', '854-956-8369', 'sage10@example.net', '2022-03-10 05:01:52', '$2y$10$1hWWg..zGN7yn9pZw19q9.GxrI.isFZJ.rTwXaRDl/DpLU/81ek1a', 'mUszHBors3', '2022-03-10 05:01:52', '2022-03-10 05:01:52', 'laki-laki', '2022-03-10', NULL, 'user-default.png'),
(5, 2, 'ahmad syahroni', 'on', '0896955567', 'usepgnwansss@gmail.com', NULL, '$2y$10$iNDI3MzNFjjU2ubHTL9WDeCkmAPfVEMw7CNTRUWiV5gW.2FFHhpFe', NULL, '2022-04-04 21:03:59', '2022-04-04 21:03:59', 'laki-laki', '2022-04-05', NULL, 'user-default.png'),
(6, 1, 'admin', '', '', 'admin@admin.com', NULL, '$2y$10$zVnraa.JT27UN7F7FqgGpuORH1zuBpsXZh4DwGmQdwrBkYegFaFCS', NULL, NULL, NULL, 'laki-laki', '2022-03-10', NULL, 'user-default.png');

-- --------------------------------------------------------

--
-- Table structure for table `voucher_kodes`
--

DROP TABLE IF EXISTS `voucher_kodes`;
CREATE TABLE IF NOT EXISTS `voucher_kodes` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `nama_promo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `promo` double(8,2) NOT NULL,
  `type_user` enum('login','all') COLLATE utf8mb4_unicode_ci DEFAULT 'all',
  `type_voucher` enum('diskon','potongan') COLLATE utf8mb4_unicode_ci DEFAULT 'diskon',
  `status` enum('on','off') COLLATE utf8mb4_unicode_ci DEFAULT 'on',
  `periode_awal` date NOT NULL,
  `periode_akhir` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `kuota` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `voucher_kodes_kode_unique` (`kode`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `voucher_kodes`
--

INSERT INTO `voucher_kodes` (`id`, `nama_promo`, `kode`, `promo`, `type_user`, `type_voucher`, `status`, `periode_awal`, `periode_akhir`, `created_at`, `updated_at`, `deleted_at`, `kuota`) VALUES
(1, 'csfs', '12dsdsew', 10.00, 'login', 'diskon', 'on', '2022-04-19', '2022-04-20', '2022-04-18 23:46:35', '2022-04-18 23:47:30', '2022-04-18 23:47:30', NULL),
(2, 'test', 'Vdas11', 18000.00, 'all', 'potongan', 'on', '2022-04-19', '2022-05-30', '2022-04-19 00:19:45', '2022-05-19 17:22:13', NULL, 1),
(3, 'sdsds', 'sss', 5.00, 'all', 'diskon', 'on', '2022-05-20', '2022-07-05', '2022-05-19 18:25:20', '2022-07-02 20:56:01', NULL, 3),
(4, 'Hari Raya', 'tes', 20.00, 'all', 'diskon', 'on', '2022-06-20', '2022-06-21', '2022-06-19 21:52:08', '2022-06-19 21:52:08', NULL, 10);

-- --------------------------------------------------------

--
-- Table structure for table `weight_ongkirs`
--

DROP TABLE IF EXISTS `weight_ongkirs`;
CREATE TABLE IF NOT EXISTS `weight_ongkirs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `pcs_dari` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pcs_sampai` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight_kg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `weight_gram` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `weight_ongkirs`
--

INSERT INTO `weight_ongkirs` (`id`, `pcs_dari`, `pcs_sampai`, `weight_kg`, `weight_gram`, `created_at`, `updated_at`) VALUES
(11, '1', '11', '1', '1000', '2022-04-17 01:22:56', '2022-04-19 00:18:25'),
(13, '11', '15', '1.5', '1500', '2022-04-17 01:49:45', '2022-04-19 00:18:48'),
(14, '12', '16', '2', '2000', '2022-04-17 01:51:59', '2022-04-18 21:19:44');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `adresses`
--
ALTER TABLE `adresses`
  ADD CONSTRAINT `adresses_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_transaksis`
--
ALTER TABLE `detail_transaksis`
  ADD CONSTRAINT `detail_transaksis_master_size_id_foreign` FOREIGN KEY (`master_size_id`) REFERENCES `master_size` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_transaksis_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `product` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_transaksis_stock_id_foreign` FOREIGN KEY (`stock_id`) REFERENCES `product_stock` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detail_transaksis_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `detail_weight_ongkirs`
--
ALTER TABLE `detail_weight_ongkirs`
  ADD CONSTRAINT `detail_weight_ongkirs_weight_ongkir_id_foreign` FOREIGN KEY (`weight_ongkir_id`) REFERENCES `weight_ongkirs` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_image`
--
ALTER TABLE `product_image`
  ADD CONSTRAINT `product_image_produk_id_foreign` FOREIGN KEY (`produk_id`) REFERENCES `product` (`id`);

--
-- Constraints for table `tracking_transaksis`
--
ALTER TABLE `tracking_transaksis`
  ADD CONSTRAINT `tracking_transaksis_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksi_evouchers`
--
ALTER TABLE `transaksi_evouchers`
  ADD CONSTRAINT `transaksi_evouchers_transaksi_id_foreign` FOREIGN KEY (`transaksi_id`) REFERENCES `transaksis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksi_evouchers_voucher_kode_id_foreign` FOREIGN KEY (`voucher_kode_id`) REFERENCES `voucher_kodes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
