<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeightOngkirsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weight_ongkirs', function (Blueprint $table) {
            $table->id();
            $table->string('pcs_dari');
            $table->string('pcs_sampai');
            $table->string('weight_kg');
            $table->string('weight_gram');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weight_ongkirs');
    }
}
