<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StoreSetting extends Model
{
    use HasFactory;
    protected $table = 'store_setting';
    protected $fillable = ['nama_toko', 'alamat', 'email', 'telepon', 'fax'];
    public $timestamps = false;
}