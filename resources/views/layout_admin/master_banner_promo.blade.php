@extends('template_admin.admin')

@section('title', 'Pengaturan Toko')
@section('container')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">E-voucher / Banner Promo</h1>
    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="from-group mb-2">
                <button class="btn btn-primary tambah-voucher" value="new">Tambah Evoucher / Promo</button>
            </div>
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
                </div>
                <div class="row ml-2 mr-2 mb-2">
                    <div class="col-lg-6">
                        <form action="" class="banner_1_promo_form">
                            <div class="form-group">
                                <label for="exampleFormControlReadonly">Banner 1</label>
                                <input type="hidden" name="status" id="status" value="1">
                                <input class="form-control" type="file" id="foto_banner_1" name="foto_banner_promo"
                                    placeholder="Readonly input here..."
                                    value="{{ asset('assets_frontend/images/banner_promo/' . $promo_banner->foto_banner_1) }}">
                                <small class="text-danger">rekomendasi image
                                    958x401,jpeg,png,jpg,gif,svg</small>
                            </div>
                        </form>
                        <div class="banner-spin-1" hidden>
                            <span><i class="fa fa-2x fa-spinner fa-spin"></i></span>
                        </div>
                        <div class="img-card-banner-1">
                            <img src="{{ asset('assets_frontend/images/banner_promo/' . $promo_banner->foto_banner_1) }}"
                                class="img-fluid" alt="Responsive image" id="foto_banner_1_img">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <form action="" class="banner_2_promo_form">
                            <div class="form-group">
                                <label for="exampleFormControlReadonly">Banner 2</label>
                                <input type="hidden" name="status" id="status" value="2">
                                <input class="form-control" type="file" id="foto_banner_2" name="foto_banner_promo"
                                    placeholder="Readonly input here..."
                                    value="{{ asset('assets_frontend/images/banner_promo/' . $promo_banner->foto_banner_2) }}">
                                <small class="text-danger">rekomendasi image
                                    958x401,jpeg,png,jpg,gif,svg</small>
                            </div>
                        </form>
                        <div class="banner-spin-2" hidden>
                            <span><i class="fa fa-2x fa-spinner fa-spin"></i></span>
                        </div>
                        <div class="img-card-banner-2">
                            <img src="{{ asset('assets_frontend/images/banner_promo/' . $promo_banner->foto_banner_2) }}"
                                class="img-fluid" alt="Responsive image" id="foto_banner_2_img">
                        </div>

                    </div>
                </div>

                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable" style="height: 100">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Nama Promo</th>
                                <th>Kode</th>
                                <th>Kuota</th>
                                <th>Promo</th>
                                <th>Type User</th>
                                <th>Type Voucher</th>
                                <th>Periode Awal</th>
                                <th>Periode Akhir</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Alamat -->
    <div id="modal_voucher" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" style="overflow: auto !important;">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content change-address">
                <div class="modal-header">
                    <h5 class="modal-title-voucher" id="exampleModalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="" id="form-voucher">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Nama Promo</label>
                            <input type="hidden" class="form-control" id="id_voucher" name="id_voucher" value="">
                            <textarea name="nama_promo" id="nama_promo" class="form-control" id="" cols="20" rows="3"></textarea>

                        </div>
                        <div class="form-group">
                            <label for="">Type User</label>
                            <select name="type_user" id="type_user" class="form-control">
                                <option value="0">Pilih</option>
                                <option value="login">User Login</option>
                                <option value="all">All User</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Type Voucher</label>
                            <select name="type_voucher" id="type_voucher" class="form-control type_voucher">
                                <option value="0">Pilih</option>
                                <option value="diskon">Diskon</option>
                                <option value="potongan">Potongan</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">Kode</label>
                            <input type="text" class="form-control" id="kode" name="kode">
                        </div>
                        <div class="form-group">
                            <label for="">Kuota</label>
                            <input type="number" min="0" value="0" class="form-control" id="kuota" name="kuota">
                        </div>
                        <div class="form-group card-evoucher" hidden>
                            <label for="" class="label-evoucher"></label>
                            <input type="number" class="form-control" id="promo" name="promo">

                        </div>
                        <div class="form-group">
                            <label for="">Periode Awal</label>
                            <input type="date" class="form-control" id="periode_awal" name="periode_awal">

                        </div>
                        <div class="form-group">
                            <label for="">Periode Akhir</label>
                            <input type="date" class="form-control" id="periode_akhir" name="periode_akhir">

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" type="submit" class="btn btn-primary add-voucher" >Simpan</button>
                        <button type="button" class="btn btn-secondary close-address" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        var table;
        $(document).ready(function() {
            $("#foto_banner_1").on('change', function() {
                let $input = $(this);
                let form = $('.banner_1_promo_form')[0];
                let data = new FormData(form);
                data.append("_method", "PUT");
                let id = '{{ $promo_banner->id }}';
                let url = '{{ route('banner_promo.update', ['banner_promo' => ':id']) }}';
                url = url.replace(":id", id);
                $('.banner-spin-1').prop('hidden', false);
                $('.img-card-banner-1').prop('hidden', true);
                $.ajax({
                    url: url,
                    data: data,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    start_time: new Date().getTime(),
                    success: function(data) {

                        $('.banner-spin-1').prop('hidden', true);
                        $('.img-card-banner-1').prop('hidden', false);
                        if (data.status) {
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            if ($input.val().length > 0) {
                                fileReader = new FileReader();
                                fileReader.onload = function(data) {
                                    let tes = $('#foto_banner_1_img').attr(
                                        'src', data.target.result);
                                }
                                fileReader.readAsDataURL($input.prop('files')[0]);
                            }
                        } else {
                            $.each(data.data, function(field_name, error) {
                                Swal.fire(
                                    'Error',
                                    error + '',
                                    'warning'
                                )
                            });
                        }
                    }
                });
            });
            $("#foto_banner_2").on('change', function() {
                let $input = $(this);
                let form = $('.banner_2_promo_form')[0];
                let data = new FormData(form);
                data.append("_method", "PUT");
                let id = '{{ $promo_banner->id }}';
                let url = '{{ route('banner_promo.update', ['banner_promo' => ':id']) }}';
                url = url.replace(":id", id);
                $('.banner-spin-2').prop('hidden', false);
                $('.img-card-banner-2').prop('hidden', true);
                $.ajax({
                    url: url,
                    data: data,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    start_time: new Date().getTime(),
                    success: function(data) {
                        $('.banner-spin-2').prop('hidden', true);
                        $('.img-card-banner-2').prop('hidden', false);
                        if (data.status) {
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            if ($input.val().length > 0) {
                                fileReader = new FileReader();
                                fileReader.onload = function(data) {
                                    let tes = $('#foto_banner_2_img').attr(
                                        'src', data.target.result);
                                }
                                fileReader.readAsDataURL($input.prop('files')[0]);
                            }
                        } else {
                            $.each(data.data, function(field_name, error) {
                                Swal.fire(
                                    'Error',
                                    error + '',
                                    'warning'
                                )
                            });
                        }
                    }
                });
            });

            table = $('#dataTable').DataTable({
                prossecing: true,
                serverSide: true,
                ajax: "{{ route('promo.index') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama_promo',
                        name: 'nama_promo'
                    },
                    {
                        data: 'kode',
                        name: 'kode'
                    },
                    {
                        data: 'kuota',
                        name: 'kuota'
                    },
                    {
                        data: 'promo',
                        name: 'promo'
                    },
                    {
                        data: 'type_user',
                        name: 'type_user'
                    },
                    {
                        data: 'type_voucher',
                        name: 'type_voucher'
                    },
                    {
                        data: 'periode_awal',
                        name: 'periode_awal'
                    },
                    {
                        data: 'periode_akhir',
                        name: 'periode_akhir'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });

            $(document).on('click', '.tambah-voucher', function(e) {
                e.preventDefault();
                $('#modal_voucher').modal('show');
                $('.modal-title-voucher').html('Menambah E-Voucher/Promo');
                $('.add-voucher').val(0);
                $('.card-evoucher').prop('hidden', true);
                $('#form-voucher')[0].reset();

            });
            $(document).on('click', '.edit-promo', function(e) {
                e.preventDefault();
                $('.card-evoucher').prop('hidden', false);
                $('.modal-title-voucher').html('Edit E-Voucher/Promo');
                $('.add-voucher').val(1);

                let id = $(this).data('id');
                let url = "{{ route('promo.edit', ['promo' => ':id']) }}";
                url = url.replace(":id", id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function(data) {
                        if (data.status == true) {
                            // toastr.success(data.msg);
                            $('#modal_voucher').modal('show');
                            $('#id_voucher').val(data.data.id);
                            $('#nama_promo').val(data.data.nama_promo);
                            $('#type_user').val(data.data.type_user);
                            $('#type_voucher').val(data.data.type_voucher);
                            if (data.data.type_voucher == 'potongan') {
                                $('.label-evoucher').html('Potongan');
                            } else {
                                $('.label-evoucher').html('Diskon');
                            }
                            $('#kode').val(data.data.kode);
                            $('#kuota').val(data.data.kuota);
                            $('#promo').val(data.data.promo);
                            $('#periode_awal').val(data.data.periode_awal);
                            $('#periode_akhir').val(data.data.periode_akhir);
                        } else {
                            toastr.warning(data.msg);
                        }
                    }
                });

            });


            $(document).on('click', '.on-status', function(e) {
                e.preventDefault();
                let $this = $(this);
                let id = $this.data('id');
                let status = $this.data('status');
                let url = '{{ route('promo.update_status', ['id' => ':id']) }}';
                url = url.replace(":id", id);
                $.ajax({
                    url: url,
                    data: {
                        'status': status,
                        '_method': "PUT"
                    },
                    method: 'POST',
                    success: function(data) {
                        if (data.status) {
                            Swal.fire(
                                'Berhasil!',
                                data.msg,
                                'success'
                            );
                            table.ajax.reload();
                        } else {
                            toastr.error(data.msg);
                        }
                    }
                });
            });

            $('.type_voucher').change(function() {

                if ($(this).val() == 'potongan') {
                    $('.card-evoucher').prop('hidden', false);
                    $('.label-evoucher').html('Potongan');
                    $('#promo').val('0');
                } else if ($(this).val() == 'diskon') {
                    $('.card-evoucher').prop('hidden', false);
                    $('.label-evoucher').html('Diskon');
                    $('#promo').val('0');
                } else {
                    $('.card-evoucher').prop('hidden', true);
                    $('.label-evoucher').html('');
                }
            });

            $(document).on('click', '.hapus-promo', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).parent().closest('.address').remove();
                        let url =
                            "{{ route('promo.destroy', ['promo' => ':id']) }}";
                        url = url.replace(":id", id);
                        $.ajax({
                            method: 'DELETE',
                            url: url,
                            success: function(data) {
                                if (data.status == true) {
                                    Swal.fire(
                                        'Berhasil!',
                                        data.msg,
                                        'success'
                                    );
                                    table.ajax.reload();
                                } else {
                                    toastr.error('Gagal, Hapus data.');
                                }
                            }
                        });
                    }
                });
            });

            $(document).on('click', '.add-voucher', function(e) {
                e.preventDefault();
                let data = $('#form-voucher').serializeArray();
                let check = $('.add-voucher').val();
                let url;
                let method;
                if (check == 0) {
                    url = "{{ route('promo.store') }}";
                } else if (check == 1) {
                    let id = $('#id_voucher').val();
                    url = "{{ route('promo.update', ['promo' => ':id']) }}";
                    url = url.replace(":id", id);
                    data = data.concat({
                        name: "_method",
                        value: "PUT"
                    });
                }

                $.ajax({
                    url: url,
                    data: data,
                    method: 'Post',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status) {
                            $(".text-error-nama_promo").remove();
                            $(".text-error-type_user").remove();
                            $(".text-error-type_voucher").remove();
                            $(".text-error-kode").remove();
                            $(".text-error-kuota").remove();
                            $(".text-error-promo").remove();
                            $(".text-error-periode_awal").remove();
                            $(".text-error-periode_akhir").remove();
                            Swal.fire(
                                'Berhasil!',
                                data.msg,
                                'success'
                            );
                            $('#form-voucher')[0].reset();
                            $('#modal_voucher').modal('hide');
                            table.ajax.reload();

                        } else {
                            $(".text-error-nama_promo").remove();
                            $(".text-error-type_user").remove();
                            $(".text-error-type_voucher").remove();
                            $(".text-error-kode").remove();
                            $(".text-error-promo").remove();
                            $(".text-error-kuota").remove();
                            $(".text-error-periode_awal").remove();
                            $(".text-error-periode_akhir").remove();

                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<small class="text-error-' + field_name +
                                    ' text-danger ">' + error + '</small>')
                            });
                            Swal.fire(
                                    'Gagal',
                                    data.msg,
                                    'error'
                                );
                        }
                    }
                });
            });

        });
    </script>


@endsection
