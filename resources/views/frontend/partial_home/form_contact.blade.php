<div class="ps-home-contact__form form-send">
    <header>
        <h3>Contact Us</h3>
        <p>Learn about our company profile, communityimpact, sustainable motivation, and more.</p>
    </header>

    <footer>
        <form>
            <div class="form-group">
                <label>Name<span>*</span></label>
                <input class="form-control" type="text" name="nama">
            </div>
            <div class="form-group">
                <label>Email<span>*</span></label>
                <input class="form-control" type="email" name="email">
            </div>
            <div class="form-group">
                <label>No.Hp/Wa<span>*</span></label>
                <input class="form-control" type="text" name="no_telp">
            </div>
            <div class="form-group">
                <label>Your message<span>*</span></label>
                <textarea class="form-control" rows="4" name="message"></textarea>
            </div>
            <div class="form-group text-center">
                <button type="submit" class="ps-btn ps-btn-msg">Send Message<i class="fa fa-angle-right"></i></button>
            </div>
        </form>
    </footer>
</div>
@section('js')
    <script>
        $(document).ready(function() {
            $('.form-send form').submit(function(e) {
                $('.ps-btn-msg').html('<i class="fa fa-spinner fa-spin"></i> Proses');
                e.preventDefault();
                let data = $('.form-send form').serializeArray();
                // let data = new FormData(this);
                console.log(data);
                let url = "{{ route('contact_us.store') }}";
                login_form(data, url)
            });

            function login_form(data, url) {
                $.ajax({
                    url: url,
                    data: data,
                    method: 'POST',
                    dataType: 'JSON',
                    success: function(data) {
                        $('.ps-btn-msg').html('Send Message');
                        if (data.status) {
                            $(".text-error-nama").remove();
                            $(".text-error-email").remove();
                            $(".text-error-no_telp").remove();
                            $(".text-error-message").remove();
                            $('.form-send form').trigger("reset");
                            toastr.success(data.msg);
                        } else if (!data.status) {
                            $(".text-error-nama").remove();
                            $(".text-error-email").remove();
                            $(".text-error-no_telp").remove();
                            $(".text-error-message").remove();
                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<span class="text-error-' + field_name +
                                    ' text-danger ">' + error + '</span>')
                            });
                            toastr.error(data.msg);
                        }
                    }
                });
            }
        });
    </script>
@endsection
