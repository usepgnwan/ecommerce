<?php

namespace Database\Seeders;

use App\Models\BannerStore;
use Illuminate\Database\Seeder;

class BannerStoreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $check = $this->command->confirm('are you sure?');
        if($check){
            BannerStore::factory()->count(2)->create();
            $this->command->info('success create banner');
        }else{
            $this->command->info('error create banner');
        }
    }
}
