<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BannerStore extends Model
{
    use HasFactory;
    protected $table = 'banner_store';
    protected $guarded = ['id'];
}
