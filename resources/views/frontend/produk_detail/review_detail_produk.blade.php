@if (count($produk) == 0)
<div class="text-center">
    Review belum tersedia
</div>
@else
<div class="row">
    <div class="col-md-12">
        @foreach ($produk as $pr)
            <div class="ps-review__content mb-3" style="margin-bottom:10px; width:100% !important;">
                <header>
                    <div class="br-wrapper br-theme-fontawesome-stars">
                        <div class="br-wrapper br-theme-fontawesome-stars">
                            <div class="br-widget" style="margin-right: 2px !important;">
                                @php
                                    $rate = [];
                                    for ($i = 5; $i > 0; $i--) {
                                        if ($i <= $pr->star) {
                                            $rate[$i] = '<a href="#" data-rating-value="1" data-rating-text="1" class="br-selected br-current"></a>';
                                        } else {
                                            $rate[$i] = '<a href="#" data-rating-value="5" data-rating-text="5" class=""></a>';
                                        }
                                    }
                                    $star = $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5];
                                @endphp
                                {{-- <a href="#" data-rating-value="1" data-rating-text="1"
                                    class="br-selected br-current"></a>
                                <a href="#" data-rating-value="1" data-rating-text="2"
                                    class="br-selected br-current"></a>
                                <a href="#" data-rating-value="1" data-rating-text="3"
                                    class="br-selected br-current"></a>
                                <a href="#" data-rating-value="1" data-rating-text="4"
                                    class="br-selected br-current"></a>
                                <a href="#" data-rating-value="5" data-rating-text="5" class=""></a> --}}
                                {!!$star!!}
                            </div>
                        </div>
                    </div>
                    <p>Dari<a href="#"> {{$pr->nama}} </a> -  {{Carbon\Carbon::parse($pr->created_at)->format('d M Y, H:i A')}} </p>
                </header>
                <p>{!! $pr->review !!}</p>
                    @if(count($pr->product_preview_images) != 0)
                        <div class="row mb-2">
                            @foreach ($pr->product_preview_images as $r )

                            <div class="col-md-3">
                                <img src="{{  asset('uploads/' . $r->image_name) }}"
                                    width="100%">
                            </div>
                            @endforeach
                        </div>
                     @endif
            </div>
        @endforeach
    </div>
    <div class="col-md-12">
        <div class="ps-product-action">
            <div class="ps-pagination">
                <?php
                // config
                    $link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
                ?>
                 @if ($produk->lastPage() > 1)
                 <ul class="pagination">
                     <li class="{{ $produk->currentPage() == 1 ? ' disabled' : '' }}">
                         <a href="{{ $produk->url(1) }}"><i class="fa fa-angle-left"></i></a>
                     </li>
                     @for ($i = 1; $i <= $produk->lastPage(); $i++)
                         <?php
                         $half_total_links = floor($link_limit / 2);
                         $from = $produk->currentPage() - $half_total_links;
                         $to = $produk->currentPage() + $half_total_links;
                         if ($produk->currentPage() < $half_total_links) {
                             $to += $half_total_links - $produk->currentPage();
                         }
                         if ($produk->lastPage() - $produk->currentPage() < $half_total_links) {
                             $from -= $half_total_links - ($produk->lastPage() - $produk->currentPage()) - 1;
                         }
                         ?>
                         @if ($from < $i && $i < $to)
                             <li class="{{ $produk->currentPage() == $i ? ' active' : '' }}">
                                 <a href="{{ $produk->url($i) }}">{{ $i }}</a>
                             </li>
                         @endif
                     @endfor
                     <li class="{{ $produk->currentPage() == $produk->lastPage() ? ' disabled' : '' }}">
                         <a href="{{ $produk->url($produk->lastPage()) }}"><i class="fa fa-angle-right"></i></a>
                     </li>
                 </ul>
             @endif
            </div>
        </div>
    </div>
</div>

@endif
