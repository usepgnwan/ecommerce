@extends('frontend.template_frontend.frontend')
@section('title', $title)
@section('css')
    <link rel="stylesheet" href="{{ asset('assets_frontend/custom/user.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/custom/custom.css') }}">
    {{-- @yield('css_extendeds') --}}
@endsection
@section('content')
    <style>
        .bs-example {
            margin: 20px;
        }

        a .rotate {
            -webkit-transform: rotate(-180deg);
            /* Chrome, Safari, Opera */
            -moz-transform: rotate(-180deg);
            /* Firefox */
            -ms-transform: rotate(-180deg);
            /* IE 9 */
            transform: rotate(-180deg);
            /* Standard syntax */
        }

        .float-center-div {
            float: none !important;
        }

    </style>
    {{-- <div class="card">
        <div class="container">
            <div class="container mt-5" id="kolom-faq">
                @foreach ($faqs as $faq)
                <div class="panel-group" id="accordion" style="margin-top:5px">
                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                    href="#collapse{{ $faq->id }}">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                > {{ $faq->question }}?
                            </h4>
                        </div>
                    </div>
                </a>
                <div id="collapse{{ $faq->id }}" class="panel-collapse collapse">
                    <div class="panel-body">
                        {{ $faq->answer }}
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div> --}}
    <div class="container text-center" style="margin-top: 50px;">
        <h3>Frequently Asked Questions (FAQ) </h3>
        <p>Kumpulan jawaban dari pertanyaan yang sering ditanyakan</p>
        <div class="col-md-8 float-center-div" style="margin-left: auto; margin-right:auto">
            <div class="panel-group mt-5 text-left" id="accordion">
                @foreach ($faqs as $faq)
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse{{ $faq->id }}">
                                <h4 class="panel-title">
                                    <span class="glyphicon glyphicon-chevron-down"></span>
                                    {{ $faq->question }}
                                </h4>
                        </div>
                        </a>
                        <div id="collapse{{ $faq->id }}" class="panel-collapse collapse">
                            <div class="panel-body">
                                <p>{{ $faq->answer }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        $(document).ready(function() {
            // Add minus icon for collapse element which is open by default
            $(".collapse.in").each(function() {
                $(this)
                    .siblings(".panel-heading")
                    .find(".glyphicon")
                    .addClass("rotate");
            });

            // Toggle plus minus icon on show hide of collapse element
            $(".collapse")
                .on("show.bs.collapse", function() {
                    $(this)
                        .parent()
                        .find(".glyphicon")
                        .addClass("rotate");
                })
                .on("hide.bs.collapse", function() {
                    $(this)
                        .parent()
                        .find(".glyphicon")
                        .removeClass("rotate");
                });
        });
    </script>
@endsection
