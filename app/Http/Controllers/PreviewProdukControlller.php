<?php

namespace App\Http\Controllers;

use App\Models\PreviewProduk;
use App\Models\PreviewProdukImage;
use App\Models\Produk;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class PreviewProdukControlller extends Controller
{
    protected $preview_produk;
    protected $transaksi;
    public function __construct(PreviewProduk $preview_produk,Transaksi $transaksi)
    {
        $this->preview_produk = $preview_produk;
        $this->transaksi = $transaksi;
    }
    public function index(Request $request)
    {
        $id_prod = $request->id == null || $request->id == '' ? '%' :$request->id;
        $preview_produk = $this->preview_produk->where('product_id', 'like', $id_prod)->orderBy('created_at', 'DESC')->get();
        // return response()->json($preview_produk);
        // die;
        if ($request->ajax()) {
            return DataTables::of($preview_produk)
                ->addIndexColumn()
                ->addColumn('tanggal', function ($row) {
                    // Carbon::parse($pr->created_at)->format('d M Y, H:i')
                    return Carbon::parse($row->created_at)->format('d M Y, H:i A');
                })
                ->addColumn('star', function ($row) {
                    $review = '<div class="row">';
                    $bintang = $row->star;
                    for ($i = 5; $i > 0; $i--) {
                        if ($i <= $bintang) {
                            $rate[$i] = '<i class="fa fa-star text-warning"></i>';
                        } else {
                            $rate[$i] = '<i class="fa fa-star "></i>';
                        }
                    }
                    $review .= '
                    <div class="row">
                        <div class="col-12">
                            <label>' . $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5] . '</label>
                        </div>
                    </div> ';
                    $review .= '</div>';
                    return  $review;
                })
                ->addColumn('action', function ($row) {
                    $a = "'nonaktif'";
                    $n = "'aktif'";
                    if($row->status == 'aktif'){
                        $actionBtn = '<a href="javascript:void(0)" title="nonaktifkan" class="primary btn btn-danger btn-sm" onclick="edit(' . $row->id .',' .$a.')"><i class="fa fa-power-off" aria-hidden="true"></i></a>';
                    }else{
                        $actionBtn = '<a href="javascript:void(0)" title="aktifkan" class="primary btn btn-primary btn-sm" onclick="edit(' . $row->id . ',' .$n.')"><i class="fa fa-power-off" aria-hidden="true"></i></a>';
                    }
                    return $actionBtn;
                })
                ->editColumn('review', function ($row) {
                    $review = '<div class="row">
                        <div class="col-md-12">'.$row->review.'</div></div>
                    ';
                    if ($row->product_preview_images->count()) {
                        foreach($row->product_preview_images as $r){
                                $review .= '<div class="row"><div class="col-md-4 mb-2">
                                                <img src="' . asset('uploads/' . $r->image_name) . '" width="150px"/>
                                            </div></div> ';
                        }
                    }
                    return $review;
                })
                ->editColumn('nama', function ($row) {
                    $review = $row->nama;
                    if($row->user_id != null){
                        $review .= '<br> <span class="badge badge-primary">User Terdaftar</span>';
                    }
                    return $review;
                })

                ->rawcolumns(['tanggal','star','action','review','nama'])
                ->make(true);
        }
        $title  = 'All Review';
        return view('layout_admin.master_product.all_penilaian', compact('preview_produk','title'));
    }

    public function show($id){
        $title = "Penilaian Produk";
        $data = Produk::withSum('stok', 'qty')->find($id);
        if (!$data) {
            abort(404);
        }
        $preview_produk = \DB::Select('SELECT sum(all_item) all_item, sum(jumlah_star) jumlah_star FROM (SELECT count(*) all_item, sum(star) jumlah_star  FROM `preview_produks` WHERE product_id = '.$id.' And status ="aktif" GROUP by star ) x');
        $preview_produk = collect($preview_produk)->first();
        // return response()->json($preview_produk );
        // die;
        $rata_rata = $preview_produk->all_item != null || $preview_produk->jumlah_star != null? round($preview_produk->jumlah_star/$preview_produk->all_item) : 0;
        $rate = [];
        for ($i = 5; $i > 0; $i--) {
            if ($i <= $rata_rata) {
                $rate[$i] = '<i class="fa fa-star text-warning"></i>';
            } else {
                $rate[$i] = '<i class="fa fa-star "></i>';
            }
        }
        $review = '
                    <div class="row">
                        <div class="col-md-12">
                            <label>' . $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5] . '</label>
                        </div>
                    </div> ';
        $all_item_prev = $preview_produk->all_item != null ? $preview_produk->all_item : 0;
        return view('layout_admin.master_product.penilaian_produk', compact('title', 'data','review','all_item_prev'));
    }

    public function detail_review(Request $request){
        $produk = $this->preview_produk->where('product_id', '=', $request->id)->where('status','=','aktif')->orderBy('created_at', 'DESC')->paginate(3);
        // echo json_encode($produk);die;
        return response()->json([
            'status' => true,
            'data'=> view('frontend.produk_detail.review_detail_produk',compact('produk'))->render()
        ]);
    }

    public function post_detail_review(Request $request){
        $data = [];
        $idpemesanan = '';
        for($i = 0; $i<count($request->nama); $i++){
            $z = 'rating-' . $i;
               if(!isset($request->$z)){
                     return response()->json(['status'=>false,'msg'=>'Rating Harus Dipilih.']);
                }
                // image
            $idpemesanan = $request->id_pemesanan[$i];
            $data['star'] = $request->$z;
            $data['product_id'] = $request->product_id[$i];
            $data['nama'] = $request->nama[$i];
            $data['user_id'] = !empty(auth()->user()) ? auth()->user()->id :NULL;
            $data['id_transaksi'] = $request->id_transaksi[$i];
            $data['id_pemesanan'] = $request->id_pemesanan[$i];
            $data['review'] = $request->review[$i];
            $data['status'] = 'aktif';
            // $data[$i]['created_at'] = date('Y-m-d H:i:s');
                // dd($data);
            $prev = PreviewProduk::create($data);
            // echo $prev['id'] . '<br>';
            if ($request->hasFile('image'.$i)) {
                $allowedfileExtension = ['jpg', 'png', 'jpeg'];
                $files = $request->file('image'.$i);
                foreach ($files as $file) {
                    $filename = $file->getClientOriginalName();
                    $extension = $file->getClientOriginalExtension();
                    $check = in_array($extension, $allowedfileExtension);
                    // dd($check);
                    if ($check) {
                        $filename = $file->store('image', ['disk' => 'public_uploads']);
                        PreviewProdukImage::create([
                            'preview_produk_id'=> $prev->id,
                            'image_name'=> $filename
                        ]);
                    }
                }
            }

        }
        // dd($data);
        // return response()->json($request);die;
        // return response()->json(['status'=>false,'msg'=>'Rating Harus Dipilih.']);
        $data = $this->transaksi->with('detail_transaksi')->withSum('detail_transaksi', 'quantity')->where('id_pemesanan', '=', $idpemesanan)->with('detail_transaksi')->with('transkaksi_evoucher')->first();
        $item_product = [];
        $preview_produks_item = 0;
        if(!is_null($data)){
            $id_prod = implode(',',array_column($data->detail_transaksi->toArray(), 'product_id'));
            $item_product = $data->status == 'selesai' ? DB::Select('SELECT   ts.id,ts.user_id, ts.id_pemesanan, CONCAT(ts.nama_depan, " ", ts.nama_belakang) nama, ts.created_at tgl_beli, ts.email,ts.no_telp, dt.* ,(SELECT COUNT(*) FROM preview_produks WHERE  product_id = dt.product_id AND id_pemesanan = ts.id_pemesanan) preview_produks_item, (SELECT image_name FROM `product_image` WHERE produk_id = dt.product_id LIMIT 1) img_name
                FROM transaksis ts
                JOIN  `detail_transaksis` dt on ts.id =dt.transaksi_id
                WHERE ts.status ="selesai" and ts.id_pemesanan = "'. $data->id_pemesanan .'" and dt.product_id in ('.$id_prod.')') : [];
            $item_product = collect($item_product);
            $preview_produks_item = array_sum(array_column($item_product->toArray(),'preview_produks_item'));
        }
        // return response()->json($item_product);
        // die;
    //    echo json_encode($data);die;
        $search =  $idpemesanan;
        return response()->json([
            'status' => true,
            'html' => view('layout_admin.transaksi.tracking', compact('data','search','item_product','preview_produks_item'))->render()
        ]);
    }
    public function update(Request $request, $id){
        // echo $request->status;
        // die;
        $find =  $this->preview_produk->find($id);
        if($find){
            $data = ['status' => $request->status];
            // dd($data);
            // die;
            $find->fill($data)->save();
            $preview_produk = \DB::Select('SELECT sum(all_item) all_item, sum(jumlah_star) jumlah_star FROM (SELECT count(*) all_item, sum(star) jumlah_star  FROM `preview_produks` WHERE product_id = '.$find->product_id.' And status ="aktif" GROUP by star ) x');
            $preview_produk = collect($preview_produk)->first();
            $rata_rata = $preview_produk->all_item != null || $preview_produk->jumlah_star != null? round($preview_produk->jumlah_star/$preview_produk->all_item) : 0;
            $rate = [];
            for ($i = 5; $i > 0; $i--) {
                if ($i <= $rata_rata) {
                    $rate[$i] = '<i class="fa fa-star text-warning"></i>';
                } else {
                    $rate[$i] = '<i class="fa fa-star "></i>';
                }
            }
            $review = '
                        <div class="row">
                            <div class="col-md-12">
                                <label>' . $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5] . '</label>
                            </div>
                        </div> ';
            $all_item_prev = $preview_produk->all_item != null ? $preview_produk->all_item : 0;
            return response()->json([
                'status' => true,
                'bintang' => $review,
                'all_item' => $all_item_prev
            ]);
        }
        // dd($request->all());
    }

    public function store(Request $request){
        $validate = Validator::make($request->all(), [
            'nama' =>  'required',
            'review' =>  'required',
            'rating' =>  'required',
            'status' =>  'required',
        ]);
        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, periksa kembali form isian ada yang belum terisi.', 'data' => $validate->errors()]);
        }

            $prev = PreviewProduk::create([
                'product_id' => $request->id,
                'status' => $request->status,
                'nama' => $request->nama,
                'star' => $request->rating,
                'review' => $request->review,
                'id_transaksi' => null,
                'id_pemesanan' => 'admin',
                'user_id' => null,
            ]);

            if ($prev) {
                if ($request->hasFile('image')) {
                    $allowedfileExtension = ['jpg', 'png', 'jpeg'];
                    $files = $request->file('image');
                    foreach ($files as $file) {
                        $filename = $file->getClientOriginalName();
                        $extension = $file->getClientOriginalExtension();
                        $check = in_array($extension, $allowedfileExtension);
                        // dd($check);
                        if ($check) {
                            $filename = $file->store('image', ['disk' => 'public_uploads']);
                            PreviewProdukImage::create([
                                'preview_produk_id'=> $prev->id,
                                'image_name'=> $filename
                            ]);
                        }
                    }
                }
                $preview_produk = DB::Select('SELECT sum(all_item) all_item, sum(jumlah_star) jumlah_star FROM (SELECT count(*) all_item, sum(star) jumlah_star  FROM `preview_produks` WHERE product_id = '.$request->id.' And status ="aktif" GROUP by star ) x');
                $preview_produk = collect($preview_produk)->first();
                $rata_rata = $preview_produk->all_item != null || $preview_produk->jumlah_star != null? round($preview_produk->jumlah_star/$preview_produk->all_item) : 0;
                $rate = [];
                for ($i = 5; $i > 0; $i--) {
                    if ($i <= $rata_rata) {
                        $rate[$i] = '<i class="fa fa-star text-warning"></i>';
                    } else {
                        $rate[$i] = '<i class="fa fa-star "></i>';
                    }
                }
                $review = '
                            <div class="row">
                                <div class="col-md-12">
                                    <label>' . $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5] . '</label>
                                </div>
                            </div> ';
                $all_item_prev = $preview_produk->all_item != null ? $preview_produk->all_item : 0;
                return ['status' => true, 'msg' => 'Review Berhasil Disimpan', 'bintang' => $review,'all_item' => $all_item_prev];
            } else {
                return ['status' => false, 'msg' => 'Review Gagal Disimpan'];
            }

        return response()->json($request);
    }
}
