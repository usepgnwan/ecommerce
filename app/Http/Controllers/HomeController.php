<?php

namespace App\Http\Controllers;

use App\Models\Adress;
use App\Models\BannerPromo;
use App\Models\BannerStore;
use App\Models\MasterCategory;
use App\Models\Produk;
use App\Models\User;
use App\Models\Review;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use RajaOngkir;
use Illuminate\Support\Str;
use PHPUnit\Framework\Constraint\Count;

class HomeController extends Controller
{
    protected $address;
    protected $user;
    protected $produk;
    protected $kategori;
    public function __construct(User $user, Adress $address, Produk $produk, MasterCategory $kategori)
    {
        $this->user = $user;
        $this->address = $address;
        $this->produk = $produk;
        $this->kategori = $kategori;
    }
    public function index()
    {
        $produk = $this->produk->withSum('stok', 'qty')->limit(5)->where('status', '=', 'Diposting')->orderBy('id', 'DESC')->get();
        // echo json_encode($produk);die;
        $banner = new BannerStore;
        $bannerPromo = new BannerPromo;
        $banner_toko = $banner->all();
        $banner_promo = $bannerPromo->first();
        $reviews = Review::where('status', '1')->get();
        return view('frontend.home', compact('banner_toko', 'produk', 'reviews','banner_promo'));
    }


    /* Bagian Untuk profil user */
    public function home_user()
    {
        $id_user = auth()->user()->id;
        $address_user = $this->address->where(['id' => auth()->user()->default_address])->first();
        $title = 'profile';
        return view('frontend.profile_user', compact('title', 'address_user', 'id_user'));
    }

    public function update_profile(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'foto' => 'image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            'name' => 'required',
            'tanggal_lahir' => 'required',
            'no_telp' => 'required',
            'jenis_kelamin' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Profile.', 'data' => $validate->errors()]);
        }
        $imgName = auth()->user()->foto;
        if ($request->hasFile('foto') && $request->foto != '') {
            $path_old = public_path('assets_frontend/images/user/') . auth()->user()->foto;
            if (file_exists($path_old) && auth()->user()->foto != null && auth()->user()->foto != 'user-default.png') {
                unlink($path_old);
            }
            $image = $request->file('foto');
            $imgName = $image->hashName();
            $image->move('assets_frontend/images/user/', $imgName);
        }

        $user = $this->user->find(auth()->user()->id);
        if ($user) {
            $data = [];
            $data['foto'] = $imgName;
            $data['name'] = $request->name;
            $data['tanggal_lahir'] = $request->tanggal_lahir;
            $data['no_telp'] = $request->no_telp;
            $data['jenis_kelamin'] = $request->jenis_kelamin;
            $user->fill($data)->save();
        }
        return response()->json([
            'status' => true,
            'data' => $user
        ]);
    }


    public function home_address()
    {
        $address_user = $this->address->where(['user_id' => auth()->user()->id])->get();
        // dd(!count($address_user));
        $rajaOngkir = new RajaOngkir;
        // return response()->json($rajaOngkir->get_data_provinsi_kota('kota', 9));
        // exit();
        $title = 'Address';
        $provinsi = $rajaOngkir->get_data_provinsi_kota('provinsi');
        return view('frontend.profile_address', compact('title', 'provinsi', 'address_user'));
    }
    public function change_default_address(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'id_address' => 'required'
        ]);
        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, alamat tidak tersedia', 'data' => $validate->errors()]);
        }

        $update = $this->user->where(['id' => auth()->user()->id])->update(['default_address' => $request->id_address]);
        if ($update) {
            return response()->json([
                'status' => true,
                'msg' => 'Berhasil, Menjadikan sebagai alamat utama',
                'data' => $this->address->where(['id' => $request->id_address])->first()
            ]);
        }
        return response()->json([
            'status' => false,
            'msg' => 'Gagal, Menjadikan sebagai alamat utama',
            'data' => []
        ]);
    }

    public function kota($id_address)
    {
        $rajaOngkir = new RajaOngkir;
        $kota = $rajaOngkir->get_data_provinsi_kota('kota', $id_address);
        return response()->json($kota);
    }
    /* Penutup Bagian Untuk profil user */

    // detail produk
    public function product_details($slug)
    {
        // session()->forget('chart_checkout');
        $produk_detail = $this->produk->withSum('stok', 'qty')->where('slug', '=', $slug)->first();
        // echo json_encode(session()->get('chart_checkout'));die;
        if (!$produk_detail) {
            abort(404);
        }

        $preview_produk = \DB::Select('SELECT sum(all_item) all_item, sum(jumlah_star) jumlah_star FROM (SELECT count(*) all_item, sum(star) jumlah_star  FROM `preview_produks` WHERE product_id = '.$produk_detail->id.' And status ="aktif" GROUP by star ) x');
        $preview_produk = collect($preview_produk)->first();

        $rata_rata = $preview_produk->all_item != null || $preview_produk->jumlah_star != null? round($preview_produk->jumlah_star/$preview_produk->all_item) : 0;
        $rate = [];
        for ($i = 5; $i > 0; $i--) {
            if ($i <= $rata_rata) {
                $rate[$i] = '<a href="#" data-rating-value="1" data-rating-text="" class="br-selected br-current"></a>';
            } else {
                $rate[$i] = '<a href="#" data-rating-value="2" data-rating-text="" class=""></a>';
            }
        }
        $bintang =  $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5];
        $jml_rev = $preview_produk->all_item != null ? $preview_produk->all_item: 0;
        // dump($bintang);
        // die;
        $id_kategory = array_column($produk_detail->kategori->toArray(), 'id');
        $produk = $this->produk
            ->inRandomOrder()
            ->withSum('stok', 'qty')
            ->limit(5)
            ->FilterProdukTerkait($id_kategory)
            ->where('status', '=', 'Diposting')
            // ->where('id', '!=', $produk_detail->id)
            ->orderBy('id', 'DESC')->get();
        // echo json_encode( array_column($produk_detail->stok->toArray(),'size', 'id')[5]['size_name']);

        $id_size = array_column($produk_detail->stok->toArray(), 'size', 'id');
        $x = 0;
        foreach ($id_size as $key => $val) {
            $id_size[$key]['qty'] = $produk_detail->stok->toArray()[$x]['qty'];
            $x++;
        }


        $link = $produk_detail->embed != Null || $produk_detail->embed != '' ? json_decode($produk_detail->embed) : [];

        // foreach( $id_size as $key => $val){
        //     // echo $key. ' ' . '<br>';
        //     var_dump($id_size[$key]['size_name']) . '<br>';
        // }
        // echo json_encode($produk);
        // die;
        return view('frontend.produk_detail.produk_detail', compact('produk', 'produk_detail', 'id_size', 'link','bintang','jml_rev'));
    }

    // produck listing
    public function listing_product(Request $request)
    {
        // dd($request->category_check);
        // $kategori = \DB::table('master_category')->get();
        // // foreach($kategori as $t){
        // //    echo  $link = $t->slug;}
        // //     die;
        $currentURL = url()->current();

        $route = Route::current()->uri;
        $routeCheck = count(explode('/', $route));

        $category_check = $routeCheck > 1 ? explode('/', $route)[1]  : '';
        // dd($category);
        // dd(explode('/', $route));
        if ($request->ajax()) {
            // echo json_encode($request->id_kategori);
            // die;
            $order_by = 'id';
            $order = 'asc';

            if ($request->short_by == 'id') {
                $order_by = 'id';
                $order = 'asc';
            } elseif ($request->short_by == 'name') {
                $order_by = 'nama_produk';
                $order = 'asc';
            } elseif ($request->short_by == 'price_low') {
                $order_by = 'harga_jual';
                $order = 'asc';
            } elseif ($request->short_by == 'price_high') {
                $order_by = 'harga_jual';
                $order = 'desc';
            } elseif ($request->short_by == 'diskon') {
                $order_by = 'diskon';
                $order = 'desc';
            }

            $produk = $this->produk
                ->withSum('stok', 'qty')
                ->where('status', '=', 'Diposting')
                ->orderBy($order_by, $order)
                ->Filter($request->id_kategori, $request->category_check)
                ->where('harga_jual', '>=', $request->min_price)
                ->where('harga_jual', '<=',  $request->max_price)
                ->where('nama_produk', 'like', '%' . $request->search . '%')
                ->paginate(8)
                ->withQueryString();
            $render = true;
            // echo json_encode($produk);die;
            return view('frontend.listing_product.listing_product', compact('render', 'produk', 'routeCheck'))->render();
        }
        $kategori = $this->kategori->all();
        $produk = $this->produk
            ->withSum('stok', 'qty')
            ->where('nama_produk', 'like', '%' . $request->search . '%')
            ->where('status', '=', 'Diposting')
            ->Filter(0, $category_check)
            ->orderBy('id', 'DESC')
            ->paginate(8)
            ->withQueryString();
        return view('frontend.listing_product.product', compact('produk', 'kategori', 'routeCheck', 'category_check'));
    }

    public function render_listing_product()
    {
        $render = true;
        $produk = $this->produk->withSum('stok', 'qty')->where('status', '=', 'Diposting')->orderBy('id', 'DESC')->paginate(1);
        // echo json_encode($produk);die;
        return view('frontend.listing_product.listing_product', compact('render', 'produk'))->render();
    }
}
