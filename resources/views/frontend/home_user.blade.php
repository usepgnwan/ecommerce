@extends('frontend.template_frontend.frontend')
@section('title', $title)
@section('css')
<link rel="stylesheet" href="{{ asset('assets_frontend/custom/user.css') }}">
<link rel="stylesheet" href="{{ asset('assets_frontend/custom/custom.css') }}">
{{-- @yield('css_extendeds') --}}
@endsection
@section('content')
    <div class="container mb-2 mt-2">
        <style>

        </style>
        <section>
            <div id="wrapper">

                <!-- Sidebar -->
                <div id="sidebar-wrapper">
                    <ul class="sidebar-nav">
                        {{-- <li class="sidebar-brand">
                            <a href="#">
                                Start Bootstrap
                            </a>
                        </li> --}}
                        <li>
                            <a  class="{{$title == 'purchase' ? 'actived' : '' }}" href="#">Histori Pembelian</a>
                        </li>
                        <li>
                            <a class="{{$title == 'profile' ? 'actived' : '' }}" href="{{route('user.profile')}}">Profile Saya</a>
                        </li>
                        <li>
                            <a class="{{$title == 'Address' ? 'actived' : '' }}" href="{{ route('user.address')}}">Manage Alamat</a>
                        </li>
                        {{-- <li>
                            <a href="#">Events</a>
                        </li>
                        <li>
                            <a href="#">About</a>
                        </li>
                        <li>
                            <a href="#">Services</a>
                        </li>
                        <li>
                            <a href="#">Contact</a>
                        </li> --}}
                    </ul>
                </div>
                <!-- /#sidebar-wrapper -->

                <!-- Page Content -->
                <div id="page-content-wrapper">
                    <div class="container-fluid">
                        <div id="page-content-wrapper">
                            <a href="#" class="btn btn-default" id="menu-toggle"><span class="glyphicon glyphicon-th-list"></span> &nbsp; MENU</a>
                            @yield('content_user')
                        </div>
                    </div>
                </div>
                <!-- /#page-content-wrapper -->

            </div>
            <!-- /#wrapper -->
        </section>
    </div>
@endsection
@section('js')
<script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
@yield('js_extended')
@endsection
