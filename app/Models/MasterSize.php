<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterSize extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "master_size";
    protected $fillable = ['size_name'];
}