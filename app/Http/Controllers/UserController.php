<?php

namespace App\Http\Controllers;

use App\Models\Adress;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    protected $address;
    protected $user;
    public function __construct(User $user, Adress $address)
    {
        $this->user = $user;
        $this->address = $address;
    }
    public function index(Request $request)
    {
        $user = $this->user->withCount('address')->where('role_id',2)->get();
        // return response()->json($user);die;
        if ($request->ajax()) {
            return DataTables::of($user)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" class="btn btn-primary btn-sm view-alamat" title="lihat alamat user"  data-id="' . $row->id . '"><i class="fa fa-solid fa-address-card"></i></a>';
                    return $btn;
                })
                ->addColumn('beli', function ($row) {
                    return 0;
                })
                ->addColumn('tanggal_daftar', function ($row) {
                    return Carbon::parse($row->created_at)->diffForHumans();
                })
                ->rawColumns(['action', 'beli', 'tanggal_daftar'])
                ->make(true);
        }
        return view('layout_admin.master_user', compact('user'));
    }
}
