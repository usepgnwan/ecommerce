@extends('template_admin.admin')

@section('title', 'Pengaturan Toko')
@section('container')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master User</h1>

    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            {{-- <div class="from-group mb-2">
            <button class="btn btn-primary" onclick="tambah()" value="new">Tambah Data</button>
        </div> --}}
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable" style="height: 100">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No.Hp/Wa</th>
                                <th>Jenis Kelamin</th>
                                <th>Alamat</th>
                                <th>Jumlah Pembelian</th>
                                <th>Tanggal Daftar</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Alamat -->
    <div id="modal_addres" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" style="overflow: auto !important;">
        <div class="modal-dialog modal-dialog-centered " role="document">
            <div class="modal-content change-address">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Size</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <form action="">
                    <div class="row all-address">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-address" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {

            $('.provinsi').selectpicker();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });

            var table = $('#dataTable').DataTable({
                prossecing: true,
                serverSide: true,
                ajax: "{{ route('master.user_index') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'name',
                        name: 'name'
                    },
                    {
                        data: 'email',
                        name: 'email'
                    },
                    {
                        data: 'no_telp',
                        name: 'no_telp'
                    },
                    {
                        data: 'jenis_kelamin',
                        name: 'jenis_kelamin'
                    },
                    {
                        data: 'address_count',
                        name: 'address_count'
                    },
                    {
                        data: 'beli',
                        name: 'beli'
                    },
                    {
                        data: 'tanggal_daftar',
                        name: 'tanggal_daftar'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });

            $(document).on('click', '.view-alamat', function() {
                $('#modal_addres').modal('show');
                $('.all-address').html(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class=" ">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b><span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                    `);
                let myid = $(this).data('id');
                let url = "{{ route('get.myaddress', ['id'=>':id']) }}";
               url = url.replace(':id', myid);
                    $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status) {
                            // toastr.success(data.msg);
                            $('.all-address').html('');
                            let i = 1;
                            $.each(data.data, function(i, res) {
                                $('.all-address').append(`
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address" data-id_adress="">
                                        <div class=" ">
                                            <div class="card-body " style="padding: 10px;">
                                                <div class="">
                                                            <div class="row" style="padding:0 10px 0 10px">
                                                                <div class="col-md-12 ">
                                                                    <span class="text-success title-address"
                                                                        style="font-size: 1.3em; color: #c99069">
                                                                        <b> Alamat ${i+1} </b> </span>
                                                                </div>
                                                            </div>
                                                        <div class="row" style="padding:0 10px 0 10px">
                                                        <div class="col-md-12 col-sm-12">
                                                            <hr>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">Nama
                                                                                Penerima</label></td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.nama_penerima} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">No. Telp</label></td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.no_telp} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Provinsi</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.provinsi}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Kota</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.kota}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">No.
                                                                                Pos</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.no_pos} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Alamat</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.alamat}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                `);
                            });
                        } else {
                            $('.all-address').html('');
                            $('.all-address').append(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class="card h-100">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b>ALAMAT BELUM TERSEDIA</b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            `);
                        }
                    }
                });
            });
        });
    </script>


@endsection
