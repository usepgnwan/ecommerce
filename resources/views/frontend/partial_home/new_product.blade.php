<div class="container">
    <div class="ps-section__header mb-50">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
                <h3 class="ps-section__title" data-mask="{{$title}}">- {{$title}}</h3>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                <div class="ps-owl-actions"><a class="ps-prev" href="#"><i
                            class="ps-icon-arrow-right"></i>Prev</a><a class="ps-next" href="#">Next<i
                            class="ps-icon-arrow-left"></i></a></div>
            </div>
        </div>
    </div>

    <div class="ps-section__content">
        <div class="ps-owl--colection owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000"
            data-owl-gap="30" data-owl-nav="false" data-owl-dots="false" data-owl-item="{{ count($produk) }}"
            data-owl-item-xs="1" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="{{ count($produk) }}"
            data-owl-duration="1000" data-owl-mousedrag="on">
            @foreach ($produk as $p)

                <div class="ps-shoes--carousel">
                    <div class="ps-shoe">
                        <div class="ps-shoe__thumbnail">
                        @if ($p->stok_sum_qty >=1)
                            <?php $number_days = (strtotime(date('Y-m-d H:i:s')) - strtotime($p->created_at)) / (60 * 60 * 24); ?>
                            @if ($number_days <= 3)
                                <div class="ps-badge"><span>New</span></div>
                            @endif
                            @if (!is_null($p->diskon) || $p->diskon != '')
                                @if ($p->diskon > 0)
                                    <div class="ps-badge ps-badge--sale  @if ($number_days <= 3) ps-badge--2nd @endif">
                                        <span>-{{ $p->diskon }}%</span>
                                    </div>
                                @endif
                            @endif
                        @else
                            <div class="ps-badge" style="background: red !important; color:white !important; padding:8px 10px 10px; "><small>Sold out.</small></div>
                        @endif
                            {{-- <a class="ps-shoe__favorite" href="#">
                                <i class="ps-icon-heart"></i>
                            </a> --}}

                            @if ($p->images->count())
                                <img src="{{ asset('uploads/' . $p->images[0]->image_name) }}" width="100%" />
                            @else
                                <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                            @endif
                            <a class="ps-shoe__overlay" href="{{ route('products.product_details',['slug'=> $p->slug])}}"></a>
                        </div>
                        <div class="ps-shoe__content">
                            <div class="ps-shoe__variants">
                                <div class="ps-shoe__variant normal">
                                    @if ($p->images->count())
                                        @foreach ($p->images as $img)
                                            <img src="{{ asset('uploads/' . $img->image_name) }}" width="100%" />
                                        @endforeach
                                    @else
                                        <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                                    @endif
                                </div>
                                <div style="font-size:0.7em">
                                    <?php  $kategory = implode(', ', array_column($p->kategori->toArray(),'category_name')); ?>
                                    <span>{{ $kategory }}</span>
                                </div>

                                {{-- <select class="ps-rating ps-shoe__rating">
                                    <option value="1">1</option>
                                    <option value="1">2</option>
                                    <option value="1">3</option>
                                    <option value="1">4</option>
                                    <option value="2">5</option>
                                </select> --}}
                            </div>
                            <div class="ps-shoe__detail">
                                <a class="ps-shoe__name" href="{{ route('products.product_details',['slug'=> $p->slug])}}" style="font-size: 0.9em !important;">
                                    {{ Str::substr($p->nama_produk, 0, 18)  }}  @if (Str::length($p->nama_produk) >= '18') ... @endif  </a>
                                {{-- <p class="ps-shoe__price">
                                      <a href="#">Men shoes</a>,
                                    <a href="#">Nike</a>,
                                    <a href="#">Jordan</a>
                                </p> --}}
                                <p class="ps-shoe__categories">
                                    @if ($p->diskon == '' || $p->diskon == null)
                                        {{ 'Rp.  ' . number_format($p->harga_jual, 0, ',', '.') }}
                                    @else
                                        <?php $potongan = ($p->harga_jual * $p->diskon) / 100;
                                        $jml = $p->harga_jual - $potongan; ?>
                                        <del>{{ 'Rp.  ' . number_format($p->harga_jual, 0, ',', '.') }}</del><br>
                                        {{ 'Rp.  ' . number_format($jml, 0, ',', '.') }}
                                    @endif
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
