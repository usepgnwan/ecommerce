<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use App\Models\FAQ;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class ContactUsController extends Controller
{
    protected $contact;
    public function __construct(ContactUs $contact)
    {
        $this->contact = $contact;
    }
    public function index(Request $request)
    {
        // $post =  $this->post->latest()->with(['category'])->whereHas('category', function (Builder $query) {
        //     $query->where('name', 'Bootstrap');
        // })->get();
        // $post =  $this->post->latest()->get();
        // return response()->json($post);
        // die;
        // $post = Category::with('posts')->get();
        // return response()->json($post);
        $contact = $this->contact->orderBy('created_at', 'DESC')->get();
        // return response()->json($contact);
        if ($request->ajax()) {
            return DataTables::of($contact)
                ->addIndexColumn()
                ->addColumn('tanggal', function ($row) {
                    return Carbon::parse($row->created_at)->diffForHumans();
                })
                ->rawcolumns(['tanggal'])
                ->make(true);
        }
        return view('layout_admin.contact_us', compact('contact'));
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'no_telp' => 'required|min:11|numeric',
            'email' => 'required|email:dns|unique:users,email',
            'message' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false,  'msg' => 'Gagal, periksa kembali form isian', 'data' => $validate->errors()]);
        }

        $data = [
            'nama' => $request->nama,
            'no_telp' =>  $request->no_telp,
            'email' =>  $request->email,
            'deskripsi' =>  $request->message,
        ];

        $response =  $this->contact->create($data);
        return response()->json([
            'status' => true,
            'msg' => 'Terima Kasih, Pesan anda berhasil terkirim.',
            'data' => $response
        ]);
    }
    public function contact()
    {
        $faqs = FAQ::get();
        $title = "FAQ";
        return view('frontend.faq', compact('faqs', 'title'));
    }
}