<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiEvoucher extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $with = ['voucher_kode'];

    public function voucher_kode(){
        return $this->belongsTo(VoucherKode::class, 'voucher_kode_id');
    }

}
