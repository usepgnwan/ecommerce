@extends('frontend.template_frontend.frontend') @section('title', 'Contact') @section('content')

<div class="container mb-2 mt-2" style="float: center">
    <div class="row">
        <div class=" form-login d-flex align-items-center justify-content-center">

                @include('frontend.partial_home.form_contact')

        </div>
    </div>
</div>
@endsection
