<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksis', function (Blueprint $table) {
            $table->id();
            $table->string('id_pemesanan')->unique();
            $table->string('user_id')->nullable();
            $table->string('nama_depan');
            $table->string('nama_belakang');
            $table->string('no_telp',50);
            $table->string('email');
            $table->string('provinsi',100);
            $table->integer('id_provinsi');
            $table->string('kota');
            $table->integer('id_kota');
            $table->string('no_pos',100);
            $table->string('alamat_lengkap');
            $table->string('catatan')->nullable();
            $table->string('kurir', 100);
            $table->string('ongkir', 100);
            $table->string('total_bayar', 100);
            $table->enum('status', ['baru','terkonfirmasi','tracking','selesai', 'rejected'])->default('baru')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksis');
    }
}
