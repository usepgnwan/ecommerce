<!DOCTYPE html>
<!--[if IE 7]><html class="ie ie7"><![endif]-->
<!--[if IE 8]><html class="ie ie8"><![endif]-->
<!--[if IE 9]><html class="ie ie9"><![endif]-->
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <!-- <link href="images/logo/logo-distro.jpeg" rel="apple-touch-icon"> -->
    <link href="{{ asset('assets_frontend/images/logo/logo-distro.jpeg') }}" rel="icon">
    <meta name="author" content="Nghia Minh Luong">
    <meta name="keywords" content="Default Description">
    <meta name="description" content="Default keyword">
    <title>{{ config('app.name') }} | @yield('title') </title>
    <!-- Fonts-->
    <link
        href="https://fonts.googleapis.com/css?family=Archivo+Narrow:300,400,700%7CMontserrat:300,400,500,600,700,800,900"
        rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/ps-icon/style.css') }}">
    <!-- CSS Library-->
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/owl-carousel/assets/owl.carousel.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets_frontend/plugins/jquery-bar-rating/dist/themes/fontawesome-stars.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/slick/slick/slick.css') }}">
    <link rel="stylesheet"
        href="{{ asset('assets_frontend/plugins/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/Magnific-Popup/dist/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/jquery-ui/jquery-ui.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/revolution/css/settings.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/revolution/css/layers.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/plugins/revolution/css/navigation.css') }}">
    <!-- Custom-->
    <link rel="stylesheet" href="{{ asset('assets_frontend/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets_frontend/css/whatsapp.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets_frontend/toast/toastr.min.css') }}">
    @yield('css')
   <script async src="https://www.googletagmanager.com/gtag/js?id=G-8S3Y0M49BQ"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-8S3Y0M49BQ');
    </script>
    <!--HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!--WARNING: Respond.js doesn't work if you view the page via file://-->
    <!--[if lt IE 9]><script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js')}}"></script>___scripts_1___<![endif]-->
</head>
<!--[if IE 7]><body class="ie7 lt-ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 8]><body class="ie8 lt-ie9 lt-ie10"><![endif]-->
<!--[if IE 9]><body class="ie9 lt-ie10"><![endif]-->

<body class="ps-loading">


    <div class="header--sidebar"></div>
    <header class="header">
        <div class="header__top">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-12 ">
                        <p>
                            Kota {{ $store_setting->kota }}, Provinsi {{ $store_setting->provinsi }}, Kode Pos: {{ $store_setting->kode_pos }}, Telp: {{ $store_setting->telepon }} - Fax: {{ $store_setting->fax }}
                        </p>
                    </div>
                    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-12 ">
                        @auth
                            <div class="header__actions">
                                <div class="btn-group ps-dropdown"><a class="dropdown-toggle" href="#"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Hallo,
                                        {{ auth()->user()->name }}<i class="fa fa-angle-down"></i></a>
                                    <ul class="dropdown-menu">
                                        @can('customer')
                                            <li><a href="{{ route('user.profile') }}">Profil</a></li>
                                            <li><a href="#">Pesanan Saya</a></li>
                                        @elsecan('admin')
                                            <li><a href="{{ route('admin.index') }}">Dashboard admin</a></li>
                                        @endcan
                                        <li>
                                            <a href="javascript:void(0)" class="logout-from">
                                                Logout
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            @else
                                <div class="header__actions"><a href="{{ route('login') }}">Login & Regiser</a>
                                @endauth
                                <!-- <div class="btn-group ps-dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">USD<i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu">
                  <li>
                    <a href="#"><img src="images/flag/usa.svg" alt=""> USD</a>
                  </li>
                  <li>
                    <a href="#"><img src="images/flag/singapore.svg" alt=""> SGD</a>
                  </li>
                  <li>
                    <a href="#"><img src="images/flag/japan.svg" alt=""> JPN</a>
                  </li>
                </ul>
              </div> -->
                                <!-- <div class="btn-group ps-dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Language<i class="fa fa-angle-down"></i></a>
                <ul class="dropdown-menu">
                  <li><a href="#">English</a></li>
                  <li><a href="#">Japanese</a></li>
                  <li><a href="#">Chinese</a></li>
                </ul>
              </div> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav class="navigation">
                <div class="container-fluid">
                    <div class="navigation__column left">
                        <div class="header__logo">
                            <a class="ps-logo" href="{{ route('homepage') }}"
                                style="color:rgb(0, 0, 0) !important; font-size:1.8rem">
                                {{ $store_setting->nama_toko }}
                            </a>
                        </div>
                    </div>
                    <div class="navigation__column center">
                        <ul class="main-menu menu">
                            <li class="menu-item "><a class="text-whites"
                                    href="{{ route('homepage') }}">Home</a>
                                {{-- <ul class="sub-menu">
                                    <li class="menu-item"><a href="index.html">Homepage #1</a></li>
                                    <li class="menu-item"><a href="#">Homepage #2</a></li>
                                    <li class="menu-item"><a href="#">Homepage #3</a></li>
                                </ul> --}}
                            </li>
                            <li class="menu-item menu-item-has-children dropdown"><a class="text-whites"
                                    href="javscript:void(0)">MarketPlace</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="{{ $store_setting->fb }}" target="_blank"> Facebook</a></li>
                                    <li class="menu-item"><a href="{{ $store_setting->instagram }}" target="_blank"> Instagram</a></li>
                                    <li class="menu-item"><a href="{{ $store_setting->shopee }}" target="_blank"> Tokopedia</a></li>
                                    <li class="menu-item"><a href="{{ $store_setting->tokopedia }}" target="_blank"> Shopee</a></li>
                                </ul>
                            </li>
                            <li class="menu-item menu-item-has-children dropdown"><a class="text-whites"
                                    href="javscript:void(0)">Produk</a>
                                <ul class="sub-menu">
                                    <li class="menu-item"><a href="{{ route('products.listing_product') }}">All
                                            Product</a></li>
                                    @foreach ($category_all as $c)
                                        <li class="menu-item"><a
                                                href="{{ url('products') . '/' . $c->slug }}">{{ $c->category_name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                            {{-- <li class="menu-item menu-item-has-children has-mega-menu"><a class="text-whites"
                                    href="#">Men</a>
                                <div class="mega-menu">
                                    <div class="mega-wrap">
                                        <div class="mega-column">
                                            <ul class="mega-item mega-features">
                                                <li><a href="product-listing.html">NEW RELEASES</a></li>
                                                <li><a href="product-listing.html">FEATURES SHOES</a></li>
                                                <li><a href="product-listing.html">BEST SELLERS</a></li>
                                                <li><a href="product-listing.html">NOW TRENDING</a></li>
                                                <li><a href="product-listing.html">SUMMER ESSENTIALS</a></li>
                                                <li><a href="product-listing.html">MOTHER'S DAY COLLECTION</a></li>
                                                <li><a href="product-listing.html">FAN GEAR</a></li>
                                            </ul>
                                        </div>
                                        <div class="mega-column">
                                            <h4 class="mega-heading">Shoes</h4>
                                            <ul class="mega-item">
                                                <li><a href="product-listing.html">All Shoes</a></li>
                                                <li><a href="product-listing.html">Running</a></li>
                                                <li><a href="product-listing.html">Training & Gym</a></li>
                                                <li><a href="product-listing.html">Basketball</a></li>
                                                <li><a href="product-listing.html">Football</a></li>
                                                <li><a href="product-listing.html">Soccer</a></li>
                                                <li><a href="product-listing.html">Baseball</a></li>
                                            </ul>
                                        </div>
                                        <div class="mega-column">
                                            <h4 class="mega-heading">CLOTHING</h4>
                                            <ul class="mega-item">
                                                <li><a href="product-listing.html">Compression & Nike Pro</a></li>
                                                <li><a href="product-listing.html">Tops & T-Shirts</a></li>
                                                <li><a href="product-listing.html">Polos</a></li>
                                                <li><a href="product-listing.html">Hoodies & Sweatshirts</a></li>
                                                <li><a href="product-listing.html">Jackets & Vests</a></li>
                                                <li><a href="product-listing.html">Pants & Tights</a></li>
                                                <li><a href="product-listing.html">Shorts</a></li>
                                            </ul>
                                        </div>
                                        <div class="mega-column">
                                            <h4 class="mega-heading">Accessories</h4>
                                            <ul class="mega-item">
                                                <li><a href="product-listing.html">Compression & Nike Pro</a></li>
                                                <li><a href="product-listing.html">Tops & T-Shirts</a></li>
                                                <li><a href="product-listing.html">Polos</a></li>
                                                <li><a href="product-listing.html">Hoodies & Sweatshirts</a></li>
                                                <li><a href="product-listing.html">Jackets & Vests</a></li>
                                                <li><a href="product-listing.html">Pants & Tights</a></li>
                                                <li><a href="product-listing.html">Shorts</a></li>
                                            </ul>
                                        </div>
                                        <div class="mega-column">
                                            <h4 class="mega-heading">BRAND</h4>
                                            <ul class="mega-item">
                                                <li><a href="product-listing.html">NIKE</a></li>
                                                <li><a href="product-listing.html">Adidas</a></li>
                                                <li><a href="product-listing.html">Dior</a></li>
                                                <li><a href="product-listing.html">B&G</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="menu-item"><a class="text-whites" href="#">Women</a></li>
                            <li class="menu-item"><a class="text-whites" href="#">Kids</a></li>
                            <li class="menu-item menu-item-has-children dropdown"><a class="text-whites"
                                    href="#">News</a>
                                <ul class="sub-menu">
                                    <li class="menu-item menu-item-has-children dropdown"><a
                                            href="blog-grid.html">Blog-grid</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item"><a href="blog-grid.html">Blog Grid 1</a></li>
                                            <li class="menu-item"><a href="blog-grid-2.html">Blog Grid 2</a></li>
                                        </ul>
                                    </li>
                                    <li class="menu-item"><a href="blog-list.html">Blog List</a></li>
                                </ul>
                            </li> --}}
                            <li class="menu-item"><a class="text-whites"
                                    href="{{ route('product.check_pesanan') }}">Check Order</a>
                            </li>
                            <li class="menu-item"><a class="text-whites"
                                    href="{{ route('contact_us.nav_contact') }}">FAQ</a>
                            </li>
                        </ul>
                    </div>
                    <div class="navigation__column right">
                        <form class="ps-search--header" action="{{ route('products.listing_product') }}"
                            method="get">
                            <input class="form-control" name="search" type="text" placeholder="Cari produk…" autocomplete="off" style="pointer-events:all">
                            <button type="submit"><i class="ps-icon-search"></i></button>
                        </form>
                        <div class="ps-cart">
                            @php
                                if (session('chart_checkout')) {
                                    $dataCheckout = session()->get('chart_checkout');
                                    $quantityCo = array_sum(array_column($dataCheckout, 'quantity'));
                                    $totalHargaCo = array_sum(array_column($dataCheckout, 'total_harga'));
                                } else {
                                    $quantityCo = 0;
                                    $totalHargaCo = 0;
                                    $dataCheckout = [];
                                }
                            @endphp
                            <a class="ps-cart__toggle" href="{{route('product.checkout_cart')}}">
                                <span>
                                    <i class="quantity-checkout-all">
                                        {{$quantityCo}}
                                    </i>
                                </span><i  class="ps-icon-shopping-cart"></i></a>
                            <div class="ps-cart__listing">
                                <div class="ps-cart__content content-chart-item">
                                    @if (count($dataCheckout) != 0)
                                        @foreach ($dataCheckout as $k => $dco )
                                            <div class="ps-cart-item">
                                                {{-- <a class="ps-cart-item__close" href="#"></a> --}}
                                                <div class="ps-cart-item__thumbnail">
                                                    <a href="#"></a>
                                                    <img src="{{$dataCheckout[$k]['images']}}" alt="">
                                                </div>
                                                <div class="ps-cart-item__content" style="text-align: left">
                                                    <a class="ps-cart-item__title"  href="#"> {{ $dataCheckout[$k]['nama_produk'] }} </a>
                                                    <p>
                                                        <span>Size:<i>{{ $dataCheckout[$k]['size'] }}</i></span><br>
                                                        <span>Quantity:<i>{{ $dataCheckout[$k]['quantity'] }}</i></span><br>
                                                            @if ($dataCheckout[$k]['harga_diskon'] !=0)
                                                                <del><span>Harga:<i>Rp. {{ $dataCheckout[$k]['harga'] }}</i></span></del><br>
                                                               <span>Harga:<i>Rp. {{ $dataCheckout[$k]['harga_diskon'] }}</i></span><br>
                                                            @else
                                                                <span>Harga:<i>Rp. {{ $dataCheckout[$k]['harga'] }}</i></span><br>
                                                            @endif
                                                        <span>Total:<i>Rp.{{ $dataCheckout[$k]['total_harga'] }}</i></span>
                                                    </p>
                                                </div>
                                            </div>
                                        @endforeach
                                    @else
                                        <div class="text-center pb-20" style="color:#2ac37d">
                                            <h4 style="color:#2ac37d !important">Belum Tersedia</h4>
                                        </div>
                                    @endif
                                </div>
                                <div class="ps-cart__total">
                                    <p>Total Item:<span class="quantityCo">{{$quantityCo}}</span></p>
                                    <p>Total Harga Item:<span class="totalHargaCo">  {{ 'Rp.  ' . number_format($totalHargaCo, 0, ',', '.') }}</span></p>
                                </div>
                                <div class="ps-cart__footer"><a class="ps-btn" href="{{ route('product.checkout_cart')}}">Check out<i
                                            class="ps-icon-arrow-left"></i></a></div>
                            </div>
                            <div class="flex-container-co">
                                <div style="width:30%"><img src="" class="img-co" width="100%"></div>
                                <div style="width:70%; text-align:left !important;">
                                    <div class=" ">
                                        <p><b class="title-co"></b></p>
                                        <p class="harga-co">
                                            <del>Rp.10.000</del><br>
                                            spa Rp.  9.500
                                        </p>
                                        <p>Quantity : <span class="quantity-co"></span></p>
                                        <p>size : <span class="size-co"> </span></p>
                                        <p >Total Harga : <span class="total-co"> </span></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="menu-toggle"><span></span></div>
                    </div>
                </div>
            </nav>
    </header>
    {{-- free ongkir off --}}
    @if ( count($evoucher_kode) != 0)
    <div class="header-services">
        <div class="ps-services owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="7000"
            data-owl-gap="0" data-owl-nav="true" data-owl-dots="false" data-owl-item="1" data-owl-item-xs="1"
            data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000"
            data-owl-mousedrag="on">
            @if ( count($evoucher_kode) == 1)
                @foreach ($evoucher_kode as $e )
                    <p class="ps-service"><strong>{{ Str::upper($e->type_voucher) }}</strong>
                    @if ($e->type_voucher == 'diskon')
                        {{ (int)$e->promo . '%'}}
                    @else
                        {{ "Rp." . number_format($e->promo , 0, ',', '.')}}
                    @endif
                         Voucher : <strong>{{ $e->kode }}</strong> </p>
                    <p class="ps-service"><strong>{{ Str::upper($e->type_voucher) }}</strong>
                        @if ($e->type_voucher == 'diskon')
                            {{ (int)$e->promo . '%'}}
                        @else
                            {{ "Rp. " . number_format($e->promo , 0, ',', '.')}}
                        @endif
                         Voucher : <strong>{{ $e->kode }}</strong> </p>
                @endforeach
            @else
                @foreach ($evoucher_kode as $e )
                    <p class="ps-service"><strong>{{ Str::upper($e->type_voucher) }}</strong>
                        @if ($e->type_voucher == 'diskon')
                            {{ (int)$e->promo . '%'}}
                        @else
                            {{ "Rp. " . number_format($e->promo , 0, ',', '.')}}
                        @endif
                         Voucher : <strong>{{ $e->kode }}</strong>
                        @if ($e->type_user != 'all')
                            Member Only
                        @endif
                        </p>
                @endforeach
            @endif

        </div>
    </div>
    @endif
    {{-- ======================= ======================= ======================= End of Header  ======================= ======================= ======================= ======================= ======================= --}}

    @yield('content')

    {{-- ======================= ======================= ======================= Footer ======================= ======================= ======================= --}}

    <div class="ps-footer bg--cover">
        <div class="ps-footer__content">
            <div class="container">
                <div class="row">
                    <!-- <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">

                                <header>
                                    <a class="ps-logo" href="index.html"><img src="images/logo-white.png" alt=""></a>
                                    <h3 class="ps-widget__title">Address Office 1</h3>
                                </header>
                                <footer>
                                    <p><strong>460 West 34th Street, 15th floor, New York</strong></p>
                                    <p>Email: <a href='mailto:support@store.com'>support@store.com</a></p>
                                    <p>Phone: +323 32434 5334</p>
                                    <p>Fax: ++323 32434 5333</p>
                                </footer>

                        </div> -->
                    <div class="col-lg-4 col-md-3 col-sm-12 col-xs-12 ">
                        <aside class="ps-widget--footer ps-widget--link">
                            <header>
                                <h3 class="ps-widget__title">Tentang kami</h3>
                            </header>
                            <footer>
                                <img src="{{ asset('assets_frontend/images/logo/logo-distro.jpeg') }}" alt="logo" style="width:260px; height:160px" >
                                <p><strong>{!! $store_setting->alamat !!}, {{ $store_setting->provinsi }},
                                        {{ $store_setting->kota }} {{ $store_setting->kode_pos }}</strong></p>
                                <p>Email: <a
                                        href='mailto:{{ $store_setting->email }}'>{{ $store_setting->email }}</a>
                                </p>
                                <p>Telp (WA): {{ $store_setting->telepon }}</p>
                                <p>Fax: {{ $store_setting->fax }}</p>
                            </footer>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                        <aside class="ps-widget--footer ps-widget--link">
                            <header>
                                <h3 class="ps-widget__title">Pembayaran</h3>
                            </header>
                            <footer>
                                <ul class="ps-list--link">
                                @if ($m_pembayaran->count())
                                    @foreach ($m_pembayaran as $mp)
                                     <li style="margin-bottom: 4px!important;">  <img style="max-width: 80px;"  src="{{ asset($mp->nama) }} " alt=""></li>
                                    @endforeach
                                @else
                                    <li style="margin-bottom: 4px!important;">Belum Tersedia</li>
                                @endif
                                </ul>
                            </footer>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                        <aside class="ps-widget--footer ps-widget--link">
                            <header>
                                <h3 class="ps-widget__title">Bantuan</h3>
                            </header>
                            <footer>
                                <ul class="ps-list--line">
                                    <li><a href="{{route('product.check_pesanan')}}">Check Order</a></li>
                                    {{-- <li><a href="#">Shipping and Delivery</a></li>
                                    <li><a href="#">Returns</a></li>
                                    <li><a href="#">Payment Options</a></li> --}}
                                    <li><a href="{{route('contact_us.nav_contact')}}">FAQ</a></li>
                                </ul>
                            </footer>
                        </aside>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-4 col-xs-12 ">
                        <aside class="ps-widget--footer ps-widget--link">
                            <header>
                                <h3 class="ps-widget__title">Kategori</h3>
                            </header>
                            <footer>
                                <ul class="ps-list--line">
                                    {{-- <li><a href="#">Shoes</a></li>
                                    <li><a href="#">Clothing</a></li>
                                    <li><a href="#">Accessries</a></li>
                                    <li><a href="#">Football Boots</a></li> --}}
                                    @foreach ($category_all as $c)
                                        <li>
                                            <a href="{{ url('products') . '/' . $c->slug }}">{{ $c->category_name }}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </footer>
                        </aside>
                    </div>
                </div>
            </div>
        </div>
        <div class="ps-footer__copyright">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <p><a href="#">{{ $store_setting->nama_toko }}</a>, Original Brand of Premium Tee.
                            <a href="#">

                            </a>
                        </p>
                        <small>&copy; SKYTHEMES, Inc. All rights Resevered. Design by Alena Studio
                            <script>
                                document.write(new Date().getFullYear())
                            </script>.
                        </small>

                    </div>
                    {{-- <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <ul class="ps-social">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                    </div> --}}
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <ul>
            <a href="{{ $store_setting->fb }}" target="__blank">
                <li class="facebook">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                    <div class="slider">
                        <p>Facebook</p>
                    </div>
                </li>
            </a>
            <a href="{{ $store_setting->instagram }}" target="__blank">
                <li class="instagram">
                    <i class="fa fa-instagram" aria-hidden="true"></i>
                    <div class="slider">
                        <p>instagram</p>
                    </div>
                </li>
            </a>
            <a href="{{ $store_setting->shopee }}" target="__blank">
                <li class="shopee">
                    <img style="margin-top:12px !important;margin-left:12px !important; width: 30px; height: 30px;" src="{{ asset('assets_frontend/images/icon/icons8-shopee.svg')}}" alt="">
                    <div class="slider">
                        <p>Shopee</p>
                    </div>
                </li>
            </a>
            <a href="{{ $store_setting->tokopedia }}" target="__blank">
                <li class="tokopedia">
                    {{-- <i class="fa fa-facebook" aria-hidden="true"></i> --}}
                    <img style="margin-top:8px !important;" src="{{ asset('assets_frontend/images/icon/logo-tokopedia.svg')}}" alt="">
                    <div class="slider">
                        <p>Tokopedia</p>
                    </div>
                </li>
            </a>
        </ul>
    </div>


    <style>
        .wrapper {
            position: fixed;
            top: 50%;
            left: 1%;
            z-index: 10016;
            transform: translate(-50%, -50%);
        }
        @media screen and (max-width: 480px) {
            .wrapper {
                display: none;
            }
        }
        .wrapper ul {
            list-style: none;
        }

        .wrapper ul li {
            width: 50px;
            height: 50px;
            position: relative;
            background: #e59500;
            /* margin: 10px 0; */
            cursor: pointer;
            /* border-radius: 3px;
                box-shadow: 0 0 10px rgba(0,0,0,0.3); */
        }

        .wrapper ul li .fa {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 20px;
            color: #fff;
        }

        .wrapper ul li.facebook {
            background: #3b5998;
            border-top-right-radius: 11px !important;
        }

        .wrapper ul li.twitter {
            background: #00aced;
        }
        .wrapper ul li.shopee {
            background: #ffffff;
            border-right: 1px #f4511e solid;
        }

        .wrapper ul li.instagram {
            /* background: #bc2a8d; */
            background-image: linear-gradient( to top,rgb(239, 169, 92), rgb(112, 65, 196));
        }
        .wrapper ul li.tokopedia {
            background: #42b549;
            border-bottom-right-radius: 11px !important;
        }

        .wrapper ul li.google {
            background: #dd4b39;
        }

        .wrapper ul li.whatsapp {
            background: #4dc247;
        }

        .wrapper ul li.facebook div.slider {
            background: #627aac;
        }

        .wrapper ul li.twitter div.slider {
            background: #7fd5f6;
        }
        .wrapper ul li.shopee div.slider {
            background: #f4511e;
        }
        .wrapper ul li.tokopedia div.slider {
            background: #42b549;
        }

        .wrapper ul li.instagram div.slider {
            /* background: #dd94c6; */
            background-image: linear-gradient( to top,rgb(239, 169, 92), rgb(112, 65, 196));
        }

        .wrapper ul li.google div.slider {
            background: #eea59c;
        }

        .wrapper ul li.whatsapp div.slider {
            background: #82d47e;
        }

        .wrapper .slider {
            content: "";
            position: absolute;
            top: 0;
            left: 51px;
            width: 0px;
            height: 50px;
            background: #eebb5c;
            border-radius: 3px;
            transition: all 0.5s 0.3s ease;
        }

        .wrapper .slider p {
            font-family: arial;
            text-transform: uppercase;
            font-size: 16px;
            font-weight: 900;
            color: #fff;
            text-align: center;
            line-height: 50px;
            opacity: 0;
            transition: all 0.6s ease;
        }

        .wrapper ul li:hover .slider {
            width: 180px;
            transition: all 0.5s ease;
        }

        .wrapper ul li:hover .slider p {
            opacity: 1;
            transition: all 1s 0.2s ease;
        }

        /* youtube link */
        .wrapper .youtube {
            position: fixed;
            bottom: 10px;
            right: 10px;
            width: 160px;
            text-align: center;
            padding: 15px 10px;
            background: #bb0000;
            border-radius: 5px;
        }

        .wrapper .youtube a {
            text-decoration: none;
            color: #fff;
            text-transform: capitalize;
            letter-spacing: 1px;
        }

    </style>


    <style>
    .flex-container-co {
        display: flex;
        background-color: DodgerBlue;
        width: 200px;
        background: #009688;
        background: #fff;
        color: #404040;
        position: fixed;
        display: flex;
        font-weight: 400;
        justify-content: space-between;
        z-index: 100016;
        position: absolute;
        top: 79%;
        right: 35%;
        font-size: 15px;
        padding: 10px 20px;
        box-shadow: 0 1px 15px rgba(32, 33, 36, 0.28);
        border-radius: 8px 0px 8px 8px;
        min-height: 119px;
        width: 240px;
        display: none;
        padding: 5px;
    }
    .flex-container-co p {
        margin-bottom: 0%;
    }
    .flex-container-co>div {
        /* background-color: #f1f1f1; */
        margin: 3px;
        padding: 5px;
    }

    .flex-container-co-show {
        display: flex !important;
    }

</style>
    <div id='whatsapp-chat' class='hide'>
        <div class='header-chat'>
            <div class='head-home'>
                <div class='info-avatar'><img src='{{ asset('assets_frontend/images/logo/logo-distro.jpeg') }}' />
                </div>
                <p><span class="whatsapp-name">{{ config('app.name') }} </span></p>

            </div>
            <div class='get-new hide'>
                <div id='get-label'></div>
                <div id='get-nama'></div>
            </div>
        </div>
        <div class='home-chat'>

        </div>
        <div class='start-chat' style="background: #e6ddd4">
            {{-- pattern="https://elfsight.com/assets/chats/patterns/whatsapp.png" --}}
            <div class="WhatsappChat__Component-sc-1wqac52-0 whatsapp-chat-body">
                <div class="WhatsappChat__MessageContainer-sc-1wqac52-1 dAbFpq">
                    <div style="opacity: 0;" class="WhatsappDots__Component-pks5bf-0 eJJEeC">
                        <div class="WhatsappDots__ComponentInner-pks5bf-1 hFENyl">
                            <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotOne-pks5bf-3 ixsrax"></div>
                            <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotTwo-pks5bf-4 dRvxoz"></div>
                            <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotThree-pks5bf-5 kXBtNt"></div>
                        </div>
                    </div>
                    <div style="opacity: 1;" class="WhatsappChat__Message-sc-1wqac52-4 kAZgZq">
                        <div class="WhatsappChat__Author-sc-1wqac52-3 bMIBDo">{{ config('app.name') }}</div>
                        <div class="WhatsappChat__Text-sc-1wqac52-2 iSpIQi">Hi 👋<br><br>Apakah ada yang bisa saya
                            bantu?
                        </div>
                        {{-- <div class="WhatsappChat__Time-sc-1wqac52-5 cqCDVm">1:40</div> --}}
                    </div>
                </div>
                <div class="WhatsappChat__MessageContainer-sc-1wqac52-1 dAbFpq">
                    <div style="opacity: 0;" class="WhatsappDots__Component-pks5bf-0 eJJEeC">
                        <div class="WhatsappDots__ComponentInner-pks5bf-1 hFENyl">
                            <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotOne-pks5bf-3 ixsrax"></div>
                            <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotTwo-pks5bf-4 dRvxoz"></div>
                            <div class="WhatsappDots__Dot-pks5bf-2 WhatsappDots__DotThree-pks5bf-5 kXBtNt"></div>
                        </div>
                    </div>
                    <div style="opacity: 1;" class="WhatsappChat__Message-sc-1wqac52-4 kAZgZq">
                        <div class="WhatsappChat__Author-sc-1wqac52-3 bMIBDo">{{ config('app.name') }}</div>
                        <div class="WhatsappChat__Text-sc-1wqac52-2 iSpIQi">
                            Note: Kami juga menerima pesanan dengan sistem pre order dengan partai besar hubungi kontak admin yg tersedia.
                        </div>
                        {{-- <div class="WhatsappChat__Time-sc-1wqac52-5 cqCDVm">1:40</div> --}}
                    </div>
                </div>
            </div>

            <div class='blanter-msg'>
                <textarea id='chat-input' placeholder='Masukan pesan anda...' maxlength='120' row='1'></textarea>
                <a href='javascript:void;' id='send-it'><svg viewBox="0 0 448 448">
                        <path d="M.213 32L0 181.333 320 224 0 266.667.213 416 448 224z" />
                    </svg></a>

            </div>
        </div>
        <div id='get-number'></div><a class='close-chat' href='javascript:void'>×</a>
    </div>
    <a class='blantershow-chat' href='javascript:void' title='Show Chat'><svg width="20" viewBox="0 0 24 24">
            <defs />
            <path fill="#eceff1"
                d="M20.5 3.4A12.1 12.1 0 0012 0 12 12 0 001.7 17.8L0 24l6.3-1.7c2.8 1.5 5 1.4 5.8 1.5a12 12 0 008.4-20.3z" />
            <path fill="#4caf50"
                d="M12 21.8c-3.1 0-5.2-1.6-5.4-1.6l-3.7 1 1-3.7-.3-.4A9.9 9.9 0 012.1 12a10 10 0 0117-7 9.9 9.9 0 01-7 16.9z" />
            <path fill="#fafafa"
                d="M17.5 14.3c-.3 0-1.8-.8-2-.9-.7-.2-.5 0-1.7 1.3-.1.2-.3.2-.6.1s-1.3-.5-2.4-1.5a9 9 0 01-1.7-2c-.3-.6.4-.6 1-1.7l-.1-.5-1-2.2c-.2-.6-.4-.5-.6-.5-.6 0-1 0-1.4.3-1.6 1.8-1.2 3.6.2 5.6 2.7 3.5 4.2 4.2 6.8 5 .7.3 1.4.3 1.9.2.6 0 1.7-.7 2-1.4.3-.7.3-1.3.2-1.4-.1-.2-.3-.3-.6-.4z" />
        </svg> Whatsapp</a>

    </main>
    <!-- JS Library-->
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/jquery/dist/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/bootstrap/dist/js/bootstrap.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/jquery-bar-rating/dist/jquery.barrating.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/owl-carousel/owl.carousel.min.js') }}">
    </script>
    {{-- <script type="text/javascript" src="{{ asset('assets_frontend/plugins/gmap3.min.js') }}"></script> --}}
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/imagesloaded.pkgd.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/isotope.pkgd.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/jquery.matchHeight-min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/slick/slick/slick.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/elevatezoom/jquery.elevatezoom.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/Magnific-Popup/dist/jquery.magnific-popup.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets_frontend/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    {{-- <script type="text/javascript"
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAx39JFH5nhxze1ZydH-Kl8xXM3OK4fvcg&amp;region=GB"></script> --}}
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.slideanims.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.navigation.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.parallax.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.actions.min.js') }}">
    </script>

    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.kenburn.min.js') }}">
    </script>
    <script type="text/javascript"
        src="{{ asset('assets_frontend/plugins/revolution/js/extensions/revolution.extension.migration.min.js') }}">
    </script>
    <!-- Custom scripts-->
    <script type="text/javascript" src="{{ asset('assets_frontend/js/main.js') }}"></script>

    <script type="text/javascript" src="{{ asset('assets_frontend/toast/toastr.min.js') }}"></script>
    @yield('js')
    <script>
        $(document).ready(function() {
            toastr.options = {
                'closeButton': true,
                'debug': false,
                'newestOnTop': false,
                'progressBar': false,
                'positionClass': 'toast-bottom-right',
                'preventDuplicates': false,
                'showDuration': '1000',
                'hideDuration': '1000',
                'timeOut': '5000',
                'extendedTimeOut': '1000',
                'showEasing': 'swing',
                'hideEasing': 'linear',
                'showMethod': 'fadeIn',
                'hideMethod': 'fadeOut',
            }

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });

            $('.logout-from').click(function() {
                $.ajax({
                    url: '{{ route('logout') }}',
                    method: 'POST',
                    success: function(data) {
                        window.location.href = "{{ route('login') }}";
                    }
                });
            });

        });
    </script>

    <script>
        /* Whatsapp Chat Widget by www.bloggermix.com */
        $(document).on("click", "#send-it", function() {
                var b = $("#get-number").text();
                var a = document.getElementById("chat-input");
                if ("" != a.value) {
                    var walink = 'https://web.whatsapp.com/send',
                                phone = '{{ $store_setting->telepon }}',
                                text = '%2AHai biLL Distro saya ingin memesan%2A',
                                text_yes = 'Pesanan Anda berhasil terkirim.',
                                text_no = 'Isilah formulir terlebih dahulu.',
                                c = $("#chat-input").val();

                            /* Smartphone Support */
                            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                var walink = 'whatsapp://send';
                            }
                                // var checkout_whatsapp = walink + '?phone=' + phone + '&text=' + text + '%0A%0A' +
                                var checkout_whatsapp = walink + '?phone=62'+ phone + '&text=' + text + '%0A' +
                                    '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _%0A%0A' +
                                    c
                                    ;
                                /* Whatsapp Window Open */
                                window.open(checkout_whatsapp, '_blank');
                } else {
                    toastr.warning('Masukan pesan anda..');
                }
            }),

            // $(document).on("click", ".informasi", function() {
            //     (document.getElementById("get-number").innerHTML = $(this)
            //         .children(".my-number")
            //         .text()),
            //     $(".start-chat,.get-new")
            //         .addClass("show")
            //         .removeClass("hide"),
            //         $(".home-chat,.head-home")
            //         .addClass("hide")
            //         .removeClass("show"),
            //         (document.getElementById("get-nama").innerHTML = $(this)
            //             .children(".info-chat")
            //             .children(".chat-nama")
            //             .text()),
            //         (document.getElementById("get-label").innerHTML = $(this)
            //             .children(".info-chat")
            //             .children(".chat-label")
            //             .text());
            // }),
            $(document).on("click", ".close-chat", function() {
                $("#whatsapp-chat")
                    .addClass("hide")
                    .removeClass("show");
            }),
            $(document).on("click", ".blantershow-chat", function() {
                $("#whatsapp-chat")
                    .addClass("show")
                    .removeClass("hide");
            });
    </script>
</body>

</html>
