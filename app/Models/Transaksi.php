<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    use HasFactory;
    protected $guarded = ['id'];


    public function detail_transaksi(){
        return $this->hasMany(DetailTransaksi::class, 'transaksi_id');
    }
    public function tracking_transaksi(){
        return $this->hasMany(TrackingTransaksi::class, 'transaksi_id');
    }

    public function transkaksi_evoucher(){
        return $this->hasMany(TransaksiEvoucher::class, 'transaksi_id');
    }
}
