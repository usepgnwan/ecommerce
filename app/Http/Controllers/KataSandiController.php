<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

use Illuminate\Support\Facades\Hash;

class KataSandiController extends Controller
{
    public function reset(){
        $password_lama=Request('passLama');
        $password_baru=Request('passBaru');
        $konfirmasi_password=Request('passKonfirmasi');

        if($password_lama=='' || $password_baru=='' || $konfirmasi_password==''){
             return response()->json([
               "code"=>400,
               "message"=>'Harap isi semua form!'
           ], 400);
        }else if(Hash::check($password_lama,auth()->user()->password)){
            if($password_baru==$konfirmasi_password){
               $check = User::find(auth()->user()->id)->update(["password"=>bcrypt($password_baru)]);
               if($check){
                return response()->json([
                    "code"=>200,
                    "message"=>"Password berhasil diperbarui!"
                ], 200);
               }
            }else{
                return response()->json([
                    "code"=>400,
                    "message"=>"Password baru dan konfirmasi password baru harus sama!"
                ], 400);
            }
        }else{
             return response()->json([
                    "code"=>400,
                    "message"=>"Password lama salah!"
                ], 400);
        }
    }
}
