@extends('frontend.template_frontend.frontend')
@section('title', 'Konfirmasi Pemesanan')
@section('content')
    <style>
        .form-login-regis {
            margin: 0px auto;
            width: 70%;
            background: white;
            padding: 20px;
            border-radius: 10px;
            margin-top: 20px;
        }

        @media only screen and (max-width: 600px) {
            .form-login-regis {
                margin: 10px auto;
                width: 90%;
            }
        }

    </style>
    <div class=" text-center form-login-regis" style="">
        <img src="{{asset('assets_frontend/images/icon/shop-bag.png')}}" alt="" title="" class="loaded" width="156" height="156">
        <h3>Selamat {{ request()->pemesan }}, Pesanan Anda Berhasil dibuat</h3>
        <p>berikut ID-Pesanan Kamu <b>{{ request()->id_pemesanan }}</b></p>
        <p>Segera konfirmasi pesanan anda dengan mengirimkan id-pesananan kamu melalui email / whatsapp yg tersedia serta
            pembayaran hanya dapat berlaku apabila dilakukan dengan metode pembayaran yang terdapat dibawah ini.</p>
        <div class="row">
            <div class="col-md-6">
                <b>Kontak</b>
                <p>Email: <a
                        href='mailto:{{ $store_setting->email }}'>{{ $store_setting->email }}</a>
                </p>
                <p>Telp (WA): {{ $store_setting->telepon }}</p>
            </div>
            <div class="col-md-6">
                <table class="table ps-checkout__products">
                    <thead>
                        <tr>
                            <th style="text-align: center;"class="text-uppercase">Metode</th>
                            <th style="text-align: center;"class="text-uppercase">No Rek</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($m_pembayaran as $mp)
                        <tr>
                            <td><img style="max-width: 80px;"
                                    src="{{ asset($mp->nama) }} " alt=""></td>
                            <td>( @if ($mp->kode != null || $mp->kode != '')
                                    {{ $mp->kode }}
                                @else
                                    -
                                @endif ) {{ $mp->no_rek }}</td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <div class="col-md-12 pt-20">
                <a href="javascript:void(0)" style="margin-top:10px;font-size: 13px !important; padding:10px !important;  text-decoration:none; color:white;"  type="button" class="btn mt-3 btn-success send-wa">Konfirmasi pesanan, whatsapp disini</a>
                <a href="{{route('admin_transaksi.detail_transaksi', ['id' => request()->id, 'id_transaksi' => request()->id_pemesanan])}}"  style="margin-top:10px;font-size: 13px !important; padding:10px !important; text-decoration:none; color:white;"  type="button" class="btn mt-3 btn-primary">Download invoice</a>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        let id = '{{ request()->id }}';
        let id_pemesanan = '{{ request()->id_pemesanan }}';

        $('.send-wa').click(function(e){
            e.preventDefault();

            let url = '{{ route("product.checkout_whatsapp") }}';
                $.ajax({
                    url: url,
                    data: {
                        id,
                        id_pemesanan
                    },
                    method: 'POST',
                    dataType:'JSON',
                    success: function(data) {
                        /* Pengaturan Whatsapp */
                            // var walink = 'https://wa.me/?',
                                var walink = 'https://web.whatsapp.com/send',
                                phone = '{{ $store_setting->telepon }}',
                                text = '%2AHai biLL Distro saya ingin memesan%2A',
                                text_yes = 'Pesanan Anda berhasil terkirim.',
                                text_no = 'Isilah formulir terlebih dahulu.';

                            /* Smartphone Support */
                            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                                var walink = 'whatsapp://send';
                            }
                                // var checkout_whatsapp = walink + '?phone=' + phone + '&text=' + text + '%0A%0A' +
                                var checkout_whatsapp = walink + '?phone=62'+ phone + '&text=' + text + '%0A%0A' +
                                    '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _%0A' +
                                    ' %0A%2AID Pemesanan%2A: ' + data.data.id_pemesanan  +
                                    ' %0A%2ANama%2A: ' + data.data.nama_depan + ' '+  data.data.nama_belakang+
                                    ' %0A%2AEmail%2A: ' + data.data.email +
                                    ' %0A%2ANomor WhatsApp%2A: ' +  data.data.no_telp +
                                    ' %0A%2AAlamat%2A: ' + data.data.alamat_lengkap + '%0A' +
                                    '_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _%0A%0A' +
                                     data.pesanan.join(' ')
                                    +'%0A_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _%0A%0A' +
                                    ' %0A%2AKurir%2A: ' + data.data.kurir  +
                                    ' %0A%2AOngkos Kirim%2A: ' + data.data.ongkir  +
                                    ' %0A%2ASub Total%2A: ' + data.sub_total  +
                                    ' %0A%2AEvoucher%2A: ' + data.evoucher  +
                                    ' %0A%2ATotal Pembayaran%2A: ' + data.data.total_bayar
                                    +'%0A_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _%0A%0A' +
                                    '%2ACatatan Pesanan%2A: ' + data.data.catatan

                                    +'%0A_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _%0A%0A' +
                                    '%2ANote%2A: Fitur COD dapat berlaku jika pemesan melakukan konfirmasi pesanan pada nomor wa ini.'
                                    ;
                                /* Whatsapp Window Open */
                                window.open(checkout_whatsapp, '_blank');
                    }
                });
        });
    });
</script>
@endsection
