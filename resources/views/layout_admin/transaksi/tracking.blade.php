@inject('carbon', 'Carbon\Carbon')
<div class="col-md-12 ml-5">
    <style>
        /* body {
                    margin-top: 20px;
                } */

        .timeline {
            border-left: 3px solid #727cf5;
            border-bottom-right-radius: 4px;
            border-top-right-radius: 4px;
            background: rgba(114, 124, 245, 0.09);
            margin: 0 auto;
            letter-spacing: 0.2px;
            position: relative;
            line-height: 1.4em;
            font-size: 1.03em;
            padding: 50px;
            list-style: none;
            text-align: left;
            max-width: 40%;
        }

        @media (max-width: 767px) {
            .timeline {
                max-width: 98%;
                padding: 25px;
            }
        }

        .timeline h1 {
            font-weight: 300;
            font-size: 1.4em;
        }

        .timeline h2,
        .timeline h3 {
            font-weight: 600;
            font-size: 1rem;
            margin-bottom: 10px;
        }

        .timeline .event {
            border-bottom: 1px dashed #e8ebf1;
            padding-bottom: 25px;
            margin-bottom: 25px;
            position: relative;
        }

        @media (max-width: 767px) {
            .timeline .event {
                padding-top: 30px;
            }
        }

        .timeline .event:last-of-type {
            padding-bottom: 0;
            margin-bottom: 0;
            border: none;
        }

        .timeline .event:before,
        .timeline .event:after {
            position: absolute;
            display: block;
            top: 0;
        }

        .timeline .event:before {
            left: -207px;
            content: attr(data-date);
            text-align: right;
            font-weight: 100;
            font-size: 0.9em;
            min-width: 120px;
        }

        @media (max-width: 767px) {
            .timeline .event:before {
                left: 0px;
                text-align: left;
            }
        }

        .timeline .event:after {
            -webkit-box-shadow: 0 0 0 3px #727cf5;
            box-shadow: 0 0 0 3px #727cf5;
            left: -55.8px;
            background: #fff;
            border-radius: 50%;
            height: 9px;
            width: 9px;
            content: "";
            top: 5px;
        }

        @media (max-width: 767px) {
            .timeline .event:after {
                left: -31.8px;
            }
        }

        .rtl .timeline {
            border-left: 0;
            text-align: right;
            border-bottom-right-radius: 0;
            border-top-right-radius: 0;
            border-bottom-left-radius: 4px;
            border-top-left-radius: 4px;
            border-right: 3px solid #727cf5;
        }

        .rtl .timeline .event::before {
            left: 0;
            right: -170px;
        }

        .rtl .timeline .event::after {
            left: 0;
            right: -55.8px;
        }
    </style>
    @if (is_null($data))
        <div class="row gutters">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 text-center">
                            <span class="text-success title-address"
                                style="font-size: 0.8em; color: #c99069; text-align:center">
                                ID Pesanan tidak ditemukan
                            </span>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    @else
        @if ((count($data->tracking_transaksi) == 0 && $data->status == 'baru') || (count($data->tracking_transaksi) != 0 && $data->status == 'baru'))
            <div class="row gutters">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                        <div class="row">
                            <div class="col-md-12 pb-20">
                                {{-- <a href="javascript:void(0)" style="margin-top:10px;font-size: 13px !important; padding:10px !important;  text-decoration:none; color:white;"  type="button" class="btn mt-3 btn-success send-wa">Konfirmasi pesanan, whatsapp disini</a> --}}
                                <a href="{{ route('admin_transaksi.detail_transaksi', ['id' => $data->id, 'id_transaksi' => $data->id_pemesanan]) }}"
                                    style="margin-top:10px;font-size: 13px !important; padding:10px !important; text-decoration:none; color:white;"
                                    type="button" class="btn mt-3 btn-primary">Download invoice</a>
                            </div>
                            <div class="col-md-12 col-sm-12 text-center">
                                <span class="text-success title-address"
                                    style="font-size: 0.9em; color: #c99069; text-align:center">
                                    Tracking Belum Tersedia,Sedang Menunggu Konfirmasi Pesanan <br> <small>Pesanan
                                        dibuat pada Tanggal :
                                        {{ $carbon::parse($data->created_at)->format('Y M d H:i') }}</small>
                                </span>
                                <div class="bukti-pembayaran">
                                    @if (isset($search) && $data->bukti_transfer == null)
                                        <div>
                                            <form action="" id="form-upload-bukti">

                                                <p>Silahkan upload bukti pembayaran dibawah ini, atau hubungi admin
                                                    untuk konfirmasi pesanan</p>
                                                <div class="ps-form--icon text-error-form"> <button
                                                        class="btn btn-primary" id="btn-save-form"
                                                        style="background:#d06418 !important; border:none">
                                                        Upload</button>
                                                    @method('PUT')
                                                    <input type="hidden" name="id" id=""
                                                        value="{{ $data->id }}">
                                                    <input style="padding:10px !important;" class="form-control"
                                                        name="bukti_pembayaran" type="file">
                                                </div>
                                            </form>
                                        </div>
                                    @elseif (isset($search) && $data->bukti_transfer != null)
                                        <span class="text-success title-address">
                                            Terima kasih, bukti pembayaran sedang di review pantau terus pesanan anda di
                                            menu Check Order untuk melihat update pembaruan dari pesanan kamu. <small>
                                        </span>
                                    @endif
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        @else
            <div id="content" style="padding:10px">
                @if (isset($search) && $data->status == 'selesai')
                    @if(isset($preview_produks_item))
                        @if ($preview_produks_item !=0 )
                            Terima Kasih, Atas kepercayaan telah berbelanja di {{ $store_setting->nama_toko }} & Reviewnya ^^
                        @else
                            Yeayy pesananmu telah selesai yuk kasih review nya dibawah ini.<br>
                        @endif
                    @endif
                <form id="form-review-product" enctype="multipart/form-data">
                    @method('POST')
                    @if (isset($item_product))
                        @if (count($item_product) != 0)
                            @foreach ($item_product as $pr)
                                <div class="row">
                                    <div class="col-md-12 card-cart">
                                        <div class="card mb-3 card-checkout"
                                            style="max-width: 900px; margin:2% auto; padding:10px; background-color:##f7f7f7; border: 1px solid #e5e5e5;"
                                            data-id-co="28-26">
                                            <div class="row no-gutters">
                                                <div class="col-md-4 pl-40">
                                                   @if($pr->img_name == null)
                                                        <img  src="{{ asset('assets_frontend/images/shoe-detail/3.jpg') }}" alt=""    style="padding:5px; border-radius:8px; height: 180px; width: 180px;">
                                                   @else
                                                            <img src="{{  asset('uploads') . '/' . $pr->img_name }}" class="card-img"
                                                            alt="Guide to Web Design"
                                                            style="padding:5px; border-radius:8px; height: 180px; width: 180px;">
                                                   @endif
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="card-body">
                                                        <div class="card-title">
                                                            <div class="flex3">
                                                                <div class="flex-items-flex3">
                                                                    <h4 class="pb-10">
                                                                        <strong> {{ $pr->nama_produk }}</strong>
                                                                    </h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="padding:11px">
                                                            <table style="width:100%; ">
                                                                <tbody>
                                                                    @if ($pr->preview_produks_item != 0)
                                                                        <tr  style="border-top: 1px solid #e5e5e5; line-height:30px;">
                                                                            <td style="width:100%;">
                                                                                <label for="">Review Telah Terkirim.</label>
                                                                            </td>
                                                                        </tr>
                                                                    @else
                                                                        <tr
                                                                            style="border-bottom: 1px solid #e5e5e5; line-height:30px;">
                                                                            <td style="width:100%;">
                                                                                <style>
                                                                                    .btn-outline-warning {
                                                                                            color: #ffa426 !important;
                                                                                            border-color: #ffa426  !important;
                                                                                    }
                                                                                    .btn-outline-warning:hover {
                                                                                        color: #fff !important;
                                                                                        background-color: #ffa426 !important;
                                                                                        border-color: #ffa426 !important;
                                                                                    }
                                                                                    .xxx>.active {
                                                                                        color: #fff !important;
                                                                                        background-color: #ffa426 !important;
                                                                                        border-color: #ffa426 !important;
                                                                                        }
                                                                                    }
                                                                                </style>
                                                                                <div class="form-group">
                                                                                    <label><strong>Rating </strong></label>
                                                                                    <div class="xxx" data-toggle="buttons">
                                                                                        <label class="btn btn-outline-warning  mx-1" id="btn-rate1">
                                                                                            <input type="radio" name="rating-{{$loop->index}}" id="rate1" value="1" autocomplete="off"
                                                                                                style="display: none">
                                                                                            <i class="fa fa-star" style="color:yellow !important;"></i> 1
                                                                                        </label>
                                                                                        <label class="btn btn-outline-warning  mx-1" id="btn-rate2">
                                                                                            <input type="radio" name="rating-{{$loop->index}}" id="rate2" value="2" autocomplete="off"
                                                                                                style="display: none">
                                                                                            <i class="fa fa-star"  style="color:yellow !important;"></i> 2
                                                                                        </label>
                                                                                        <label class="btn btn-outline-warning  mx-1" id="btn-rate3">
                                                                                            <input type="radio" name="rating-{{$loop->index}}" id="rate3" value="3" autocomplete="off"
                                                                                                style="display: none">
                                                                                            <i class="fa fa-star"  style="color:yellow !important;"></i> 3
                                                                                        </label>
                                                                                        <label class="btn btn-outline-warning  mx-1" id="btn-rate4">
                                                                                            <input type="radio" name="rating-{{$loop->index}}" id="rate4" value="4" autocomplete="off"
                                                                                                style="display: none">
                                                                                            <i class="fa fa-star"  style="color:yellow !important;"></i> 4
                                                                                        </label>
                                                                                        <label class="btn btn-outline-warning  mx-1" id="btn-rate5">
                                                                                            <input type="radio" name="rating-{{$loop->index}}" id="rate5" value="5" autocomplete="off"
                                                                                                style="display: none">
                                                                                            <i class="fa fa-star"  style="color:yellow !important;"></i> 5
                                                                                        </label>
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr
                                                                            style="border-bottom: 1px solid #e5e5e5; line-height:30px;">
                                                                            <td style="width:100%;">
                                                                                <label for="">Pilih Gambar</label>
                                                                            <input type="file"  name="image{{$loop->index}}[]" multiple>
                                                                            <input type="hidden" name="nama[]" value="{{ $pr->nama}}">
                                                                            <input type="hidden" name="product_id[]" value="{{ $pr->product_id}}">
                                                                            <input type="hidden" name="id_transaksi[]" value="{{ $pr->id}}">
                                                                            <input type="hidden" name="id_pemesanan[]" value="{{ $pr->id_pemesanan}}">
                                                                            <input type="hidden" name="user_id[]" value="{{ $pr->user_id}}">
                                                                            <br>
                                                                            </td>
                                                                        </tr>
                                                                        <tr
                                                                            style="border-bottom: 1px solid #e5e5e5; line-height:30px;">
                                                                            <td style="width:100%;">
                                                                                <textarea name="review[]" class="form-control" id="" style="height: 100px; width: 100%" placeholder="Review"></textarea>
                                                                            </td>
                                                                        </tr>
                                                                    @endif
                                                                </tbody>
                                                            </table>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    @endif
                    @if(isset($preview_produks_item))
                        @if ($preview_produks_item == 0 )
                            <div class="row">
                                <div class="col-md-12 pb-20">
                                    <button class="btn btn-primary" type="submit" style="background:#d06418 !important; border:none"><i class="fa fa-star"> Simpan Review </i></button>
                                </div>
                            </div>
                        @endif
                    @endif
                </form>
                @else
                    @if (isset($search) && $data->bukti_transfer == null)
                        <div class="bukti-pembayaran">
                            <div class="mb-2">
                                <form action="" id="form-upload-bukti">
                                    <p>Silahkan upload bukti pembayaran dibawah ini, atau hubungi admin untuk konfirmasi
                                        pesanan
                                    </p>
                                    <div class="ps-form--icon text-error-form mb-2" style="margin-bottom:10px"> <button
                                            class="btn btn-primary" id="btn-save-form"
                                            style="background:#d06418 !important; border:none">
                                            Upload</button>
                                        @method('PUT')
                                        <input type="hidden" name="id" id="" value="{{ $data->id }}">
                                        <input style="padding:10px !important;" class="form-control"
                                            name="bukti_pembayaran" type="file">
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif

                    @if ($data->bukti_transfer != null)
                        <div class="row text-center">
                            <div class="col-md-12" style="margin: 0 auto;">
                                @if (isset($status_admin))
                                    <img src="{{ asset('assets_frontend/uploads/bukti_transfer/' . $data->bukti_transfer) }}"
                                        style="width:180px; height: 180px;" />
                                @else
                                    <div class="col-md-12 pb-20">
                                        Bukti Pembayaran, Telah Terkonfirmasi terima kasih.<br>
                                        <a href="{{ route('admin_transaksi.detail_transaksi', ['id' => $data->id, 'id_transaksi' => $data->id_pemesanan]) }}"
                                            style="margin-top:10px;font-size: 13px !important; padding:10px !important; text-decoration:none; color:white;"
                                            type="button" class="btn mt-3 btn-primary">Download invoice</a>
                                    </div>
                                @endif
                                <div>
                                    <a href="{{ asset('assets_frontend/uploads/bukti_transfer/' . $data->bukti_transfer) }}"
                                        target="_blank" download=""><i class="fa fa-download"></i></a>
                                </div>
                            </div>
                        </div>
                    @endif
                    <ul class="timeline">
                        @foreach ($data->tracking_transaksi as $tc)
                            <li class="event"
                                data-date="{{ $carbon::parse($tc->tanggal_tracking)->format('Y M d H:i') }}">
                                <h3>{{ $tc->title }}</h3>
                                <p>{{ $tc->deskripsi }}</p>
                            </li>
                        @endforeach
                    </ul>
            </div>
        @endif
    @endif


    @endif
</div>
