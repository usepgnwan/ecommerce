<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeightOngkir extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    // protected $with = ['detail_weight_ongkir'];
    public function detail_weight_ongkir(){
        return $this->hasMany(DetailWeightOngkirs::class,'weight_ongkir_id');
    }
}
