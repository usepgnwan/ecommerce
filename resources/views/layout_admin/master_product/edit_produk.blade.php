@extends('template_admin.admin')

@section('title', $title)
@section('container')
    <style>
        .file {
            visibility: hidden;
            position: absolute;
        }

    </style>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Edit Product</h1>

    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Edit Produk </h6>
                </div>
                <div class="card-body">
                    <div class="container">
                        <form method="POST" enctype="multipart/form-data"
                            action="{{ route('product.update', $product->id) }}" id="form-tambah-produk">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="nama_produk">Nama Produk *</label>
                                <input type="text" class="form-control" id="nama_produk" name="nama_produk"
                                    placeholder="Nama Produk" value="{{ $product->nama_produk }}" required>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi *</label>
                                <textarea class="form-control" id="deskripsi" name="deskripsi" placeholder="" rows="4"
                                    required>{{ $product->deskripsi }}</textarea>
                            </div>
                            <div class="form-group">
                                <label><strong>Foto Produk *</strong></label>
                                <div class="custom-file">
                                    <input type="file" name="foto[]" multiple class="custom-file-input form-control"
                                        id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div class="container">
                                <div class="row">
                                    @foreach ($product->images as $foto)
                                        <div class="col-2 border mx-2" id="imagedelete{{ $foto->id }}">
                                            <button type="button" class="close"
                                                onclick="hapus_item({{ $foto->id }},'image')">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <img src="{{ asset('uploads/' . $foto->image_name) }}" width="100%">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group my-3">
                                <label for="youtube_video">Link Sematkan Video Youtube*</label>
                                <div class="row">
                                    <div class="col-10">
                                        <input type="text" class="form-control" id="youtube_video"
                                            placeholder="contoh: https://youtube.com/video1" r>
                                    </div>
                                    <div class="col-2">
                                        <button class="btn btn-primary" type="button" onclick="show_badge('embed')"><i
                                                class="fa fa-plus-circle"></i></button>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        Link Sematan anda: </div>
                                    <div class="col-8" id="resultembed">
                                    </div>
                                    <div id="resultformembed">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group mt-5">
                                <label><strong>Stok *</strong></label> <a class="mr-3 btn btn-primary" style="float:right"
                                    href="javascript:void(0)" onclick="add_sizeform()"><i class="fa fa-plus-circle"></i>
                                </a>
                                @foreach ($product['stok'] as $stok)
                                    <div class="row mt-3" id="stokdelete{{ $stok->id }}">
                                        <div class="col-5">
                                            <select class="form-control" id="size" name="size[]" required>
                                                <option value="">-Pilih Size Produk-</option>
                                                @foreach ($sizes as $size)
                                                    <option {{ $stok->size == $size->id ? 'selected' : '' }}
                                                        value="{{ $size->id }}">{{ $size->size_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <input type="text" class="form-control" id="stok" name="stok[]"
                                                placeholder="Jumlah Stok" value="{{ $stok->qty }}" required>
                                        </div>
                                        <div class="col-2">
                                            <button class="btn btn-danger" type="button"
                                                onclick="hapus_item({{ $stok->id }},'stok')"><i
                                                    class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                @endforeach
                                <div id="formna"></div>
                            </div>
                            <div class="form-group">
                                <label for="kategori">Kategori *</label>
                                <div class="row">
                                    <div class="col-12">
                                        <select class="form-control selectsearch" multiple id="kategori" data-live-search="true" name="kategori[]">
                                            @foreach ($categories as $category)
                                                {{-- <option value="{{ $category->id }}|{{ $category->category_name }}">
                                                    {{ $category->category_name }}
                                                </option> --}}
                                                <option value="{{ $category->id }}" @if(in_array($category->id, array_column($product->kategori->toArray(), 'id'))) selected @endif>
                                                    {{ $category->category_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-2"> <button class="btn btn-primary" type="button"
                                            onclick="show_badge('kategori')"><i class="fa fa-plus-circle"></i></button>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        Kategori: </div>
                                    <div class="col-8" id="resultkategori"></div>
                                    <div id="resultformkategori"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="harga_beli"> Harga Beli *</label>
                                        <input type="text" class="form-control" id="harga_beli" name="harga_beli"
                                            placeholder="Harga Beli" value="{{ $product->harga_beli }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="harga_jual">Harga Jual *</label>
                                        <input type="text" class="form-control" id="harga_jual" name="harga_jual"
                                            placeholder="Harga Jual" value="{{ $product->harga_jual }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="diskon">Diskon</label>
                                        <input type="text" class="form-control" id="diskon" name="diskon"
                                            value="{{ $product->diskon }}" placeholder="Diskon">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status">Status *</label>
                                <select class="form-control" id="status" name="status" required>
                                    <option {{ $product->status == '' ? 'selected' : '' }} value="">-Pilih Status Produk-
                                    </option>
                                    <option {{ $product->status == 'Diposting' ? 'selected' : '' }} value="Diposting">
                                        Diposting</option>
                                    <option {{ $product->status == 'Belum Diposting' ? 'selected' : '' }}
                                        value="Belum Diposting">Belum Diposting</option>
                                </select>
                            </div>
                            <div class="form-group text-right">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="button" class="btn btn-primary" onclick="submit_data()">Submit</button>
                            </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.1/tinymce.min.js"></script>
    <script>
        var dataembed = [];
        var datakategori = [];
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 200,
                menubar: false,
                plugins: [
                    'advlist autolink lists link charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime  table contextmenu paste code wordcount'
                ],
                mobile: {
                    theme: 'mobile'
                },
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],
                setup: function(editor) {
                    editor.on('change', function() {
                        editor.save();
                    });
                },
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });
            $('.selectsearch').selectpicker();
            @if ($product->embed)
                @foreach (json_decode($product->embed) as $link_embed)
                    dataembed.push("{{ $link_embed }}");
                    show_badge('embed')
                @endforeach
            @endif

            // {{--  @foreach ($product->kategori as $kategori)
            //     datakategori.push({
            //     "id":"{{ $kategori->id }}",
            //     "nama":"{{ $kategori->category_name }}"
            //     });
            //     show_badge('kategori')
            // @endforeach --}}

            $('input[type="file"]').on("change", function() {
                let filenames = [];
                let files = this.files;
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            });
        });

        function add_sizeform() {
            $('#formna').append(`<div class="row mt-3" id="stok-size">
                                <div class="col-5">
                                    <select class="form-control" id="size" name="size[]">
                                        <option value="">-Pilih Size Produk-</option>
                                        @foreach ($sizes as $size)
                                            <option value="{{ $size->id }}">{{ $size->size_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-5">
                                    <input type="number" class="form-control" id="stok" name="stok[]"
                                        placeholder="Jumlah Stok">
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-danger" type="button" id="minus"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>  `);
        }

        $('#formna').on('click', '#minus', function(e) {
            e.preventDefault();

            $(this).parent().parent().remove();
        });

        function submit_data() {
            if ($("#form-tambah-produk")[0].checkValidity()) {
                $('#form-tambah-produk').submit()
            } else {
                Swal.fire("Close", "Mohon Isi Semua Field bertanda Bintang!", "error");
            }
        }

        function hapus_item(id, item) {
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    if (item == 'kategori') {
                        $.ajax({
                            type: "post",
                            url: "{{ route('product.remove_kategori') }}",
                            data: {
                                '_token': '{{ @csrf_token() }}',
                                '_method': 'delete',
                                'master_category_id': id,
                                'produk_id': "{{ $product->id }}"
                            },
                            success: function(response) {
                                if (response.success) {
                                    index = datakategori.findIndex(x => x.id === id);

                                    datakategori.splice(index);
                                    $('#badgekategori' + id).remove();
                                    $('#formkategori' + id).remove();

                                    Swal.fire(
                                        'Berhasil!',
                                        item + ' berhasil dihapus!',
                                        'success'
                                    )
                                }
                            }
                        });
                    } else {
                        $.ajax({
                            type: "post",
                            url: "{{ route('product.remove_item') }}",
                            data: {
                                '_token': '{{ @csrf_token() }}',
                                '_method': 'delete',
                                'id': id,
                                'item': item
                            },
                            success: function(response) {
                                if (response.success) {
                                    if (item == 'stok') {
                                        $('#stokdelete' + id).remove();
                                    } else if (item == 'image') {
                                        $('#imagedelete' + id).remove();
                                    }
                                    Swal.fire(
                                        'Berhasil!',
                                        item + ' berhasil dihapus!',
                                        'success'
                                    )
                                }
                            }
                        });
                    }
                }
            })

        }

        function show_badge(type) {
            if (type == 'embed') {
                if ($("#youtube_video").val()) {
                    dataembed.push($("#youtube_video").val());
                }
                $("#youtube_video").val('');

                $('#result' + type).html('');
                $('#resultform' + type).html('');
                $.each(dataembed, function(index, val) {
                    $('#result' + type).append(
                        `<a href="javascript:void(0)" class="badge badge-lg badge-success mx-1" id="badgeembed` +
                        index +
                        `" onclick="hapus_badge('embed',` + index + `)"> ` + val +
                        ` <i class="fa fa-times"></i></a>`)
                    $('#resultform' + type).append(`<input type="hidden" name="embed_youtube[]" id="formembed` +
                        index + `"value="` + val + `">`)
                });
                // console.log(dataembed)
            }
            // else if (type == 'kategori') {
            //     if ($("#kategori").val()) {
            //         var isi = $("#kategori").val().split("|");
            //         datakategori.push({
            //             "id": isi[0],
            //             "nama": isi[1]
            //         });
            //     }
            //     $("#kategori").val('');

            //     $('#result' + type).html('');
            //     $('#resultform' + type).html('');
            //     $.each(datakategori, function(index, val) {
            //         $('#result' + type).append(
            //             `<a href="javascript:void(0)" class="badge badge-lg badge-success mx-1" id="badgekategori` +
            //             val['id'] +
            //             `" onclick="hapus_item(` + val['id'] + `,'kategori')"> ` + val['nama'] +
            //             ` <i class="fa fa-times"></i></a>`)
            //         $('#resultform' + type).append(`<input type="hidden" name="kategori[]" id="formkategori` +
            //             val['id'] + `" value="` + val['id'] + `" required>`)
            //     });
            // }
        }

        function hapus_badge(type, index) {
            if (type == 'embed') {
                dataembed.splice(index, 1);
                $('#badgeembed' + index).remove();
                $('#formembed' + index).remove();
            }
        }
    </script>
@endsection
