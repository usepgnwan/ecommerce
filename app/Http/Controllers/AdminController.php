<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaksi;
use App\Models\ProductStock;
use App\Models\Produk;
use App\Models\Transaksi;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;

class AdminController extends Controller
{
    public function index()
    {
        $data_overview = [
            "produk_diposting" => Produk::where(['status' => 'Diposting'])->count(),
            "produk_belum_diposting" => Produk::where(['status' => 'Belum Diposting'])->count(),
            "jumlah_user" => User::count(),
            "jumlah_pesanan" => Transaksi::where(['status' => 'Baru'])->count(),
        ];


        $sold = [];
        $trx = DetailTransaksi::limit(5)->groupby('product_id')->get();
        foreach ($trx as $dt_trx) {
            $satupersen = (int)ProductStock::where(['id_produk' => $dt_trx->product_id])->sum('qty') / 100;
            if ($satupersen != 0) {
                $persen = (int)DetailTransaksi::where(['product_id' => $dt_trx->product_id])->sum('quantity') / $satupersen;
                $sold[] = [
                    "nama" => $dt_trx->nama_produk,
                    "terjual" => DetailTransaksi::where(['product_id' => $dt_trx->product_id])->sum('quantity'),
                    "total_stok" => DetailTransaksi::where(['product_id' => $dt_trx->product_id])->sum('quantity') + ProductStock::where(['id_produk' => $dt_trx->product_id])->sum('qty'),
                    "persentase" =>  $persen,
                ];
            }
        }
        $title = 'Dashboard';
        $transaksi = Transaksi::limit('5')->orderBy('created_at', 'DESC')->get();
        $tahunbulan = [];
        $jumlah = [];
        for ($i = 1; $i <= 6; $i++) {
            $tahun = date("Y", strtotime(date('Y-m-01') . " -$i months"));
            $bulan = date("F", strtotime(date('Y-m-01') . " -$i months"));
            array_push($tahunbulan, $bulan . '-' . $tahun);
            array_push($jumlah, Transaksi::whereMonth('created_at', date("m%", strtotime(date('Y-m-01') . " -$i months")))->whereYear('created_at', $tahun)->count());
        }
        return view('layout_admin.dashboard', compact('title', 'data_overview', 'sold', 'transaksi', 'tahunbulan', 'jumlah'));
    }
}