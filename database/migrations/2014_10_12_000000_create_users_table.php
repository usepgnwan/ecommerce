<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('role_id');
            $table->string('name');
            $table->string('privacy_term');
            $table->string('no_telp');
            $table->string('email')->unique();
            $table->enum('jenis_kelamin', ['laki-laki','perempuan'])->default('laki-laki')->nullable();
            $table->date('tanggal_lahir')->default(Carbon::now());
            $table->string('default_address')->nullable();
            $table->string('foto')->default('user-default.png')->nullable();
            
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
