<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Produk extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $table = 'product';
    protected $fillable = ['status', 'nama_produk', 'id_kategori', 'id_size', 'deskripsi', 'harga_beli', 'harga_jual', 'diskon', 'embed', 'slug'];
    protected $with = ['images', 'stok', 'kategori'];

    public function images()
    {
        return $this->hasMany(ProductImage::class, 'produk_id');
    }

    public function stok()
    {
        return $this->hasMany(ProductStock::class, 'id_produk');
    }

    public function kategori()
    {
        return $this->belongsToMany(MasterCategory::class, 'produk_kategori');
    }

    public function scopeFilterProdukTerkait($query, array $search)
    {
        $query->when($search ?? false, function ($query) use ($search) {
            $query->whereHas('kategori', function ($query)  use ($search) {
                $query->whereIn('id',  $search);
            });
                // ->orWhere('title', 'like', '%' . $search . '%');
        });
    }
    public function scopeFilter($query, $id_kategori,$category)
    {
        $query->when($id_kategori != 0 || $id_kategori != '0' ?? false, function ($query) use ($id_kategori) {
            $query->whereHas('kategori', function ($query)  use ($id_kategori) {
                $query->whereIn('id',  $id_kategori);
            });
                // ->orWhere('title', 'like', '%' . $search . '%');
        });
        $query->when($category ?? false, function ($query) use ($category) {
            $query->whereHas('kategori', function ($query)  use ($category) {
                $query->where('slug','=', $category);
            });
        });
    }

    public function product_preview(){
        return $this->hasMany(PreviewProduk::class, 'product_id');
    }
}
