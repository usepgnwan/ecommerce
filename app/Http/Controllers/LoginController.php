<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Rules\MatchOldPassword;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    protected $user;
    public function __construct(User $user)
    {
        $this->user = $user;
    }
    public function index(){
        return view('frontend.template_frontend.LoginRegis');
    }
    public function login(Request $request){
        $validate = Validator::make($request->all(), [
            'email' => 'required|email:dns',
            'password' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'valid'=>false, 'msg' => 'Gagal, periksa kembali form isian', 'data' => $validate->errors()]);
        }
        $credentials = [
            'email' => $request->email,
            'password' =>$request->password,
        ];
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            return response()->json([
                'status' => true,
                'valid'=>true,
                'msg' => 'Sukses, Login Berhasil',
                'data' => auth()->user()
            ]);
            // return redirect()->intended('dashboard');
        }
        return response()->json(['status' => false, 'valid'=>true, 'msg' => 'Tidak valid email atau password salah.', 'data' => []]);
        // return back()->with('login_error', 'Error Periksa Kembali Email Atau Password');

    }
    public function registrasi(Request $request){
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'no_telp' => 'required|min:11|numeric',
            'email' => 'required|email:dns|unique:users,email',
            'password' => 'min:6',
            'repeat_password' => 'min:6|same:password',
            'tanggal_lahir'=>'required',
            'jenis_kelamin'=>'required'
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'valid'=>false, 'msg' => 'Gagal, veriksa kembali form isian', 'data' => $validate->errors()]);
        }

        $data = [
            'name' => $request->nama,
            'no_telp' =>  $request->no_telp,
            'email' =>  $request->email,
            'tanggal_lahir' =>  $request->tanggal_lahir,
            'jenis_kelamin' =>  $request->jenis_kelamin,
            'role_id' => 2,
            'password' => bcrypt($request->password),
            'privacy_term'=>$request->privacy_policy
        ];

        $response =  User::create($data);
        session()->flash('success', 'Berhasil Registrasi, Selamat Berbelanja.');
        return response()->json([
            'status'=>true,
            'valid'=>true,
            'msg' => 'Berhasil Registrasi, Selamat Berbelanja.',
            'data'=> $response
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerate();
        return redirect()->route('login');
    }

    public function change_password(Request $request){
        $validate = Validator::make($request->all(),[
            'old_password' => ['required', new MatchOldPassword],
            'new_password'=>'min:6',
            'repeat_password'=>'min:6|same:new_password'
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false,  'msg' => 'Gagal, veriksa kembali form isian', 'data' => $validate->errors()]);
        }
        $data = ['password'=> bcrypt($request->new_password)];

        $check = $this->user->find(auth()->user()->id)->update($data);
        if($check){
            return response()->json([
                'status' => true,
                'msg' => 'Berhasil, Ubah Password',
                'data' => auth()->user()
            ]);
        }
    }
}
