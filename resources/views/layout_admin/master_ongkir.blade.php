@extends('template_admin.admin')

@section('title', 'Berat Ongkir')
@section('container')
<div class="row mt-3">
    <!-- Datatables -->
    <div class="col-lg-12">
        <div class="from-group mb-2">
            <a class="btn btn-primary btn-berat-ongkir" value="new" href="javascript:void(0)"><i
                    class="fa fa-plus-circle"></i> Tambah</a>
        </div>
        {{-- <div class="from-group mb-2">
            <button class="btn btn-primary" onclick="tambah()" value="new">Tambah Data</button>
        </div> --}}
        <div class="card mb-4">
            <div class="card-header">Penentuan Berat Ongkir</div>
            {{-- <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
            </div> --}}
            <div class="table-responsive p-3">
                <table class="table align-items-center table-flush" id="dataTableBerat" style="width: 100">
                    <thead class="thead-light">
                        <tr>
                            <th style="width: 10%">No</th>
                            <th style="width: 20%">Dari item (pcs)</th>
                            <th style="width: 20%">Sampai dengan item (pcs)</th>
                            <th style="width: 20%">Berat Kg</th>
                            <th style="width: 20%">Berat Gram</th>
                            <th style="width: 10%">Opsi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal Pembayaran -->
<div id="modal_berat" class="modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="overflow: auto !important;">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content change-address">
            <div class="modal-header">
                <h5 class="modal-title-berat-ongkir" id="exampleModalLabel">Tambah Berat Ongkir</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="" id="form-berat-ongkir">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Dari item (pcs)</label>
                        <input type="hidden" class="form-control" id="id_berat" name="id_berat">
                        <input type="number" class="form-control" id="pcs_dari" name="pcs_dari" min="0">
                    </div>
                    <div class="form-group">
                        <label for="">Sampai dengan item (pcs)</label>
                        <input type="number" class="form-control" id="pcs_sampai"
                            name="pcs_sampai" min="0">
                    </div>
                    <div class="form-group">
                        <label for="">Berat Kg <br> <small class="text-danger">gunakan titik jika memiliki koma misal (1.5)</small></label>
                        <input type="number" class="form-control" id="weight_kg"
                            name="weight_kg" min="0" value="0">
                    </div>
                    <div class="form-group">
                        <label for="">Berat Gram</label>
                        <input type="text" class="form-control" id="weight_gram"
                            name="weight_gram" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-berat-ongkir" value="0">Simpan</button>
                    <button type="button" class="btn btn-secondary close-address"
                        data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>

    let dataTableBerat;
        $(document).ready(function() {
            // norek
            dataTableBerat = $('#dataTableBerat').DataTable({
                prossecing: true,
                serverSide: true,
                ajax: "{{ route('ongkir.index') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'pcs_dari',
                        name: 'pcs_dari'
                    },
                    {
                        data: 'pcs_sampai',
                        name: 'pcs_sampai'
                    },
                    {
                        data: 'weight_kg',
                        name: 'weight_kg'
                    },
                    {
                        data: 'weight_gram',
                        name: 'weight_gram'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });

            $(document).on('click', '.hapus-weight', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).parent().closest('.address').remove();
                        let url =
                            "{{ route('ongkir.destroy', ['ongkir' => ':id']) }}";
                        url = url.replace(":id", id);
                        $.ajax({
                            method: 'DELETE',
                            url: url,
                            success: function(data) {
                                if (data.status == true) {
                                    toastr.success(data.msg);
                                    dataTableBerat.ajax.reload();
                                } else {
                                    toastr.error('Gagal, Hapus data.');
                                }
                            }
                        });
                    }
                });
            });

            $(document).on('click', '.btn-berat-ongkir', function() {
                $('#modal_berat').modal('show');
                $('#form-berat-ongkir')[0].reset();
                $('.modal-title-berat-ongkir').html('Tambah Berat Ongkir');
                $('.add-berat-ongkir').val(0);
            });
            $(document).on('click', '.edit-weight', function() {
                $('#modal_berat').modal('show');
                $('.modal-title-berat-ongkir').html('Edit Berat Ongkir');
                $('.add-berat-ongkir').val(1);
                let id = $(this).data('id');
                let url = "{{ route('ongkir.edit', ['ongkir' => ':id']) }}";
                url = url.replace(":id", id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function(data) {
                        if (data.status == true) {
                            toastr.success(data.msg);
                            $('#modal_berat').modal('show');
                            $('#id_berat').val(data.data.id);
                            $('#pcs_dari').val(data.data.pcs_dari);
                            $('#pcs_sampai').val(data.data.pcs_sampai);
                            $('#weight_kg').val(data.data.weight_kg);
                            $('#weight_gram').val(data.data.weight_gram);
                        } else {
                            toastr.warning(data.msg);
                        }
                    }
                });
            });

            $(document).on('input','#weight_kg', function(){
                let $berat = $(this).val();
                $berat =   $berat != '' ? $berat : 0;
                //alert($berat);
                $berat = parseFloat($berat);
                $berat *= 1000;
                $('#weight_gram').val($berat);
            });

            $(document).on('click', '.add-berat-ongkir', function(e) {
                e.preventDefault();
                let data = $('#form-berat-ongkir').serializeArray();
                let check = $('.add-berat-ongkir').val();
                let url;
                let method;
                if (check == 0) {
                    url = "{{ route('ongkir.store') }}";
                } else if (check == 1) {
                    let id = $('#id_berat').val();
                    url = "{{ route('ongkir.update', ['ongkir' => ':id']) }}";
                    url = url.replace(":id", id);
                    data = data.concat({
                        name: "_method",
                        value: "PUT"
                    });
                }

                $.ajax({
                    url: url,
                    data: data,
                    method: 'POST',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status) {
                            $(".text-error-pcs_dari").remove();
                            $(".text-error-pcs_sampai").remove();
                            $(".text-error-weight_kg").remove();
                            $(".text-error-weight_gram").remove();
                            toastr.success(data.msg);
                            $('#form-berat-ongkir')[0].reset();
                            $('#modal_berat').modal('hide');
                            dataTableBerat.ajax.reload();

                        } else {
                            $(".text-error-pcs_dari").remove();
                            $(".text-error-pcs_sampai").remove();
                            $(".text-error-weight_kg").remove();
                            $(".text-error-weight_gram").remove();
                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<small class="text-error-' + field_name +
                                    ' text-danger ">' + error + '</small>')
                            });
                            toastr.warning(data.msg);
                        }
                    }
                });
            });
        });
</script>
@endsection
