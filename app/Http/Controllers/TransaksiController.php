<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaksi;
use App\Models\ProductStock;
use App\Models\TrackingTransaksi;
use App\Models\Transaksi;
use App\Models\TransaksiEvoucher;
use App\Models\VoucherKode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use PDF;
use Svg\Tag\Rect;

class TransaksiController extends Controller
{
    protected $transaksi;
    protected $transaksi_evoucher;
    protected $detail_transaksi;
    public function __construct(Transaksi $transaksi, TransaksiEvoucher $transaksi_evoucher, DetailTransaksi $detail_transaksi)
    {

        $this->transaksi = $transaksi;
        $this->transaksi_evoucher = $transaksi_evoucher;
        $this->detail_transaksi = $detail_transaksi;
    }

    public function index(Request $request)
    {
        $status = explode('.', $request->status);
        $data = $this->transaksi->withSum('detail_transaksi', 'quantity')->whereIn('status', $status)->orderBy('id','DESC')->get();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('nama', function ($row) {
                    $check = $row->user_id !== null ? '<span class="badge badge-primary">User Terdaftar</span>' : '';
                    return $row->nama_depan . ' ' . $row->nama_belakang . ' ' . $check;
                })
                ->addColumn('alamat', function ($row) {
                    return 'Kota ' . $row->kota . ' Provinsi ' . $row->provins . ' No.Pos ' . $row->no_pos . ' (' . $row->alamat_lengkap . ')';
                })
                ->addColumn('status', function ($row) {
                    $status = '';



                    if ($row->status == 'rejected') {
                        $status = '<span class="badge badge-danger">' . $row->status . '</span>';
                    } else if ($row->status == 'selesai' || $row->status == 'baru') {
                        $status = '<span class="badge badge-primary">' . $row->status . '</span>';
                    } else if ($row->status == 'terkonfirmasi') {
                        $status = '<span class="badge badge-info">' . $row->status . '</span>';
                    } else if ($row->status == 'tracking') {
                        $status = '<span class="badge badge-warning">' . $row->status . '</span>';
                    }

                    return $status;
                })
                ->addColumn('kontak', function ($row) {
                    return 'Email: ' . $row->email . '<br>' . 'Telp/Wa: ' . $row->no_telp;
                })
                ->addColumn('total_bayar', function ($row) {
                    return  'Total Bayar: ' . $row->total_bayar . '<br>Ongkir: ' . $row->ongkir . '-' . $row->kurir;
                })
                ->addColumn('tanggal', function ($row) {
                    return Carbon::parse($row->created_at)->diffForHumans() . '<br>(' . Carbon::parse($row->created_at)->format('Y M d H:i') . ')';
                })
                ->addColumn('bukti', function ($row) {
                    $gambar = '-';
                    if($row->bukti_transfer != NULL){
                    $gambar = '<div class="row">
                                <div class="col-md-4">
                                    <img src="' . asset('assets_frontend/uploads/bukti_transfer/' . $row->bukti_transfer) . '" style="width:120px; height: 120px;"/>
                                </div>
                            </div>
                            <a href="' . asset('assets_frontend/uploads/bukti_transfer/' . $row->bukti_transfer) . '" download ><i class="fa fa-download"></i></a>
                            ';
                    }

                    return $gambar;
                })
                ->addColumn('action', function ($row) {
                    // if($row->status == 'rejected'){
                    $btn = ' <a href="' . route('admin_transaksi.detail_transaksi', ['id' => $row->id, 'id_transaksi' => $row->id_pemesanan,'modal'=>true]) . '" class="mt-1 btn btn-primary btn-sm"  data-id="' . $row->id . '"><i class="fa fa-solid fa-eye"></i></a>';
                    $btn .= ' <a title="dowload invoice" href="' . route('admin_transaksi.detail_transaksi', ['id' => $row->id, 'id_transaksi' => $row->id_pemesanan]) . '" class="mt-1 btn btn-success btn-sm"  data-id="' . $row->id . '"><i class="fa fa-solid fa-file-pdf"></i></a>';
                    // $btn .= ' <a href="javascript:void(0)" class="mt-1 btn btn-primary btn-sm show-detail" data-id="' . $row->id . '"  data-id_transaksi="' . $row->id_pemesanan . '"><i class="fa fa-solid fa-eye"></i></a>';
                    // $btn .= ' <a href="javascript:void(0)" class="mt-1 btn btn-warning btn-sm check-id"  data-id="' . $row->id . '"  data-id_transaksi="' . $row->id_transaksi . '"><i class="fa fa-solid fa-edit"></i></a>';
                    // $btn .= ' <a href="javascript:void(0)" class="mt-1 btn btn-danger btn-sm hapus-weight"  data-id="' . $row->id . '"><i class="fa fa-solid fa-trash"></i></a>';
                    // }

                    if($row->status == 'baru' ){
                            $btn .= ' <a href="javascript:void(0)" title="ubah status transaksi" class="mt-1 btn btn-warning btn-sm edit-status" data-id="' . $row->id . '"  data-id_transaksi="' . $row->id_pemesanan . '"><i class="fa fa-solid fa-edit"></i></a>';
                    }
                    if($row->bukti_transfer == NULL){
                        $btn .= ' <a href="javascript:void(0)" title="upload bukti transfer" class="mt-1 btn btn-primary btn-sm upload-bukti" data-id="' . $row->id . '"  data-id_transaksi="' . $row->id_pemesanan . '"><i class="fa fa-solid fa-plus"></i></a>';
                    }
                    return $btn;
                })
                ->rawColumns(['action', 'nama', 'alamat', 'status', 'kontak', 'total_bayar', 'tanggal','bukti'])
                ->make(true);
        }
    }
    public function store(Request $request)
    {
        // echo json_encode($request->evoucher_payment);die;
        $cart = is_null(session()->get('chart_checkout')) ? [] : session()->get('chart_checkout');
        $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
        if (count($cart) == 0) {
            return response()->json([
                'status' => False,
                'status_product' => False,
                'msg' => 'Transaksi gagal keranjang masih kosong'
            ]);
        }
        $validate = Validator::make($request->all(), [
            'nama_depan' => 'required',
            'nama_belakang' => 'required',
            'no_telp' => 'required',
            'email' => 'required|email:dns',
            'provinsi' => 'required|not_in:0',
            'kota' => 'required|not_in:0',
            'ongkir_payment' => 'required|not_in:0',
            'no_pos' => 'required',
            'alamat_lengkap' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false,   'status_product' => True, 'msg' => 'Gagal, periksa kembali form isian.', 'data' => $validate->errors()]);
        }

        $provinsi = explode('|',  $request->provinsi);
        $kota = explode('|',  $request->kota);
        $kurir =  explode('|', $request->kurir);
        $check = DB::select('SELECT Max(id) AS maximum_id  FROM   transaksis limit 1');

        $nama_kurir = $kurir[0] . ' ' . $kurir[1] . ' (' . $kurir[2] . ')';
        $i = collect($check)->first()->maximum_id + 1;
        $data = [
            "id_pemesanan" => "BDST" .  time() . mt_rand(10, 99) . $i,
            "user_id" =>   !is_null(auth()->user()) ? auth()->user()->id : NULL,
            "nama_depan" => $request->nama_depan,
            "nama_belakang" => $request->nama_belakang,
            "no_telp" => $request->no_telp,
            "email" => $request->email,
            "provinsi" => $provinsi[1],
            "id_provinsi" => $provinsi[0],
            "kota" => $kota[1],
            "id_kota" => $kota[0],
            "no_pos" => $request->no_pos,
            "alamat_lengkap" => $request->alamat_lengkap,
            "catatan" => $request->catatan,
            "kurir" => $nama_kurir,
            "ongkir" => $request->ongkir_payment,
            "total_bayar" => $request->total_bayar_payment,
        ];
        $transaksi = $this->transaksi->create($data);
        $data_transaksi['id_pemesanan'] = $transaksi->id_pemesanan;
        $data_transaksi['id'] = $transaksi->id;
        $data_transaksi['pemesan'] = $transaksi->nama_depan . ' ' . $transaksi->nama_belakang;
        // insert pocher
        if (count($epromo) != 0) {
            // $evromotransaksi = [];
            foreach ($request->evoucher_payment as $v) {
                $vc = explode('|', $v);
                // array_push($evromotransaksi, [
                //     'transaksi_id'=>'1',
                //     'voucher_kode_id'=> $vc[0],
                //     'evocher'=>$vc[1],
                // ]);

                $this->transaksi_evoucher->create([
                    'transaksi_id' => $transaksi->id,
                    'voucher_kode_id' => $vc[0],
                    'evocher' => $vc[1],
                ]);
            }
            // echo json_encode($evromotransaksi);
            // $this->transaksi_evoucher->insert($evromotransaksi);
        }

        foreach ($cart as $c) {
            $data_cart = [
                'transaksi_id' => $transaksi->id,
                'product_id' => $c['id_produk'],
                'stock_id' => $c['id_stok'],
                'master_size_id' => $c['id_size'],
                'nama_produk' => $c['nama_produk'],
                'diskon' => $c['diskon'],
                'quantity' => $c['quantity'],
                'size' => $c['size'],
                'sub_total' => $c['harga'],
                'harga_diskon' => $c['harga_diskon'],
                'total_item' => $c['total_harga'],
            ];
            $this->detail_transaksi->create($data_cart);
        }
        if (isset($epromo)) {
            $id_promo = array_column($epromo,'id');
            $idx =1;
            foreach($id_promo as $id){
                $v = VoucherKode::find($id);
                $kuota = (int)$v->kuota- $idx;
                $v->fill(['kuota'=> $kuota])->save();
            }
            unset($epromo);
            $epromo = session()->put('e-promo', []);
        }
        if (isset($cart)) {
            unset($cart);
            $cart = session()->put('chart_checkout', []);
        }
        return response()->json(['status' => True, 'status_product' => True, 'msg' => 'Pesanan berhasil dibuat segera konfirmasi pesanan anda', 'data' => $data_transaksi]);
    }

    public function konfirmasi_pesanan(Request $request)
    {
        // dd($request->all());
        $data =   $this->transaksi->where('id', '=', $request->id)->where('id_pemesanan', '=', $request->id_pemesanan)->first();
        // return response()->json($data);
        if( !isset($data)){
            return abort(404,'Page not found');
        }

        return view('frontend.product_checkout.konfirmasi_pesanan');
    }


    // Transaksi reject
    public function rejected_view()
    {
        $status = 'rejected';
        $title = 'Transaksi rejected (spam)';
        return view('layout_admin.transaksi.rejected_selesai_transaksi', compact('status', 'title'));
    }
    public function selesai_view()
    {
        $status = 'selesai';
        $title = 'Transaksi selesai';
        return view('layout_admin.transaksi.rejected_selesai_transaksi', compact('status', 'title'));
    }

    public function all_transaksi()
    {
        $status = 'baru.terkonfirmasi.tracking.selesai.rejected';
        return view('layout_admin.transaksi.all_transaksi', compact('status'));
    }


    public function checkout_whatsapp(Request $request)
    {
        $data = $this->transaksi->withSum('detail_transaksi', 'quantity')->where('id', '=', $request->id)->where('id_pemesanan', '=', $request->id_pemesanan)->with('detail_transaksi')->with('transkaksi_evoucher')->first();

        $tampung = [];
        $text = '';


        $textVoucher = '';
        foreach ($data['transkaksi_evoucher'] as $i => $v) {
            $check = $v->voucher_kode->type_voucher == 'diskon' ? ' (' . $v->voucher_kode->promo . '%) ' : ' ';
            $textVoucher .= $v->voucher_kode->nama_promo . $check . $v->evocher . ', ';
        }

        $i = 1;
        $sub_harga = [];
        foreach ($data->detail_transaksi as $c) {
            // $harga = $c->diskon == 0 ||$c->diskon == null ? 'Rp.' . number_format($c->sub_total, 0, ',', '.') : ' (disc '. $c->diskon.'%) '.  'Rp.' . number_format($c->harga_diskon, 0, ',', '.');
            $disc = $c->diskon == 0 || $c->diskon == null ?  '' : ' (disc ' . $c->diskon . '%)';
            $hargaDisc = $c->diskon == 0 || $c->diskon == null ?  'Rp.' . number_format($c->sub_total, 0, ',', '.') : '~Rp.' . number_format($c->sub_total, 0, ',', '.') . '~ (Rp.' . number_format($c->harga_diskon, 0, ',', '.') . ')';
            $hargaSub = $c->diskon == 0 || $c->diskon == null ?  'Rp.' . number_format($c->sub_total, 0, ',', '.') . 'x' . $c->quantity :  'Rp.' . number_format($c->harga_diskon, 0, ',', '.') . 'x' . $c->quantity;
            //    $text = '%0A%2APesanan '.$i++.'%2A: '. $c->nama_produk .' Ukuran '. $c->size. ' ' . $harga .' x' . $c->quantity . ' Rp.' .number_format($c->total_item, 0, ',', '.');
            // foreach($c['transkaksi_evoucher'] as $ev){
            //     dd($ev);
            // }
            $text = '%0A%2APesanan ' . $i++ . '%2A: ' . $c->nama_produk . '%0AQTY: ' . $c->size . ' x ' . $c->quantity . '%0AHarga ' . $disc  . ':' .  $hargaDisc . '%0ASub Harga: ' . $hargaSub . '%0ATotal Harga: ' . ' Rp.' . number_format($c->total_item, 0, ',', '.');
            array_push($sub_harga, $c->total_item);
            $tampung[] = $text;
        }
        return response()->json(
            [
                'status' => True,
                'sub_total' => 'Rp.' . number_format(array_sum($sub_harga), 0, ',', '.'),
                'evoucher' => $textVoucher,
                'pesanan' => $tampung,
                'data' => $data
            ]
        );
    }

    public function detail_transaksi(Request $request)
    {
        // echo $request->id . $request->id_transaksi;

        $data =   $this->transaksi->withSum('detail_transaksi', 'quantity')->with('detail_transaksi')->with('transkaksi_evoucher')->with('tracking_transaksi')->where('id', '=', $request->id)->where('id_pemesanan', '=', $request->id_transaksi)->first();
        // return response()->json($data);
        if( !isset($data)){
            return abort(404,'Page not found');
        }

        if(isset($request->modal)){
            return view('layout_admin.transaksi.detail_transaksi', compact('data'))->render();
        }
        // $arr = [

        //     'title' => 'Welcome to ItSolutionStuff.com',

        //     'date' => date('m/d/Y')

        // ];
        $check = true;
        $pdf = PDF::loadView('layout_admin.transaksi.modal_detail_transaksi', compact('data', 'check'))->setPaper('a4', 'portrait');

        return $pdf->download('Invoice-'. $data->nama_depan .'-'. $data->nama_belakang.'-'. $data->created_at.'.pdf');
        // return view('layout_admin.transaksi.detail_transaksi', compact('data'))->render();
    }

    public function update_transaksi(Request $request){

        $check = $this->transaksi->with('detail_transaksi')->with('tracking_transaksi')->find($request->id);
        if($request->cek == 'upload'){
            $imgName = null;
            if ($request->hasFile('bukti_transfer') && $request->bukti_transfer != '') {
                $image = $request->file('bukti_transfer');
                $imgName = $image->hashName();
                $image->move('assets_frontend/uploads/bukti_transfer', $imgName);
            }
            if ($check) {
                if($imgName == NULL){
                    return response()->json(
                        [
                            'status' => false,
                            'msg' => 'Gagal Upload Bukti Transfer, file masih kosong.',
                        ]
                    );
                }

                $check->fill(['bukti_transfer'=> $imgName])->save();
                // $tracking = [
                //     [
                //         'title'=> 'Pesanan baru',
                //         'deskripsi'=> 'Pesanan baru dibuat.',
                //         'tanggal_tracking'=> $check->created_at,
                //         'transaksi_id'=> $check->id,
                //         'created_at'=> date('Y-m-d H:i:s')
                //     ],
                //     [
                //         'title'=> 'Bukti pembayaran',
                //         'deskripsi'=> 'Upload bukti pembayaran.',
                //         'tanggal_tracking'=> date('Y-m-d H:i:s'),
                //         'transaksi_id'=> $check->id,
                //         'created_at'=> date('Y-m-d H:i:s')
                //     ]

                // ];

                // TrackingTransaksi::insert($tracking);
                if(count($check->tracking_transaksi)  == 0){
                    $track['title']= 'Pesanan baru';
                    $track['deskripsi']= 'Pesanan baru dibuat.';
                    $track['tanggal_tracking']= $check->created_at;
                    $track['transaksi_id']= $check->id;
                        TrackingTransaksi::create($track);
                }
                $dataTracking['title']= 'Bukti pembayaran';
                $dataTracking['deskripsi']= 'Upload bukti pembayaran.';
                $dataTracking['tanggal_tracking']= date('Y-m-d H:i:s');
                $dataTracking['transaksi_id']=$check->id;
                    TrackingTransaksi::create($dataTracking);
                return response()->json(
                    [
                        'status' => true,
                        'msg' => 'Berhasil Upload Bukti Transfer.',
                    ]
                );
            }
            return;
        }
        $status_admin = true;
        if ($check) {
            $check->fill(['status'=> $request->status])->save();
            if(count($check->tracking_transaksi)  == 0){
                $dataTracking['title']= 'Pesanan baru';
                $dataTracking['deskripsi']= 'Pesanan baru dibuat.';
                $dataTracking['tanggal_tracking']= $check->created_at;
                $dataTracking['transaksi_id']= $check->id;
                    TrackingTransaksi::create($dataTracking);
            }
            if($request->status == 'rejected'){
                $dataTracking['title']= 'Pesanan rejected / ditolak';
                $dataTracking['deskripsi']= 'Pesanan ditolak atau tidak dapat dilanjutkan.';
                $dataTracking['tanggal_tracking']= date('Y-m-d H:i:s');
                $dataTracking['transaksi_id']= $check->id;
                    TrackingTransaksi::create($dataTracking);
                    $data=   $this->transaksi->with('tracking_transaksi')->find($request->id);
                return response()->json(
                    [
                        'status' => true,
                        'msg' => 'Transaksi Ditolak / Rejected.',
                        'html' => view('layout_admin.transaksi.tracking', compact('data','status_admin'))->render(),
                        'data' => $data
                    ]
                );
            }else{
                $stock_id = array_column($check->detail_transaksi->toArray(),'quantity','stock_id');
                foreach($stock_id as $key => $i){
                    $prod =  ProductStock::find($key);
                    $qty_update = $prod->qty - $i;
                    $prod->fill(['qty'=> $qty_update])->save();
                }
                $dataTracking['title']= 'Pesanan Terkonfirmasi';
                $dataTracking['deskripsi']= 'Pesanan telah terkonfirmasi.';
                $dataTracking['tanggal_tracking']= date('Y-m-d H:i:s');
                $dataTracking['transaksi_id']= $check->id;
                TrackingTransaksi::create($dataTracking);
                $data=  $this->transaksi->with('tracking_transaksi')->find($request->id);
                return response()->json(
                    [
                        'status' => true,
                        'msg' => 'Transaksi Terkonfirmasi.',
                        'html' => view('layout_admin.transaksi.tracking', compact('data','status_admin'))->render(),
                        'data' => $data
                    ]
                );
            }

            echo json_encode($dataTracking);
        }
    }

    public function update_tracking(Request $request){

        $validate = Validator::make($request->all(), [
            'status' =>  'required',
            'deskripsi' =>  'required',
            'title' =>  'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, form wajib diisi.', 'data' => $validate->errors()]);
        }

        $check = $this->transaksi->with('tracking_transaksi')->find($request->id);
        // echo json_encode($request->status);die;
        if($check->status != 'selesai' || $check != 'tracking'){
            $check->fill(['status'=> 'tracking'])->save();
        }
        $dataTracking['title']= $request->title;
        $dataTracking['deskripsi']= $request->deskripsi;
        $dataTracking['tanggal_tracking']= date('Y-m-d H:i:s');
        $dataTracking['transaksi_id']= $check->id;
        TrackingTransaksi::create($dataTracking);
        $msg =   'Tracking berhasil di buat';
        if($request->status == 'selesai'){
            // echo $request->status;
            $check->fill(['status'=> 'selesai'])->save();
            $msg =   'Pesanan Telah Selesai';
        }
        // echo $msg;die;
        $status_admin = true;
        $data =  $this->transaksi->find($request->id);
        return response()->json(
            [
                'status' => true,
                'msg' => $msg,
                'html' => view('layout_admin.transaksi.tracking', compact('data','status_admin'))->render(),
                'data' => $data
            ]
        );
    }

    public function check_pesanan(){
        return view('frontend.product_checkout.checking_tracking');
    }
    public function post_check_pesanan(Request $request){
        // echo $request->search;die;
        $data = $this->transaksi->with('detail_transaksi')->withSum('detail_transaksi', 'quantity')->where('id_pemesanan', '=', $request->search)->with('detail_transaksi')->with('transkaksi_evoucher')->first();
        $item_product = [];
        $preview_produks_item = 0;
        if(!is_null($data)){
            $id_prod = implode(',',array_column($data->detail_transaksi->toArray(), 'product_id'));
            $item_product = $data->status == 'selesai' ? DB::Select('SELECT   ts.id,ts.user_id, ts.id_pemesanan, CONCAT(ts.nama_depan, " ", ts.nama_belakang) nama, ts.created_at tgl_beli, ts.email,ts.no_telp, dt.* ,(SELECT COUNT(*) FROM preview_produks WHERE  product_id = dt.product_id AND id_pemesanan = ts.id_pemesanan) preview_produks_item, (SELECT image_name FROM `product_image` WHERE produk_id = dt.product_id LIMIT 1) img_name
                FROM transaksis ts
                JOIN  `detail_transaksis` dt on ts.id =dt.transaksi_id
                WHERE ts.status ="selesai" and ts.id_pemesanan = "'. $data->id_pemesanan .'" and dt.product_id in ('.$id_prod.')') : [];
            $item_product = collect($item_product);
            $preview_produks_item = array_sum(array_column($item_product->toArray(),'preview_produks_item'));
        }
        // return response()->json($item_product);
        // die;
    //    echo json_encode($data);die;
        $search =  $request->search;
        return response()->json([
            'status' => true,
            'html' => view('layout_admin.transaksi.tracking', compact('data','search','item_product','preview_produks_item'))->render()
        ]);
    }


    public function update_transaksi_bukti_pembayaran(Request $request){
        $validate = Validator::make($request->all(), [
            'bukti_pembayaran' =>  'required|image|mimes:jpeg,png,jpg,pdf|max:20000',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, periksa kembali form isian.', 'data' => $validate->errors()]);
        }

        $check = $this->transaksi->with('tracking_transaksi')->find($request->id);

        $imgName = null;
        if ($request->hasFile('bukti_pembayaran') && $request->bukti_pembayaran != '') {
            $image = $request->file('bukti_pembayaran');
            $imgName = $image->hashName();
            $image->move('assets_frontend/uploads/bukti_transfer', $imgName);
        }

        if ($check) {
            $check->fill(['bukti_transfer'=> $imgName])->save();
            // $check->fill(['bukti_transfer'=> $imgName])->save();
            // $dataTracking['title']= 'Bukti pembayaran';
            // $dataTracking['deskripsi']= 'Upload bukti pembayaran.';
            // $dataTracking['tanggal_tracking']= date('Y-m-d H:i:s');
            // $dataTracking['transaksi_id']= $check->id;
            $tracking = [
                [
                    'title'=> 'Pesanan baru',
                    'deskripsi'=> 'Pesanan baru dibuat.',
                    'tanggal_tracking'=> $check->created_at,
                    'transaksi_id'=> $check->id,
                    'created_at'=> date('Y-m-d H:i:s')
                ],
                [
                    'title'=> 'Bukti pembayaran',
                    'deskripsi'=> 'Upload bukti pembayaran.',
                    'tanggal_tracking'=> date('Y-m-d H:i:s'),
                    'transaksi_id'=> $check->id,
                    'created_at'=> date('Y-m-d H:i:s')
                ]

            ];
            // echo json_encode($tracking);die;
            TrackingTransaksi::insert($tracking);
            return response()->json(
                [
                    'status' => true,
                    'msg' => 'Berhasil Upload Bukti Transfer.',
                    'data'=> '<span class="text-success title-address" >
                                Terima kasih, bukti pembayaran sedang di review pantau terus pesanan anda di menu Check Order untuk melihat update pembaruan dari pesanan kamu. <small>
                            </span>'
                ]
            );
        }
    }
}
