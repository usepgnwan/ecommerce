@extends('template')

@section('judul', $title)

@section('content')

    <div class="row">
        <div class="col-md-12 mt-3">
            <div class="table-responsive">
                <table class="table table-hover" id="tblTest">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Add To Chart</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">1</th>
                            <td>Laptop Hp Gaming</td>
                            <form action="{{ route('chart.store') }}" method="post">
                                @csrf
                                <td>
                                    <input type="number" value="0" name="quantity" class="form-control chart-input">
                                    <input type="hidden" value="1" name="id" class="form-control id-input">
                                    <input type="hidden" name="title" value="Laptop Hp Gaming"
                                        class="form-control title-input">
                                </td>
                                <td> <button type="submit">haha</button></td>
                            </form>
                        </tr>
                        <tr>
                            <th scope="row">2</th>
                            <td>Laptop Asus Predator</td>
                            <td>
                                <input type="number" value="0" class="form-control chart-input">
                                <input type="hidden" value="2" class="form-control id-input">
                                <input type="hidden" value="Laptop Asus Predator" class="form-control title-input">
                            </td>
                            <td><input type="button" class="btn btn-success" name="" id="" value="add"></td>
                        </tr>
                        <tr>
                            <th scope="row">3</th>
                            <td>Laptop Lenovo</td>
                            <td>
                                <input type="number" value="0" class="form-control chart-input">
                                <input type="hidden" value="3" class="form-control id-input">
                                <input type="hidden" value="Laptop Lenovo" class="form-control title-input">
                            </td>
                            <td><input type="button" class="btn btn-success" name="" id="" value="add"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                    // 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        })
        $('#tblTest input[type="button"]').click(function() {
            let quantity = $(this).closest('tr').find(".chart-input").val();
            let id = $(this).closest('tr').find(".id-input").val();
            let title = $(this).closest('tr').find(".title-input").val();
            // alert(quantity + ' id ' + id + ' Title ' + title)

            $.ajax({
                url: "{{ route('chart.store') }}",
                type: 'POST',
                data: {
                    id,
                    quantity,
                    title,
                },
                dataType: 'json',
                success: function(ok) {
                    if (ok.status == true) {
                        window.location.href =
                            "{{ route('chart.wilshit') }}";
                    }
                }
            })
        });
    </script>
@endsection
