<a style="background-color: #d2b19b !important;" class="sidebar-brand d-flex align-items-center justify-content-center"
    href="{{ url('/') }}">
    <div class="sidebar-brand-icon">
        <img src="{{ asset('file/img/logo/logo-distro.jpeg') }}" />
    </div>
    <div class="sidebar-brand-text mx-3">BillDistro</div>
</a>

<hr class="sidebar-divider my-0" />
<li class="nav-item  ">
    <a class="nav-link" href="{{route('admin.index')}}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
</li>
<hr class="sidebar-divider" />
<div class="sidebar-heading">Features</div>
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap" aria-expanded="true"
        aria-controls="collapseBootstrap">
        <i class="far fa-fw fa-window-maximize"></i>
        <span>Data Master</span>
    </a>
    <div id="collapseBootstrap" class="collapse" aria-labelledby="headingBootstrap"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data Master</h6>
            {{-- <a class="collapse-item" href="">Role</a> --}}
            <a class="collapse-item" href="{{ route('master.user_index') }}">User</a>
            <a class="collapse-item" href="{{ route('master_size') }}">Size</a>
            <a class="collapse-item" href="{{ route('master_category') }}">Kategori</a>
        </div>
    </div>
</li>
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap1" aria-expanded="true"
        aria-controls="collapseBootstrap1">
        <i class="far fa-fw fa-window-maximize"></i>
        <span>Data Produk</span>
    </a>
    <div id="collapseBootstrap1" class="collapse" aria-labelledby="headingBootstrap"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Produk</h6>
            <a class="collapse-item" href="{{ route('product.index') }}">Daftar Produk</a>
            <a class="collapse-item" href="{{ route('product.create') }}">Tambah Produk</a>
        </div>
    </div>
</li>
<li class="nav-item">
    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseBootstrap2" aria-expanded="true"
        aria-controls="collapseBootstrap">
        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
        <span>Data Transaksi</span>
    </a>
    <div id="collapseBootstrap2" class="collapse" aria-labelledby="headingBootstrap"
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Data Transaksi</h6>
            {{-- <a class="collapse-item" href="">Role</a> --}}
            <a class="collapse-item" href="{{ route('metode_pembayaran.index') }}">Metode Pembayaran</a>
            <a class="collapse-item" href="{{ route('ongkir.index') }}">Berat Ongkir</a>
            <a class="collapse-item" href="{{ route('banner_promo.index') }}">E-voucher / Promo</a>
            <a class="collapse-item" href="{{ route('admin_transaksi.all') }}">Transaksi</a>
            <a class="collapse-item" href="{{ route('admin_transaksi.selesai') }}">Transaksi Selesai</a>
            <a class="collapse-item" href="{{ route('admin_transaksi.rejected') }}">Transaksi Rejected</a>
            <a class="collapse-item" href="{{ route('penilain.index') }}">All Review Produk</a>
        </div>
    </div>
</li>
<li class="nav-item">
    <a class="nav-link collapsed" href="{{ route('contact_us.index') }}">
        <i class="fas fa-envelope fa-fw  mr-2"></i>
        <span>Contact Us / Pesan</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link collapsed" href="{{ route('pengaturan_toko') }}">
        <i class="fas fa-cogs fa-sm fa-fw mr-2"></i>
        <span>Pengaturan Toko</span>
    </a>
</li>
<li class="nav-item">
    <a class="nav-link collapsed" href="{{ route('faq.index') }}">
        <i class="fas fa-question fa-sm fa-fw mr-2"></i>
        <span>FAQ</span>
    </a>
</li>
