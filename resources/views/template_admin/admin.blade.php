<!DOCTYPE html>
<html lang="en">

<head>

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link href="{{ asset('file/img/logo/logo2.png') }}" rel="icon" />
    <link href="{{ asset('assets_frontend/images/logo/logo-distro.jpeg') }}" rel="icon">
    <title>{{ config('app.name') }} | @yield('title') </title>
    <link href="{{ asset('file/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('file/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('file/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('file/css/ruang-admin.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('file/select-type/css/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css" />

    <script src="{{ asset('file/vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('file/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('file/vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('file/vendor/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('file/vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('file/select-type/js/bootstrap-select.min.js') }}"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('assets_frontend/toast/toastr.min.css') }}">
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"
        integrity="sha512-AA1Bzp5Q0K1KanKKmvN/4d3IRKVlv9PYgwFPvm32nPO6QS8yH1HO7LbgB1pgiOxPtfeg5zEn2ba64MUcqJx6CA=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script> --}}

    {{-- <script src="{{ asset('file/vendor/sweatalert/sweetalert.min.js') }}"></script>
    @include('sweetalert::alert') --}}
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    {{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.1/tinymce.min.js"></script> --}}
    {{-- <script src="https://cdn.ckeditor.com/ckeditor5/33.0.0/classic/ckeditor.js"></script> --}}

</head>

<body id="page-top">
    <div id="wrapper">
        <!-- Sidebar -->
        <ul class="navbar-nav sidebar sidebar-light accordion sidebar-brand" id="accordionSidebar">
            @include('template_admin.navbar')
        </ul>
        <!-- Sidebar -->
        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">
                <!-- TopBar -->

                @include('template_admin.topbar')
                <!-- Topbar -->

                <!-- Container Fluid-->

                <div class="container-fluid" id="container-wrapper">
                    @yield('container')
                </div>

                <!---Container Fluid-->
            </div>
            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>copyright &copy;
                            <script>
                                document.write(new Date().getFullYear());
                            </script>

                        </span>
                    </div>
                </div>
            </footer>
            <!-- Footer -->
        </div>
    </div>

    <!-- Scroll to top -->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    {{-- modal reset password  --}}

    <div class="modal" id="modalPassword" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ganti Password </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="exampleInputPassword1">Password Lama</label>
                    <input type="password" class="form-control" id="ubahPasswordLama" placeholder="Password Lama Anda">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Password Baru</label>
                    <input type="password" class="form-control" id="ubahPasswordBaru" placeholder="Password Baru">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Konfirmasi Password Baru</label>
                    <input type="password" class="form-control" id="ubahKonfirmasiPasswordBaru" placeholder="Konfirmasi Password Baru">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="prosesUbahPassword()">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
            </div>
        </div>
        </div>

</body>

<script src="{{ asset('file/js/ruang-admin.min.js') }}"></script>
<script src="{{ asset('file/vendor/chart.js/Chart.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets_frontend/toast/toastr.min.js') }}"></script>

<script>
    $(document).ready(function() {
        toastr.options = {
            'closeButton': true,
            'debug': false,
            'newestOnTop': false,
            'progressBar': false,
            'positionClass': 'toast-top-right',
            'preventDuplicates': false,
            'showDuration': '1000',
            'hideDuration': '1000',
            'timeOut': '5000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut',
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
    });

    const prosesUbahPassword=()=>{
        var passBaru=$('#ubahPasswordBaru').val();
        var passKonfirmasi=$('#ubahKonfirmasiPasswordBaru').val();
        var passLama=$('#ubahPasswordLama').val()
        if(passBaru=="" || passLama=="" || passKonfirmasi==""){
            Swal.fire(
                'Gagal!',
                'Harap isi semua form!',
                'error'
            )
        }else if($('#ubahPasswordBaru').val() != $('#ubahKonfirmasiPasswordBaru').val()) {
             Swal.fire(
                'Gagal!',
                'Password baru dan konfirmasi password baru harus sama!',
                'error'
            )
        }else{
            $.ajax({
                type: "POST",
                url: "{{ route('proses.reset.password') }}",
                data: {
                    "passLama":passLama,
                    "passBaru":passBaru,
                    "passKonfirmasi":passKonfirmasi
                },
                success: function (response) {
                    Swal.fire(
                        'Berhasil!',
                        'Password berhasil diperbaharui!',
                        'success'
                    )
                },error: function (xhr, ajaxOptions, thrownError) {
                     Swal.fire(
                        'Gagal!',
                        xhr.responseJSON.message,
                        'error'
                    )
                }
            });
    }
    }
</script>
@yield('js')
</html>
