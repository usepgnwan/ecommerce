@extends('frontend.template_frontend.frontend')
@section('title', 'Product Checkout')
@section('content')

    <style>
        .flex3 {
            display: flex;
            justify-content: space-between;
            /* background-color: green; */
        }

        .flex-items-flex3 {
            /* background-color: #f4f4f4;
                                                                                        width: 100px;
                                                                                        height: 50px; */
            margin: 10px;
            /* text-align: center; */
            /* font-size: 40px; */
        }

    </style>
    <div class="container-fluid">
        <div class="ps-content pl-60 pr-60">
            <div class="row">
                <div class="col-md-6 card-cart">
                    @php
                        if (session('chart_checkout')) {
                            $dataCheckout = session()->get('chart_checkout');
                            $quantityCo = array_sum(array_column($dataCheckout, 'quantity'));
                            $totalHargaCo = array_sum(array_column($dataCheckout, 'total_harga'));
                        } else {
                            $quantityCo = 0;
                            $totalHargaCo = 0;
                            $dataCheckout = [];
                        }
                    @endphp
                    @if (count($dataCheckout) != 0)
                        @foreach ($dataCheckout as $k => $c)
                            <div class="card mb-3 card-checkout"
                                style="max-width: 900px; margin:2% auto; padding:10px; background-color:##f7f7f7; border: 1px solid #e5e5e5;"
                                data-id-co="{{ $k }}">
                                <div class="row no-gutters">
                                    <div class="col-md-4 pl-40">
                                        <img src="{{ $dataCheckout[$k]['images'] }}" class="card-img"
                                            alt="Guide to Web Design"
                                            style="padding:5px; border-radius:8px; height: 180px; width: 180px;">
                                    </div>
                                    <div class="col-md-8">
                                        <div class="card-body">
                                            <div class="card-title">
                                                <div class="flex3">
                                                    <div class="flex-items-flex3">
                                                        <h4 class="pb-10">
                                                            <strong>{{ $dataCheckout[$k]['nama_produk'] }}</strong>
                                                        </h4>
                                                        @if ($dataCheckout[$k]['diskon'] != null || $dataCheckout[$k]['diskon'] != '')
                                                            <del>Rp.{{ number_format($dataCheckout[$k]['harga'], 0, ',', '.') }}</del>
                                                            &nbsp;
                                                            Rp.{{ number_format($dataCheckout[$k]['harga_diskon'], 0, ',', '.') }}
                                                        @else
                                                            Rp.
                                                            {{ number_format($dataCheckout[$k]['harga'], 0, ',', '.') }}
                                                        @endif

                                                    </div>
                                                    <div class="flex-items-flex3">
                                                        <div class="ps-remove delete-co" style="text-align:end"> </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div style="padding:11px">
                                                <table style="width:100%; ">
                                                    <tbody>
                                                        <tr style="border-bottom: 1px solid #e5e5e5; line-height:30px;">
                                                            <td style="width:80%;"><strong>Size</strong></td>
                                                            <td style="text-align:right">{{ $dataCheckout[$k]['size'] }}
                                                            </td>
                                                        </tr>
                                                        @if ($dataCheckout[$k]['diskon'] != null || $dataCheckout[$k]['diskon'] != '')
                                                            <tr style="border-bottom: 1px solid #e5e5e5; line-height:30px;">
                                                                <td style="width:80%;"><strong>Diskon</strong></td>
                                                                <td style="text-align:right">
                                                                    {{ $dataCheckout[$k]['diskon'] }}%
                                                                </td>
                                                            </tr>
                                                        @endif
                                                        <tr style="border-bottom: 1px solid #e5e5e5; line-height:30px;">
                                                            <td style="width:80%;"><strong>Quantity</strong></td>
                                                            <td style="text-align:right">
                                                                <input type="number"
                                                                    style="margin:10px 0px; max-width:100px"
                                                                    name="size_input" id="size_input" min="1"
                                                                    value="{{ $dataCheckout[$k]['quantity'] }}">
                                                                {{-- @php
                                                                    $xx = 20;
                                                                @endphp
                                                                <select name="size_input"
                                                                    style="border: 1px solid #ccc; background:white">
                                                                    @for ($i = 1; $i <= $xx; $i++)
                                                                        @if ($i == $dataCheckout[$k]['quantity'])
                                                                            <option value="{{ $i }}" selected="">
                                                                                {{ $i }}</option>
                                                                        @else
                                                                            <option value="{{ $i }}">
                                                                                {{ $i }}</option>
                                                                        @endif
                                                                    @endfor --}}
                                                                </select>
                                                            </td>
                                                        </tr>
                                                        <tr style="line-height:40px;">
                                                            <td style="width:80%;"><strong>Total Harga</strong></td>
                                                            <td style="text-align:right"><span
                                                                    class="totalHargaItem">Rp.{{ number_format($dataCheckout[$k]['total_harga'], 0, ',', '.') }}</span>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            {{-- <div>
                                                <h4 style="font-weight: 400;">Product Description</h4>
                                                <hr>
                                                <p class="card-text" style="padding:5px;">A web design e-book by Ezike
                                                    Ebuka,
                                                    it
                                                    covers HTML, CSS and
                                                    Bootstrap. The e-book would help you to quickly get started in web
                                                    design.
                                                </p>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="card mb-3"
                            style="max-width: 900px; margin:2% auto; padding:10px; background-color:##f7f7f7; border: 1px solid #e5e5e5;">
                            <div class="row no-gutters">
                                <div class="col-md-12">
                                    <div class="card-body">
                                        <div class="text-center pb-20" style="color:#2ac37d">
                                            <h4 style="color:#2ac37d !important">Belum Tersedia</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12  pt-15 ">
                    <div class="ps-shipping">
                        <div class="ps-cart__actions">
                            <div class="ps-cart__promotion">
                                <div class="form-group">
                                    <div class="ps-form--icon"><i class="fa fa-angle-right"></i>
                                        <input class="form-control kode-voucher-input" type="text"
                                            placeholder="Kode Voucher">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button class="ps-btn ps-btn--gray btn-promo"
                                        style="font-size: 13px !important; padding:10px !important;">Tambahkan</button>
                                </div>
                            </div>
                            @php
                                  $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
                                  $checkPromo = count($epromo) != 0  ? true : false;
                            @endphp
                            <div class="ps-checkout" style="border-top:1px solid #e5e5e5; line-height:30px; ">
                                <table class="table ps-checkout__products table-co-co">
                                    <tbody>
                                        <tr>
                                            <td>Quantity</td>
                                            <td> <span class="qtyTotal">
                                                    {{ $quantityCo }}</span> </td>
                                        </tr>
                                        <tr>
                                            <td>Sub Total</td>
                                            <td> <span class="Subtotal"> Rp.
                                                    {{ number_format($totalHargaCo, 0, ',', '.') }}</span> </td>
                                        </tr>
                                        <tr>
                                            <td>Promo / E-Voucher</td>
                                            <td><span id="text-epromo">@if ($checkPromo)
                                                Evoucher Tersedia @else -
                                            @endif</span></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table class="table ps-checkout__products table-co-co">
                                    <tbody class="epromo">
                                        @php
                                            $voucher = 0;
                                        @endphp
                                        @if ($checkPromo)
                                            @foreach ($epromo as $i => $p )
                                                @php
                                                    $labeldisc ='';
                                                    $namapromo='';
                                                    $check =  explode('.', $i);
                                                    if($check[0] =='potongan'){
                                                        $labeldisc = 'Potongan'  . ' -Rp. ' .number_format($p->promo, 0, ',', '.');
                                                        $namapromo = $p->nama_promo;
                                                    }else if($check[0] =='diskon'){
                                                        $disc =  ($totalHargaCo * $p->promo ) / 100;
                                                        $labeldisc =  'Diskon'  .   ' -Rp. ' . number_format($disc, 0, ',', '.');
                                                        $namapromo = $p->nama_promo . '(' .$p->promo. '%)';
                                                    }

                                                    $voucherCount = [];
                                                    if(count($epromo) != 0){
                                                        foreach($epromo as $ix => $val){
                                                            $check = explode('.', $ix);
                                                            if($check[0] =='diskon'){
                                                                array_push($voucherCount, ($val->promo * $totalHargaCo) /100);
                                                            }else{
                                                                array_push($voucherCount, $val->promo );
                                                            }
                                                        }
                                                    }

                                                    $voucher = array_sum($voucherCount);
                                                @endphp
                                            <tr class="epormo-badge" data-id="{{$i}}">
                                                <td>{{$p->type_voucher}} {{$namapromo}}</td>
                                                <td>
                                                    <div class="col-8" id="resultembed">
                                                        <span href="javascript:void(0)" id="badgeembed0"
                                                            class="badge badge-primary"
                                                            style="background:#66bb6a !important;">
                                                            {{$labeldisc}}
                                                            <a href="javascript:void(0)" id="removebadge">
                                                                <i class="fa fa-times text-white" style="color:white"></i>
                                                            </a>
                                                        </span>
                                                    </div>

                                                    </td>
                                            </tr>
                                            @endforeach
                                        @endif
                                    </tbody>
                                </table>
                                <table class="table ps-checkout__products table-co-co">
                                    <tbody>
                                        <tr>
                                            <td>Ongkos Kirim</td>
                                            <td><span class="ongkos-bayar">Belum ditetapkan</span></td>
                                        </tr>
                                        <tr style="border-top:1px solid #e7e7e7">
                                            <td><b>Total Bayar</b></td>
                                            <td><b><span class="total-harga-input"><span class="totalBayar"> Rp.
                                                            {{ number_format($totalHargaCo - $voucher, 0, ',', '.') }}</span></span></b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                {{-- <h5><strong>Promo / E-Voucher:</strong> &nbsp; <span> - </span>
                                    <div class="col-8" id="resultembed">
                                        <span href="javascript:void(0)" id="badgeembed0" class="badge badge-primary"
                                            onclick="hapus_badge('embed',0)" style="background:#66bb6a !important;">
                                            Potongan Rp. 20000000
                                            <a href="javascript:void(0)">
                                                <i class="fa fa-times text-white" style="color:white"></i>
                                            </a>
                                        </span>
                                    </div>

                                </h5>
                                <h5><strong>Quantity:</strong> &nbsp; <span class="qtyTotal">
                                        {{ $quantityCo }}</span></h5>
                                <h5><strong>Subtotal:</strong> &nbsp; <span class="Subtotal"> Rp.
                                        {{ number_format($totalHargaCo, 0, ',', '.') }}</span></h5>
                                <h5><strong>Total Bayar:</strong> &nbsp; <span class="totalBayar"> Rp.
                                        {{ number_format($totalHargaCo, 0, ',', '.') }}</span></h5> --}}
                                <div class="ps-product__shopping pt-20">
                                    <a href="{{ route('product.checkout_cart_payment') }}"
                                        style="font-size: 13px !important; padding:10px !important; text-decoration:none; color:white;"
                                        type="button" class="ps-btn mb-10 ps-checktot-product">Lanjutkan ke
                                        pembayaran</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
@endsection

@section('js')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {

            $(document).on('input', 'input[name="size_input"]', function(e) {
                let $this = $(this);

                let id = $this.parent().closest('.card-checkout').data('id-co');
                let url = "{{ route('chart.update', ['chart' => ':id']) }}";
                url = url.replace(":id", id);
                if ($this.val() == '' || $this.val() == 0) {
                    toastr.warning('Produk Tidak Boleh Kosong / 0');
                    e.target.value = 1
                    update_cart($this, url, 1)
                } else {
                    // alert($this.val());
                    //
                    update_cart($this, url, $this.val())

                }
            });

            function update_cart($this, url, $val) {
                $.ajax({
                    method: 'POST',
                    url: url,
                    start_time: new Date().getTime(),
                    data: {
                        _method: 'PUT',
                        qty: $val
                    },
                    success: function(data) {
                        let timerInterval
                        Swal.fire({
                            title: 'Proses',
                            html: 'I will close in <strong></strong> seconds.<br/><br/>',
                            timer: (new Date().getTime() - this.start_time),
                            didOpen: () => {
                                const content = Swal.getHtmlContainer()
                                const $ = content.querySelector.bind(content)
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    Swal.getHtmlContainer().querySelector(
                                            'strong')
                                        .textContent = (Swal
                                            .getTimerLeft() / 1000)
                                        .toFixed(0)
                                }, 100)
                            },
                            willClose: () => {
                                if (!data.status) {
                                    toastr.warning('Periksa kembali form');
                                } else {
                                    evoucherCount(data)
                                    // $this.parent().closest('.card-checkout').remove();
                                    $('.quantity-checkout-all').html(data.countQuantity);
                                    $this.parent().closest('.card-checkout').find('.totalHargaItem')
                                        .html(data.totalHargaItem);
                                    $('.Subtotal').html(data.subTotal);
                                    $('.totalBayar').html(data.countTotal);
                                    $('.qtyTotal').html(data.countQuantity);
                                    $('.quantityCo').html(data.countQuantity);
                                    $('.totalHargaCo').html(data.countTotal);
                                    $('.content-chart-item').html('');
                                    $.each(data.all_checkout, function(i, res) {
                                        let harga = 0;
                                        if (data.all_checkout[i].harga_diskon !=
                                            0) {
                                            harga =
                                                `<del><span>Harga:<i>Rp.${data.all_checkout[i].harga}  </i> </span></del> <br>
                                                                        <span>Harga: <i>${data.all_checkout[i].harga_diskon}</i></span> <br> `;
                                        } else {
                                            harga =
                                                ` <span>Harga:<i>Rp.${data.all_checkout[i].harga}</i></span> <br>`;
                                        }
                                        $('.content-chart-item').append(`
                                                                    <div class="ps-cart-item">
                                                                        <div class="ps-cart-item__thumbnail">
                                                                            <a href="#"></a>
                                                                            <img src="${data.all_checkout[i].images}" alt="">
                                                                        </div>
                                                                        <div class="ps-cart-item__content" style="text-align: left">
                                                                            <a class="ps-cart-item__title"  href="#">${data.all_checkout[i].nama_produk}</a>
                                                                            <p>
                                                                                <span>Size:<i> ${data.all_checkout[i].size}</i></span><br>
                                                                                <span>Quantity:<i>${data.all_checkout[i].quantity}</i></span><br>
                                                                                ${harga}
                                                                                <span>Total:<i>Rp.${data.all_checkout[i].total_harga}</i></span>
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                `);
                                    })
                                }
                                clearInterval(timerInterval)
                            }
                        });
                    }
                });
            }
            $(document).on('click', '.delete-co', function(e) {
                e.preventDefault();
                let $this = $(this);
                let id = $this.parent().closest('.card-checkout').data('id-co');

                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        let url = "{{ route('chart.destroy', ['chart' => ':id']) }}";
                        url = url.replace(":id", id);
                        $.ajax({
                            method: 'DELETE',
                            url: url,
                            start_time: new Date().getTime(),
                            success: function(data) {
                                Swal.fire({
                                    title: 'Proses',
                                    html: 'I will close in <strong></strong> seconds.<br/><br/>',
                                    timer: (new Date().getTime() - this
                                        .start_time),
                                    didOpen: () => {
                                        const content = Swal
                                            .getHtmlContainer()
                                        const $ = content.querySelector
                                            .bind(content)
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            Swal.getHtmlContainer()
                                                .querySelector(
                                                    'strong')
                                                .textContent = (Swal
                                                    .getTimerLeft() /
                                                    1000)
                                                .toFixed(0)
                                        }, 100)
                                    },
                                    willClose: () => {
                                        clearInterval(timerInterval)
                                    }
                                });
                                if (!data.status) {
                                    toastr.warning('Periksa kembali form');
                                } else {
                                    if(data.evocherstatus){
                                        $('#text-epromo').html('-');
                                        $('.epromo').html('');
                                    }else{
                                    evoucherCount(data)

                                    }
                                    $this.parent().closest('.card-checkout').remove();
                                    $('.quantity-checkout-all').html(data
                                        .countQuantity);
                                    $('.Subtotal').html(data.subTotal);
                                    $('.totalBayar').html(data.countTotal);
                                    $('.qtyTotal').html(data.countQuantity);
                                    $('.quantityCo').html(data.countQuantity);
                                    $('.totalHargaCo').html(data.countTotal);
                                    $('.content-chart-item').html('');
                                    $.each(data.all_checkout, function(i, res) {
                                        let harga = 0;
                                        if (data.all_checkout[i].harga_diskon !=
                                            0) {
                                            harga =
                                                `<del><span>Harga:<i>Rp.${data.all_checkout[i].harga}  </i> </span></del> <br>
                                                            <span>Harga: <i>${data.all_checkout[i].harga_diskon}</i></span> <br> `;
                                        } else {
                                            harga =
                                                ` <span>Harga:<i>Rp.${data.all_checkout[i].harga}</i></span> <br>`;
                                        }
                                        $('.content-chart-item').append(`
                                                        <div class="ps-cart-item">
                                                            <div class="ps-cart-item__thumbnail">
                                                                <a href="#"></a>
                                                                <img src="${data.all_checkout[i].images}" alt="">
                                                            </div>
                                                            <div class="ps-cart-item__content" style="text-align: left">
                                                                <a class="ps-cart-item__title"  href="#">${data.all_checkout[i].nama_produk}</a>
                                                                <p>
                                                                    <span>Size:<i> ${data.all_checkout[i].size}</i></span><br>
                                                                    <span>Quantity:<i>${data.all_checkout[i].quantity}</i></span><br>
                                                                    ${harga}
                                                                    <span>Total:<i>Rp.${data.all_checkout[i].total_harga}</i></span>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    `);
                                    })
                                }
                            }
                        });
                        let xx = $('.card-checkout').length - 1;

                        if (xx <= 0) {
                            $('.content-chart-item').html(`
                                            <div class="text-center pb-20" style="color:#2ac37d">
                                                <h4 style="color:#2ac37d !important">Belum Tersedia</h4>
                                            </div>
                                        `);
                            $('.card-cart').html(`
                                                <div class="card mb-3" style="max-width: 900px; margin:2% auto; padding:10px; background-color:##f7f7f7; border: 1px solid #e5e5e5;">
                                                    <div class="row no-gutters">
                                                        <div class="col-md-12">
                                                            <div class="card-body">
                                                                <div class="text-center pb-20" style="color:#2ac37d">
                                                                    <h4 style="color:#2ac37d !important">Belum Tersedia</h4>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        `);
                        }
                    }
                });
            });
        });

        $(document).on('click', '.btn-promo', function(e) {
            e.preventDefault();
            let url = "{{ route('product.add_promo') }}";
            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    kode_voucher: $('.kode-voucher-input').val()
                },
                start_time: new Date().getTime(),
                success: function(data) {
                    if (!data.status) {
                        // toastr.warning('Periksa kembali form');
                        Swal.fire(
                            'Warning',
                            data.msg,
                            'warning');
                    } else {
                        $('.kode-voucher-input').val('');
                        Swal.fire({
                            title: 'Proses',
                            html: 'I will close in <strong></strong> seconds.<br/><br/>',
                            timer: (new Date().getTime() - this
                                .start_time),
                            didOpen: () => {
                                const content = Swal
                                    .getHtmlContainer()
                                const $ = content.querySelector
                                    .bind(content)
                                Swal.showLoading()
                                timerInterval = setInterval(() => {
                                    Swal.getHtmlContainer()
                                        .querySelector(
                                            'strong')
                                        .textContent = (Swal
                                            .getTimerLeft() /
                                            1000)
                                        .toFixed(0)
                                }, 100)
                            },
                            willClose: () => {
                                // Swal.fire(
                                //     'Success',
                                //     data.msg,
                                //     'success');
                                $('#text-epromo').html('voucher tersedia');
                                evoucherCount(data);
                                toastr.success(data.msg);
                                clearInterval(timerInterval)
                            }
                        });

                        // $this.parent().closest('.card-checkout').remove();
                        // $('.quantity-checkout-all').html(data
                        //     .countQuantity);
                        // $('.Subtotal').html(data.countTotal);
                        // $('.totalBayar').html(data.countTotal);
                        // $('.qtyTotal').html(data.countQuantity);
                        // $('.quantityCo').html(data.countQuantity);
                        // $('.totalHargaCo').html(data.countTotal);
                        // $('.content-chart-item').html('');
                        // $.each(data.all_checkout, function(i, res) {
                        //     let harga = 0;
                        //     if (data.all_checkout[i].harga_diskon !=
                        //         0) {
                        //         harga =
                        //             `<del><span>Harga:<i>Rp.${data.all_checkout[i].harga}  </i> </span></del> <br>
                    //                         <span>Harga: <i>${data.all_checkout[i].harga_diskon}</i></span> <br> `;
                        //     } else {
                        //         harga =
                        //             ` <span>Harga:<i>Rp.${data.all_checkout[i].harga}</i></span> <br>`;
                        //     }
                        //     $('.content-chart-item').append(`
                    //                     <div class="ps-cart-item">
                    //                         <div class="ps-cart-item__thumbnail">
                    //                             <a href="#"></a>
                    //                             <img src="${data.all_checkout[i].images}" alt="">
                    //                         </div>
                    //                         <div class="ps-cart-item__content" style="text-align: left">
                    //                             <a class="ps-cart-item__title"  href="#">${data.all_checkout[i].nama_produk}</a>
                    //                             <p>
                    //                                 <span>Size:<i> ${data.all_checkout[i].size}</i></span><br>
                    //                                 <span>Quantity:<i>${data.all_checkout[i].quantity}</i></span><br>
                    //                                 ${harga}
                    //                                 <span>Total:<i>Rp.${data.all_checkout[i].total_harga}</i></span>
                    //                             </p>
                    //                         </div>
                    //                     </div>
                    //                 `);
                        // })
                    }
                }
            });
        });
        function evoucherCount(data) {
            $('.totalBayar').html(data.countTotal);
            $.each(data.evocher, function(i, d) {
                let labelvc;
                let namavc;
                let check = i.split('.')[0];
                // alert(check)
                if (check == 'potongan') {
                    labelvc = 'Potongan -' + formatRupiah(d.promo);
                    namavc = d.nama_promo;
                } else if (check == 'diskon') {
                    let disc =(data.NormalTotal * d.promo) / 100;
                    labelvc = 'Diskon -' +  formatRupiah(disc);
                    namavc = ` ${d.nama_promo} diskon (${d.promo}%)`
                }
                $('.epromo').html('');

                $('.epromo').append(`
                    <tr class="epormo-badge" data-id="${i}">
                        <td>
                            ${namavc}
                        </td>
                        <td>
                            <div class="col-8" id="resultembed">
                                <span href="javascript:void(0)" id="badgeembed0"
                                    class="badge badge-primary"
                                    style="background:#66bb6a !important;">
                                    ${labelvc}
                                    <a href="javascript:void(0)" id="removebadge">
                                        <i class="fa fa-times text-white" style="color:white"></i>
                                    </a>
                                </span>
                            </div>
                        </td>
                    </tr>
                `);
            });
        }

        $(document).on('click', '#removebadge', function(e) {
            let $this = $(this);
            e.preventDefault();
            let id = $this.parent().closest('.epormo-badge').data('id');
            Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        let url = "{{ route('product.delete_promo') }}";
                        url = url.replace(":id", id);
                        $.ajax({
                            method: 'POST',
                            url: url,
                            start_time: new Date().getTime(),
                            data:{
                                id:id
                            },
                            success: function(data) {
                                Swal.fire({
                                    title: 'Proses',
                                    html: 'I will close in <strong></strong> seconds.<br/><br/>',
                                    timer: (new Date().getTime() - this
                                        .start_time),
                                    didOpen: () => {
                                        const content = Swal
                                            .getHtmlContainer()
                                        const $ = content.querySelector
                                            .bind(content)
                                        Swal.showLoading()
                                        timerInterval = setInterval(() => {
                                            Swal.getHtmlContainer()
                                                .querySelector(
                                                    'strong')
                                                .textContent = (Swal
                                                    .getTimerLeft() /
                                                    1000)
                                                .toFixed(0)
                                        }, 100)
                                    },
                                    willClose: () => {

                                        $this.parent().closest('.epormo-badge').remove();
                                        clearInterval(timerInterval)
                                    }
                                });
                                if (!data.status) {
                                    toastr.warning('Periksa kembali form');
                                } else {
                                    if(data.evocherstatus){
                                        $('#text-epromo').html('-');
                                        $('.epromo').html('');
                                    }else{
                                    evoucherCount(data)
                                    }
                                    $('.quantity-checkout-all').html(data.countQuantity);
                                    $('.Subtotal').html(data.subTotal);
                                    $('.totalBayar').html(data.countTotal);
                                    $('.qtyTotal').html(data.countQuantity);
                                    $('.quantityCo').html(data.countQuantity);
                                    $('.totalHargaCo').html(data.countTotal);
                                }
                            }
                        });
                    }
                });
        });
        //

        function formatRupiah(angka) {
            var number_string = angka.toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return "Rp. " + rupiah;
        }
    </script>
@endsection
