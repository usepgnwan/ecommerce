<?php

namespace App\Providers;

use App\Models\MasterCategory;
use App\Models\MetodePembayaran;
use App\Models\VoucherKode;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\App;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (! App::runningInConsole()) { 

            if (Schema::hasTable('store_setting')) { 
                $store_setting = DB::table('store_setting')->where(['id' => '1'])->first();
                View::share('store_setting', $store_setting);
            }
            if (Schema::hasTable('master_category')) {
                $category_all = MasterCategory::get();
                View::share('category_all',  $category_all);
            }

            if (Schema::hasTable('metode_pembayarans')) {
                $m_pembayaran = MetodePembayaran::where('status','=','on')->get();
                View::share('m_pembayaran',  $m_pembayaran);
            }

            if (Schema::hasTable('voucher_kodes')) {
                $evoucher_kode = DB::select('SELECT * FROM  voucher_kodes  WHERE status = "on"  AND deleted_at IS NULL AND CURDATE() BETWEEN periode_awal And periode_akhir;');
                View::share('evoucher_kode',  $evoucher_kode);
            }

        }
        Schema::defaultStringLength(255);
        Paginator::useBootstrap();
    }
}
