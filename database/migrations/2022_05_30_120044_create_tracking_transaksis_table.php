<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_transaksis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaksi_id')
            ->references('id')
            ->on('transaksis')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('title');
            $table->string('deskripsi');
            $table->timestamp('tanggal_tracking');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tracking_transaksis');
    }
}
