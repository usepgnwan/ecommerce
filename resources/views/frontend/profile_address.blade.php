@extends('frontend.home_user')

@section('content_user')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script>

    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/i18n/defaults-*.min.js"></script>

    <div class="row">
        <div class="container-fluid">
            <div>
                <div class="row justify-content-between">
                    <div class="col-md-6">
                        <h4>Manage Alamat</h4>
                    </div>
                    <div class="col-md-6  ">
                        <div style="text-align:end"><a href="javascript:void(0)"
                                class="btn btn-sm btn-success tambah-address"><span class="fa fa-plus"></span> &nbsp;
                                Tambah
                                Alamat </a></div>
                    </div>
                </div>
                <hr>
            </div>
            <div class="row all-address">
                <form action="">
                    <?php $id = 1; ?>
                    @if (count($address_user) != 0)
                        @foreach ($address_user as $addr)
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address"
                                data-id_adress="{{ $addr->id }}">
                                <div class="card h-100">
                                    <div class="card-body " style="padding: 20px;">
                                        <div class="row gutters">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-4">
                                                            <span class="text-success title-address"
                                                                style="font-size: 1.3em; color: #c99069">
                                                                <b> Alamat {{ $id++ }} </b> </span>
                                                        </div>
                                                        <div class="col-md-6" style="text-align: end">
                                                            {{-- <span class="text-primary"><a href="javascript:void(0)">
                                                                    Jadikan Utama
                                                                </a></span> --}}
                                                            &nbsp;
                                                            <span class="text-primary"><a href="javascript:void(0)"
                                                                    id="ubah-address"> Ubah
                                                                </a></span> &nbsp;
                                                            <span class="text-primary"><a href="javascript:void(0)"
                                                                    id="deleted-address"> Delete </a></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="padding:0 20px 0 30px">
                                                <div class="col-md-12 col-sm-12">
                                                    <hr>
                                                    <table>
                                                        <tbody>
                                                            <tr>
                                                                <td style="width:150px;" valign="top"><label for="">Nama
                                                                        Penerima</label></td>
                                                                <td width="20px" valign="top">:</td>
                                                                <td valign="top"> {{ $addr->nama_penerima }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:150px;" valign="top"><label for="">No. Telp </label></td>
                                                                <td width="20px" valign="top">:</td>
                                                                <td valign="top"> {{ $addr->no_telp }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:150px;" valign="top"><label
                                                                        for="">Provinsi</label>
                                                                </td>
                                                                <td width="20px" valign="top">:</td>
                                                                <td valign="top">{{ $addr->provinsi }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:150px;" valign="top"><label
                                                                        for="">Kota</label>
                                                                </td>
                                                                <td width="20px" valign="top">:</td>
                                                                <td valign="top">{{ $addr->kota }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:150px;" valign="top"><label for="">No.
                                                                        Pos</label>
                                                                </td>
                                                                <td width="20px" valign="top">:</td>
                                                                <td valign="top">{{ $addr->no_pos }} </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width:150px;" valign="top"><label
                                                                        for="">Alamat</label>
                                                                </td>
                                                                <td width="20px" valign="top">:</td>
                                                                <td valign="top">{{ $addr->alamat }} </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                            <div class="card h-100">
                                <div class="card-body " style="padding: 20px;">
                                    <div class="row gutters">
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                <div class="row">
                                                    <div class="col-md-12 col-sm-12 text-center">
                                                        <span class="text-success title-address"
                                                            style="font-size: 0.8em; color: #c99069; text-align:center">
                                                            <b>ALAMAT BELUM TERSEDIA</b> </span>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        {{-- <div class="row" style="padding:0 20px 0 30px">
                                            <hr>
                                        </div> --}}
                                    </div>

                                </div>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Password -->
    <div id="modal_address" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" style="overflow: auto !important;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content change-address">
                <form action="">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Password</h5>
                        {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> --}}
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Nama Penerima</label>
                                    <input type="text" class="form-control" name="nama" placeholder="Nama Penerima">
                                    <input type="hidden" class="form-control" name="id_alamat" >
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">No.telp /Wa Penerima</label>
                                    <input type="text" class="form-control" name="no_telp"
                                        placeholder="No.telp /Wa Penerima">
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Provinsi</label>
                                    <select name="provinsi" id="" class="form-control provinsi" data-live-search="true">
                                        <option value="0">- Pilih -</option>
                                        @foreach ($provinsi['results'] as $item)
                                            <option value="{{ $item['province_id'] }}|{{ $item['province'] }}">
                                                {{ $item['province'] }}</option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Kota / Kabupaten</label>
                                    <select name="kota" id="" class="form-control provinsi" data-live-search="true">
                                        <option value="">- Pilih -</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">No.Pos</label>
                                    <input type="text" class="form-control" name="no_pos" placeholder="No.Pos">
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Alamat</label>
                                    <textarea name="alamat" class="form-control" id="" style="height: 90px; resize:none;"
                                        cols="num" rows="num"></textarea>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-address">Close</button>
                        <button type="submit" class="btn btn-primary add-address" value="0" data-idaddr='0'>Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_extended')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {

            $('.provinsi').selectpicker();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $('.tambah-address').click(function() {
                $('.add-address').val(0);
                $('.add-address').attr('data-idaddr', 0);
                $('.change-address form')[0].reset();
                $('#modal_address').show();
            });
            $('.close-address').click(function() {
                $('.change-address form')[0].reset();
                $('#modal_address').hide();
            });

            $(document).on('click', '#ubah-address', function(e) {
                e.preventDefault();
                let id = $(this).parent().closest('.address').data('id_adress');
                let url = '{{ route('address.edit', ['address' => ':id']) }}';
                url = url.replace(":id", id);
                $('.add-address').val(1);
                // $('.change-address form')[0].reset();
                $('#modal_address').show();
                $('.add-address').attr('data-idaddr', id);
                $('[name="id_alamat"]').val(id);
                $.ajax({
                    url: url,
                    // data: form,
                    method: 'GET',
                    success: function(res) {
                        $('[name="nama"]').val(res.data.nama_penerima);
                        $('[name="no_telp"]').val(res.data.no_telp);
                        $('select[name=provinsi]').val(res.data.id_provinsi);
                        $('[name="provinsi"]').selectpicker('val', res.data.id_provinsi + '|' +
                            res.data.provinsi);
                        $('[name="no_pos"]').val(res.data.no_pos);
                        $('[name="alamat"]').val(res.data.alamat);
                        kota(res.data.id_provinsi, res.data.id_kota);
                    }
                });

            });

            $(document).on('click', '#deleted-address', function(e) {
                e.preventDefault();
                let id = $(this).parent().closest('.address').data('id_adress');
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).parent().closest('.address').remove();
                        let url = "{{ route('address.destroy', ['address' => ':id']) }}";
                        url = url.replace(":id", id);
                        $.ajax({
                            method: 'DELETE',
                            url: url,
                            success: function(data) {
                                if (data.status == true) {
                                    toastr.error(data.msg);
                                    //  let  test =  $(this).parent().closest('.address').find('.title-address').html();

                                } else {
                                    toastr.error('Gagal, Hapus data alamat.');
                                }
                            }
                        });

                        let check = $('.address').length
                        if (check == 0) {
                            $('.all-address').append(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class="card h-100">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b>ALAMAT BELUM TERSEDIA</b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            `);
                        }
                    }
                });
            });

            $("[name='provinsi']").change(function() {
                let $provinsi = $(this).val();
                if ($provinsi == 0) {
                    $("[name='kota']").html('');
                    $("[name='kota']").append(`<option value="0">- Pilih -</option>`);
                    $("[name='kota']").selectpicker('refresh');
                    $("[name='no_pos']").val('');
                } else {
                    $provinsi = $provinsi.split("|");
                    $("[name='no_pos']").val('');
                    kota($provinsi[0]);
                }
            });

            function kota($id, $checkid = null) {
                let url = '{{ route('get.kota', ['id_address' => ':id']) }}';
                url = url.replace(":id", $id);
                $.ajax({
                    url: url,
                    // data: form,
                    method: 'GET',
                    success: function(data) {
                        console.log(data.results);
                        $("[name='kota']").html('');
                        $.each(data.results, function(i, res) {
                            if ($checkid == res.city_id) {
                                $("[name='kota']").append(
                                    `<option value="${res.city_id}|${res.city_name}" data-pos="${res.postal_code}" selected>${res.city_name}</option>`
                                );
                            } else {
                                $("[name='kota']").append(
                                    `<option value="${res.city_id}|${res.city_name}" data-pos="${res.postal_code}" >${res.city_name}</option>`
                                );
                            }
                        });
                        $("[name='kota']").selectpicker('refresh');
                    }
                });
            }

            $("[name='kota']").change(function() {
                $("[name='no_pos']").val($(this).find('option:selected').data("pos"));
            });

            $('.change-address form').submit(function(e) {
                e.preventDefault();
                let form = $(this).serializeArray();
                let check = $('.add-address').val();
                let url = '';
                if (check == 0) {
                    url = "{{ route('address.store') }}";
                } else if (check == 1) {
                   form = form.concat({
                        name: "_method",
                        value: "PUT"
                    });
                    let id = $('[name="id_alamat"]').val();
                    url = '{{ route('address.update', ['address' => ':id']) }}';
                    url = url.replace(":id", id);

                }
                $.ajax({
                    url: url,
                    data: form,
                    method: 'Post',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status) {
                            $('#modal_address').hide();
                            $('.change-address form')[0].reset();
                            toastr.success(data.msg);
                            validate();
                            $('.all-address').html('');
                            let i = 1;
                            $.each(data.data, function(i, res) {
                                $('.all-address').append(`
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address" data-id_adress="${res.id}">
                                        <div class="card h-100">
                                            <div class="card-body " style="padding: 20px;">
                                                <div class="row gutters">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4">
                                                                    <span class="text-success title-address"
                                                                        style="font-size: 1.3em; color: #c99069">
                                                                        <b> Alamat ${i+1} </b> </span>
                                                                </div>
                                                                <div class="col-md-6" style="text-align: end">
                                                                    {{-- <span class="text-primary"><a href="javascript:void(0)">
                                                                    Jadikan Utama
                                                                    </a></span> --}}
                                                                    &nbsp;
                                                                    <span class="text-primary"><a href="#" id="ubah-address"> Ubah </a></span>
                                                                    &nbsp;
                                                                    <span class="text-primary"><a href="#" id="deleted-address">
                                                                            Delete
                                                                        </a></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding:0 20px 0 30px">
                                                        <div class="col-md-12 col-sm-12">
                                                            <hr>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">Nama
                                                                                Penerima</label></td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.nama_penerima} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">No. Telp</label></td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.no_telp} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Provinsi</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.provinsi}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Kota</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.kota}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">No.
                                                                                Pos</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.no_pos} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Alamat</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.alamat}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                `);
                            });
                        } else {
                            validate();
                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<span class="text-error-' + field_name +
                                    ' danger-text-error ">' + error + '</span>')
                            });
                            toastr.error(data.msg);
                        }
                    }
                });
            });
        });

        function validate() {
            $(".text-error-nama").remove();
            $(".text-error-no_telp").remove();
            $(".text-error-provinsi").remove();
            $(".text-error-kota").remove();
            $(".text-error-no_pos").remove();
            $(".text-error-alamat").remove();
        }
    </script>
@endsection
