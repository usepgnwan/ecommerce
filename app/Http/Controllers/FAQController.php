<?php

namespace App\Http\Controllers;

use App\Models\FAQ;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class FAQController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layout_admin.master_faq');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_data()
    {
        $data = FAQ::select("id", "question", "most_question", "answer")->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $actionBtn = ' <a href="javascript:void(0)" class="primary btn btn-primary btn-sm" onclick="edit(' . $row->id . ')"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" onclick="hapus(' . $row->id . ')"><i class="fa fa-trash"></i> Hapus</a>';
                return $actionBtn;
            })
            ->addColumn('most_question', function ($row) {
                if ($row->most_question == 1) {

                    $status = '<span class="badge badge-success">Ya</span>';
                } else {
                    $status = '<span class="badge badge-danger">Tidak</span>';
                }
                return $status;
            })
            ->rawColumns(['action', 'most_question'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = FAQ::create([
            'question' => request('question'),
            'answer' => request('answer'),
            'most_question' => request('most_question')
        ]);
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\FAQ  $FAQ
     * @return \Illuminate\Http\Response
     */
    public function show(FAQ $FAQ)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\FAQ  $FAQ
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return FAQ::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\FAQ  $FAQ
     * @return \Illuminate\Http\Response
     */
    public function update($id)
    {
        $data = FAQ::find($id)->update([
            'question' => request('question'),
            'answer' => request('answer'),
            'most_question' => request('most_question')
        ]);
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\FAQ  $FAQ
     * @return \Illuminate\Http\Response
     */
    public function destroy(FAQ $FAQ)
    {
        $id = request('id');
        $data = FAQ::find($id)->delete();
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}