<?php

namespace Database\Seeders;

use App\Models\BannerPromo;
use Illuminate\Database\Seeder;

class BannerPromoSedeer extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            BannerPromo::create(['foto_banner_1' => 'default-banner.png', 'foto_banner_2' => 'default-banner.png']);

    }
}
