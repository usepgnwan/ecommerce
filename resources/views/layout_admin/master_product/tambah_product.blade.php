@extends('template_admin.admin')

@section('title', 'Master Product')
@section('container')
    <style>
        .file {
            visibility: hidden;
            position: absolute;
        }

    </style>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master Product</h1>

    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Tambah Produk Baru</h6>
                </div>
                <div class="card-body">
                    <div class="container">
                        <form method="POST" enctype="multipart/form-data" action="{{ route('product.store') }}"
                            id="form-tambah-produk">
                            @if (isset($errors) && count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @csrf
                            <div class="form-group">
                                <label for="nama_produk">Nama Produk *</label>
                                <input type="text" class="form-control @error('nama_produk') is-invalid @enderror"
                                    id="nama_produk" name="nama_produk" placeholder="Nama Produk"
                                    value="{{ @old('nama_produk') }}" required>
                            </div>
                            <div class="form-group">
                                <label for="deskripsi">Deskripsi *</label>
                                <textarea class="form-control @error('deskripsi') is-invalid @enderror" id="deskripsi" name="deskripsi"
                                   rows="4" required>{{ @old('deskripsi') }}</textarea>
                            </div>
                            <div class="form-group">
                                <label><strong>Foto Produk (Multiple Images/Bisa Banyak Foto)</strong></label>
                                <div class="custom-file">
                                    <input type="file" name="foto[]" multiple class="custom-file-input form-control"
                                        id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                </div>
                            </div>
                            <div class="form-group my-3">
                                <label for="youtube_video">Link Sematkan Video Youtube</label>
                                <div class="row">
                                    <div class="col-10">
                                        <input type="text" class="form-control" id="youtube_video"
                                            placeholder="contoh: https://youtube.com/video1" r>
                                    </div>
                                    <div class="col-2">
                                        <button class="btn btn-primary" type="button" onclick="show_badge('embed')"><i
                                                class="fa fa-plus-circle"></i></button>
                                    </div>
                                </div>
                                {{-- <div class="row">
                                    <div class="col-3">
                                        Link Sematan anda: </div>
                                    <div class="col-8" id="resultembed"></div>
                                    <div id="resultformembed"></div>
                                </div> --}}
                            </div>
                            <div class="form-group">
                                <label><strong>Stok *</strong></label>
                                @if (@old('size'))
                                    @foreach (@old('stok') as $size)
                                        <div class="row mt-3">
                                            <div class="col-5">
                                                <select
                                                    class="form-control @error('size.' . $loop->index) is-invalid @enderror"
                                                    id="size" name="size[]" required>
                                                    <option value="">-Pilih Size Produk-</option>
                                                    @foreach ($sizes as $size)
                                                        <option
                                                            {{ @old('size')[$loop->index] == $size->id ? 'selected' : '' }}
                                                            value="{{ $size->id }}">{{ $size->size_name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-5">
                                                <input type="text"
                                                    class="form-control @error('stok.' . $loop->index) is-invalid @enderror"
                                                    id="stok" name="stok[]" placeholder="Jumlah Stok" required
                                                    value="{{ @old('stok')[$loop->index] }}">
                                            </div>
                                            <div class="col-2">
                                                <button class="btn btn-primary" type="button" onclick="add_sizeform()"><i
                                                        class="fa fa-plus-circle"></i></button> <button
                                                    class="btn btn-danger" type="button" id="minus"><i
                                                        class="fa fa-minus-circle"></i></button>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <div class="row">
                                        <div class="col-5">
                                            <select class="form-control" id="size" name="size[]" required>
                                                <option value="">-Pilih Size Produk-</option>
                                                @foreach ($sizes as $size)
                                                    <option value="{{ $size->id }}">{{ $size->size_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-5">
                                            <input type="text" class="form-control" id="stok" name="stok[]"
                                                placeholder="Jumlah Stok" required>
                                        </div>
                                        <div class="col-2">
                                            <button class="btn btn-primary" type="button" onclick="add_sizeform()"><i
                                                    class="fa fa-plus-circle"></i></button>
                                        </div>
                                    </div>
                                @endif
                                <div id="formna"></div>
                            </div>
                            <div class="form-group">
                                <label for="kategori">Kategori *</label>
                                <div class="row">
                                    <div class="col-12">
                                        <select class="form-control selectsearch @error('kategori') is-invalid @enderror"
                                            multiple id="kategori" data-live-search="true" name="kategori[]">
                                            {{-- <option value="">-Pilih Kategori Produk-</option> --}}
                                            @foreach ($categories as $category)
                                                <option {{ @old('kategori') == $category->id ? 'selected' : '' }}
                                                    value="{{ $category->id }}">
                                                    {{ $category->category_name }}
                                                </option>
                                                {{-- <option value="{{ $category->id }}|{{ $category->category_name }}">
                                                    {{ $category->category_name }}
                                                </option> --}}
                                            @endforeach
                                        </select>
                                    </div>
                                    {{-- <div class="col-2"> <button class="btn btn-primary" type="button"
                                            onclick="show_badge('kategori')"><i class="fa fa-plus-circle"></i></button>
                                    </div> --}}
                                </div>
                                <div class="row">
                                    <div class="col-3">
                                        Kategori: </div>
                                    <div class="col-8" id="resultkategori"></div>
                                    <div id="resultformkategori"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="harga_beli"> Harga Beli *</label>
                                        <input type="text" class="form-control @error('harga_beli') is-invalid @enderror"
                                            id="harga_beli" name="harga_beli" placeholder="Harga Beli"
                                            value="{{ @old('harga_beli') }}" required>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="harga_jual">Harga Jual *</label>
                                        <input type="text" class="form-control @error('harga_jual') is-invalid @enderror"
                                            id="harga_jual" name="harga_jual" placeholder="Harga Jual" required
                                            value="{{ @old('harga_jual') }}">
                                    </div>
                                </div>
                                <div class="col-sm-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="diskon">Diskon</label>
                                        <input type="text" class="form-control" id="diskon" name="diskon"
                                            value="{{ @old('diskon') }}" placeholder="Diskon">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status">Status *</label>
                                <select class="form-control @error('status') is-invalid @enderror" id="status" name="status"
                                    required>
                                    <option {{ @old('status') == '' ? 'selected' : '' }} value="">-Pilih Status Produk-
                                    </option>
                                    <option {{ @old('status') == 'Diposting' ? 'selected' : '' }} value="Diposting">
                                        Diposting</option>
                                    <option {{ @old('status') == 'Belum Diposting' ? 'selected' : '' }}
                                        value="Belum Diposting">Belum Diposting</option>
                                </select>
                            </div>

                            <div class="form-group text-right">
                                <button type="reset" class="btn btn-danger">Reset</button>
                                <button type="button" class="btn btn-primary" onclick="submit_data()">Submit</button>
                            </div>
                    </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.1/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {
            tinymce.init({
                selector: 'textarea',
                height: 200,
                menubar: false,
                plugins: [
                    'advlist autolink lists link charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime  table contextmenu paste code wordcount'
                ],
                mobile: {
                    theme: 'mobile'
                },
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],
                setup: function(editor) {
                    editor.on('change', function() {
                        editor.save();
                    });
                },
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });
            $('.selectsearch').selectpicker();
            $('input[type="file"]').on("change", function() {
                let filenames = [];
                let files = this.files;
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            });
        });

        function add_sizeform() {
            $('#formna').append(`<div class="row mt-3" id="stok-size">
                                <div class="col-5">
                                    <select class="form-control" id="size" name="size[]">
                                        <option value="">-Pilih Size Produk-</option>
                                        @foreach ($sizes as $size)
                                            <option value="{{ $size->id }}">{{ $size->size_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-5">
                                    <input type="number" class="form-control" id="stok" name="stok[]"
                                        placeholder="Jumlah Stok">
                                </div>
                                <div class="col-2">
                                    <button class="btn btn-danger" type="button" id="minus"><i class="fa fa-minus-circle"></i></button>
                                </div>
                            </div>  `);
        }

        $('#formna').on('click', '#minus', function(e) {
            e.preventDefault();

            $(this).parent().parent().remove();
        });

        var dataembed = [];
        var datakategori = [];
        var isidatakategori = [];

        function submit_data() {
            if ($("#form-tambah-produk")[0].checkValidity() && datakategori) {
                $('#form-tambah-produk').submit()
            } else {
                Swal.fire("Close", "Mohon Isi Semua Field bertanda Bintang!", "error");
                // $('#form-tambah-produk').submit()
            }
        }


        function show_badge(type) {
            if (type == 'embed') {
                dataembed.push($("#youtube_video").val());
                $("#youtube_video").val('');

                $('#result' + type).html('');
                $('#resultform' + type).html('');
                $.each(dataembed, function(index, val) {
                    // console.log(index, val)
                    $('#result' + type).append(
                        `<a href="javascript:void(0)" class="badge badge-lg badge-success mx-1" id="badgeembed` +
                        index +
                        `" onclick="hapus_badge('embed',` + index + `)"> ` + val +
                        ` <i class="fa fa-times"></i></a>`)
                    $('#resultform' + type).append('<input type="hidden" name="embed_youtube[]" id="formembed' +
                        index + '"value="' + val + '">')
                });
            } else if (type == 'kategori') {
                datakategori.push($("#kategori").val());
                isidatakategori.push($('#kategori').data("isi1"));
                console.log(isidatakategori);
                $("#kategori").val('');

                $('#result' + type).html('');
                $('#resultform' + type).html('');
                $.each(datakategori, function(index, val) {
                    var isi = val.split("|");
                    $('#result' + type).append(
                        `<a href="javascript:void(0)" class="badge badge-lg badge-success mx-1" id="badgekategori` +
                        index +
                        `" onclick="hapus_badge('kategori',` + index + `)"> ` + isi[1] +
                        ` <i class="fa fa-times"></i></a>`)
                    $('#resultform' + type).append(`<input type="hidden" name="kategori[]" id="formkategori` +
                        index + `" value="` + isi[0] + `">`)
                });
            }
        }

        function hapus_badge(type, index) {
            if (type == 'embed') {
                $('#badgeembed' + index).remove();
                $('#formembed' + index).remove();
            } else if (type == 'kategori') {
                $('#badgekategori' + index).remove();
                $('#formkategori' + index).remove();
            }
        }
    </script>
@endsection
