<?php

namespace App\Http\Controllers;

use App\Models\BannerStore;
use App\Models\Review;
use App\Models\StoreSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\File;
use RajaOngkir;
use Yajra\DataTables\Facades\DataTables;

class StoreSettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $banner = new BannerStore;
        $banner_toko = $banner->all();
        $rajaOngkir = new RajaOngkir;
        $toko = StoreSetting::where(['id' => '1'])->first();
        $provinsi = $rajaOngkir->get_data_provinsi_kota('provinsi');
        return view('layout_admin.store_setting', compact('toko', 'provinsi', 'banner_toko'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\StoreSetting  $storeSetting
     * @return \Illuminate\Http\Response
     */
    public function show(StoreSetting $storeSetting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\StoreSetting  $storeSetting
     * @return \Illuminate\Http\Response
     */
    public function edit(StoreSetting $storeSetting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\StoreSetting  $storeSetting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StoreSetting $storeSetting)
    {
        // echo json_encode($request->all());die;

        $validate = Validator::make($request->all(), [
            'nama_toko' => 'required',
            'alamat' => 'required',
            'email' => 'required',
            'telepon' => 'required',
            'fax' => 'required',
            'kota' => 'required|not_in:0',
            'provinsi' => 'required|not_in:0',
            'no_pos' => 'required',
            'fb' => 'required',
            'shopee' => 'required',
            'tokopedia' => 'required',
            'instagram' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Store Setting.', 'data' => $validate->errors()]);
        }

        $kota = explode('|', request('kota'));
        $provinsi = explode('|', request('provinsi'));
        $update =  StoreSetting::where(['type' => 'alamat'])->update([
            'nama_toko' => request('nama_toko'),
            'alamat' => request('alamat'),
            'email' => request('email'),
            'telepon' => request('telepon'),
            'fax' => request('fax'),
            'id_provinsi' => $provinsi[0],
            'provinsi' => $provinsi[1],
            'id_kota' => $kota[0],
            'kota' => $kota[1],
            'kode_pos' => request('no_pos'),
            'fb' => request('fb'),
            'shopee' => request('shopee'),
            'tokopedia' => request('tokopedia'),
            'instagram' => request('instagram'),
        ]);
        // echo json_encode($update);
        // if($update == 1){
        return response()->json(['status' => true, 'msg' => 'Berhasil ubah setting store']);
        // }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\StoreSetting  $storeSetting
     * @return \Illuminate\Http\Response
     */
    public function destroy(StoreSetting $storeSetting)
    {
        //
    }


    public function update_banner(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'foto_banner' => 'image|mimes:jpeg,png,jpg|max:10000',
            'title' => 'required|max:30',
            'deskripsi' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Banner.', 'data' => $validate->errors()]);
        }
        $banner = new BannerStore;
        $check =  $banner->find($id);
        $imgName = $check->foto_banner;
        if ($check) {
            if ($request->hasFile('foto_banner') && $request->foto_banner != '') {
                $path_old = public_path('assets_frontend/images/slider/') .  $check->foto_banner;
                if (file_exists($path_old) && $check->foto_banner != null && $check->foto_banner != '3.jpg' && $check->foto_banner != '2.jpg') {
                    unlink($path_old);
                }
                $image = $request->file('foto_banner');
                $imgName = $image->hashName();
                $image->move('assets_frontend/images/slider/', $imgName);
            }

            if ($check) {
                $data = [];
                $data['foto_banner'] = $imgName;
                $data['title'] = $request->title;
                $data['deskripsi'] = $request->deskripsi;
                $check->fill($data)->save();
            }
        }

        return response()->json([
            'status' => true,
            'data' => $check
        ]);
    }

    public function update_banner_status(Request $request, $id)
    {
        $banner = new BannerStore;
        $check = $banner->where(['status' => 'off'])->get();
        $banner_check = $banner->find($id);
        if (count($check) >= 1 && $request->status == 'on') {
            return response()->json([
                'status' => false,
                'msg' => "Gagal, banner minimal satu harus ada yang aktif",
                'text' => 'btn btn-primary mt-2 btn-status',
                'data' => $banner_check
            ]);
        } else if (count($check) >= 1 && $request->status == 'off') {
            $banner_check->fill(['status' => 'on'])->save();
            return response()->json([
                'status' => true,
                'msg' => "Banner diaktifkan",
                'text' => 'btn btn-primary mt-2 btn-status',
                'data' => $banner_check
            ]);
        } else {
            $banner_check->fill(['status' => 'off'])->save();
            return response()->json([
                'status' => true,
                'msg' => "Banner dinonaktifkan",
                'text' => 'btn btn-danger mt-2 btn-status',
                'data' => $banner_check
            ]);
        }
    }

    function background_testimoni(Request $request)
    {
        if ($request->hasFile('background_testimoni')) {
            if (StoreSetting::where(['id' => 1])->first()->background_testimoni != null) {
                $image_path = 'uploads/' . StoreSetting::where(['id' => 1])->first()->background_testimoni;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $allowedfileExtension = ['jpg', 'png', 'jpeg'];
            $file = $request->file('background_testimoni');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $filename = $file->store('background_testimoni', ['disk' => 'public_uploads']);
            $hasil = StoreSetting::where(['id' => 1])->update([
                'background_testimoni' => $filename
            ]);
            if ($hasil) {
                return ['success' => true, 'image' => $filename];
            } else {
                return ['success' => false];
            }
        } else {
            return ['success' => false, 'message' => 'kolom foto harap diisi'];
        }
    }

    public function reviews()
    {
        $data = Review::select("id", "name", "star", "image", "review", "status")->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $actionBtn = ' <a href="javascript:void(0)" class="primary btn btn-primary btn-sm" onclick="edit(' . $row->id . ')"><i class="fa fa-edit"></i></a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" onclick="hapus(' . $row->id . ')"><i class="fa fa-trash"></i></a>';
                return $actionBtn;
            })
            ->addColumn('reviews', function ($row) {
                $review = '<div class="row">';
                $bintang = $row->star;
                for ($i = 5; $i > 0; $i--) {
                    if ($i <= $bintang) {
                        $rate[$i] = '<i class="fa fa-star text-warning"></i>';
                    } else {
                        $rate[$i] = '<i class="fa fa-star "></i>';
                    }
                }
                if ($row->status) {
                    $status = '<span class="badge badge-success">Diposting</span>';
                } else {
                    $status = '<span class="badge badge-danger">Belum DIposting</span>';
                }
                if ($row->image != null) {
                    $review .= '
                    <div class="row">
                        <div class="col-4">
                            <img src="' . asset('uploads/' . $row->image) . '" width="100%"/>
                        </div>
                        <div class="col-8">
                            <h6>' . $row->name . '</h6>
                            <label>' . $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5] . '</label> ' . $status . '
                            ' . $row->review . '
                        </div>
                    </div> ';
                }
                $review .= '</div>';
                return  $review;
            })
            ->rawColumns(['action', 'reviews'])
            ->make(true);
    }

    public function make_review(Request $request)
    {
        if ($request->hasFile('image')) {
            $allowedfileExtension = ['jpg', 'png', 'jpeg'];
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $filename = $file->store('image', ['disk' => 'public_uploads']);
            $hasil = Review::create([
                'name' => request('nama'),
                'star' => request('rating'),
                'review' => request('review'),
                'status' => request('status'),
                'image' => $filename
            ]);

            if ($hasil) {
                return ['success' => true, 'image' => $filename];
            } else {
                return ['success' => false];
            }
        } else {

            return ['success' => false, 'message' => 'kolom foto harap diisi'];
        }
    }

    public function review($id)
    {
        return Review::where(['id' => $id])->first();
    }

    public function edit_review($id, Request $request)
    {
        if ($request->hasFile('image')) {
            if (Review::where(['id' => $id])->first()->image != null) {
                $image_path = 'uploads/' . Review::where(['id' => 1])->first()->image;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
            $allowedfileExtension = ['jpg', 'png', 'jpeg'];
            $file = $request->file('image');
            $filename = $file->getClientOriginalName();
            $extension = $file->getClientOriginalExtension();
            $filename = $file->store('image', ['disk' => 'public_uploads']);
            if (Review::where(['id' => $id])->first()->image != null) {
                $image_path = 'uploads/' . Review::where(['id' => 1])->first()->image;
                if (File::exists($image_path)) {
                    File::delete($image_path);
                }
            }
        } else {
            $hasil = Review::where(['id' => $id])->update([
                'name' => request('nama'),
                'star' => request('rating'),
                'review' => request('review'),
                'status' => request('status'),
            ]);
        }
        if ($hasil) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}