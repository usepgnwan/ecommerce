@extends('frontend.template_frontend.frontend')

@section('title', 'Home')

@section('content')
    <main class="ps-main">
        <div class="ps-banner">
            @include('frontend.partial_home.banner_frontend')
        </div>
        <div class="ps-section ps-section--top-sales ps-owl-root pt-80 pb-80">
            @include('frontend.partial_home.new_product', [
                'title' => 'New Product',
                'produk' => $produk,
            ])
        </div>
        <div class="ps-section--offer">
            <div class="ps-column">
                <a class="ps-offer" href="#">
                    <img src="{{ asset('assets_frontend/images/banner_promo/'. $banner_promo->foto_banner_1) }}" alt=""></a>
            </div>
            <div class="ps-column">
                <a class="ps-offer" href="#">
                    <img src="{{ asset('assets_frontend/images/banner_promo/'. $banner_promo->foto_banner_2) }}" alt=""></a>
            </div>
        </div>
        <div class="ps-section ps-section--top-sales ps-owl-root pt-80 pb-80">
            @include('frontend.partial_home.top_sales')
            {{-- @include('frontend.partial_home.new_product',['title'=>'Top Sales', 'produk'=> $produk]) --}}
        </div>
        <div class="ps-home-testimonial bg--parallax pb-80"
            data-background="{{ asset('uploads/' . $store_setting->background_testimoni) }}">
            <div class="container">
                <div class="owl-slider" data-owl-auto="true" data-owl-loop="false" data-owl-speed="5000"
                    data-owl-gap="0" data-owl-nav="false" data-owl-dots="true" data-owl-item="1" data-owl-item-xs="1"
                    data-owl-item-sm="1" data-owl-item-md="1" data-owl-item-lg="1" data-owl-duration="1000"
                    data-owl-mousedrag="on" data-owl-animate-in="fadeIn" data-owl-animate-out="fadeOut">
                    @foreach ($reviews as $review)
                        <div class="ps-testimonial">
                            <div class="ps-testimonial__thumbnail"><img style="width: 100px; height: 100px;" src="{{ asset('uploads/' . $review->image) }}"
                                    alt=""><i class="fa fa-quote-left"></i></div>
                            <header>
                                <select class="ps-rating">
                                    <option value="{{ $review->star }}">1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                                <p>{{ $review->name }}</p>
                            </header>
                            <footer>
                                {!! $review->review !!}
                            </footer>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>
        <div class="container">
            <!-- <div class="ps-home-contact container"> -->
            {{-- <div id="contact-map" data-address="New York, NY" data-title="BAKERY LOCATION!" data-zoom="17"></div> --}}
            @include('frontend.partial_home.form_contact')
        </div>
    @endsection
