@extends('template_admin.admin')

@section('title', 'Metode Pembayaran')
@section('container')
<div class="row mt-3">
    <!-- Datatables -->
    <div class="col-lg-12">
        <div class="from-group mb-2">
            <a class="btn btn-primary btn-pembayaran" value="new" href="javascript:void(0)"><i
                    class="fa fa-plus-circle"></i> Tambah</a>
        </div>
        {{-- <div class="from-group mb-2">
            <button class="btn btn-primary" onclick="tambah()" value="new">Tambah Data</button>
        </div> --}}
        <div class="card mb-4">
            <div class="card-header">Metode Pembayaran</div>
            {{-- <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
            </div> --}}
            <div class="table-responsive p-3">
                <table class="table align-items-center table-flush" id="dataTableNoRek" style="width: 100">
                    <thead class="thead-light">
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Kode</th>
                            <th>No.Rek</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Modal Pembayaran -->
<div id="modal_pembayaran" class="modal" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalCenterTitle" aria-hidden="true" style="overflow: auto !important;">
    <div class="modal-dialog modal-dialog-centered " role="document">
        <div class="modal-content change-address">
            <div class="modal-header">
                <h5 class="modal-title-pembayaran" id="exampleModalLabel">Tambah Metode Pembayaran</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="" id="form-pembayaran">
                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Nama</label>
                        <input type="hidden" class="form-control" id="id_pembayaran" name="id_pembayaran">
                        <select name="nama_pembayaran" id="nama_pembayaran" class="form-control" id="">
                            <option value="0"> Pilih</option>
                            <option value="assets_frontend/images/payment/bca.svg"> BCA</option>
                            <option value="assets_frontend/images/payment/bjb.svg"> BJB</option>
                            <option value="assets_frontend/images/payment/BNI.svg"> BNI</option>
                            <option value="assets_frontend/images/payment/cimb.webp"> CIMB</option>
                            <option value="assets_frontend/images/payment/dana.svg"> DANA</option>
                            <option value="assets_frontend/images/payment/fastpay.png"> Fastpay</option>
                            <option value="assets_frontend/images/payment/gopay.svg"> GOPAY</option>
                            <option value="assets_frontend/images/payment/mandiri.svg"> MANDIRI</option>
                            <option value="assets_frontend/images/payment/ovo.svg"> OVO</option>
                            <option value="assets_frontend/images/payment/shopeepay.svg"> Shopeepay</option>
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="">Kode</label>
                        <input type="text" class="form-control" id="kode_pembayaran" name="kode_pembayaran">
                    </div>
                    <div class="form-group">
                        <label for="">No.Rek</label>
                        <input type="text" class="form-control" id="norek_pembayaran"
                            name="norek_pembayaran">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary add-pembayaran" value="0">Simpan</button>
                    <button type="button" class="btn btn-secondary close-address"
                        data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
       // Bagian pembayaran
       let dataTableNoRek
        $(document).ready(function() {
            // norek
            dataTableNoRek = $('#dataTableNoRek').DataTable({
                prossecing: true,
                serverSide: true,
                ajax: "{{ route('metode_pembayaran.index') }}",
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'kode',
                        name: 'kode'
                    },
                    {
                        data: 'no_rek',
                        name: 'no_rek'
                    },
                    {
                        data: 'action',
                        name: 'action'
                    }
                ]
            });


            $(document).on('click', '.btn-pembayaran', function() {
                $('#modal_pembayaran').modal('show');
                $('#form-pembayaran')[0].reset();
                $('.modal-title-pembayaran').html('Tambah Metode Pembayaran');
                $('.add-pembayaran').val(0);
            });
            $(document).on('click', '.edit-pembayaran', function() {
                $('#modal_pembayaran').modal('show');
                let id = $(this).data('id');
                $('.modal-title-pembayaran').html('Edit Metode Pembayaran');
                $('.add-pembayaran').val(1);
                let url = "{{ route('metode_pembayaran.edit', ['metode_pembayaran' => ':id']) }}";
                url = url.replace(":id", id);
                $.ajax({
                    method: 'GET',
                    url: url,
                    success: function(data) {
                        if (data.status == true) {
                            toastr.success(data.msg);
                            $('#modal_pembayaran').modal('show');
                            $('#id_pembayaran').val(data.data.id);
                            $('#nama_pembayaran').val(data.data.nama);
                            $('#kode_pembayaran').val(data.data.kode);
                            $('#norek_pembayaran').val(data.data.no_rek);
                        } else {
                            toastr.warning(data.msg);
                        }
                    }
                });
            });
            $(document).on('click', '.hapus-pembayaran', function() {
                let id = $(this).data('id');
                Swal.fire({
                    title: 'Apakah anda yakin?',
                    text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Hapus',
                    cancelButtonText: 'Batal',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $(this).parent().closest('.address').remove();
                        let url =
                            "{{ route('metode_pembayaran.destroy', ['metode_pembayaran' => ':id']) }}";
                        url = url.replace(":id", id);
                        $.ajax({
                            method: 'DELETE',
                            url: url,
                            success: function(data) {
                                if (data.status == true) {
                                    toastr.success(data.msg);
                                    dataTableNoRek.ajax.reload();
                                } else {
                                    toastr.error('Gagal, Hapus data.');
                                }
                            }
                        });
                    }
                });
            });
        });


        $(document).on('click', '.add-pembayaran', function(e) {
            e.preventDefault();
            let data = $('#form-pembayaran').serializeArray();
            let check = $('.add-pembayaran').val();
            let url;
            let method;
            if (check == 0) {
                url = "{{ route('metode_pembayaran.store') }}";
            } else if (check == 1) {
                let id = $('#id_pembayaran').val();
                url = "{{ route('metode_pembayaran.update', ['metode_pembayaran' => ':id']) }}";
                url = url.replace(":id", id);
                data = data.concat({
                    name: "_method",
                    value: "PUT"
                });
            }

            $.ajax({
                url: url,
                data: data,
                method: 'Post',
                dataType: 'JSON',
                success: function(data) {
                    if (data.status) {
                        $(".text-error-nama_pembayaran").remove();
                        $(".text-error-kode_pembayaran").remove();
                        $(".text-error-norek_pembayaran").remove();
                        toastr.success(data.msg);
                        $('#form-pembayaran')[0].reset();
                        $('#modal_pembayaran').modal('hide');
                        dataTableNoRek.ajax.reload();

                    } else {
                        $(".text-error-nama_pembayaran").remove();
                        $(".text-error-kode_pembayaran").remove();
                        $(".text-error-norek_pembayaran").remove();
                        $.each(data.data, function(field_name, error) {
                            $(document).find('[name=' + field_name + ']').after(
                                '<small class="text-error-' + field_name +
                                ' text-danger ">' + error + '</small>')
                        });
                        toastr.warning(data.msg);
                    }
                }
            });
        });

        $(document).on('click', '.on-pembayaran', function(e) {
            e.preventDefault();
            let $this = $(this);
            let id = $this.data('id');
            let status = $this.data('status');
            let url = '{{ route('metode_pembayaran.update_status', ['id' => ':id']) }}';
            url = url.replace(":id", id);
            $.ajax({
                url: url,
                data: {
                    'status': status,
                    '_method': "PUT"
                },
                method: 'POST',
                success: function(data) {
                    if (data.status) {
                        Swal.fire(
                            'Berhasil!',
                            data.msg,
                            'success'
                        );
                        dataTableNoRek.ajax.reload();
                    } else {
                        toastr.error(data.msg);
                    }
                }
            });
        });

</script>
@endsection
