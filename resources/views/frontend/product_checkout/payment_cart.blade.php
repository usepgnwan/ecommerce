@extends('frontend.template_frontend.frontend')
@section('title', 'Product Payment Checkout')
@section('content')
    @php
    if (session('chart_checkout')) {
        $dataCheckout = session()->get('chart_checkout');
        $quantityCo = array_sum(array_column($dataCheckout, 'quantity'));
        $totalHargaCo = array_sum(array_column($dataCheckout, 'total_harga'));
    } else {
        $quantityCo = 0;
        $totalHargaCo = 0;
        $dataCheckout = [];
    }
    @endphp
    <div class="container-fluid">
        <div class="ps-checkout pt-50 pb-80">
            <div class="ps-container order_payment">
                <form>
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <div class="ps-shipping mb-10"
                                style=" padding:30px; background-color:##f7f7f7; border: 1px solid #e5e5e5;">
                                <div class="row no-gutters">
                                    <div class="col-md-12">
                                        <div class="card-body">
                                            <h3>FITUR COD</h3>
                                            <p>Order dapat dilakukan secara COD, Konfirmasi saat pesanan telah diselesaikan
                                                dan akan di
                                                arahkan ke whatsapp admin yang telah tersedia</p>
                                            <h3>NOTE</h3>
                                            <p>JANGAN LUPA ID PESANAN DARI ORDERAN KAMU DISIMPAN YA, AGAR KAMU DAPAT
                                                MEMANTAU
                                                ORDERAN DI MENU CHECK ORDER.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if (count($dataCheckout) != 0)
                                <style>
                                    .bootstrap-select {
                                        width: 100% !important;
                                        color: #999;
                                        opacity: 1;
                                    }

                                </style>
                                <div class="ps-checkout__billing"
                                    style=" padding:30px; background-color:##f7f7f7; border: 1px solid #e5e5e5;">
                                    <h3>Informasi Saya</h3>
                                    <div class="form-group form-group--inline">
                                        <label>Nama Depan<span>*</span>
                                        </label>
                                        <input class="form-control" type="text" name="nama_depan" id="nama_depan">
                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>Nama Belakang<span>*</span>
                                        </label>
                                        <input class="form-control" type="text" name="nama_belakang" id="nama_belakang">
                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>No.Telp / Wa<span>*</span>
                                        </label>
                                        <input class="form-control" type="text" name="no_telp" id="no_telp">
                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>Email<span>*</span>
                                        </label>
                                        <input class="form-control" type="email" name="email" id="email">
                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>Provinsi<span>*</span>
                                        </label>
                                        {{-- <input class="form-control" type="text"> --}}
                                        <select name="provinsi" id="" class="provinsi" data-live-search="true">
                                            <option value="0" disabled selected>- Pilih -</option>
                                            @foreach ($provinsi['results'] as $item)
                                                <option value="{{ $item['province_id'] }}|{{ $item['province'] }}">
                                                    {{ $item['province'] }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>Kota<span>*</span>
                                        </label>
                                        <div class="kota-card-proses" hidden>
                                            <span>
                                                <i class="fa fa-spinner fa-spin"></i>
                                            </span>
                                        </div>
                                        <div class="kota-card" hidden="false">
                                            <select name="kota" id="kota" class="provinsi" data-live-search="true">
                                                <option value="">- Pilih -</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>No.Pos<span>*</span>
                                        </label>
                                        <input class="form-control" type="text" name="no_pos" id="no_pos">
                                    </div>
                                    <div class="form-group form-group--inline">
                                        <label>Alamat Lengkap<span>*</span>
                                        </label>
                                        <textarea class="form-control" rows="2"
                                        placeholder="Masukan alamat lengkap" name="alamat_lengkap" id="alamat_lengkap"></textarea>

                                    </div>
                                    {{-- <div class="form-group">
                                        <div class="ps-checkbox">
                                            <input class="form-control" type="checkbox" id="cb01">
                                            <label for="cb01">Create an account?</label>
                                        </div>
                                    </div> --}}
                                    <h3 class="mt-40"> Informasi Tambahan</h3>
                                    <div class="form-group form-group--inline textarea">
                                        <label>Note</label>
                                        <textarea class="form-control" rows="5"
                                            placeholder="Masukan catatan pesanan anda disini.." name="catatan" id="catatan"></textarea>
                                    </div>
                                </div>
                            @else
                                <div class="ps-checkout__billing"
                                    style=" padding:30px; background-color:##f7f7f7; border: 1px solid #e5e5e5;">
                                    <div class="row no-gutters">
                                        <div class="col-md-12">
                                            <div class="card-body">
                                                <div class="text-center pb-20" style="color:#2ac37d">
                                                    <h4 style="color:#2ac37d !important">Tas Belanja anda masih kosong!</h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            @endif
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 ">
                            {{-- <div class="ps-shipping mb-10">
                                <h3>FITUR COD</h3>
                                <p>Order dapat dilakukan secara COD, Konfirmasi saat pesanan telah diselesaikan dan akan di
                                    arahkan ke whatsapp admin yang telah tersedia</p>
                                <h3>NOTE</h3>
                                <p>JANGAN LUPA ID PESANAN DARI ORDERAN KAMU DISIMPAN YA, AGAR KAMU DAPAT MEMANTAU
                                ORDERAN DI MENU CHECK ORDER.</p>
                            </div> --}}
                            <div class="ps-checkout__order">
                                <header>
                                    <h3>Orderan Saya</h3>
                                </header>
                                <div class="content">
                                    @php
                                        $voucher = 0;
                                    @endphp
                                    @if (count($dataCheckout) != 0)
                                        <table class="table ps-checkout__products">
                                            <thead>
                                                <tr>
                                                    <th class="text-uppercase">Product</th>
                                                    <th class="text-uppercase">Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($dataCheckout as $k => $c)
                                                    <tr>
                                                        <td>{{ $dataCheckout[$k]['nama_produk'] }}
                                                            x {{ $dataCheckout[$k]['quantity'] }} <br>
                                                            <small>
                                                                @if ($dataCheckout[$k]['diskon'] != null || $dataCheckout[$k]['diskon'] != '')
                                                                    <del>Rp.
                                                                        {{ number_format($dataCheckout[$k]['harga'], 0, ',', '.') }}</del>
                                                                    &nbsp;
                                                                    Rp.
                                                                    {{ number_format($dataCheckout[$k]['harga_diskon'], 0, ',', '.') }}
                                                                @else
                                                                    Rp.
                                                                    {{ number_format($dataCheckout[$k]['harga'], 0, ',', '.') }}
                                                                @endif
                                                                &nbsp; Size: {{ $dataCheckout[$k]['size'] }}
                                                            </small>
                                                        </td>
                                                        <td> Rp.
                                                            {{ number_format($dataCheckout[$k]['total_harga'], 0, ',', '.') }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <table class="table ps-checkout__products">
                                            <thead>
                                                <tr>
                                                    <th class="text-uppercase">Kurir</th>
                                                    <th class="text-uppercase">Ongkos Kirim</th>
                                                </tr>
                                            </thead>
                                            <tbody class="kurir-ongkir">
                                            </tbody>
                                        </table>
                                        <table class="table ps-checkout__products">
                                            <tbody>
                                                <tr>
                                                    <td>Sub Total</td>
                                                    <td> Rp. {{ number_format($totalHargaCo, 0, ',', '.') }} </td>
                                                </tr>
                                                @php
                                                    $epromo = is_null(session()->get('e-promo'))  ? [] : session()->get('e-promo');
                                                    $checkPromo = count($epromo) != 0  ? true : false;
                                                @endphp
                                                <tr>
                                                    <td>Promo / E-Voucher</td>
                                                    <td><span id="text-epromo">@if ($checkPromo)
                                                        Evoucher Tersedia @else - <input type="hidden" name="evoucher_payment[]" value="0" readonly>
                                                    @endif</span></td>
                                                </tr>

                                                @if ($checkPromo)
                                                    @foreach ($epromo as $i => $p )
                                                        @php
                                                            $labeldisc ='';
                                                            $namapromo='';
                                                            $check =  explode('.', $i);
                                                            if($check[0] =='potongan'){
                                                                $labeldisc = 'Potongan'  . ' -Rp. ' .number_format($p->promo, 0, ',', '.');
                                                                $namapromo = $p->nama_promo;
                                                            }else if($check[0] =='diskon'){
                                                                $disc =  ($totalHargaCo * $p->promo ) / 100;
                                                                $labeldisc =  'Diskon'  .   ' -Rp. ' . number_format($disc, 0, ',', '.');
                                                                $namapromo = $p->nama_promo . '(' .$p->promo. '%)';
                                                            }

                                                            $voucherCount = [];
                                                            if(count($epromo) != 0){
                                                                foreach($epromo as $ix => $val){
                                                                    $check = explode('.', $ix);
                                                                    if($check[0] =='diskon'){
                                                                        array_push($voucherCount, ($val->promo * $totalHargaCo) /100);
                                                                    }else{
                                                                        array_push($voucherCount, $val->promo );
                                                                    }
                                                                }
                                                            }

                                                            $voucher = array_sum($voucherCount);
                                                        @endphp
                                                    <tr class="epormo-badge" data-id="{{$i}}">
                                                        <td>{{$p->type_voucher}} {{$namapromo}}</td>
                                                        <td>
                                                            <div class="col-8" id="resultembed">
                                                                <span href="javascript:void(0)" id="badgeembed0"
                                                                    class="badge badge-primary"
                                                                    style="background:#66bb6a !important;">
                                                                    {{$labeldisc}}
                                                                </span>
                                                                <input type="hidden" name="evoucher_payment[]" value="{{$p->id}}|{{$labeldisc}}" readonly>
                                                            </div>

                                                            </td>
                                                    </tr>
                                                    @endforeach
                                                @endif
                                                <tr>
                                                    <td>Ongkos Kirim</td>
                                                    <td><span class="ongkos-bayar">0</span> <input type="hidden" name="ongkir_payment" autocomplete="off" value="0" readonly></td>
                                                </tr>
                                                <tr style="border-top:1px solid #e7e7e7">
                                                    <td><b>Total Bayar</b></td>
                                                    <td><b><span class="total-harga-input">Rp. {{ number_format($totalHargaCo  - $voucher , 0, ',', '.') }}</span></b>
                                                        <input type="hidden" name="total_bayar_payment" autocomplete="off" id="total_bayar_payment" value="Rp. {{ number_format($totalHargaCo  - $voucher , 0, ',', '.') }}">
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    @else
                                        <h3>Belum Tersedia</h3>
                                    @endif
                                </div>
                                <footer>
                                    <h3>Metode Pembayaran</h3>
                                    <div class="form-group cheque">
                                        <div class="ps-radio">
                                            <input class="form-control" type="radio" id="rdo01" name="payment" checked="">
                                            {{-- <label for="rdo01">Cheque Payment</label> --}}
                                            <p>Pembayaran hanya dapat dilakukan dengan mentransfer dengan list No Rek
                                                Dibawah
                                                ini, Selain metode pembayaran dibawah ini pembayaran tidak dapat diproses.
                                                Hati-hati akan penipuan yang mengatas namakan
                                                {{ $store_setting->nama_toko }}</p>
                                            <table class="table ps-checkout__products">
                                                <thead>
                                                    <tr>
                                                        <th class="text-uppercase">Metode</th>
                                                        <th class="text-uppercase">No Rek</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if ($metode_pembayaran->count())
                                                        @foreach ($metode_pembayaran as $mp)
                                                            <tr>
                                                                <td><img style="max-width: 80px;"
                                                                        src="{{ asset($mp->nama) }} " alt=""></td>
                                                                <td>( @if ($mp->kode != null || $mp->kode != '')
                                                                        {{ $mp->kode }}
                                                                    @else
                                                                        -
                                                                    @endif ) {{ $mp->no_rek }}</td>
                                                            </tr>
                                                        @endforeach
                                                    @else
                                                        <tr>
                                                            <td colspan="2" style="text-align: center">Belum Tersedia</td>
                                                        </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="form-group paypal">
                                        {{-- <div class="ps-radio ps-radio--inline">
                                                <input class="form-control" type="radio" name="payment" id="rdo02">
                                                <label for="rdo02">Paypal</label>
                                            </div>
                                            <ul class="ps-payment-method">
                                                <li><a href="#"><img src="{{ asset('assets_frontend/images/payment/bca.svg')}} " alt=""></a></li>
                                                <li><a href="#"><img src="{{ asset('assets_frontend/images/payment/2.png')}}" alt=""></a></li>
                                                <li><a href="#"><img src="{{ asset('assets_frontend/images/payment/3.png')}}" alt=""></a></li>
                                            </ul> --}}
                                        <button type="submit" class="ps-btn ps-btn--fullwidth order_payment_submit" id="btn_pembayaran" style="margin-top: 0px!important;">Selesaikan  Pesanan<i class="ps-icon-next"></i></button>
                                    </div>
                                </footer>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- {{ $store_setting->k }} --}}
@endsection

@section('js')
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $(document).ready(function() {

            let qty = '{{ $quantityCo }}';
            let voucher = '{{$voucher}}';
            $('.kota-card').prop('hidden', false);

            $('.provinsi').selectpicker();
            $("[name='provinsi']").change(function() {
                let $provinsi = $(this).val();
                $('.kota-card').prop('hidden', true);
                $('.kota-card-proses').prop('hidden', false);
                if ($provinsi == 0) {
                    $("[name='kota']").html('');
                    $("[name='kota']").append(`<option value="0">- Pilih -</option>`);
                    $("[name='kota']").selectpicker('refresh');
                    $("[name='no_pos']").val('');
                } else {
                    $provinsi = $provinsi.split("|");
                    $("[name='no_pos']").val('');
                    kota($provinsi[0]);
                }

            });

            function kota($id, $checkid = null) {
                let url = '{{ route('get.kota', ['id_address' => ':id']) }}';
                url = url.replace(":id", $id);
                $.ajax({
                    url: url,
                    // data: form,
                    method: 'GET',
                    success: function(data) {
                        console.log(data.results);
                        $("[name='kota']").html('');
                        $.each(data.results, function(i, res) {
                            $('.kota-card').prop('hidden', false);
                            $('.kota-card-proses').prop('hidden', true);
                            if ($checkid == res.city_id) {
                                $("[name='kota']").append(
                                    `<option value="${res.city_id}|${res.city_name}" data-pos="${res.postal_code}" selected>${res.city_name}</option>`
                                );
                            } else {
                                $("[name='kota']").append(
                                    `<option value="${res.city_id}|${res.city_name}" data-pos="${res.postal_code}" >${res.city_name}</option>`
                                );
                            }
                        });
                        $("[name='kota']").selectpicker('refresh');
                        $("[name='kota']").first().change();
                    }
                });

            }

            $("[name='kota']").change(function() {
                $("[name='no_pos']").val($(this).find('option:selected').data("pos"));
                let url = '{{ route('product.checkout_cart_check_ongkir') }}';
                $('.kurir-ongkir').html(`
                    <tr>
                        <td colspan='2' style="text-align:center">
                            <span> <i class="fa fa-spinner fa-spin"></i> </span>
                        </td>
                    </tr>
                `);
                $.ajax({
                    url: url,
                    data: {
                        id_kota_pengirim: $(this).val(),
                        qty: qty
                    },
                    method: 'POST',
                    dataType: 'JSON',
                    success: function(data) {
                        $('.kurir-ongkir').html('');
                        $.each(data.results, function(i, val) {
                            // console.log(val.code);
                            $.each(val.costs, function(x, cos) {
                                $('.kurir-ongkir').append(`
                                    <tr>
                                        <td>
                                            <div class="ps-radio radio-kurir">
                                                <input class="form-control" type="radio" id="rdo${x}" name="kurir" value="${val.code}|${cos.service}|${cos.description}|${cos.cost[0].etd}|${cos.cost[0].value}">
                                                <label for="rdo${x}">${val.code} ${cos.service} </label>
                                                <p> <small>${cos.description} Estimasi : ${cos.cost[0].etd} Hari</small></p>
                                            </div>
                                           </td>
                                        <td>${formatRupiah(cos.cost[0].value)}</td>
                                    </tr>
                                `);
                                console.log(val.code + cos.service + cos.cost[0]
                                    .value);
                            });
                        });
                        $("[name='kurir']").first().click();
                    }
                });
            });

            $(document).on('click', "[name='kurir']", function() {
                let val = $(this).val().split('|');
                let $total = '{{ $totalHargaCo }}';
                let ongkir = formatRupiah(val[4]);
                $('.ongkos-bayar').html(ongkir);
                $('[name="ongkir_payment"]').val(ongkir);
                $total = parseInt($total) + parseInt(val[4]) - parseInt(voucher);
                $('.total-harga-input').html(formatRupiah($total));
                $('[name="total_bayar_payment"]').val(formatRupiah($total));
            });
        });

        function formatRupiah(angka) {
            var number_string = angka.toString(),
                split = number_string.split(","),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            // tambahkan titik jika yang di input sudah menjadi angka ribuan
            if (ribuan) {
                separator = sisa ? "." : "";
                rupiah += separator + ribuan.join(".");
            }

            rupiah = split[1] != undefined ? rupiah + "," + split[1] : rupiah;
            return "Rp. " + rupiah;
        }

        function validate_data(){
            $(".text-error-nama_depan").remove();
            $(".text-error-nama_belakang").remove();
            $(".text-error-no_telp").remove();
            $(".text-error-email").remove();
            $(".text-error-provinsi").remove();
            $(".text-error-kota").remove();
            $(".text-error-no_pos").remove();
            $(".text-error-alamat_lengkap").remove();
            $(".text-error-ongkir_payment").remove();
        }
        // transaksi
        $('.order_payment form').submit(function(e) {
          e.preventDefault();
          $('#btn_pembayaran').html('<span> <i class="fa fa-spinner fa-spin"></i> </span> &nbsp; Proses');
          $('#btn_pembayaran').prop('disabled',true);
            let data = $('.order_payment form').serializeArray();
            $.ajax({
                url: '{{route("transaksi.store")}}',
                data: data,
                method: 'POST',
                dataType: 'JSON',
                start_time: new Date().getTime(),
                success: function(data) {
                    setTimeout(() => {
                        if(!data.status && !data.status_product){
                            validate_data();
                            Swal.fire(
                                'Error',
                                data.msg,
                                'error');
                        }else if(!data.status && data.status_product){
                            validate_data();
                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<span class="text-error-' + field_name +
                                    ' text-danger ">' + error + '</span>')
                            });

                            Swal.fire(
                                'Error',
                                data.msg,
                                'error');
                        }else if(data.status && data.status_product){
                            validate_data();
                            toastr.success(data.msg);
                            $('.order_payment form')[0].reset();
                            const usp = new URLSearchParams(data.data)

                            // window.location.href = '{{url("products/konfirmasi_pesanan")}}' + '?id=' + data.data.id + '&id_pesanan=' + data.data.id_pemesanan + '&pemesan=' + data.data.pemesan;
                            window.location.href = '{{url("/products/checkout/cart/payment/konfirmasi")}}' + '?' + decodeURIComponent(usp.toString());
                        }
                        $('#btn_pembayaran').html('Selesaikan  Pesanan<i class="ps-icon-next"></i>');
                        $('#btn_pembayaran').prop('disabled',false);
                    }, (new Date().getTime() - this.start_time));
                }
                });
        });
        // $(document).on('submit','.order_payment_submit', function(e){
        //     e.preventDefault();
        //     let data = $('.order_payment').serializeArray();
        //     console.log(data);
        // });
    </script>
@endsection
