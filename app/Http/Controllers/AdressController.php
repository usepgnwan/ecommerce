<?php

namespace App\Http\Controllers;

use App\Models\Adress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdressController extends Controller
{
    protected $address;
    public function __construct(Adress $address)
    {
        $this->address = $address;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'no_telp' => 'required',
            'kota' => 'required|not_in:0',
            'provinsi' => 'required|not_in:0',
            'alamat' => 'required',
            'no_pos' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Simpan Alamat.', 'data' => $validate->errors()]);
        }

        $kota = explode('|', $request->kota);
        $provinsi = explode('|', $request->provinsi);
        $data = [
            'user_id' => auth()->user()->id,
            'nama_penerima' => $request->nama,
            'no_telp' => $request->no_telp,
            'id_provinsi' => $provinsi[0],
            'provinsi' => $provinsi[1],
            'id_kota' => $kota[0],
            'kota' => $kota[1],
            'no_pos' => $request->no_pos,
            'alamat' => $request->alamat,
        ];

        $address = $this->address->create($data);
        return response()->json([
            'status' => true,
            'msg' => 'Alamat Berhasil disimpan.',
            'data' => $this->address->where(['user_id' => auth()->user()->id])->get()
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = $this->address->find($id);
        if ($check) {
            $check->first();
            return response()->json(['status' => true, 'msg' => 'data ada', 'data'=> $check]);
        }
        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'nama' => 'required',
            'no_telp' => 'required',
            'kota' => 'required|not_in:0',
            'provinsi' => 'required|not_in:0',
            'alamat' => 'required',
            'no_pos' => 'required'
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Alamat.', 'data' => $validate->errors()]);
        }

        $kota = explode('|', $request->kota);
        $provinsi = explode('|', $request->provinsi);
        $data = [
            // 'user_id' => auth()->user()->id,
            'nama_penerima' => $request->nama,
            'no_telp' => $request->no_telp,
            'id_provinsi' => $provinsi[0],
            'provinsi' => $provinsi[1],
            'id_kota' => $kota[0],
            'kota' => $kota[1],
            'no_pos' => $request->no_pos,
            'alamat' => $request->alamat,
        ];

        $check = $this->address->find($id);
        if ($check) {
            $check->fill($data)->save();
            return response()->json(['status' => true,
            'msg' => 'Alamat Berhasil diubah.',
            'data' => $this->address->where(['user_id' => auth()->user()->id])->get()
        ]);
        }
        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $check = $this->address->find($id);

        if ($check) {
            $check->delete();
            return response()->json(['status' => true, 'msg' => 'Berhasil Hapus Data']);
        }
        return response()->json(['status' => false, 'msg' => 'Gagal Hapus Data']);
    }

    public function getMyAddress($id){
        $address = $this->address->where(['user_id' => $id])->get();

        if(count($address) !=0){
            return response()->json(['status' => true, 'msg' => 'Alamat tersedia', 'data'=>$address]);
        }else{
            return response()->json(['status' => false, 'msg' => 'Alamat belum tersedia', 'data'=>[]]);
        }
    }
}
