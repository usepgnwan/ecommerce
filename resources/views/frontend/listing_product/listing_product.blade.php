<!-- Custom scripts-->
@isset($render)
    <script type="text/javascript" src="{{ asset('assets_frontend/js/main.js') }}"></script>
@endisset
@if ($produk->count())
<div style="height: 100% !important; position: relative !important;">
    <div class="ps-product__columns">
        @foreach ($produk as $p)
            <div class="ps-product__column">
                <div class="ps-shoe mb-30">
                    <div class="  ps-shoe__thumbnail  @if ($routeCheck <=1)  ps-shoe__thumbnail_small @endif ">
                        @if ($p->stok_sum_qty >=1)
                            <?php $number_days = (strtotime(date('Y-m-d H:i:s')) - strtotime($p->created_at)) / (60 * 60 * 24); ?>
                            @if ($number_days <= 3)
                                <div class="ps-badge"><span>New</span></div>
                            @endif
                            @if (!is_null($p->diskon) || $p->diskon != '')
                                @if ($p->diskon > 0 )
                                    <div class="ps-badge ps-badge--sale  @if ($number_days <= 3) ps-badge--2nd @endif">
                                        <span>-{{ $p->diskon }}%</span>
                                    </div>
                                @endif
                            @endif
                        @else
                            <div class="ps-badge" style="background: red !important; color:white !important; padding:8px 10px 10px; "><small>Sold out.</small></div>
                        @endif
                        {{-- <a class="ps-shoe__favorite" href="#">
                        <i class="ps-icon-heart"></i>
                    </a> --}}

                        @if ($p->images->count())
                            <img src="{{ asset('uploads/' . $p->images[0]->image_name) }}" width="100%" />
                        @else
                            <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                        @endif
                        <a class="ps-shoe__overlay"
                            href="{{ route('products.product_details', ['slug' => $p->slug]) }}"></a>
                    </div>
                    <div class="ps-shoe__content">
                        <div class="ps-shoe__variants">
                            <div class="ps-shoe__variant normal">
                                @if ($p->images->count())
                                    @foreach ($p->images as $img)
                                        <img src="{{ asset('uploads/' . $img->image_name) }}" width="100%" />
                                    @endforeach
                                @else
                                    <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                                @endif

                            </div>
                            <div style="font-size:0.7em">
                                <?php $kategory = implode(', ', array_column($p->kategori->toArray(), 'category_name')); ?>
                                <span>{{ $kategory }}</span>
                            </div>
                            {{-- <select class="ps-rating ps-shoe__rating">
                                <option value="1">1</option>
                                <option value="1">2</option>
                                <option value="1">3</option>
                                <option value="1">4</option>
                                <option value="2">5</option>
                            </select> --}}
                        </div>
                        <div class="ps-shoe__detail">
                            <a class="ps-shoe__name" href="{{ route('products.product_details',['slug'=> $p->slug])}}" style="font-size: 0.9em !important;">
                                {{ Str::substr($p->nama_produk, 0, 18) }} @if (Str::length($p->nama_produk) >= '18')
                                    ...
                                @endif </a>
                            {{-- <p class="ps-shoe__price">
                            <a href="#">Men shoes</a>,
                            <a href="#">Nike</a>,
                            <a href="#">Jordan</a>

                        </p> --}}
                            <p class="ps-shoe__categories">
                                @if ($p->diskon == '' || $p->diskon == null)
                                    {{ 'Rp.  ' . number_format($p->harga_jual, 0, ',', '.') }}
                                @else
                                    <?php $potongan = ($p->harga_jual * $p->diskon) / 100;
                                    $jml = $p->harga_jual - $potongan; ?>
                                    <del>{{ 'Rp.  ' . number_format($p->harga_jual, 0, ',', '.') }}</del><br>
                                    {{ 'Rp.  ' . number_format($jml, 0, ',', '.') }}
                                @endif
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach

    </div>
    <div class="ps-product-action">
        <div class="ps-pagination">
            <?php
            // config
            $link_limit = 7; // maximum number of links (a little bit inaccurate, but will be ok for now)
            ?>
            @if ($produk->lastPage() > 1)
                <ul class="pagination">
                    <li class="{{ $produk->currentPage() == 1 ? ' disabled' : '' }}">
                        <a href="{{ $produk->url(1) }}"><i class="fa fa-angle-left"></i></a>
                    </li>
                    @for ($i = 1; $i <= $produk->lastPage(); $i++)
                        <?php
                        $half_total_links = floor($link_limit / 2);
                        $from = $produk->currentPage() - $half_total_links;
                        $to = $produk->currentPage() + $half_total_links;
                        if ($produk->currentPage() < $half_total_links) {
                            $to += $half_total_links - $produk->currentPage();
                        }
                        if ($produk->lastPage() - $produk->currentPage() < $half_total_links) {
                            $from -= $half_total_links - ($produk->lastPage() - $produk->currentPage()) - 1;
                        }
                        ?>
                        @if ($from < $i && $i < $to)
                            <li class="{{ $produk->currentPage() == $i ? ' active' : '' }}">
                                <a href="{{ $produk->url($i) }}">{{ $i }}</a>
                            </li>
                        @endif
                    @endfor
                    <li class="{{ $produk->currentPage() == $produk->lastPage() ? ' disabled' : '' }}">
                        <a href="{{ $produk->url($produk->lastPage()) }}"><i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>
            @endif
        </div>
    </div>
</div>
@else
    <div class="row mt-3 ">
        <div class="col-md-12 d-flex justify-content-center text-center" style="color:#2ac37d">
            <div>
                <div>
                    <h4 style="color:#2ac37d !important">Product Not Found</h4>
                </div>
            </div>
        </div>
    </div>
@endif
