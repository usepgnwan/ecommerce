<?php

namespace App\Http\Controllers;

use App\Models\MasterSize;
use Illuminate\Http\Request;
use DataTables;

class MasterSizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layout_admin.master_size');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_data()
    {
        $data = MasterSize::select("id", "size_name")->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $actionBtn = ' <a href="javascript:void(0)" class="primary btn btn-primary btn-sm" onclick="edit(' . $row->id . ')"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" onclick="hapus(' . $row->id . ')"><i class="fa fa-trash"></i> Hapus</a>';
                return $actionBtn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = MasterSize::create(['size_name' => request('size_name')]);
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\MasterSize  $masterSize
     * @return \Illuminate\Http\Response
     */
    public function show(MasterSize $masterSize)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\MasterSize  $masterSize
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return MasterSize::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\MasterSize  $masterSize
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $id = request('id');
        $data = MasterSize::find($id)->update(['size_name' => request('size_name')]);
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\MasterSize  $masterSize
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterSize $masterSize)
    {
        $id = request('id');
        $data = MasterSize::find($id)->delete();
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}