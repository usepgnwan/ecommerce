@extends('template_admin.admin')

@section('title', 'Semua Transaksi')
@section('container')
    <div class="row mt-3">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header">Transaksi Selesai</div>

                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="Tabletransaksi" style="width: 100">
                        <thead class="thead-light">
                            <tr>
                                <th style="width: 10%">No</th>
                                <th style="width: 20%">ID Pemesanan</th>
                                <th style="width: 20%">Nama</th>
                                <th style="width: 20%">Tanggal Beli</th>
                                <th style="width: 20%">Kontak</th>
                                <th style="width: 10%">Harga Beli</th>
                                <th style="width: 10%">Quantity</th>
                                <th style="width: 10%">Size</th>
                                <th style="width: 10%">Total Harga</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        let transaksi;
        $(document).ready(function() {
            // norek
            transaksi = $('#Tabletransaksi').DataTable({
                prossecing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('product.list_transaksi_selesai', ['id'=> request()->id]) }}",
                    // data: function(d) {
                    //     d.id = '{{request()->id}}';
                    // }
                },
                columns: [
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'id_pemesanan',
                        name: 'id_pemesanan'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'tgl_beli',
                        name: 'tgl_beli'
                    },
                    {
                        data: 'kontak',
                        name: 'kontak'
                    },
                    {
                        data: 'harga',
                        name: 'harga'
                    },
                    {
                        data: 'quantity',
                        name: 'quantity'
                    },
                    {
                        data: 'size',
                        name: 'size'
                    },
                    {
                        data: 'total_item',
                        name: 'total_item'
                    },
                ]
            });
        });
    </script>
@endsection
