<div class="rev_slider fullscreenbanner" id="home-banner">
    <ul>
        @if ($banner_toko[0]['status'] == 'on')
            <li class="ps-banner" data-index="rs-2972" data-transition="random" data-slotamount="default"
                data-hideafterloop="0" data-hideslideonmobile="off" data-rotate="0"><img class="rev-slidebg"
                    src="{{ asset('assets_frontend/images/slider'). '/'. $banner_toko[0]['foto_banner'] }}" alt="" data-bgposition="center center"
                    data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" data-no-retina>
                <div class="tp-caption ps-banner__title" id="layer21" data-x="['left','left','left','left']"
                    data-hoffset="['-60','15','15','15']" data-y="['middle','middle','middle','middle']"
                    data-voffset="['-60','-40','-50','-70']" data-type="text" data-responsive_offset="on"
                    data-textAlign="['center','center','center','center']"
                    data-frames="[{&quot;delay&quot;:1200,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                    <p class="text-uppercase">{!!$banner_toko[0]['title']!!}</p>
                </div>
                <div class="tp-caption ps-banner__description" id="layer211" data-x="['left','left','left','left']"
                    data-hoffset="['-60','15','15','15']" data-y="['middle','middle','middle','middle']"
                    data-voffset="['30','50','50','50']" data-type="text" data-responsive_offset="on"
                    data-textAlign="['center','center','center','center']"
                    data-frames="[{&quot;delay&quot;:1200,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                    <p>{!!$banner_toko[0]['deskripsi']!!}</p>
                </div>
            </li>
        @endif
        @if ($banner_toko[1]['status'] == 'on')
            <li class="ps-banner ps-banner--white" data-index="rs-100" data-transition="random" data-slotamount="default"
                data-hideafterloop="0" data-hideslideonmobile="off" data-rotate="0">
                <img class="rev-slidebg" src="{{ asset('assets_frontend/images/slider'). '/'. $banner_toko[1]['foto_banner'] }}" alt=""
                    data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5"
                    data-no-retina>
                <div class="tp-caption ps-banner__title" id="layer339" data-x="['left','left','left','left']"
                    data-hoffset="['-60','15','15','15']" data-y="['middle','middle','middle','middle']"
                    data-voffset="['-60','-40','-50','-70']" data-type="text" data-responsive_offset="on"
                    data-textAlign="['center','center','center','center']"
                    data-frames="[{&quot;delay&quot;:1200,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                    <p class="text-uppercase">{!!$banner_toko[1]['title']!!}</p>
                </div>
                <div class="tp-caption ps-banner__description" id="layer2-14" data-x="['left','left','left','left']"
                    data-hoffset="['-60','15','15','15']" data-y="['middle','middle','middle','middle']"
                    data-voffset="['30','50','50','50']" data-type="text" data-responsive_offset="on"
                    data-textAlign="['center','center','center','center']"
                    data-frames="[{&quot;delay&quot;:1200,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;x:50px;opacity:0;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:300,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;x:50px;opacity:0;&quot;,&quot;ease&quot;:&quot;Power3.easeInOut&quot;}]">
                    <p>{!!$banner_toko[1]['deskripsi']!!}</p>
                </div>
            </li>
        @endif
    </ul>
</div>
