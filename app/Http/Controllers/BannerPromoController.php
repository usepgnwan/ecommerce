<?php

namespace App\Http\Controllers;

use App\Models\BannerPromo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BannerPromoController extends Controller
{
    protected $promo_banner;
    public function __construct(BannerPromo $promo_banner)
    {
        $this->promo_banner = $promo_banner;
    }
    public function index()
    {
        $promo_banner = $this->promo_banner->first();
        return view('layout_admin.master_banner_promo', compact('promo_banner'));
    }
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'foto_banner_promo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:10000',
        ]);
        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Banner.', 'data' => $validate->errors()]);
        }
        $check =  $this->promo_banner->find($id);
        if ($request->status == 1) {
            $imgName = $check->foto_banner_1;
        } else if ($request->status == 2){
            $imgName = $check->foto_banner_2;
        }
        if ($check) {
            if ($request->hasFile('foto_banner_promo') && $request->foto_banner_promo != '') {
                // $path_old = public_path('assets_frontend/images/slider/') .  $check->foto_banner;
                // // if (file_exists($path_old) && $check->foto_banner != null && $check->foto_banner != '3.jpg' && $check->foto_banner != '2.jpg') {
                // //     unlink($path_old);
                // // }
                $image = $request->file('foto_banner_promo');
                $imgName = $image->hashName();
                $image->move('assets_frontend/images/banner_promo/', $imgName);
            }

            if ($check) {
                $data = [];
                if ($request->status == 1) {
                    $data['foto_banner_1'] = $imgName;
                } else if ($request->status == 2){
                    $data['foto_banner_2'] = $imgName;
                }

        // echo json_encode($data);die;
                $check->fill($data)->save();
            }
        }

        return response()->json([
            'status' => true,
            'data' => $check
        ]);
    }
}
