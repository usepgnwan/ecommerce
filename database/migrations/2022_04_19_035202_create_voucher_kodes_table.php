<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherKodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher_kodes', function (Blueprint $table) {
            $table->id();
            $table->string('nama_promo');
            $table->string('kode')->unique();
            $table->float('promo');
            $table->enum('type_user', ['login','all'])->default('all')->nullable();
            $table->enum('type_voucher', ['diskon','potongan'])->default('diskon')->nullable();
            $table->enum('status', ['on','off'])->default('on')->nullable();
            $table->date('periode_awal');
            $table->date('periode_akhir');
            $table->integer('kuota')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher_kodes');
    }
}
