<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePreviewProduksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preview_produks', function (Blueprint $table) {
            $table->id();
            $table->string('product_id')->nullable();
            $table->string('id_transaksi')->nullable();
            $table->string('id_pemesanan')->nullable();
            $table->string('user_id')->nullable();
            $table->string('nama')->nullable();
            $table->string('review')->nullable();
            $table->string('star')->nullable();
            $table->enum('status', ['aktif','nonaktif'])->default('aktif')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preview_produks');
    }
}
