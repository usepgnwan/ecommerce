<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Before composer update or install
---
Step instalation project : 

- Deleted all helper / custom vendor on composer.json find "files" in line this : 
```
"autoload": {
        "psr-4": {
            "App\\": "app/",
            "Database\\Factories\\": "database/factories/",
            "Database\\Seeders\\": "database/seeders/"
        },
        "files": [
            "app/helpers/api_rajaongkir.php"
        ]
    },
```
- comment all code in boot funtion App\Provider\AppServiceProvider: 
```
    public function boot()
    {
        // if (Schema::hasTable('store_setting')) {

        //     $store_setting = DB::table('store_setting')->where(['id' => '1'])->first();
        //     View::share('store_setting', $store_setting);
        // }
        // if (Schema::hasTable('master_category')) {
        //     $category_all = MasterCategory::get();
        //     View::share('category_all',  $category_all);
        // }

        // if (Schema::hasTable('metode_pembayarans')) {
        //     $m_pembayaran = MetodePembayaran::where('status','=','on')->get();
        //     View::share('m_pembayaran',  $m_pembayaran);
        // }

        // if (Schema::hasTable('voucher_kodes')) {
        //     $evoucher_kode = DB::select('SELECT * FROM  voucher_kodes  WHERE status = "on"  AND deleted_at IS NULL AND CURDATE() BETWEEN periode_awal And periode_akhir;');
        //     View::share('evoucher_kode',  $evoucher_kode);
        // }

        // Paginator::useBootstrap();
    }
```
- you can use php generate:key/ migrate / seeder / factory now.
- then you can try to using composer i / update on terminal
- end the last step uncoment boot function & add again "files" into composer.json like before step 
- to use helper / vendor custom write on terminal like this:

```
composer dump-autoload
```
---- 
## THANKS
