@extends('template_admin.admin')

@section('title', 'Master Size')
@section('container')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master Size</h1>

    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="from-group mb-2">
                <button class="btn btn-primary" onclick="tambah()" value="new">Tambah Data</button>
            </div>
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>Size</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>





    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Size</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="" name="form" id="form" enctype="multipart/form-data">
                        <div class="form-group">
                            @csrf
                            <label for="size_name">Nama Size</label>
                            <input type="hidden" class="form-control" id="id" name="id">
                            <input type="text" class="form-control" id="size_name" name="size_name">
                            <div id="method"></div>
                        </div>

                    </form>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary btn-save" id="tombolsimpan">Simpan</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        // datatabel
        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('data_size') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'size_name',
                    name: 'size_name'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });


        function open_modal(judul, tombolSimpan, method) {
            $('#modal').modal('show');
            $('#tombolsimpan').text(tombolSimpan);
            $('.modal-title').text(judul);
            $('#form')[0].reset();
            if (method != 'edit') {
                $("input[name=id]").val('');
                $("input[name=_method]").val('');
                $('#tombolsimpan').attr('onclick', 'aksi("tambah")')
            } else {
                $('#tombolsimpan').attr('onclick', 'aksi("edit")')
            }
        };

        function tambah() {
            open_modal('Tambah Size', 'Simpan', 'tambah')
        }

        function edit(id) {
            var link = " {{ route('get_size', 'id') }}";
            var link_tujuan = link.replace("id", id);
            $.ajax({
                type: "get",
                url: link_tujuan,
                success: function(response) {
                    open_modal('Edit Size', 'Ubah', 'edit')
                    $('#id').val(response.id);
                    $('#size_name').val(response.size_name);
                    $('#method').html('@method("put")')
                }
            });
        }

        function hapus(id) {
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "post",
                        url: "{{ route('hapus_size') }}",
                        data: {
                            "id": id,
                            "_method": "delete",
                            "_token": "{{ @csrf_token() }}"
                        },
                        success: function(response) {
                            if (response.success == true) {
                                $('#modal').modal('hide');
                                Swal.fire(
                                    'Berhasil!',
                                    'Data berhasil dihapus!',
                                    'success'
                                )
                                table.ajax.reload();
                            } else {
                                Swal.fire(
                                    'Gagal!',
                                    'Data gagal dihapus!',
                                    'error'
                                )
                            }
                        }
                    });
                }
            })
        }

        function aksi(method) {
            if (method == 'tambah') {
                $.ajax({
                    type: "post",
                    url: "{{ route('tambah_size') }}",
                    data: $('#form').serialize(),
                    success: function(response) {

                        if (response.success == true) {
                            $('#modal').modal('hide');
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            table.ajax.reload();
                        } else {
                            Swal.fire(
                                'Gagal!',
                                'Data gagal disimpan!',
                                'error'
                            )
                        }
                    }
                });

            } else if (method == 'edit') {
                $.ajax({
                    type: "post",
                    url: "{{ route('update_size') }}",
                    data: $('#form').serialize(),
                    success: function(response) {
                        if (response.success == true) {
                            $('#modal').modal('hide');
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            table.ajax.reload();
                        } else {
                            Swal.fire(
                                'Gagal!',
                                'Data gagal disimpan!',
                                'error'
                            )
                        }
                    }
                });

            }

        }
    </script>
@endsection
