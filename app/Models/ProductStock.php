<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductStock extends Model
{
    use HasFactory;
    protected $table = 'product_stock';
    protected $fillable = ['id_produk', 'size', 'qty'];
    protected $with = ['size'];
    public function stok()
    {
        return $this->belongsTo('product_stock', 'id_produk');
    }
    public function size()
    {
        return $this->belongsTo(MasterSize::class, 'size')->select('id','size_name');
    }
}
