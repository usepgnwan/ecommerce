<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateStoreSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_setting', function (Blueprint $table) {
            $table->id();
            $table->string('nama_toko');
            $table->string('type')->nullable();
            $table->string('alamat');
            $table->string('email');
            $table->string('telepon');
            $table->string('fax');
            $table->string('provinsi')->nullable();
            $table->string('kota')->nullable();
            $table->string('kode_pos')->nullable();
            $table->longText('deskripsi')->nullable();
            $table->string('id_provinsi')->nullable();
            $table->string('id_kota')->nullable();
            $table->longText('shopee')->nullable();
            $table->longText('fb')->nullable();
            $table->longText('instagram')->nullable();
            $table->longText('tokopedia')->nullable();
            $table->string('background_testimoni')->nullable();
        });
        DB::table('store_setting')->insert([
            'nama_toko' => 'contoh',
            'alamat' => 'contoh alamat',
            'email' => 'contoh@email.com',
            'telepon' => '07580768954',
            'fax' => 'contoh'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_setting');
    }
}