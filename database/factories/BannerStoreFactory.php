<?php

namespace Database\Factories;

use App\Models\BannerStore;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerStoreFactory extends Factory
{
    protected $model = BannerStore::class;
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $banner = ['3.jpg', '2.jpg'];
        $title = ['Gratis Ongkir', 'Promo Lebaran'];
        return [
            'title' => $title[array_rand($title)],
            'deskripsi' => $this->faker->sentence(30),
            'status' => 'on',
            'foto_banner'=> $banner[array_rand($banner)],
        ];
    }
}
