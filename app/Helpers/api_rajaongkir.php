<?php

use GuzzleHttp\Client as GuzzleHttpClient;

class RajaOngkir
{
    private  $headers = [
        'Content-Type' => 'application/json',
        'key' => '17a9fa5768eafc86acdcd5948603886c',
    ];

    public function get_data_provinsi_kota($type, $params = null)
    {
        $headers =  $this->headers;
        $client = new GuzzleHttpClient([
            'base_uri' => 'https://api.rajaongkir.com',
            'headers' => $headers
        ]);
        $response = '';
        if($type =='provinsi' && is_null($params)){
                $url = "starter/province";
                $response = $client->request('GET', $url);
        }else if($type =='kota' && !is_null($params)){
            $url = "starter/city";
            $params = [
                'query' => [
                    'province' => $params
                ]
            ];
            $response = $client->request('GET', $url, $params);
        }

        $body = $response->getBody();
        $body_array = json_decode($body, true);
        return $body_array['rajaongkir'];
    }

    public function get_ongkir($kota_pengirim, $kota_penerima,$kurir, $berat){
        $headers =  $this->headers;
        $client = new GuzzleHttpClient([
            'base_uri' => 'https://api.rajaongkir.com',
            'headers' => $headers
        ]);
        $body = [
            'form_params' => [
                'origin' => $kota_pengirim,
                'destination' => $kota_penerima,
                'courier' => $kurir,
                'weight' => $berat
            ]
        ];
        $url = "/starter/cost/";
        $response = $client->request('POST', $url, $body);

        $body = $response->getBody();
        $body_array = json_decode($body, true);
        return $body_array['rajaongkir'];
    }
}
