@extends('template_admin.admin')

@section('title', 'Semua Transaksi')
@section('container')
    <div class="row mt-3">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="card mb-4">
                <div class="card-header">Semua Transaksi</div>
                {{-- <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
            </div> --}}
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="nama_produk">Filter *</label>
                                <select class="form-control filter-status" name="" id="">
                                    <option value="baru.terkonfirmasi.tracking.selesai.rejected">All</option>
                                    <option value="baru">Baru</option>
                                    <option value="terkonfirmasi">Terkonfirmasi</option>
                                    <option value="tracking">Tracking</option>
                                    <option value="selesai">Selesai</option>
                                    <option value="rejected">Rejected</option>
                                </select>

                            </div>
                        </div>
                    </div>

                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="Tabletransaksi" style="width: 100">
                        <thead class="thead-light">
                            <tr>
                                <th style="width: 10%">Action</th>
                                <th style="width: 10%">No</th>
                                <th style="width: 20%">ID PEMESANAN</th>
                                <th style="width: 20%">NAMA</th>
                                <th style="width: 20%">Kontak</th>
                                <th style="width: 10%">Bukti Transfer</th>
                                <th style="width: 20%">Alamat</th>
                                <th style="width: 10%">Status</th>
                                <th style="width: 10%">Jumlah Pesanan</th>
                                <th style="width: 10%">Total Bayar</th>
                                <th style="width: 10%">Tanggal</th>
                                <th style="width: 10%">Bukti Transfer</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabels"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body modal-detail">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle-form"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body form-all">
                    <form action="">
                        @method('PUT')
                        @csrf
                        <div class="modal-form">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary btn-save-form">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
    <script>
        let transaksi;
        $(document).ready(function() {
            // norek
            transaksi = $('#Tabletransaksi').DataTable({
                prossecing: true,
                serverSide: true,
                ajax: {
                    url: "{{ route('transaksi.index') }}",
                    data: function(d) {
                        d.status = $('.filter-status').val();
                    }
                },
                columns: [{
                        data: 'action',
                        name: 'action',
                        searchable: false
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex'
                    },
                    {
                        data: 'id_pemesanan',
                        name: 'id_pemesanan'
                    },
                    {
                        data: 'nama',
                        name: 'nama'
                    },
                    {
                        data: 'kontak',
                        name: 'kontak'
                    },
                    {
                        data: 'bukti',
                        name: 'bukti'
                    },
                    {
                        data: 'alamat',
                        name: 'alamat'
                    },
                    {
                        data: 'status',
                        name: 'status'
                    },
                    {
                        data: 'detail_transaksi_sum_quantity',
                        name: 'detail_transaksi_sum_quantity'
                    },
                    {
                        data: 'total_bayar',
                        name: 'total_bayar'
                    },
                    {
                        data: 'tanggal',
                        name: 'tanggal'
                    }
                ]
            });

            $(document).on('click', '.edit-status', function(e) {
                e.preventDefault();
                $('#modal-2').modal('show');
                $('#modalTitle-form').html('Ubah Status');
                let id = $(this).data('id');
                $('.modal-form').html(`
                        <div class="form-group">
                            <label for="category_name">Status</label>
                                <input type="hidden" class="form-control" id="id" name="id" value="${id}">
                                <input type="hidden" class="form-control" id="status" name="cek" value="update-status">
                                <select class="form-control filter-status" name="status" id="">
                                    <option value="terkonfirmasi">Konfirmasi</option>
                                    <option value="rejected">Rejected</option>
                                </select>
                        </div>
                `);
            });
            $(document).on('click', '.upload-bukti', function(e) {
                e.preventDefault();
                $('#modal-2').modal('show');
                $('#modalTitle-form').html('Upload Bukti Transfer');
                let id = $(this).data('id');
                $('.modal-form').html(`
                        <div class="form-group">
                            <label for="category_name">Bukti Transfer</label>
                                <input type="hidden" class="form-control" id="id" name="id" value="${id}">
                                <input type="hidden" class="form-control" id="status" name="cek" value="upload">
                                <input type="file" class="form-control" id="bukti_transfer" name="bukti_transfer">
                        </div>
                `);
            });

            $('.btn-save-form').click(function(e) {
                e.preventDefault();
                let data = new FormData($('.form-all form')[0]);
                $('.btn-save-form').prop('disabled',true);
                $('.btn-save-form').html('<span><i class="fa fa-spinner fa-spin"></i> &nbsp; Proses</span>');
                $.ajax({
                    type: "post",
                    url: "{{ route('admin_transaksi.update_transaksi') }}",
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType:'JSON',
                    success: function(data) {
                        $('.btn-save-form').prop('disabled',false);
                        $('.btn-save-form').html('simpan');
                        if(data.status){
                            transaksi.ajax.reload();
                            $('#modal-2').modal('hide');
                            Swal.fire(
                                'Berhasil',
                                data.msg,
                                'success'
                            );
                        }else{
                            Swal.fire(
                                'Gagal!',
                                data.msg,
                                'error'
                            )
                        }
                    },
                });
            });
            $(document).on('click', '.show-detail', function(e) {
                e.preventDefault();
                $('#modal').modal('show');
                let id = $(this).data('id');
                let id_transaksi = $(this).data('id_transaksi');
                let url = '{{ route('admin_transaksi.detail_transaksi') }}';
                let modal = true;
                $('#modalLabels').html('Detail Transaksi');
                $('.modal-detail').html(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class=" ">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b><span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                    `);
                $.ajax({
                    url: url,
                    data: {
                        id,
                        id_transaksi,
                        modal
                    },
                    method: 'GET',
                    // dataType: 'JSON',
                    start_time: new Date().getTime(),
                    success: function(data) {
                        // console.log(this.start_time);
                        setTimeout(() => {
                            $('.modal-detail').html(data);
                        }, (new Date().getTime() - this.start_time));
                    }
                });
            });
        });

        $('.filter-status').change(function() {
            transaksi.ajax.reload();
        });
    </script>
@endsection
