<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\AdressController;
use App\Http\Controllers\BannerPromoController;
use App\Http\Controllers\chartController;
use App\Http\Controllers\ContactUsController;
use App\Http\Controllers\FAQController;
// use App\Http\Controllers\FrontendProductController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MasterCategoryController;
use App\Http\Controllers\MasterProductController;
use App\Http\Controllers\MasterSizeController;
use App\Http\Controllers\MetodePembayaranController;
use App\Http\Controllers\PreviewProdukControlller;
use App\Http\Controllers\StoreSettingController;
use App\Http\Controllers\TransaksiController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\VoucherKodeController;
use App\Http\Controllers\WeightOngkirController;
use App\Http\Controllers\KataSandiController;
use App\Models\MasterCategory;
use Illuminate\Support\Facades\DB;
// use Illuuse App\Http\Controllers\StoreSettingController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::prefix('/chart')->group(function () {
    Route::get('/wilshit', [chartController::class, 'wilshit'])->name('chart.wilshit');
});

// chart to checkout
Route::resource('/chart', chartController::class);

#================================ ROUTE ADMIN TRANSAKSION ===================================================
Route::middleware('auth', 'role:admin')->group(function () {
    Route::post('ubah_password',[KataSandiController::class,'reset'])->name('proses.reset.password');
    Route::resource('/admin', AdminController::class);
    Route::group(['prefix' => 'admin'], function () {
        // Route::resource('/', AdminController::class)->name('admin.index');
        Route::get('/transaksi/rejected', [TransaksiController::class, 'rejected_view'])->name('admin_transaksi.rejected');
        Route::get('/transaksi/selesai', [TransaksiController::class, 'selesai_view'])->name('admin_transaksi.selesai');
        Route::get('/transaksi/all', [TransaksiController::class, 'all_transaksi'])->name('admin_transaksi.all');
        Route::put('/transaksi/update_transaksi', [TransaksiController::class, 'update_transaksi'])->name('admin_transaksi.update_transaksi');
        Route::put('/transaksi/update_tracking', [TransaksiController::class, 'update_tracking'])->name('admin_transaksi.update_tracking');
    });
});
Route::get('admin/transaksi/detail/', [TransaksiController::class, 'detail_transaksi'])->name('admin_transaksi.detail_transaksi');
#================================ ROUTE ADMIN TRANSAKSION ===================================================

// Route::middleware('auth', 'role:admin')->group(function () {
//     Route::resource('/admin', AdminController::class);
// });


Route::get('/', [HomeController::class, 'index'])->name('homepage');


Route::prefix('customer')->group(function () {
    Route::prefix('account')->group(function () {
        Route::get('login', [LoginController::class, 'index'])->name('login')->middleware('guest');
        Route::post('login', [LoginController::class, 'login'])->name('login.post');
        Route::post('logout', [LoginController::class, 'logout'])->name('logout');
        Route::post('registrasi', [LoginController::class, 'registrasi'])->name('registrasi');
    });
});

// ====================================================== route admin ========================================================================
// master
Route::middleware('auth', 'role:admin')->group(function () {
    Route::group(['prefix' => 'master'], function () {
        // master size
        Route::group(['prefix' => 'size'], function () {
            Route::get('/', [MasterSizeController::class, 'index'])->name('master_size');
            Route::get('/sizes', [MasterSizeController::class, 'show_data'])->name('data_size');
            Route::post('/', [MasterSizeController::class, 'store'])->name('tambah_size');
            Route::get('{id}', [MasterSizeController::class, 'edit'])->name('get_size');
            Route::put('/', [MasterSizeController::class, 'update'])->name('update_size');
            Route::delete('/', [MasterSizeController::class, 'destroy'])->name('hapus_size');
        });

        // master category
        Route::group(['prefix' => 'category'], function () {
            Route::get(
                '/',
                [MasterCategoryController::class, 'index']
            )->name('master_category');
            Route::get('/categorys', [MasterCategoryController::class, 'show_data'])->name('data_category');
            Route::post('/', [MasterCategoryController::class, 'store'])->name('tambah_category');
            Route::get('{id}', [MasterCategoryController::class, 'edit'])->name('get_category');
            Route::put(
                '/',
                [MasterCategoryController::class, 'update']
            )->name('update_category');
            Route::delete('/', [MasterCategoryController::class, 'destroy'])->name('hapus_category');
        });

        // master product
        Route::resource('/product', MasterProductController::class);
        Route::get('/products', [MasterProductController::class, 'show_data'])->name('product.dataproduct');
        Route::delete('/remove_item', [MasterProductController::class, 'remove_item'])->name('product.remove_item');
        Route::delete('/remove_kategori', [MasterProductController::class, 'remove_kategori'])->name('product.remove_kategori');
        Route::get('/products/transaksi/list/{id}', [MasterProductController::class, 'list_transaksi_selesai'])->name('product.list_transaksi_selesai');
        // Route::get('/products/penilain/{id}', [MasterProductController::class, 'penilaian_produk'])->name('product.penilaian_produk');

        // previw produk
        Route::resource('products/preview/penilain', PreviewProdukControlller::class);
        // pengaturan toko
        Route::group(['prefix' => 'pengaturan_toko'], function () {
            Route::get('/', [StoreSettingController::class, 'index'])->name('pengaturan_toko');
            Route::get('/tes', [StoreSettingController::class, 'create'])->name('a');
            Route::put('/', [StoreSettingController::class, 'update'])->name('ubah_data_toko');
            Route::put('/update_banner/{id}', [StoreSettingController::class, 'update_banner'])->name('pengaturan_toko.update_banner');
            Route::put('/update_banner_status/{id}', [StoreSettingController::class, 'update_banner_status'])->name('pengaturan_toko.update_status_banner');
            Route::put('/update_background_testimoni', [StoreSettingController::class, 'background_testimoni'])->name('pengaturan_toko.backround_testimoni');
            Route::get('/reviews', [StoreSettingController::class, 'reviews'])->name('pengaturan_toko.reviews');
            Route::get('/review/{id}', [StoreSettingController::class, 'review'])->name('pengaturan_toko.review');
            Route::post('/reviews', [StoreSettingController::class, 'make_review'])->name('pengaturan_toko.post_reviews');
            Route::put('/reviews/{id}', [StoreSettingController::class, 'edit_review'])->name('pengaturan_toko.edit_reviews');
        });

        // user master
        Route::prefix('/user')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('master.user_index');
        });
        // user contact_us
        Route::prefix('/contact_us')->group(function () {
            Route::get('/', [ContactUsController::class, 'index'])->name('contact_us.index');
        });

        //metode pembayaran
        Route::resource('/metode_pembayaran', MetodePembayaranController::class);
        Route::put('metode_pembayaran/update_status/{id}', [MetodePembayaranController::class, 'update_status'])->name('metode_pembayaran.update_status');

        //weight ongkir
        Route::resource('berat/ongkir', WeightOngkirController::class);

        //update banner promo
        Route::resource('evoucher/banner_promo', BannerPromoController::class);
        //kode voucher
        Route::resource('vocher/promo', VoucherKodeController::class);
        Route::put('vocher/promo/update_status/{id}', [VoucherKodeController::class, 'update_status'])->name('promo.update_status');

        //faq
        Route::get('faq/data', [FAQController::class, 'show_data'])->name('faq.data');
        Route::resource('faq', FAQController::class);
    });
});
// ====================================================== route frontend ========================================================================

// User Menu
Route::middleware('auth', 'role:customer')->group(function () {
    Route::prefix('user')->group(function () {
        // user profile
        Route::get('/profile', [HomeController::class, 'home_user'])->name('user.profile');
        Route::put('/profile', [HomeController::class, 'update_profile'])->name('update.profile');
        Route::put('/change/password', [LoginController::class, 'change_password'])->name('change.password');
        // penutup user profile
        Route::get('/address', [HomeController::class, 'home_address'])->name('user.address');
        Route::put('/change/default_address', [HomeController::class, 'change_default_address'])->name('change.default_address');
    });
});

Route::prefix('data')->group(function () {
    Route::get('kota/{id_address}', [HomeController::class, 'kota'])->name('get.kota');
    Route::resource('/address',  AdressController::class);
    Route::get('/my_address/{id}', [AdressController::class, 'getMyAddress'])->name('get.myaddress');
    Route::get('/contact_us', [ContactUsController::class, 'contact'])->name('contact_us.nav_contact');
    Route::post('/contact_us', [ContactUsController::class, 'store'])->name('contact_us.store');
});

// Route::prefix('product/{produk:slug}')->group(function () {
//     Route::get('lists', [FrontendProductController::class, 'index'])->name('product.lists');
// });

// frontend detail
Route::prefix('products/')->group(function () {
    Route::get('/details/{slug}', [HomeController::class, 'product_details'])->name('products.product_details');
    Route::get('/', [HomeController::class, 'listing_product'])->name('products.listing_product');
    Route::get('/render_listing_product', [HomeController::class, 'render_listing_product'])->name('products.render_listing_product');
    $kategori = MasterCategory::get();
    foreach ($kategori as $t) {
        Route::get('/' . $t->slug, [HomeController::class, 'listing_product']);
    }

    Route::get('/checkout/cart', [chartController::class, 'listing_checkout_cart'])->name('product.checkout_cart');
    Route::get('/checkout/cart/payment', [chartController::class, 'payment_checkout_cart'])->name('product.checkout_cart_payment');
    Route::post('/checkout/cart/check_ongkir', [chartController::class, 'check_ongkir'])->name('product.checkout_cart_check_ongkir');
    Route::post('/checkout/cart/add_promo', [chartController::class, 'add_promo'])->name('product.add_promo');
    Route::post('/checkout/cart/delete_promo', [chartController::class, 'delete_promo'])->name('product.delete_promo');

    Route::resource('/transaksi', TransaksiController::class);
    Route::get('/checkout/cart/payment/konfirmasi', [TransaksiController::class, 'konfirmasi_pesanan'])->name('product.konfirmasi_pesanan');
    Route::post('/checkout/cart/payment/checkout_whatsapp', [TransaksiController::class, 'checkout_whatsapp'])->name('product.checkout_whatsapp');
    Route::get('/check/pesanan', [TransaksiController::class, 'check_pesanan'])->name('product.check_pesanan');
    Route::post('/check/pesanan/tracking', [TransaksiController::class, 'post_check_pesanan'])->name('product.post_check_pesanan');
    Route::put('/check/pesanan/tracking', [TransaksiController::class, 'update_transaksi_bukti_pembayaran'])->name('product.put_check_pesanan');
    // review produk
    Route::get('/detail/review/', [PreviewProdukControlller::class, 'detail_review'])->name('product.detail_review');
    Route::post('/detail/review/', [PreviewProdukControlller::class, 'post_detail_review'])->name('product.post_detail_review');

});
