@extends('template_admin.admin')

@section('title', 'Semua Transaksi')
@section('container')
    @inject('carbon', 'Carbon\Carbon')
    <div class="row mt-3 mb-8">
        <!-- Datatables -->
        <div class="col-lg-12 this-banner" data-id="1">
            <div class="card mb-4">
                <div class="card-header">
                    <div class="d-flex">
                        <div class="mr-auto p-2col-10">
                            <h3 class="text-title-header">Tracking Paket</h3> <span class="text-body-header">
                                <p>Input tracking pengiriman paket.</p>
                            </span>
                        </div>
                        <div class=" p-2 col-2 ">
                            <p align="right">
                                <button class="btn btn-primary mt-2" title="Lihat Tracking" type="button"
                                    data-toggle="collapse" data-target="#image1" aria-expanded="true"
                                    aria-controls="image1">
                                    <i class="fa fa-solid fa-eye"></i>
                                </button>
                                <span class="checking-btn">
                                    @if ($data->status == 'baru')
                                    <button class="btn btn-warning mt-2 edit-status" title="on" type="button" value="on">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                    </button>
                                    @elseif($data->status != 'rejected' && $data->status != 'selesai')

                                        <button class="btn btn-warning mt-2 add-tracking" title="on" type="button" value="on">
                                            <i class="fa fa-plus" aria-hidden="true"></i>
                                        </button>
                                    @endif
                            </span>

                            </p>
                        </div>

                    </div>
                </div>
                <div class="collapse show" id="image1" style="">

                    <div class="col-md-12 ml-5 tracking-card">
                        <style>
                            /* body {
                                        margin-top: 20px;
                                    } */

                            .timeline {
                                border-left: 3px solid #727cf5;
                                border-bottom-right-radius: 4px;
                                border-top-right-radius: 4px;
                                background: rgba(114, 124, 245, 0.09);
                                margin: 0 auto;
                                letter-spacing: 0.2px;
                                position: relative;
                                line-height: 1.4em;
                                font-size: 1.03em;
                                padding: 50px;
                                list-style: none;
                                text-align: left;
                                max-width: 40%;
                            }

                            @media (max-width: 767px) {
                                .timeline {
                                    max-width: 98%;
                                    padding: 25px;
                                }
                            }

                            .timeline h1 {
                                font-weight: 300;
                                font-size: 1.4em;
                            }

                            .timeline h2,
                            .timeline h3 {
                                font-weight: 600;
                                font-size: 1rem;
                                margin-bottom: 10px;
                            }

                            .timeline .event {
                                border-bottom: 1px dashed #e8ebf1;
                                padding-bottom: 25px;
                                margin-bottom: 25px;
                                position: relative;
                            }

                            @media (max-width: 767px) {
                                .timeline .event {
                                    padding-top: 30px;
                                }
                            }

                            .timeline .event:last-of-type {
                                padding-bottom: 0;
                                margin-bottom: 0;
                                border: none;
                            }

                            .timeline .event:before,
                            .timeline .event:after {
                                position: absolute;
                                display: block;
                                top: 0;
                            }

                            .timeline .event:before {
                                left: -207px;
                                content: attr(data-date);
                                text-align: right;
                                font-weight: 100;
                                font-size: 0.9em;
                                min-width: 120px;
                            }

                            @media (max-width: 767px) {
                                .timeline .event:before {
                                    left: 0px;
                                    text-align: left;
                                }
                            }

                            .timeline .event:after {
                                -webkit-box-shadow: 0 0 0 3px #727cf5;
                                box-shadow: 0 0 0 3px #727cf5;
                                left: -55.8px;
                                background: #fff;
                                border-radius: 50%;
                                height: 9px;
                                width: 9px;
                                content: "";
                                top: 5px;
                            }

                            @media (max-width: 767px) {
                                .timeline .event:after {
                                    left: -31.8px;
                                }
                            }

                            .rtl .timeline {
                                border-left: 0;
                                text-align: right;
                                border-bottom-right-radius: 0;
                                border-top-right-radius: 0;
                                border-bottom-left-radius: 4px;
                                border-top-left-radius: 4px;
                                border-right: 3px solid #727cf5;
                            }

                            .rtl .timeline .event::before {
                                left: 0;
                                right: -170px;
                            }

                            .rtl .timeline .event::after {
                                left: 0;
                                right: -55.8px;
                            }

                        </style>
                        @if (count($data->tracking_transaksi) == 0 && $data->status == 'baru' || count($data->tracking_transaksi) != 0 && $data->status == 'baru')
                            <div class="row gutters">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 text-center">
                                                <span class="text-success title-address"
                                                    style="font-size: 0.8em; color: #c99069; text-align:center">
                                                    Tracking Belum Tersedia, Menunggu Konfirmasi Pesanan <br> <small>Pesanan
                                                        dibuat pada Tanggal :
                                                        {{  date('d M Y H:i A', strtotime($data->created_at)) }}</small>
                                                </span>
                                                @if ($data->bukti_transfer != NULL  && $data->status == 'baru')
                                                    <p>
                                                       User Telah mengaupload Bukti pembayaran
                                                        <div class=" ">
                                                            <div class="col-md-12" style="margin: 0 auto;">
                                                                <img src="{{asset('assets_frontend/uploads/bukti_transfer/' . $data->bukti_transfer )}}" style="width:180px; height: 180px;"/>
                                                            </div>
                                                        </div>
                                                        <a href="{{asset('assets_frontend/uploads/bukti_transfer/' . $data->bukti_transfer )}}" download="" ><i class="fa fa-download"></i></a>
                                                    </p>
                                                @endif
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div id="content" style="padding:10px">
                                                @if ($data->bukti_transfer != NULL)
                                                        <div class="row text-center" >
                                                            <div class="col-md-12" style="margin: 0 auto;">
                                                            Bukti Pembayaran<br>
                                                                <img src="{{asset('assets_frontend/uploads/bukti_transfer/' . $data->bukti_transfer )}}" style="width:180px; height: 180px;"/>
                                                                <div>

                                                        <a href="{{asset('assets_frontend/uploads/bukti_transfer/' . $data->bukti_transfer )}}" download=""><i class="fa fa-download"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                @endif
                                <ul class="timeline">
                                    @foreach ($data->tracking_transaksi as $tc)
                                        <li class="event"
                                            data-date="{{ $carbon::parse($tc->tanggal_tracking)->format('Y M d H:i') }}">
                                            <h3>{{ $tc->title }}</h3>
                                            <p>{{ $tc->deskripsi }}</p>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif


                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="card" style="margin-bottom:20px">
                <div class="card-header">
                    Invoice :
                    <strong>{{ $data->id_pemesanan }} / {{ $data->created_at }}</strong>
                    <span class="float-right"> <strong>Status:</strong> <span class="text-status">  {{ $data->status }}</span>

                </div>
                <div class="card-body">
                    <div class="row mb-4">
                        <div class="col-sm-6">
                            <h6 class="mb-3">Dari:</h6>
                            <div>
                                <strong>{{ $store_setting->nama_toko }}</strong>
                            </div>
                            <div>
                                {!! $store_setting->alamat !!},
                                {{ $store_setting->provinsi }},
                                {{ $store_setting->kota }}
                                {{ $store_setting->kode_pos }}
                            </div>
                            <div>Email: {{ $store_setting->email }} </div>
                            <div>Telp (WA): {{ $store_setting->telepon }}</div>
                        </div>

                        <div class="col-sm-6">
                            <h6 class="mb-3">Kepada:</h6>
                            <div>
                                <strong>{{ $data->nama_depan }} {{ $data->nama_belakang }}</strong>
                            </div>
                            <div>
                                {!! $data->alamat_lengkap !!},
                                {{ $data->provinsi }},
                                {{ $data->kota }}
                                {{ $data->kode_pos }}
                            </div>
                            <div>Email: {{ $data->email }} </div>
                            <div>Telp (WA): {{ $data->no_telp }}</div>
                            <p><strong> Catatan Pesanan</strong>: {{ $data->catatan }}</p>
                        </div>
                    </div>
                    <div class="table-responsive-sm">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="center">#</th>
                                    <th>Item</th>
                                    <th>Ukuran</th>
                                    <th>Diskon Item</th>
                                    <th class="right">Harga</th>
                                    <th class="center">Qty</th>
                                    <th class="right">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sub_harga = [];
                                @endphp
                                @foreach ($data->detail_transaksi as $c)
                                    <tr>
                                        <td class="center">{{ $loop->iteration }}</td>
                                        <td class="left strong">{{ $c->nama_produk }}</td>
                                        <td class="left">{{ $c->size }}</td>
                                        <td class="left">
                                            {{ $disc = $c->diskon == 0 || $c->diskon == null ? '-' : $c->diskon . '%' }}
                                        </td>

                                        <td class="right">{!! $hargaDisc = $c->diskon == 0 || $c->diskon == null ? 'Rp.' . number_format($c->sub_total, 0, ',', '.') : '<del>Rp.' . number_format($c->sub_total, 0, ',', '.') . '</del> Rp.' . number_format($c->harga_diskon, 0, ',', '.') !!}</td>
                                        <td class="center">{{ $c->quantity }}</td>
                                        <td class="right"> Rp.{{ number_format($c->total_item, 0, ',', '.') }}
                                        </td>
                                        @php array_push($sub_harga, $c->total_item) @endphp
                                    </tr>
                                @endforeach

                            </tbody>
                        </table>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-sm-5">

                        </div>

                        <div class="col-lg-4 col-sm-5 ml-auto">
                            <table class="table table-clear">
                                <tbody>
                                    <tr>
                                        <td class="left">
                                            <strong>Total Item</strong>
                                        </td>
                                        <td class="right">
                                            {{ $data->detail_transaksi_sum_quantity }}</td>
                                    </tr>
                                    <tr>
                                        <td class="left">
                                            <strong>Subtotal</strong>
                                        </td>
                                        <td class="right"> Rp.
                                            {{ number_format(array_sum($sub_harga), 0, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <td class="left">
                                            <strong>Kurir & Ongkir</strong><br>{{ $data->kurir }}
                                        </td>
                                        <td class="right" style="width: 46%">{{ $data->ongkir }}</td>
                                    </tr>
                                    <tr>
                                        @php
                                            $textVoucher = '';
                                            $kodePromo = '';
                                            foreach ($data['transkaksi_evoucher'] as $i => $v) {
                                                $check = $v->voucher_kode->type_voucher == 'diskon' ? ' (' . $v->voucher_kode->promo . '%) ' : ' ';
                                                $textVoucher .= $v->evocher . ', ';
                                                $kodePromo .= $v->voucher_kode->nama_promo . $check . ', ';
                                            }
                                        @endphp

                                        <td class="left">
                                            <strong>Evoucher</strong><br>
                                            {{ $kodePromo }}
                                        </td>
                                        <td class="right">


                                            {{ $textVoucher }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="left">
                                            <strong>Total</strong>
                                        </td>
                                        <td class="right">
                                            <strong>{{ $data->total_bayar }}</strong>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>

                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalTitle-form"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body form-all">
                    <form action="">
                        @method('PUT')
                        @csrf
                        <div class="modal-form">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                    <button type="button" class="btn btn-primary btn-save-form">Simpan</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('js')
<script>
            $(document).on('click', '.edit-status', function(e) {
                e.preventDefault();
                $('#modal-2').modal('show');
                $('#modalTitle-form').html('Ubah Status');
                let id = '{{$data->id}}';
                $('.modal-form').html(`
                        <div class="form-group">
                            <label for="category_name">Status</label>
                                <input type="hidden" class="form-control" id="id" name="id" value="${id}">
                                <input type="hidden" class="form-control" id="status" name="cek" value="update-status">
                                <input type="hidden" class="form-control" id="status" name="cek_detail" value="true">
                                <select class="form-control filter-status" name="status" id="">
                                    <option value="terkonfirmasi">Konfirmasi</option>
                                    <option value="rejected">Rejected</option>
                                </select>
                        </div>
                `);
            });
            $(document).on('click','.add-tracking',function(e){
                e.preventDefault();
                $('#modal-2').modal('show');
                $('#modalTitle-form').html('Update Tracking Pesanana');
                let id = '{{$data->id}}';
                $('.modal-form').html(`
                        <div class="form-group">
                            <label for="category_name">Status</label>
                                <input type="hidden" class="form-control" id="id" name="id" value="${id}">
                                <input type="hidden" class="form-control" id="status" name="cek" value="update-tracking">
                                <select class="form-control filter-status" name="status" id="">
                                    <option value="tracking">Tracking</option>
                                    <option value="selesai">Selesai</option>
                                </select>
                        </div>
                        <div class="form-group">
                            <label for="category_name">Title</label>
                                <input type="input" class="form-control" id="title" name="title" value="">
                        </div>
                        <div class="form-group">
                            <label for="category_name">Deskripsi</label>
                                <textarea class="form-control" name="deskripsi"></textarea>
                        </div>
                `);
            });
           $('.btn-save-form').click(function(e) {
                e.preventDefault();
                $('.btn-save-form').prop('disabled',true);
                $('.btn-save-form').html('<span><i class="fa fa-spinner fa-spin"></i> &nbsp; Proses</span>');
                let data = new FormData($('.form-all form')[0]);
                $('.checking-btn').html('<span><i class="fa fa-spinner fa-spin"></i></span>');

                $('.tracking-card').html(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class=" ">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b><span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                    `);

                let url = '';
                let check  = $('[name="cek"]').val();
                if(check == 'update-tracking'){
                    url = '{{ route('admin_transaksi.update_tracking') }}';
                }else{
                    url = '{{ route('admin_transaksi.update_transaksi') }}';
                }

                $.ajax({
                    type: "post",
                    url:  url,
                    data: data,
                    processData: false,
                    contentType: false,
                    dataType:'JSON',
                    start_time: new Date().getTime(),
                    success: function(data) {
                        $('.btn-save-form').prop('disabled',false);
                        $('.btn-save-form').html('simpan');
                        if(data.status){
                            // transaksi.ajax.reload();
                            $('#modal-2').modal('hide');
                            Swal.fire(
                                'Berhasil',
                                data.msg,
                                'success'
                            );
                            setTimeout(() => {

                                        if(data.data.status == 'rejected' || data.data.status == 'selesai'){
                                            console.log(data.data.status)
                                            $('.checking-btn').html('');
                                        }else{
                                            $('.checking-btn').html(`  <button class="btn btn-warning mt-2 add-tracking" title="on" type="button" value="on">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>`);
                                        }
                                        $('.text-status').html(data.data.status);
                                        $('.tracking-card').html(data.html);
                            }, (new Date().getTime() - this.start_time));

                        }else{
                            Swal.fire(
                                'Gagal!',
                                data.msg,
                                'error'
                            )
                            if(check == 'update-tracking'){
                                $('.checking-btn').html(`  <button class="btn btn-warning mt-2 add-tracking" title="on" type="button" value="on">
                                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                                    </button>`);
                                url = '{{ route('admin_transaksi.update_tracking') }}';
                            }

                        }
                    },
                });
            });
</script>
@endsection
