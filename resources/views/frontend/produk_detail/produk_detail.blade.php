@extends('frontend.template_frontend.frontend') @section('title', 'Product Detail') @section('content')
<main class="ps-main">
    {{-- <div class="test  pt-10">
        <div class="container" style="padding:0;font-size:1.em!important">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <li class="breadcrumb-item"><a href="#">Home</a></li>
                          <li class="breadcrumb-item"><a href="#">Product</a></li>
                          <li class="breadcrumb-item active" aria-current="page">Detail</li>
                          <li class="breadcrumb-item active" aria-current="page">produk-unggulan-100</li>
                        </ol>
                      </nav>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="ps-product--detail pt-60 ">
        <div class="ps-container">
            <div class="row">
                <div class="col-lg-10 col-md-12 col-lg-offset-1">
                    <div class="ps-product__thumbnail">
                        <div class="ps-product__preview">
                            <div class="ps-product__variants">
                                @if ($produk_detail->images->count())
                                    @foreach ($produk_detail->images as $p)
                                        <div class="item">
                                            <img style="width: 66px !important; height: 66px !important;"
                                                src="{{ asset('uploads/' . $p->image_name) }}" alt="">
                                        </div>
                                    @endforeach
                                @else
                                    <div class="item">
                                        <img style="width: 66px !important; height: 66px !important;"
                                            src="{{ asset('assets_frontend/images/shoe-detail/2.jpg') }}" alt="">
                                    </div>
                                @endif
                            </div>
                            @if (count($link) != 0)
                                @foreach ($link as $l)
                                    <a class="popup-youtube ps-product__video" href="{{ $l }}">
                                        <img src="{{ asset('assets_frontend/images/shoe-detail/2.jpg') }}" alt=""><i
                                            class="fa fa-play"></i>
                                    </a>
                                @endforeach
                            @endif
                        </div>
                        <div class="ps-product__image">
                            @if ($produk_detail->images->count())
                                @foreach ($produk_detail->images as $p)
                                    <div class="item">
                                        <img class="zoom" src="{{ asset('uploads/' . $p->image_name) }}"
                                            alt="" data-zoom-image="{{ asset('uploads/' . $p->image_name) }}">
                                    </div>
                                @endforeach
                            @else
                                <div class="item">
                                    <img class="zoom"
                                        src="{{ asset('assets_frontend/images/shoe-detail/3.jpg') }}" alt=""
                                        data-zoom-image="{{ asset('assets_frontend/images/shoe-detail/3.jpg') }}">
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="ps-product__thumbnail--mobile">
                        <div class="ps-product__main-img">
                            @if ($produk_detail->images->count())
                                <img src="{{ asset('uploads/' . $produk_detail->images[0]->image_name) }}"
                                    width="100%" />
                            @else
                                <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                            @endif

                        </div>
                        <div class="ps-product__preview owl-slider" data-owl-auto="true" data-owl-loop="true"
                            data-owl-speed="5000" data-owl-gap="20" data-owl-nav="true" data-owl-dots="false"
                            data-owl-item="3" data-owl-item-xs="3" data-owl-item-sm="3" data-owl-item-md="3"
                            data-owl-item-lg="3" data-owl-duration="1000" data-owl-mousedrag="on">
                            @if ($produk_detail->images->count())
                                @foreach ($produk_detail->images as $p)
                                    <img src="{{ asset('uploads/' . $p->image_name) }}" width="100%" />
                                @endforeach
                            @else
                                <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                            @endif
                        </div>
                    </div>
                    <div class="ps-product__info">
                        <div class="ps-product__rating">
                                <div class="br-wrapper br-theme-fontawesome-stars">
                                    <div class="br-wrapper br-theme-fontawesome-stars">
                                        <div class="br-widget">
                                            {{-- <a href="#" data-rating-value="1" data-rating-text="" class="br-selected br-current"></a>
                                            <a href="#" data-rating-value="1" data-rating-text="" class="br-selected br-current"></a>
                                            <a href="#" data-rating-value="1" data-rating-text="" class="br-selected br-current"></a>
                                            <a href="#" data-rating-value="1" data-rating-text="" class="br-selected br-current"></a>
                                            <a href="#" data-rating-value="2" data-rating-text="" class=""></a> --}}
                                            {!!$bintang!!}
                                        </div>
                                    </div>
                            </div>
                            {{-- <select class="ps-rating">
                                 <option value="2"></option>
                                <option value="2"></option>
                                <option value="2"></option>
                                <option value="2"></option>
                                <option value="2"></option>
                                {!!$bintang!!}
                            </select> --}}
                            <a href="#">
                                @if ($produk_detail->diskon != '' || $produk_detail->diskon != null)
                                    diskon {{ $produk_detail->diskon }}%
                                @endif
                            </a>
                        </div>
                        <h1>{{ $produk_detail->nama_produk }}</h1>
                        <p class="ps-product__category"><?php $kategory = implode(', ', array_column($produk_detail->kategori->toArray(), 'category_name')); ?>
                            {{ $kategory }}</p>
                        @if ($produk_detail->diskon != '' || $produk_detail->diskon != null)
                            @php
                                $potongan = ($produk_detail->harga_jual * $produk_detail->diskon) / 100;
                                $jml = $produk_detail->harga_jual - $potongan;
                            @endphp
                        @else
                            @php
                                $jml = 0;
                            @endphp
                        @endif
                        <h3 class="ps-product__price harga-detail" data-harga="{{$produk_detail->harga_jual}}"  data-harga-diskon="{{$jml}}">
                            @if ($produk_detail->diskon == '' || $produk_detail->diskon == null)
                                {{ 'Rp.  ' . number_format($produk_detail->harga_jual, 0, ',', '.') }}
                            @else
                                <del> {{ 'Rp.  ' . number_format($produk_detail->harga_jual, 0, ',', '.') }}</del>
                                {{ 'Rp.  ' . number_format($jml, 0, ',', '.') }}
                            @endif

                        </h3>
                        <div class="ps-product__block ps-product__quickview">
                            {{-- <h4>QUICK REVIEW</h4> --}}
                            <h4>DESKRIPSI SINGKAT</h4>
                            <p>{!! substr($produk_detail->deskripsi, 0, 150) !!} @if (strlen($produk_detail->deskripsi) >= 150)
                                    ...
                                @endif
                            </p>
                            <p><b>Note:</b> Kami juga menerima pesanan dengan sistem pre order dengan partai besar hubungi kontak admin yg tersedia.</p>

                        </p>
                        </div>
                        <div class="ps-product__block ps-product__size">
                            <h4>PILIH SIZE <a data-toggle="collapse" href="#cekStok" role="button"
                                    aria-expanded="false" aria-controls="cekStok" title="Cek Stok">Cek Stok :
                                    {{ $produk_detail->stok_sum_qty }}</a> </h4>
                            <div class="collapse" id="cekStok">
                                <div class="card card-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><b>Size</b></th>
                                                <th><b>Stok</b></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($id_size as $key => $s)
                                                @if ($id_size[$key]['id'] != null)
                                                    <tr>
                                                        <td>{{ $id_size[$key]['size_name'] }}</td>
                                                        <td>{{ $id_size[$key]['qty'] }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="product-detail-id"  data-id-produt="{{ $produk_detail->id }}">
                            <select class="ps-select selectpicker" name="size_input">
                                <option value="0" selected>Select Size</option>
                                @foreach ($id_size as $key => $s)
                                    @if ($id_size[$key]['id'] != null)
                                        @if ($id_size[$key]['qty'] <= 0)
                                            <s>
                                                <option value="{{ $key }}|{{ $id_size[$key]['id'] }}|{{ $id_size[$key]['size_name']}}"
                                                    disabled> {{ $id_size[$key]['size_name'] }}</option>
                                            </s>
                                        @else
                                            <option value="{{ $key }}|{{ $id_size[$key]['id'] }}|{{ $id_size[$key]['size_name']}}">
                                                {{ $id_size[$key]['size_name'] }}</option>
                                        @endif
                                    @endif
                                @endforeach
                            </select>
                            <div class="form-group ">
                                <input class="form-control" name="quantity_input" type="number" value="1" min="1">
                            </div>
                            </div>
                        </div>
                        <div class="ps-product__shopping"><button type="button"
                                class="ps-btn mb-10 ps-checktot-product">Add to cart<i
                                    class="ps-icon-next"></i></button>
                            {{-- <div class="ps-product__actions"><a class="mr-10" href="whishlist.html"><i
                                            class="ps-icon-heart"></i></a><a href="compare.html"><i
                                            class="ps-icon-share"></i></a></div> --}}
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="ps-product__content mt-50">
                        <ul class="tab-list" role="tablist">
                            <li class="active">
                                <a href="#tab_01" aria-controls="tab_01" role="tab" data-toggle="tab">Deskripsi</a>
                                </li>
                            <li><a href="#tab_02" aria-controls="tab_02" role="tab" data-toggle="tab" class="tab_review" data-id="{{$produk_detail->id}}">Review</a></li>
                        </ul>
                    </div>
                    <div class="tab-content mb-60">
                        <div class="tab-pane active" role="tabpanel" id="tab_01">
                            {!! $produk_detail->deskripsi !!}
                        </div>
                        <div class="tab-pane" role="tabpanel" id="tab_02">
                            <p class="mb-20">{{$jml_rev}} review dari <strong>{{ $produk_detail->nama_produk }}</strong></p>
                            <div class="ps-review card_review">
                                <div class="ps-review__thumbnail"><img src="images/user/1.jpg" alt=""></div>
                                <div class="ps-review__content">
                                    <header>
                                        <select class="ps-rating">
                                            <option value="1">1</option>
                                            <option value="1">2</option>
                                            <option value="1">3</option>
                                            <option value="1">4</option>
                                            <option value="5">5</option>
                                        </select>
                                        <p>By<a href=""> Alena Studio</a> - November 25, 2017</p>
                                    </header>
                                    <p>Soufflé danish gummi bears tart. Pie wafer icing. Gummies jelly beans powder.
                                        Chocolate bar pudding macaroon candy canes chocolate apple pie chocolate cake.
                                        Sweet caramels sesame snaps halvah bear claw wafer. Sweet
                                        roll soufflé muffin topping muffin brownie. Tart bear claw cake tiramisu
                                        chocolate bar gummies dragée lemon drops brownie.</p>
                                </div>
                            </div>
                            {{-- <form class="ps-product__review" action="_action" method="post">
                                <h4>ADD YOUR REVIEW</h4>
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                                        <div class="form-group">
                                            <label>Name:<span>*</span></label>
                                            <input class="form-control" type="text" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label>Email:<span>*</span></label>
                                            <input class="form-control" type="email" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label>Your rating<span></span></label>
                                            <select class="ps-rating">
                                                    <option value="1">1</option>
                                                    <option value="1">2</option>
                                                    <option value="1">3</option>
                                                    <option value="1">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12 ">
                                        <div class="form-group">
                                            <label>Your Review:</label>
                                            <textarea class="form-control" rows="6"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <button class="ps-btn ps-btn--sm">Submit<i
                                                        class="ps-icon-next"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- {{ var_dump($produk)}} --}}
    @if ($produk->count() > 1)
        <div class="ps-section ps-section--top-sales ps-owl-root pt-80 pb-80">
            @include('frontend.partial_home.new_product', [
                'title' => 'Related Products',
                'produk' => $produk,
            ])
        </div>
    @endif
</main>
@endsection

@section('js')
<script>
    $(document).ready(function() {
        $(document).on('click', '.owl-stage .owl-item>img',function(){
            let img =  $(this).attr('src');
            $('.ps-product__thumbnail--mobile>.ps-product__main-img>img').attr('src', img);
        });
    });
</script>
<script>
    $('.ps-checktot-product').click(function(e) {
        e.preventDefault();
        $(this).prop('disabled', true);
        $(this).html('<span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>');
        let id = $('.product-detail-id').data('id-produt');
        let size = $('[name="size_input"]').val();
        let quantity = $('[name="quantity_input"]').val();
        let harga =  $('.harga-detail').data('harga');
        let harga_diskon = $('.harga-detail').data('harga-diskon');
        $.ajax({
            url: '{{ route('chart.store') }}',
            method: 'POST',
            data: {
                id:id,
                size:size,
                quantity:quantity,
                harga:harga,
                harga_diskon:harga_diskon
            },
            success: function(data) {
                if(!data.status){
                    // $(".text-error-size").remove();
                    // $(".text-error-quantity").remove();
                    // $.each(data.data, function(field_name, error) {
                    //             $(document).find('[name=' + field_name + ']').after(
                    //                 '<small class="text-error-' + field_name +
                    //                 ' text-danger ">' + error + '</small>')
                    // });
                    toastr.warning('Periksa kembali form');
                    $('.ps-checktot-product').prop('disabled', false);
                    $('.ps-checktot-product').html('Add to cart');
                }else{
                    // $(".text-error-size").remove();
                    // $(".text-error-quantity").remove();

                    $('.flex-container-co').addClass('flex-container-co-show');
                    $('.title-co').html(data.data_co.nama_produk);
                    $('.quantity-co').html(data.data_co.quantity);
                    $('.size-co').html(data.data_co.size);
                    $('.quantity-checkout-all').html(data.countQuantity);
                    $('.img-co').attr('src', data.data_co.images);
                    let Harga;
                    if(data.data_co.harga_diskon  != 0){
                        Harga = ` <del>${data.data_co.harga}</del><br> ${data.data_co.harga_diskon}`;
                    }else{
                        Harga = ` ${data.data_co.harga}`;
                    }
                    $('.harga-co').html(Harga);
                    $('.total-co').html(data.data_co.total_harga);
                    $('.quantityCo').html(data.countQuantity);
                    $('.totalHargaCo').html(data.countTotal);


                    $('.content-chart-item').html('');

                    $.each(data.all_checkout, function(i, res){
                        let harga = 0;
                        if(data.all_checkout[i].harga_diskon != 0){
                            harga  =`<del><span>Harga:<i>Rp. ${data.all_checkout[i].harga}  </i> </span></del> <br>
                                     <span>Harga: <i>${data.all_checkout[i].harga_diskon}</i></span> <br> `;
                        }else{
                            harga  = ` <span>Harga:<i>Rp. ${data.all_checkout[i].harga}</i></span> <br>`;
                        }
                        $('.content-chart-item').append(`
                            <div class="ps-cart-item">
                                <div class="ps-cart-item__thumbnail">
                                    <a href="#"></a>
                                    <img src="${data.all_checkout[i].images}" alt="">
                                </div>
                                <div class="ps-cart-item__content" style="text-align: left">
                                    <a class="ps-cart-item__title"  href="#">${data.all_checkout[i].nama_produk}</a>
                                    <p>
                                        <span>Size:<i> ${data.all_checkout[i].size}</i></span><br>
                                        <span>Quantity:<i>${data.all_checkout[i].quantity}</i></span><br>
                                        ${harga}
                                        <span>Total:<i>Rp. ${data.all_checkout[i].total_harga}</i></span>
                                    </p>
                                </div>
                            </div>
                        `);

                    })

                    setTimeout(function() {
                        $('.flex-container-co').removeClass('flex-container-co-show');
                        $('.ps-checktot-product').prop('disabled', false);
                        $('.ps-checktot-product').html('Add to cart');
                    }, 1500);
                }
                // window.location.href = "{{ route('login') }}";
            }
        });
    });
    let idx = '';
    $('.tab_review').click(function(e){
        // $('.card_review').html('jajajd');
        let $this = $(this);
        let id = $this.data('id');
        idx =id;
        $('.card_review').html(`
        <div class="text-center">
            <span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>
        </div>`);
        $.ajax({
            method:'GET',
            url: '{{ route("product.detail_review") }}',
            data:{
                id :id
            },
            start_time: new Date().getTime(),
            success: function(data) {
                if(data.status){
                    setTimeout(() => {
                        $('.card_review').html(data.data);
                    }, (new Date().getTime() - this.start_time));
                }
            }
        });
    });

    $(document).on('click', '.pagination a', function(event) {
            event.preventDefault();
            var page = $(this).attr('href').split('page=')[1];
            fetch_data(page);
        });
    function fetch_data(page) {
            $.ajax({
                url: '{{ route("product.detail_review") }}',
                data: {
                    page: page,
                    id:idx
                },
                method: 'GET',
                start_time: new Date().getTime(),
                success: function(data) {
                    setTimeout(function() {
                        $('.card_review').html(data.data);
                    }, (new Date().getTime() - this.start_time));
                }
            });
        }
</script>
@endsection
