<?php

namespace App\Http\Controllers;

use App\Models\MasterCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class MasterCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('layout_admin.master_category');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show_data()
    {
        $data = MasterCategory::select("id", "category_name",'foto')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $actionBtn = ' <a href="javascript:void(0)" class="primary btn btn-primary btn-sm" onclick="edit(' . $row->id . ')"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" onclick="hapus(' . $row->id . ')"><i class="fa fa-trash"></i> Hapus</a>';
                return $actionBtn;
            })
            ->addColumn('foto', function ($row) {
                $gambar='<div class="row">';
                if ($row->foto != null) {
                        $gambar .= '
                            <div class="col-md-12">
                                <img src="' . asset('assets_frontend/uploads/kategori/' . $row->foto) . '" width="100%"/>
                            </div> ';
                } else {
                    $gambar .= '<div class="col-md-12">
                                    No Image
                                </div>';
                }
                $gambar .='</div>';
                return  $gambar;
            })
            ->rawColumns(['action','foto'])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        dd($request->all());
        $imgName = null;
        if ($request->hasFile('foto') && $request->foto != '') {
            $image = $request->file('foto');
            $imgName = $image->hashName();
            $image->move('assets_frontend/uploads/kategori', $imgName);
        }
        // dd($imgName);
        $data = MasterCategory::create([
            'category_name' => request('category_name'),
            'slug'=> Str::slug(request('category_name')),
            'foto'=>$imgName
        ]);

        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\MasterCategory  $masterCategory
     * @return \Illuminate\Http\Response
     */
    public function show(MasterCategory $masterCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\MasterCategory  $masterCategory
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return MasterCategory::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\MasterCategory  $masterCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = request('id');
        $data = MasterCategory::find($id);
        $imgName = $data->foto;

        if ($data) {
            if ($request->hasFile('foto') && $request->foto != '') {
                $path_old = public_path('assets_frontend/uploads/kategori') .  $data->foto;
                if (file_exists($path_old) && $data->foto != null && $data->foto != '3.jpg' && $data->foto != '2.jpg') {
                    unlink($path_old);
                }
                $image = $request->file('foto');
                $imgName = $image->hashName();
                $image->move('assets_frontend/uploads/kategori', $imgName);
            }

            $data->update(['category_name' => request('category_name'),'slug'=> Str::slug(request('category_name')), 'foto'=> $imgName]);
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\MasterCategory  $masterCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(MasterCategory $masterCategory)
    {
        $id = request('id');
        $data = MasterCategory::find($id)->delete();
        if ($data) {
            return ['success' => true];
        } else {
            return ['success' => false];
        }
    }
}
