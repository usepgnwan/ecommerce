<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PreviewProduk extends Model
{
    use HasFactory;
    protected $guarded = ['id'];
    protected $with = ['product_preview_images'];

    public function product_preview_images(){
        return $this->hasMany(PreviewProdukImage::class, 'preview_produk_id');
    }
}
