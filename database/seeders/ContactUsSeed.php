<?php

namespace Database\Seeders;

use App\Models\ContactUs;
use Illuminate\Database\Seeder;

class ContactUsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count =  $this->command->ask('how much?');
        $check = $this->command->confirm('are you sure?');
        if($check){
            ContactUs::factory()->count($count)->create();
            $this->command->info('success create contact us');
        }else{
            $this->command->info('error create contact us');
        }
    }
}
