<html>

<head>
    <style>
        /** Define the margins of your page **/
        @page {
            margin: 100px 25px;
        }

        header {
            position: fixed;
            top: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #03a9f4;
            color: #36322e;
            text-align: center;
            line-height: 35px;
        }

        footer {
            position: fixed;
            bottom: -60px;
            left: 0px;
            right: 0px;
            height: 50px;

            /** Extra personal styles **/
            background-color: #f7f7f7;
            color: #36322e;
            text-align: center;
            line-height: 35px;
        }

    </style>
    @if (isset($check))
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.1.3/dist/css/bootstrap.min.css"
            integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    @endif
</head>

<body>
    <!-- Define header and footer blocks before your content -->
    {{-- <header>
            Our Code World
        </header> --}}

    <footer>
       Terima Kasih, telah berbelanja di {{$store_setting->nama_toko}} &copy; <?php echo date('Y'); ?>
    </footer>

    <!-- Wrap the content of your PDF inside a main tag -->
    <main>
        {{-- <p style="page-break-after: always;">
                Content Page 1
            </p>
            <p style="page-break-after: always;">
                Content Page 2
            </p> --}}


        <div class="row " style=" margin: -30px 0px !important; ">
            <!-- Datatables -->
            <div class="col-lg-12">
                <div class=" ">
                    <div class="card-header">
                        Invoice :
                        <strong>{{ $data->id_pemesanan }} / {{ $data->created_at }}</strong>
                        <span class="float-right"> <strong>Status:</strong> {{ $data->status }}</span>

                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table style="width:100%">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h6 class="mb-3">Dari:</h6>
                                                <div>
                                                    <strong>{{ $store_setting->nama_toko }}</strong>
                                                </div>
                                                <div>
                                                    {!! $store_setting->alamat !!},
                                                    {{ $store_setting->provinsi }},
                                                    {{ $store_setting->kota }}
                                                    {{ $store_setting->kode_pos }}
                                                </div>
                                                <div>Email: {{ $store_setting->email }} </div>
                                                <div>Telp (WA): {{ $store_setting->telepon }}</div>
                                            </td>
                                            <td>
                                                <h6 class="mb-3">Kepada:</h6>
                                                <div>
                                                    <strong>{{ $data->nama_depan }}
                                                        {{ $data->nama_belakang }}</strong>
                                                </div>
                                                <div>
                                                    {!! $data->alamat_lengkap !!},
                                                    {{ $data->provinsi }},
                                                    {{ $data->kota }}
                                                    {{ $data->kode_pos }}
                                                </div>
                                                <div>Email: {{ $data->email }} </div>
                                                <div>Telp (WA): {{ $data->no_telp }}</div>
                                                <p><strong> Catatan Pesanan</strong>: {{ $data->catatan }}</p>

                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="table-responsive-sm">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th class="center">#</th>
                                        <th>Item</th>
                                        <th>Ukuran</th>
                                        <th>Diskon Item</th>
                                        <th class="right">Harga</th>
                                        <th class="center">Qty</th>
                                        <th class="right">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $sub_harga = [];
                                    @endphp
                                    @foreach ($data->detail_transaksi as $c)
                                        <tr>
                                            <td class="center">{{ $loop->iteration }}</td>
                                            <td class="left strong">{{ $c->nama_produk }}</td>
                                            <td class="left">{{ $c->size }}</td>
                                            <td class="left">
                                                {{ $disc = $c->diskon == 0 || $c->diskon == null ? '-' : $c->diskon . '%' }}
                                            </td>

                                            <td class="right">{!! $hargaDisc = $c->diskon == 0 || $c->diskon == null ? 'Rp.' . number_format($c->sub_total, 0, ',', '.') : '<del>Rp.' . number_format($c->sub_total, 0, ',', '.') . '</del> Rp.' . number_format($c->harga_diskon, 0, ',', '.') !!}</td>
                                            <td class="center">{{ $c->quantity }}</td>
                                            <td class="right">
                                                Rp.{{ number_format($c->total_item, 0, ',', '.') }}
                                            </td>
                                            @php array_push($sub_harga, $c->total_item) @endphp
                                        </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-sm-5">
                            </div>

                            <div class="col-lg-5 col-sm-5 ml-auto">
                                <table class="table table-clear">
                                    <tbody>
                                        <tr>
                                            <td class="left">
                                                <strong>Total Item</strong>
                                            </td>
                                            <td class="right">
                                                {{ $data->detail_transaksi_sum_quantity }}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Subtotal</strong>
                                            </td>
                                            <td class="right"> Rp.
                                                {{ number_format(array_sum($sub_harga), 0, ',', '.') }}</td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Kurir & Ongkir</strong><br>{{ $data->kurir }}
                                            </td>
                                            <td class="right" style="width: 46%">{{ $data->ongkir }}</td>
                                        </tr>
                                        <tr>
                                            @php
                                                $textVoucher = '';
                                                $kodePromo = '';
                                                foreach ($data['transkaksi_evoucher'] as $i => $v) {
                                                    $check = $v->voucher_kode->type_voucher == 'diskon' ? ' (' . $v->voucher_kode->promo . '%) ' : ' ';
                                                    $textVoucher .= $v->evocher . ', ';
                                                    $kodePromo .= $v->voucher_kode->nama_promo . $check . ', ';
                                                }
                                            @endphp

                                            <td class="left">
                                                <strong>Evoucher</strong><br>
                                                {{ $kodePromo }}
                                            </td>
                                            <td class="right">


                                                {{ $textVoucher }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="left">
                                                <strong>Total</strong>
                                            </td>
                                            <td class="right">
                                                <strong>{{ $data->total_bayar }}</strong>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>

    </main>
</body>

</html>
