@extends('frontend.template_frontend.frontend') @section('title', 'Login & Registrasi') @section('content')
<div class="container mb-2 mt-2">
    <style>
        .form-login-regis {
            margin: 0px auto;
            width: 600px;
        }

        @media only screen and (max-width: 600px) {
            .form-login-regis {
                margin: 10px auto;
                width: 100%;
            }
        }

    </style>
    <div class="row">
        <div class=" form-login d-flex align-items-center justify-content-center">
            <form>
                <div class="container form-login-regis">
                    <div class="header-form">
                        <h1>- Masuk</h1>
                    </div>
                    <div class="row">
                        @if (session()->has('success'))
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="form-group">
                                    <div class="alert alert-success" role="alert">
                                        <strong>Selamat</strong> {{ session('success') }}
                                        <button type="button" class="close" data-dismiss="alert"
                                            aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        @endif
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="form-group">
                                <label>Email <sub>*</sub></label>
                                <input class="form-control" type="email" placeholder="" name="email">
                            </div>
                        </div>
                        <div class="form-add">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="form-group">
                                    <label>Password <sub>*</sub></label>
                                    <input class="form-control" type="password" placeholder="" name="password">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="mt-3">
                        <div class="forget-pass">
                            <p> <a href="#"> lupa kata sandi anda ?</a> </p>
                        </div>
                        <div style="margin:15px auto">
                            <p id="unvalid"></p>
                            <button type="submit" class="btn btn-danger btn-login" value="1">Login</button>
                            <button type="button" class="btn btn-outline-success btn-regis" value="0">Bergabunglah
                                dengan kami</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('js')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $('.btn-regis').click(function() {
            $('#unvalid').html('');
            let check = $(this).val();
            if (check == 0) {
                $(this).html('kembali Ke login');
                $('.btn-login').html('Bergabunglah dengan kami');
                $(this).val(1);
                $('.header-form').html(`
                            <h1>- Bergabunglah dengan Kami</h1>
                            <p style="text-align: justify;">Halo, untuk memberikan pengalaman berbelanja yang lebih  menyenangkan dan tawaran menarik di Bill Distro. Segera Bergabung, gunakan alamat email Anda sebagai username. jika anda memerlukan bantuan, Hubungi kami melalui email atau nomer  berikut 08555 atau billdistrooo</p>
                    `);

                $('.form-add').html(`
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="form-group">
                                <label>Nama <sub>*</sub></label>
                                <input class="form-control" type="text" placeholder="" name="nama">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="form-group">
                                <label>Nomer Telp/Wa <sub>*</sub></label>
                                <input class="form-control" type="text" placeholder="" name="no_telp">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="phone">Tanggal Lahir<sub>*</sub></label>
                                <input type="date" class="form-control" id="tanggal_lahir"
                                    placeholder="Masukan Tanggal Lahir" name="tanggal_lahir">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="row gutters">
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <h6 class="mt-3 mb-2 text-primary">Jenis Kelamin<sub>*</sub></h6>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            id="flexRadioDefault1" name="jenis_kelamin" value="laki-laki" checked>
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Laki-laki
                                        </label>
                                    </div>
                                </div>
                                <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio"
                                            id="flexRadioDefault1" name="jenis_kelamin" value="perempuan">
                                        <label class="form-check-label" for="flexRadioDefault1">
                                            Perempuan
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="form-group">
                                <label>Password <sub>*</sub></label>
                                <input class="form-control" type="password" placeholder="" name="password">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="form-group">
                                <label>Ulangi Password <sub>*</sub></label>
                                <input class="form-control" type="password" placeholder="" name="repeat_password">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3" >
                            <div class="form-check">
                                <input type="checkbox" class="form-check-input" id="exampleCheck1" name="privacy_policy">
                                <span class="form-check-label" for="exampleCheck1">Saya menyetujui untuk mengizinkan Bill Distro memproses data pribadi saya untuk mengelola akun pribadi saya, sesuai dengan Privacy Notice </span>
                                <br>
                                <span id="check-privasi"></span>
                            </div>
                        </div>
                    `);
                $('.forget-pass').prop('hidden', true);
                $('.btn-login').val(0);
            } else {
                $(this).html('Bergabunglah dengan kami');
                $('.btn-login').html('Login');
                $(this).val(0);
                $('.header-form').html(`
                            <h1>- Masuk</h1>
                    `);
                $('.form-add').html(`
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                    <div class="form-group">
                                        <label>Password <sub>*</sub></label>
                                        <input class="form-control" type="password" placeholder="" name="password">
                                    </div>
                                </div>
                    `);
                $('.forget-pass').prop('hidden', false);
                $('.btn-login').val(1);
            }
        });

        $('.form-login form').submit(function(e) {
            e.preventDefault();
            $('.btn-login').prop('disabled', true);
            let data = $('.form-login form').serializeArray();
            // let data = new FormData(this);
            // console.log(data);
            let check = $('[name="privacy_policy"]').is(':checked');
            let checkVal = $('.btn-login').val();
            let url;
            if (checkVal == 1) {
                url = "{{ route('login.post') }}";
                login_form('login', data, url);
            } else {
                url = "{{ route('registrasi') }}";
                if (check) {
                    $('#check-privasi').html('');
                    login_form('regis', data, url);
                } else {
                    $('.btn-login').prop('disabled', false);
                    $('#check-privasi').html(
                        '<small class="text-danger">Mohon Diceklis Untuk Melanjutkan Registrasi</small>'
                    );
                }
            }
        });


        function login_form(type, data, url) {
            $.ajax({
                url: url,
                data: data,
                method: 'POST',
                dataType: 'JSON',
                success: function(data) {
                    $('.btn-login').prop('disabled', false);
                    // console.log(data)
                    if (!data.status && !data.valid) {
                        toastr.error(data.msg);
                        $('#unvalid').html('');
                        if (type == 'regis') {
                            $(".text-error-nama").remove();
                            $(".text-error-no_telp").remove();
                            $(".text-error-repeat_password").remove();
                            $(".text-error-tanggal_lahir").remove();
                        }
                        $(".text-error-password").remove();
                        $(".text-error-email").remove();
                        $.each(data.data, function(field_name, error) {
                            $(document).find('[name=' + field_name + ']').after(
                                '<span class="text-error-' + field_name +
                                ' text-danger ">' + error + '</span>')
                        });
                    }

                    if (data.status && data.valid) {
                        if (type == 'regis') {
                            toastr.success(data.msg);
                            window.location.href = "{{ route('login') }}";
                        } else if (type == 'login') {
                            toastr.success(data.msg);
                            if (data.data.role.description == 'admin') {
                                window.location.href = "{{ route('admin.index') }}";
                            } else {
                                window.location.href = "{{ route('homepage') }}";
                            }
                        }
                    } else if (!data.status && data.valid) {
                        toastr.error(data.msg);
                        if (type == 'login') {
                            $(".text-error-password").remove();
                            $(".text-error-email").remove();
                            $('#unvalid').html(
                                `<small class="text-danger">Tidak Valid Email atau Password Salah.</small>`
                            );
                        }
                    }
                }
            });
        }
    });
</script>
@endsection
