@extends('template_admin.admin')

@section('title', 'Master Product')
@section('container')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Master Product</h1>

    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="from-group mb-2">
                <a class="btn btn-primary" value="new" href="{{ route('product.create') }}"><i
                        class="fa fa-plus-circle"></i> Tambah Product</a>
            </div>
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <!-- <h6 class="m-0 font-weight-bold text-primary">DataTables</h6> -->
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th style="width: 30%">Nama Produk</th>
                                {{-- <th>Kategori</th>
                                    <th>Diskon</th> --}}
                                <th>Harga</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script>
        // datatabel
        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('product.dataproduct') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'gambar',
                    name: 'gambar',
                },
                // {
                //     data: 'id_kategori',
                //     name: 'id_kategori'
                // },
                {
                    data: 'harga_jual',
                    name: 'harga_jual'
                },
                // {
                //     data: 'diskon',
                //     name: 'diskon'
                // },
                {
                    data: 'status_produk',
                    name: 'status_produk'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });


        function open_modal(judul, tombolSimpan, method) {
            $('#modal').modal('show');
            $('#tombolsimpan').text(tombolSimpan);
            $('.modal-title').text(judul);
            $('#form')[0].reset();
            if (method != 'edit') {
                $("input[name=id]").val('');
                $("input[name=_method]").val('');
                $('#tombolsimpan').attr('onclick', 'aksi("tambah")')
            } else {
                $('#tombolsimpan').attr('onclick', 'aksi("edit")')
            }
        };

        function tambah() {
            open_modal('Tambah Product', 'Simpan', 'tambah')
        }

        function edit(id) {
            var link = " {{ route('product.edit', 'id') }}";
            var link_tujuan = link.replace("id", id);
            $.ajax({
                type: "get",
                url: link_tujuan,
                success: function(response) {
                    open_modal('Edit Product', 'Ubah', 'edit')
                    $('#id').val(response.id);
                    $('#Product_name').val(response.Product_name);
                    $('#method').html('@method("put")')
                }
            });
        }

        function hapus(id) {
            var link = "{{ route('product.destroy', 'id') }}";
            link = link.replace('id', id)
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang dihapus tidak dapat dikembalikan lagi!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal',
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "post",
                        url: link,
                        data: {
                            "id": id,
                            "_method": "delete",
                            "_token": "{{ @csrf_token() }}"
                        },
                        success: function(response) {
                            if (response.success == true) {
                                $('#modal').modal('hide');
                                Swal.fire(
                                    'Berhasil!',
                                    'Data berhasil dihapus!',
                                    'success'
                                )
                                table.ajax.reload();
                            } else {
                                Swal.fire(
                                    'Gagal!',
                                    'Data gagal dihapus!',
                                    'error'
                                )
                            }
                        }
                    });
                }
            })
        }

        function aksi(method) {
            if (method == 'tambah') {
                $.ajax({
                    type: "post",
                    url: "{{ route('product.store') }}",
                    data: $('#form').serialize(),
                    success: function(response) {

                        if (response.success == true) {
                            $('#modal').modal('hide');
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            table.ajax.reload();
                        } else {
                            Swal.fire(
                                'Gagal!',
                                'Data gagal disimpan!',
                                'error'
                            )
                        }
                    }
                });

            } else if (method == 'edit') {
                var link = "{{ route('product.update', 'id') }}";
                link = link.replace('id', $('#id').val())
                $.ajax({
                    type: "post",
                    url: link,
                    data: $('#form').serialize(),
                    success: function(response) {
                        if (response.success == true) {
                            $('#modal').modal('hide');
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            table.ajax.reload();
                        } else {
                            Swal.fire(
                                'Gagal!',
                                'Data gagal disimpan!',
                                'error'
                            )
                        }
                    }
                });

            }

        }

        // swall if success
        @if (Session::get('data'))
            Swal.fire(
            'Berhasil!',
            '{{ Session::get('data') }}',
            'success'
            )
        @endif
    </script>
@endsection
