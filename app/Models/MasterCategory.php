<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterCategory extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = "master_category";
    protected $fillable = ['category_name','slug','foto'];

    public function product()
    {
        return $this->belongsToMany(Produk::class, 'produk_kategori');
    }
}
