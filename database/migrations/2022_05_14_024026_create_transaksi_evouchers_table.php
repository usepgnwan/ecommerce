<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransaksiEvouchersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksi_evouchers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaksi_id')
            ->references('id')
            ->on('transaksis')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreignId('voucher_kode_id')
            ->references('id')
            ->on('voucher_kodes')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('evocher');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaksi_evouchers');
    }
}
