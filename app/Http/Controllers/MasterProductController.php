<?php

namespace App\Http\Controllers;

use App\Models\KategoriProduk;
use App\Models\MasterCategory;
use App\Models\MasterSize;
use App\Models\Produk;
use App\Models\ProductImage;
use App\Models\ProductStock;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class MasterProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Daftar Produk";
        // $data = Produk::select("id", "nama_produk", "harga_jual", "harga_beli", "diskon", "status")->get();
        // echo json_encode($data);die;
        return view('layout_admin.master_product.master_product', compact('title'));
    }


    public function show_data()
    {
        $data = Produk::withSum('stok', 'qty')->orderBy('id', 'DESC')->get();
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('gambar', function ($row) {
                // $kategori = '';
                // foreach ($row->kategori as $key) {
                //     $kategori .= $key->category_name . ', ';
                // }
                $kategori = implode(', ', array_column($row->kategori->toArray(), 'category_name'));
                // ProductStock::select(DB::raw('sum(qty) as stok'))->where(['id_produk' => $row->id])->first()->stok
                $gambar = '
                    <div class="row">
                    <div class="col-8">
                    <h4>   <a href="'. route("products.product_details",["slug"=> $row->slug ]) .'" target="_blank">' . $row->nama_produk . '</a></h4>
                    <p style="font-size:10px;">Category: <br>' . $kategori . '</p>
                    <p style="font-size:10px;">
                    Size : ' .  implode(', ', array_column(array_column($row->stok->toArray(), 'size'), 'size_name')) . ' <br>
                    Stok: ' . $row->stok_sum_qty . ' <br>
                    Image: '. $row->images->count() .'</p>
                    </div>';
                $gambar .= '<div class="row mb-2">';
                if ($row->images->count()) {
                    $gambar .= '<div class="col-md-4">
                                        <img src="' . asset('uploads/' . $row->images[0]->image_name) . '" width="100%"/>
                                    </div> ';
                    // foreach ($row->images as $i => $val) {
                    //     $gambar .= '<div class="col-md-4">
                    //                     <img src="' . asset('uploads/' . $val->image_name) . '" width="100%"/>
                    //                 </div> ' . $row->images->count();
                    // }
                } else {
                    $gambar .= '<div class="col-md-12">
                                    No Image
                                </div>';
                }
                $gambar .= '</div>';
                return $gambar;
            })
            ->addColumn('status_produk', function ($row) {
                $preview_produk = DB::Select('SELECT sum(all_item) all_item, sum(jumlah_star) jumlah_star FROM (SELECT count(*) all_item, sum(star) jumlah_star  FROM `preview_produks` WHERE product_id = '.$row->id.' And status ="aktif" GROUP by star ) x');
                $jml_transaksi = DB::Select('SELECT count(*) jml FROM transaksis ts JOIN  detail_transaksis dt on ts.id =dt.transaksi_id WHERE ts.status ="selesai" and dt.product_id =  '.$row->id.'');
                $jml_transaksi = collect($jml_transaksi)->first()->jml;
                $preview_produk = collect($preview_produk)->first();

                $rata_rata = $preview_produk->all_item != null || $preview_produk->jumlah_star != null? round($preview_produk->jumlah_star/$preview_produk->all_item) : 0;
                $rate = [];
                for ($i = 5; $i > 0; $i--) {
                    if ($i <= $rata_rata) {
                        $rate[$i] = '<i class="fa fa-star text-warning"></i>';
                    } else {
                        $rate[$i] = '<i class="fa fa-star "></i>';
                    }
                }
                $jml_rev = $preview_produk->all_item != null ? $preview_produk->all_item: 0;
                $review = '
                <div class="row">
                    <div class="col-12">
                    <small>Transaksi Selesai: '. $jml_transaksi.'</small><br>
                    <small>Jumlah Review Aktif: '. $jml_rev.'</small>
                    </div>
                    <div class="col-12">
                        <label>' . $rate[1] . '' . $rate[2] . '' . $rate[3] . '' . $rate[4] . '' . $rate[5] . '</label>
                    </div>
                    <br>
                </div> ';



                if ($row->status == 'Diposting') {
                    $status = '<span class="badge badge-lg badge-success">' . $row->status . '</span>' . $review;
                } else {
                    $status = '<span class="badge badge-lg badge-danger">' . $row->status . '</span>' . $review;
                }
                return $status;
            })
            ->addColumn('harga_jual', function ($row) {
                if ($row->diskon == "" || $row->diskon == null) {
                    $hasil_rupiah = "Rp.  " . number_format($row->harga_jual, 0, ',', '.');
                } else {
                    $potongan = $row->harga_jual * $row->diskon / 100;
                    $jml = $row->harga_jual - $potongan;
                    $hasil_rupiah = '<small><del>Rp. ' . number_format($row->harga_jual, 0, ',', '.') . '</del></small> <br> ' . "Rp.  " . number_format($jml, 0, ',', '.') . '<br>( disc: ' . $row->diskon . '%)';
                }
                return $hasil_rupiah;
            })
            ->addColumn('action', function ($row) {
                $actionBtn = '  <a href="' . route("product.show", $row->id) . '" class="primary btn btn-success btn-sm"><i class="fa fa-eye"></i> </a>
                                <a href="' . route("product.edit", $row->id) . '" class="primary btn btn-primary btn-sm"><i class="fa fa-edit"></i> </a>
                                <a href="javascript:void(0)" class="delete btn btn-danger btn-sm" onclick="hapus(' . $row->id . ')"><i class="fa fa-trash"></i></a>
                                <a href="' . route("penilain.show", $row->id) . '" title="lihat penilaian produk" class=" btn btn-outline-warning btn-sm"><i class="fa fa-star"></i></a>
                                <a href="' . route("product.list_transaksi_selesai", $row->id) . '" title="lihat transaksi produk" class=" btn btn-outline-primary btn-sm"><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                                ';
                return $actionBtn;
            })
            ->rawColumns(['action', 'status_produk', 'gambar', 'harga_jual'])
            ->make(true);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = MasterCategory::get();
        $sizes = MasterSize::get();
        return view('layout_admin.master_product.tambah_product', compact('categories', 'sizes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'size.*' => 'required',
            'stok.*' => 'required',
            'kategori' => 'required',
            'harga_beli' => 'required',
            'harga_jual' => 'required',
            'status' => 'required',
        ];
        $customMessages = [
            'required' => 'Kolom :attribute wajib diisi.'
        ];
        $this->validate($request, $rules, $customMessages);

        $embed = json_encode($request->embed_youtube);
        $embed == 'null' ? $embed = NULL : $embed;
        $produk = Produk::create([
            'status' => request('status'),
            'nama_produk' => request('nama_produk'),
            // 'id_kategori' => request('kategori'),
            'deskripsi' => request('deskripsi'),
            'harga_beli' => request('harga_beli'),
            'harga_jual' => request('harga_jual'),
            'diskon' => request('diskon'),
            'slug' => \Str::slug(request('nama_produk')),
            'embed' =>  $embed,
        ]);

        $category = request('kategori');
        foreach ($category as $key) {
            KategoriProduk::create([
                'produk_id' => $produk->id,
                'master_category_id' => $key
            ]);
        }

        $data = $request->stok;
        $i = -1;
        foreach ($request->size as $size) {
            $i++;
            ProductStock::create([
                'id_produk' => $produk->id,
                'size' => $size,
                'qty' => $data[$i]
            ]);
        }

        if ($request->hasFile('foto')) {
            $allowedfileExtension = ['jpg', 'png', 'jpeg'];
            $files = $request->file('foto');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                // dd($check);
                if ($check) {
                    $filename = $file->store('product_images', ['disk' => 'public_uploads']);
                    ProductImage::create([
                        'produk_id' => $produk->id,
                        'image_name' => $filename
                    ]);
                }
            }
        }
        return Redirect::route('product.index')->with(['data' => 'Produk Berhasil Ditambahkan!']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Produk::where(['id' => $id])->first();
        $title = $product->nama_produk;

        return view('layout_admin.master_product.preview', compact('product', 'title'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $title = "Edit Produk";
        $product = [];
        $categories = MasterCategory::get();
        // dd($categories);
        $sizes = MasterSize::get();
        $product = Produk::where(['id' => $id])->first();
        $product['stok'] = ProductStock::where(['id_produk' => $id])->get();
        $product['images'] = ProductImage::where(['produk_id' => $id])->get();
        return view('layout_admin.master_product.edit_produk', compact('product', 'title', 'categories', 'sizes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $rules = [
            'nama_produk' => 'required',
            'deskripsi' => 'required',
            'size.*' => 'required',
            'stok.*' => 'required',
            'kategori' => 'required',
            'harga_beli' => 'required',
            'harga_jual' => 'required',
            'status' => 'required',
        ];
        $customMessages = [
            'required' => 'Kolom :attribute wajib diisi.'
        ];
        $this->validate($request, $rules, $customMessages);

        $embed = json_encode($request->embed_youtube);
        $embed == 'null' ? $embed = NULL : $embed;

        $data1 = [];
        $data1['status'] = request('status');
        $data1['nama_produk'] = request('nama_produk');
        $data1['deskripsi'] = request('deskripsi');
        $data1['harga_beli'] = request('harga_beli');
        $data1['harga_jual'] = request('harga_jual');
        $data1['diskon'] = request('diskon');
        $data1['slug'] = \Str::slug(request('nama_produk'));
        $data1['embed'] = $embed;

        $produk = Produk::where(['id' => $id])->update($data1);

        $status_kategori = KategoriProduk::where(['produk_id' => $id])->delete();
        if ($status_kategori) {
            foreach (request('kategori') as $kategori) {
                KategoriProduk::create([
                    'produk_id' => $id,
                    'master_category_id' => $kategori
                ]);
            }
        }

        $data = $request->stok;
        $i = -1;
        foreach ($request->size as $size) {
            $i++;
            $status = ProductStock::where(['id_produk' => $id, 'size' => $size])->count();
            if ($status == 0) {
                ProductStock::create([
                    'id_produk' => $id,
                    'size' => $size,
                    'qty' => $data[$i]
                ]);
            } else if ($status > 0) {
                ProductStock::where(['id_produk' => $id, 'size' => $size])->update([
                    'id_produk' => $id,
                    'size' => $size,
                    'qty' => $data[$i]
                ]);
            }
        }

        if ($request->hasFile('foto')) {
            $allowedfileExtension = ['pdf', 'jpg', 'png', 'docx', 'jpeg'];
            $files = $request->file('foto');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                $extension = $file->getClientOriginalExtension();
                $check = in_array($extension, $allowedfileExtension);
                // dd($check);
                if ($check) {
                    $filename = $file->store('product_images', ['disk' => 'public_uploads']);
                    ProductImage::create([
                        'produk_id' => $id,
                        'image_name' => $filename
                    ]);
                }
            }
        }
        return Redirect::route('product.index')->with(['data' => 'Produk Berhasil Diubah!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Models\Produk  $produk
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Produk::where(['id' => $id])->delete();
        if ($data) {
            return ['success' => true];
        }
    }

    public function remove_item()
    {
        if (request('item') == 'stok') {
            $data = ProductStock::where(['id' => request('id')])->delete();
        } else if (request('item') == 'image') {
            $image = ProductImage::where(['id' => request('id')]);
            $image_path = 'uploads/' . $image->first()->image_name;
            if (File::exists($image_path)) {
                File::delete($image_path);
            }
            $data = $image->delete();
        }
        if ($data) {
            return ['success' => true];
        }
    }

    public function remove_kategori()
    {
        $data = KategoriProduk::where(['master_category_id' => request('master_category_id'), 'produk_id' => request('produk_id')])->delete();

        return ['success' => true];
    }

    public function list_transaksi_selesai(Request $request, $id){
        $data = DB::Select('SELECT ts.id, ts.id_pemesanan, CONCAT(ts.nama_depan, " ", ts.nama_belakang) nama, ts.created_at tgl_beli, ts.email,ts.no_telp, dt.* FROM transaksis ts JOIN  `detail_transaksis` dt on ts.id =dt.transaksi_id WHERE ts.status ="selesai" and dt.product_id = '.$id.'');
        $data = collect($data);
        // return response()->json($data);
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('harga', function ($row) {
                    // Carbon::parse($pr->created_at)->format('d M Y, H:i')
                    $disc = $row->diskon == 0 || $row->diskon == null ? '-' : $row->diskon . '%';

                    $hargaDisc = $row->diskon == 0 || $row->diskon == null ? 'Rp.' . number_format($row->sub_total, 0, ',', '.') : '<del>Rp.' . number_format($row->sub_total, 0, ',', '.') . '</del> Rp.' . number_format($row->harga_diskon, 0, ',', '.');
                    $review = '
                    <div class="row">
                        <div class="col-12">
                        <small>Diskon: '. $disc.'</small><br>
                        <small>Harga: '. $hargaDisc.'</small>
                        </div>
                        <br>
                    </div> ';
                    return $review;
                })
                ->addColumn('kontak', function ($row) {
                    $review = '
                    <div class="row">
                        <div class="col-12">
                        <small>Email: '. $row->email.'</small><br>
                        <small>No Telp: '. $row->no_telp.'</small>
                        </div>
                        <br>
                    </div> ';
                    return $review;
                })
                ->addColumn('total_item', function ($row) {
                    $total_harga = 'Rp.' . number_format($row->total_item, 0, ',', '.');
                    return $total_harga;
                })
                ->editColumn('tgl_beli', function ($row) {
                    return '<small>'. Carbon::parse($row->tgl_beli)->format('d M Y, H:i A') . '</small>';
                })
                ->editColumn('id_pemesanan', function ($row) {
                    return '<small>'.  $row->id_pemesanan  . '</small>';
                })
                ->rawcolumns(['id_pemesanan','tgl_beli','harga','kontak','total_item'])
                ->make(true);
        }
        return view('layout_admin.master_product.transaksi_selesai_produk', compact('data'));
    }
}
