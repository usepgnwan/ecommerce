<?php

namespace App\Http\Controllers;

use App\Models\VoucherKode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class VoucherKodeController extends Controller
{
    protected $voucher;
    public function __construct(VoucherKode $voucher)
    {
        $this->voucher = $voucher;
    }
    public function index(Request $request)
    {
        $data = $this->voucher->all();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" class="btn btn-warning btn-sm edit-promo"  data-id="' . $row->id . '"><i class="fa fa-solid fa-edit"></i></a>';
                    $btn .= ' <a href="javascript:void(0)" class="btn btn-danger btn-sm hapus-promo"  data-id="' . $row->id . '"><i class="fa fa-solid fa-trash"></i></a>';
                    if ($row->status == 'on') {
                        $btn .= ' <a href="javascript:void(0)" class="btn mt-1 btn-primary btn-sm on-status" title="non aktifkan" data-status="off"  data-id="' . $row->id . '"><i class="fa fa-power-off" aria-hidden="true"></i></a>';
                    } else {
                        $btn .= ' <a href="javascript:void(0)" class="btn mt-1 btn-danger btn-sm on-status" title="aktifkan" data-status="on" data-id="' . $row->id . '"><i class="fa fa-power-off" aria-hidden="true"></i></a>';
                    }
                    return $btn;
                })
                ->addColumn('promo', function ($row) {
                    if($row->type_voucher == 'diskon'){
                        return 'Diskon ' . $row->promo . '%';
                    }else{
                       return   "Potongan Rp." . number_format($row->promo , 0, ',', '.');
                    }
                })
                ->addColumn('type_user', function ($row) {
                    if($row->type_user == 'login'){
                        return 'User Login' ;
                    }else{
                        return  'All User';
                    }
                })
                ->addColumn('type_voucher', function ($row) {
                    return \Str::upper($row->type_voucher);
                })
                ->addColumn('periode_awal', function ($row) {
                    return date('d M Y', strtotime($row->periode_awal));
                })
                ->addColumn('periode_akhir', function ($row) {
                    return date('d M Y', strtotime($row->periode_akhir));
                })
                ->rawColumns(['action', 'promo','type_user'])
                ->make(true);
        }
    }

    public function update_status(Request $request, $id)
    {
        $data = [];
        $msg = '';
        if ($request->status == 'off') {
            $data = [
                'status' => 'off',
            ];
            $msg = 'Voucher / Promo Berhasil dinonaktifkan.';
        } else {
            $data = [
                'status' => 'on',
            ];
            $msg = 'Voucher / Promo Berhasil diaktifkan.';
        }


        $check = $this->voucher->find($id);
        if ($check) {
            $check->fill($data)->save();
            return response()->json(
                [
                    'status' => true,
                    'msg' => $msg,
                    'data' => $check
                ]
            );
        }
    }


    public function edit($id)
    {
        $check = $this->voucher->find($id);
        if ($check) {
            $check->first();
            return response()->json(['status' => true, 'msg' => 'data ada', 'data' => $check]);
        }
        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }

    public function destroy($id)
    {
        $check = $this->voucher->find($id);

        if ($check) {
            $check->delete();
            return response()->json(['status' => true, 'msg' => 'Berhasil Hapus Data']);
        }
        return response()->json(['status' => false, 'msg' => 'Gagal Hapus Data']);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_promo' => 'required',
            'type_user' => 'required|not_in:0',
            'type_voucher' => 'required|not_in:0',
            'kode' => 'required|not_in:0|unique:voucher_kodes,kode',
            'promo' => 'required|not_in:0',
            'kuota' => 'required',
            'periode_awal' => 'required',
            'periode_akhir' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Simpan Promo / E-Voucher.', 'data' => $validate->errors()]);
        }

        $data = [
            'nama_promo' => $request->nama_promo,
            'type_user' => $request->type_user,
            'type_voucher' => $request->type_voucher,
            'kode' => $request->kode,
            'kuota' => $request->kuota,
            'promo' => $request->promo,
            'periode_awal' => $request->periode_awal,
            'periode_akhir' => $request->periode_akhir,
            'status' => 'on',
        ];

        $voucher = $this->voucher->create($data);
        return response()->json([
            'status' => true,
            'msg' => ' Promo / E-Voucher Berhasil disimpan.',
            'data' =>  $voucher
        ]);
    }


    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'nama_promo' => 'required',
            'type_user' => 'required|not_in:0',
            'type_voucher' => 'required|not_in:0',
            'kode' => 'required|not_in:0|unique:voucher_kodes,kode,'. $id . '',
            'promo' => 'required|not_in:0',
            'kuota' => 'required',
            'periode_awal' => 'required',
            'periode_akhir' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Promo / E-Voucher.', 'data' => $validate->errors()]);
        }

        $data = [
            'nama_promo' => $request->nama_promo,
            'type_user' => $request->type_user,
            'type_voucher' => $request->type_voucher,
            'kode' => $request->kode,
            'promo' => $request->promo,
            'kuota' => $request->kuota,
            'periode_awal' => $request->periode_awal,
            'periode_akhir' => $request->periode_akhir,
            'status' => 'on',
        ];

        $check = $this->voucher->find($id);
        if ($check) {
            $check->fill($data)->save();
            return response()->json([
                'status' => true,
                'msg' => 'Promo / E-Voucher Berhasil diubah.',
                'data' => $check
            ]);
        }

        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }
}
