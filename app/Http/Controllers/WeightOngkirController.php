<?php

namespace App\Http\Controllers;

use App\Models\WeightOngkir;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class WeightOngkirController extends Controller
{
    protected $weightOngkir;
    public function __construct(WeightOngkir $weightOngkir)
    {
        $this->weightOngkir = $weightOngkir;
    }
    public function index(Request $request)
    {
        $data = $this->weightOngkir->all();
        // echo json_encode($data);
        // return response()->json($user);die;
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" class="btn btn-warning btn-sm edit-weight"  data-id="' . $row->id . '"><i class="fa fa-solid fa-edit"></i></a>';
                    $btn .= ' <a href="javascript:void(0)" class="btn btn-danger btn-sm hapus-weight"  data-id="' . $row->id . '"><i class="fa fa-solid fa-trash"></i></a>';
                    return $btn;
                })
                ->addColumn('weight_kg', function ($row) {
                    return $row->weight_kg . ' kg';
                })
                ->addColumn('weight_gram', function ($row) {
                    return $row->weight_gram . ' gram';
                })
                ->rawColumns(['action', 'weight_kg', 'weight_gram'])
                ->make(true);
        }

        return view('layout_admin.master_ongkir');
    }

    public function destroy($id)
    {
        $check = $this->weightOngkir->find($id);

        if ($check) {
            $check->delete();
            return response()->json(['status' => true, 'msg' => 'Berhasil Hapus Data']);
        }
        return response()->json(['status' => false, 'msg' => 'Gagal Hapus Data']);
    }
    public function edit($id)
    {
        $check = $this->weightOngkir->find($id);
        if ($check) {
            $check->first();
            return response()->json(['status' => true, 'msg' => 'data ada', 'data' => $check]);
        }
        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'pcs_dari' => 'required|not_in:0',
            'pcs_sampai' => 'required|not_in:0',
            'weight_kg' => 'required|not_in:0',
            'weight_gram' => 'required|not_in:0',
        ]);

        if ($request->pcs_dari > $request->pcs_sampai) {
            $validate->errors()->add('pcs_dari', 'item dari tidak boleh lebih besar dari item sampai');
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Berat Ongkir.', 'data' => $validate->errors()]);
        }

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Simpan Berat Ongkir.', 'data' => $validate->errors()]);
        }

        $data = [
            'pcs_dari' => $request->pcs_dari,
            'pcs_sampai' => $request->pcs_sampai,
            'weight_kg' => $request->weight_kg,
            'weight_gram' => $request->weight_gram,
        ];

        $weightOngkir = $this->weightOngkir->create($data);
        // $dataOngkir = [];

        // for($i = $request->pcs_dari;$i <= $request->pcs_sampai; $i++ ){
        //     $dataOngkir[] = [
        //         'jumlah'=> $i,
        //     ];
        // }

        $check = $this->weightOngkir->find($weightOngkir->id);
        // $check->detail_weight_ongkir()->createMany($dataOngkir);
        return response()->json([
            'status' => true,
            'msg' => 'Alamat Berhasil disimpan.',
            'data' =>  $weightOngkir
        ]);
    }

    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'pcs_dari' => 'required|not_in:0',
            'pcs_sampai' => 'required|not_in:0',
            'weight_kg' => 'required|not_in:0',
            'weight_gram' => 'required|not_in:0',
        ]);
        // echo $request->pcs_dari . $request->pcs_sampai;
        if ($request->pcs_dari > $request->pcs_sampai) {
            $validate->errors()->add('pcs_dari', 'item dari tidak boleh lebih besar dari item sampai');
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Berat Ongkir.', 'data' => $validate->errors()]);
        }
        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Berat Ongkir.', 'data' => $validate->errors()]);
        }
        $data = [
            'pcs_dari' => $request->pcs_dari,
            'pcs_sampai' => $request->pcs_sampai,
            'weight_kg' => $request->weight_kg,
            'weight_gram' => $request->weight_gram,
        ];
        // $dataOngkir =[];
        // for($i = $request->pcs_dari;$i <= $request->pcs_sampai; $i++ ){
        //     $dataOngkir[] = [
        //         'jumlah'=> $i,
        //     ];
        // }
        $check = $this->weightOngkir->find($id);
        if ($check) {
            // $check->detail_weight_ongkir()->delete();
            // $check->detail_weight_ongkir()->createMany($dataOngkir);
            $check->fill($data)->save();
            return response()->json([
                'status' => true,
                'msg' => 'Berat Ongkir Berhasil diubah.',
                'data' => $check
            ]);
        }

        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }
}
