@extends('template_admin.admin')

@section('title', 'Pengaturan Toko')
@section('container')

    <style>
        .container-img {
            max-width: 1424px;
        }

        .heroContent {
            position: absolute;
            top: 30%;
            left: 40%;
            transform: translate(-50%, -50%);
        }

        .heroContent {
            color: white !important;
        }

    </style>
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Pengaturan Toko</h1>
    </div>
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="tab1" data-toggle="tab" href="#tabs-1" role="tab">Keterangan Toko</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tab2" data-toggle="tab" href="#tabs-2" role="tab">Ubah Banner</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tab3" data-toggle="tab" href="#tabs-3" role="tab">Testimoni</a>
        </li>
    </ul><!-- Tab panes -->
    <div class="tab-content">
        <div class="tab-pane active" id="tabs-1" role="tabpanel">
            <div class="row mt-3">
                <!-- Datatables -->
                <div class="col-lg-12">
                    <div class="card mb-4">
                        {{-- <div class="card-header">
                            Keterangan Toko
                        </div> --}}
                        <div class="card-body">
                            <form action="#" name="form" id="form" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-6">
                                        <div class="form-group">
                                            @csrf
                                            @method('put')
                                            <label for="nama_toko">Nama Toko</label>
                                            <input type="text" class="form-control" id="nama_toko" name="nama_toko"
                                                value="{{ $toko->nama_toko }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control" id="email" name="email"
                                                value="{{ $toko->email }}">
                                            <div class="form-group">
                                                <label for="telepon">Telepon</label>
                                                <input type="text" class="form-control" id="telepon" name="telepon"
                                                    value="{{ $toko->telepon }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="fax">Fax</label>
                                                <input type="text" class="form-control" id="fax" name="fax"
                                                    value="{{ $toko->fax }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="fax">Facebook</label>
                                                <input type="text" class="form-control" id="fb" name="fb"
                                                    value="{{ $toko->fb }}" placeholder="Link menggunakan http/https">
                                            </div>
                                            <div class="form-group">
                                                <label for="fax">Instagram</label>
                                                <input type="text" class="form-control" id="instagram" name="instagram"
                                                    value="{{ $toko->instagram }}"
                                                    placeholder="Link menggunakan http/https">
                                            </div>
                                            <div class="form-group">
                                                <label for="fax">Shopee</label>
                                                <input type="text" class="form-control" id="shopee" name="shopee"
                                                    value="{{ $toko->shopee }}" placeholder="Link menggunakan http/https">
                                            </div>
                                            <div class="form-group">
                                                <label for="fax">Tokopedia</label>
                                                <input type="text" class="form-control" id="tokopedia" name="tokopedia"
                                                    value="{{ $toko->tokopedia }}"
                                                    placeholder="Link menggunakan http/https">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <div class="form-group">
                                            <label for="alamat">Alamat</label>
                                            <textarea class="form-control" name="alamat" id="alamat">{{ $toko->alamat }}</textarea>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="form-group">
                                                <label for="fullName">Provinsi</label>
                                                <select name="provinsi" id="" class="form-control provinsi"
                                                    data-live-search="true" onchange="cari_kota(this.value)">
                                                    @foreach ($provinsi['results'] as $item)
                                                        @if ($item['province_id'] == $toko->id_provinsi)
                                                            <option
                                                                value="{{ $toko->id_provinsi }}|{{ $toko->provinsi }}"
                                                                selected>{{ $toko->provinsi }}</option>
                                                        @else
                                                            <option
                                                                value="{{ $item['province_id'] }}|{{ $item['province'] }}">
                                                                {{ $item['province'] }}</option>
                                                        @endif
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="form-group">
                                                <label for="fullName">Kota / Kabupaten</label>
                                                <select name="kota" id="kota" class="form-control provinsi"
                                                    data-live-search="true" onchange="kode_pos()">
                                                    {{-- <option value="{{ $toko->kota }}">{{ $toko->kota }}</option> --}}
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                            <div class="form-group">
                                                <label for="fullName">No.Pos</label>
                                                <input type="text" class="form-control" name="no_pos" placeholder="No.Pos"
                                                    value="{{ $toko->kode_pos }}">
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="container text-right">
                                        <button class="btn btn-primary" onclick="perbarui()" type="button">Perbarui</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="tabs-2" role="tabpanel">
            <div class="row mt-3">
                @foreach ($banner_toko as $data)
                    <div class="col-lg-12 this-banner" data-id="{{ $data->id }}">
                        <div class="card mb-4">
                            <div class="card-header">
                                <div class="d-flex">
                                    <div class="mr-auto p-2col-10">
                                        <h3 class="text-title-header">{!! $data->title !!}</h3> <span
                                            class="text-body-header">{!! $data->deskripsi !!}</span>
                                    </div>
                                    <div class=" p-2 col-2 ">
                                        <p align="right">
                                            <button class="btn btn-warning mt-2" title="Simpan Perubahan" type="button"
                                                data-toggle="collapse" data-target="#image{{ $data->id }}"
                                                aria-expanded="false" aria-controls="image{{ $data->id }}">
                                                <i class="fa fa-solid fa-edit"></i>
                                            </button>
                                            @if ($data->status == 'on')
                                                <button class="btn btn-primary mt-2 btn-status"
                                                    title="{{ $data->status }}" type="button"
                                                    value="{{ $data->status }}">
                                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                                </button>
                                            @else
                                                <button class="btn btn-danger mt-2 btn-status"
                                                    title="{{ $data->status }}" type="button"
                                                    value="{{ $data->status }}">
                                                    <i class="fa fa-power-off" aria-hidden="true"></i>
                                                </button>
                                            @endif
                                        </p>
                                    </div>

                                </div>
                            </div>
                            <div class="collapse" id="image{{ $data->id }}">
                                <div class="card-body">
                                    <div class="row ">
                                        <div class="col-md-12 ">
                                            <form enctype="multipart/form-data">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="">Judul</label>
                                                            <input type="input" class="form-control" id="title"
                                                                name="title" placeholder="Judul"
                                                                value="{{ $data->title }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlTextarea1">Deskripsi <br><small
                                                                    class="text-danger">Gunakan Shift Untuk Baris
                                                                    Kecil</small> </label>
                                                            <textarea class="form-control" id="deskripsi" name="deskripsi" rows="1">{{ $data->deskripsi }}</textarea>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <label for="exampleFormControlReadonly">Banner</label>
                                                            <input class="form-control" type="file" id="foto-banner"
                                                                name="foto_banner" placeholder="Readonly input here..."
                                                                id="exampleFormControlReadonly"
                                                                value="{{ asset('assets_frontend/images/slider/') . '/' . $data->foto_banner }}">
                                                            <small class="text-danger">rekomendasi image
                                                                1920x650,1920x750,3840x1500 </small>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12">
                                                        <div class="form-group">
                                                            <button class="btn btn-primary save-change" type="submit">
                                                                Simpan Perubahan
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                    <div class="card card-body">
                                        <div class="container-img">
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <img src="{{ asset('assets_frontend/images/slider/') . '/' . $data->foto_banner }}"
                                                        alt="image not found" id="img-banner" class="img-fluid">
                                                    <div class="heroContent">
                                                        <h1 class="text-banner">{!! $data->title !!}</h1>
                                                        <div class="pHero">
                                                            {!! $data->deskripsi !!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="tab-pane" id="tabs-3" role="tabpanel">
            <div class="card mt-3">
                <div class="row">
                    <div class="col-7">
                        <div class="card-header"> Testimoni
                            <button type="button" class="btn btn-primary btn-sm float-right" onclick="tambah_review()"><i
                                    class="fa fa-plus-circle"></i> Tambah
                                Testimoni</button>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive p-3">
                                <table class="table align-items-center table-flush" id="dataTable-review"
                                    style="height: 100">
                                    <thead class="thead-light">
                                        <tr>
                                            <th width="100%">Review</th>
                                            <th>Opsi</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="card-header">Background Testimoni</div>
                        <div class="card-body">
                            <form method="post" id="upload-image-form" enctype="multipart/form-data">
                                @csrf
                                @method('put')
                                <div class="form-group">
                                    <label><strong>Foto Background Testimoni</strong></label>
                                    <div class="custom-file">
                                        <input type="file" name="background_testimoni"
                                            class="custom-file-input form-control" id="customFile">
                                        <label class="custom-file-label" for="customFile">Choose file</label>
                                    </div>
                                </div>
                                <div class="form-group text-right">
                                    <button type="reset" class="btn btn-danger btn-sm">Reset</button>
                                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-save"></i>
                                        Simpan</button>
                                </div>
                            </form>
                        </div>
                        <div class="col-12 p-5" id="image_preview">
                            @if ($toko->background_testimoni)
                                <img src="{{ asset('uploads/' . $toko->background_testimoni) }}" width="100%">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Row -->
    <!-- Modal -->
    <div class="modal fade" id="modal_review" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <form enctype="multipart/form-data" id="review-form">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Tambah Review</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body">
                        <div class="form-group">
                            <label for="">Status Review</label>
                            @csrf
                            <div data-toggle="buttons">
                                <label class="btn btn-outline-danger " id="btn-post">
                                    <input type="radio" name="status" id="post" value="" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-power-off"></i> Belum Dipublikasi
                                </label>
                                <label class="btn btn-outline-success" id="btn-post1">
                                    <input type="radio" name="status" id="post1" value="1" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-check"></i> Dipublikasi
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="nama" name="nama">
                        </div>
                        <div class="form-group">
                            <label><strong>Foto </strong></label>
                            <div class="custom-file">
                                <input type="file" name="image" class="custom-file-input form-control" id="customFile">
                                <label class="custom-file-label" for="customFile">Choose file</label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label><strong>Rating </strong></label>
                            <div data-toggle="buttons">
                                <label class="btn btn-outline-warning  mx-1" id="btn-rate1">
                                    <input type="radio" name="rating" id="rate1" value="1" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-star"></i> 1
                                </label>
                                <label class="btn btn-outline-warning  mx-1" id="btn-rate2">
                                    <input type="radio" name="rating" id="rate2" value="2" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-star"></i> 2
                                </label>
                                <label class="btn btn-outline-warning  mx-1" id="btn-rate3">
                                    <input type="radio" name="rating" id="rate3" value="3" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-star"></i> 3
                                </label>
                                <label class="btn btn-outline-warning  mx-1" id="btn-rate4">
                                    <input type="radio" name="rating" id="rate4" value="4" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-star"></i> 4
                                </label>
                                <label class="btn btn-outline-warning  mx-1" id="btn-rate5">
                                    <input type="radio" name="rating" id="rate5" value="5" autocomplete="off"
                                        style="display: none">
                                    <i class="fa fa-star"></i> 5
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="review">Review</label>
                            <textarea class="form-control" name="review" id="review"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
            </form>
        </div>
    </div>
    </div>
@endsection
@section('js')

<script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/6.0.1/tinymce.min.js"></script>
    <script>
        $(document).ready(function() {

            // $('#tab3').tab('show');
            // form background testimoni
            $('.selectsearch').selectpicker();
            $('input[type="file"]').on("change", function() {
                let filenames = [];
                let files = this.files;
                if (files.length > 1) {
                    filenames.push("Total Files (" + files.length + ")");
                } else {
                    for (let i in files) {
                        if (files.hasOwnProperty(i)) {
                            filenames.push(files[i].name);
                        }
                    }
                }
                $(this)
                    .next(".custom-file-label")
                    .html(filenames.join(","));
            });
            // end form background testimoni

            let id = '{{ $toko->id_provinsi }}|{{ $toko->provinsi }}';
            let idkota = '{{ $toko->id_kota }}|{{ $toko->kota }}';
            cari_kota(id, idkota)
            // alert(id)
            // let cc =  $("select[name='provinsi']").val(id).trigger('change');
            // alert(cc)
            tinymce.init({
                selector: 'textarea',
                height: 200,
                menubar: false,
                plugins: [
                    'advlist autolink lists link charmap print preview anchor textcolor',
                    'searchreplace visualblocks code fullscreen',
                    'insertdatetime  table contextmenu paste code wordcount'
                ],
                mobile: {
                    theme: 'mobile'
                },
                toolbar: 'insert | undo redo |  formatselect | bold italic backcolor  | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help',
                content_css: [
                    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
                    '//www.tiny.cloud/css/codepen.min.css'
                ],
                setup: function(editor) {
                    editor.on('change', function() {
                        editor.save();
                    });
                },
                force_br_newlines: true,
                force_p_newlines: false,
                forced_root_block: ''
            });

            $("[name='title']").change(function(e) {
                e.preventDefault();
                $(this).parent().closest('.this-banner').find('.text-banner').html($(this).val());
                $(this).parent().closest('.this-banner').find('.text-title-header').html($(this).val());
            });
            $("[name='deskripsi']").on('change', function(e) {
                e.preventDefault();
                $(this).parent().closest('.this-banner').find('.pHero').html($(this).val());
                $(this).parent().closest('.this-banner').find('.text-body-header').html($(this).val());
            });

            $("[name='foto_banner']").on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        let tes = $input.parent().closest('.this-banner').find('#img-banner').attr(
                            'src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });
            // banner update
            $('.btn-status').on('click', function(e) {
                e.preventDefault();
                let $this = $(this).parent().closest('.this-banner');
                let id = $this.data('id');
                let status = $this.find('.btn-status').val();
                let url = '{{ route('pengaturan_toko.update_status_banner', ['id' => ':id']) }}';
                url = url.replace(":id", id);
                $.ajax({
                    url: url,
                    data: {
                        'status': status,
                        '_method': "PUT"
                    },
                    method: 'POST',

                    success: function(data) {
                        if (data.status) {
                            Swal.fire(
                                'Berhasil!',
                                data.msg,
                                'success'
                            )
                            $this.find('.btn-status').val(data.data.status);
                            $this.find('.btn-status').attr('class', data.text);
                            $this.find('.btn-status').attr('title', data.data.status);
                        } else {
                            toastr.error(data.msg);
                        }
                    }
                });
            });

            $('.save-change').on('click', function(e) {
                e.preventDefault();
                let $this = $(this).parent().closest('.this-banner');
                let id = $this.data('id');
                let form = $(this).parent().closest('.this-banner form')[0];
                let data = new FormData(form);
                data.append("_method", "PUT");
                let url = '{{ route('pengaturan_toko.update_banner', ['id' => ':id']) }}';
                url = url.replace(":id", id);

                $.ajax({
                    url: url,
                    data: data,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    success: function(data) {

                        if (data.status) {
                            Swal.fire(
                                'Berhasil!',
                                'Data berhasil disimpan!',
                                'success'
                            )
                            // $this.find("#title").val(data.data.title);
                            // $this.find("#deskripsi").val(data.data.deskripsi);
                            $this.find('.pHero').html(data.data.deskripsi);
                            $this.find('.text-body-header').html(data.data.deskripsi);
                            $this.find(".text-error-title").remove();
                            $this.find(".text-error-deskripsi").remove();
                            $this.find(".text-error-foto_banner").remove();
                        } else {
                            $this.find(".text-error-title").remove();
                            $this.find(".text-error-deskripsi").remove();
                            $this.find(".text-error-foto_banner").remove();
                            $.each(data.data, function(field_name, error) {
                                $this.find('[name=' + field_name + ']').after(

                                    '<small class="text-error-' + field_name +
                                    ' text-danger ">' + error + '</small>');
                            });
                            toastr.error('Gagal, Terjadi Kesalahan Saat Ubah Profile.');
                        }
                    }
                });
            });
        });

        // datatable review
        var table = $('#dataTable-review').DataTable({
            prossecing: true,
            serverSide: true,
            "autoWidth": false,
            ajax: "{{ route('pengaturan_toko.reviews') }}",
            columns: [{
                    data: 'reviews',
                    name: 'reviews',
                },
                {
                    data: 'action',
                    name: 'action'
                }
            ]
        });
        // end datatable review

        function cari_kota(id, idkota = null) {
            // alert(idkota)
            var link = "{{ route('get.kota', 'id') }}";
            url = link.replace('id', id);
            $.ajax({
                type: "get",
                url: url,
                success: function(response) {
                    $("[name='kota']").html('');
                    $.each(response.results, function(i, res) {
                        let kotas = `${res.city_id}|${res.city_name}`;
                        if (idkota == kotas) {
                            $("[name='kota']").append(
                                `<option value="${res.city_id}|${res.city_name}" data-pos="${res.postal_code}" selected>${res.city_name}</option>`
                            );
                        } else {
                            $("[name='kota']").append(
                                `<option value="${res.city_id}|${res.city_name}" data-pos="${res.postal_code}">${res.city_name}</option>`
                            );
                        }

                    });
                    $("[name='kota']").selectpicker('refresh');
                }
            });
        }

        function kode_pos() {
            $("[name='no_pos']").val($("#kota").find('option:selected').data("pos"));
        };


        function perbarui() {
            $.ajax({
                type: "post",
                url: "{{ route('ubah_data_toko') }}",
                data: $('#form').serialize(),
                success: function(response) {
                    if (response.status) {
                        $(".text-error-nama_toko").remove();
                        $(".text-error-alamat").remove();
                        $(".text-error-email").remove();
                        $(".text-error-telepon").remove();
                        $(".text-error-fax").remove();
                        $(".text-error-kota").remove();
                        $(".text-error-provinsi").remove();
                        $(".text-error-no_pos").remove();
                        $(".text-error-fb").remove();
                        $(".text-error-shopee").remove();
                        $(".text-error-tokopedia").remove();
                        $(".text-error-instagram").remove();
                        Swal.fire(
                            'Berhasil!',
                            'Data berhasil disimpan!',
                            'success'
                        )
                    } else {
                        toastr.warning(response.msg);
                        $(".text-error-nama_toko").remove();
                        $(".text-error-alamat").remove();
                        $(".text-error-email").remove();
                        $(".text-error-telepon").remove();
                        $(".text-error-fax").remove();
                        $(".text-error-kota").remove();
                        $(".text-error-provinsi").remove();
                        $(".text-error-no_pos").remove();
                        $(".text-error-fb").remove();
                        $(".text-error-shopee").remove();
                        $(".text-error-tokopedia").remove();
                        $(".text-error-instagram").remove();
                        $.each(response.data, function(field_name, error) {
                            $(document).find('[name=' + field_name + ']').after(
                                '<small class="text-error-' + field_name +
                                ' text-danger">' + error + '</small>')
                        });
                    }
                }
            });
        }

        // background testimoni
        $('#upload-image-form').submit(function(e) {
            e.preventDefault();
            let formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: "{{ route('pengaturan_toko.backround_testimoni') }}",
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response.success == true) {
                        this.reset();
                        Swal.fire(
                            'Berhasil!',
                            'backround berhasil diperbaharui!',
                            'success')
                        $('#image_preview').html(
                            `<img src="{{ asset('uploads/`+response.image+`') }}" width="100%">`)
                    } else {
                        Swal.fire(
                            'Gagal!',
                            response.message,
                            'error')
                        $('#image_preview').html('')
                    }
                },
            });
        })

        //modal review

        function tambah_review() {
            $('#modal_review').modal('show')
        }

        function edit(id) {
            var link = " {{ route('pengaturan_toko.review', 'id') }}";
            var link = link.replace("id", id);
            $.ajax({
                type: "get",
                url: link,
                success: function(response) {
                    $('[name="nama"]').val(response.name);
                    $('[name="review"]').val(response.review);
                    $('#rate' + response.star).prop("checked", true);
                    $('#btn-rate' + response.star).addClass("active");
                    $('#post' + response.status).prop("checked", true);
                    $('#btn-post' + response.status).addClass("active");
                    $('#review-form').append(` @method('PUT') `);
                    $('#review-form').append(`<input type="hidden" value="` + response.id + `" name="id">`);
                    $('#modal_review').modal('show');

                }
            });
        }

        $('#review-form').submit(function(e) {
            e.preventDefault();
            console.log(e);
            let formData = new FormData(this);
            if ($('#review-form').find('[name="_method"]').val()) {
                var link = "{{ route('pengaturan_toko.edit_reviews', 'id') }}";
                link = link.replace("id", $('#review-form').find('[name="id"]').val())
            } else {
                var link = "{{ route('pengaturan_toko.post_reviews') }}";
            }

            $.ajax({
                type: 'POST',
                url: link,
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    if (response.success == true) {
                        this.reset();
                        Swal.fire(
                            'Berhasil!',
                            'backround berhasil diperbaharui!',
                            'success')

                        $('#modal_review').modal('hide')
                        location.reload()
                    } else {
                        Swal.fire(
                            'Gagal!',
                            response.message,
                            'error')
                        $('#image_preview').html('')
                    }
                },
            });
        });

    </script>

@endsection
