<div class="container">
    <div class="ps-section__header mb-50">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 ">
                <h3 class="ps-section__title" data-mask="Category">Category</h3>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 ">
                <div class="ps-owl-actions"><a class="ps-prev" href="#"><i
                            class="ps-icon-arrow-right"></i>Prev</a><a class="ps-next" href="#">Next<i
                            class="ps-icon-arrow-left"></i></a></div>
            </div>
        </div>
    </div>
    <div class="ps-section__content">
        <div class="ps-owl--colection owl-slider" data-owl-auto="true" data-owl-loop="true" data-owl-speed="5000"
            data-owl-gap="30" data-owl-nav="false" data-owl-dots="false" data-owl-item="{{ count($produk) }}"
            data-owl-item-xs="1" data-owl-item-sm="2" data-owl-item-md="3" data-owl-item-lg="{{ count($produk) }}"
            data-owl-duration="1000" data-owl-mousedrag="on">
            @foreach ($category_all as $p)
                <div class="">
                    <div class="ps-shoe ps-category">
                        <div class="ps-shoe__thumbnail">
                            @if ($p->foto != null)
                                <img src="{{ asset('assets_frontend/uploads/kategori/' . $p->foto) }}" width="100%" />
                            @else
                                <img src="{{ asset('assets_frontend/images/shoe/3.jpg') }}" alt="">
                            @endif
                            <a class="ps-shoe__overlay" href="{{ url('products') . '/' . $p->slug }}"></a>
                        </div>

                        <div class="">
                            <div class="ps-shoe__detail" style="min-height: 40px !important;">
                                <a class="ps-shoe__name" href="{{ url('products') . '/' . $p->slug }}">
                                    {{ Str::substr($p->category_name, 0, 23) }} @if (Str::length($p->category_name) >= '23')
                                    ...
                                    @endif</a>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>
