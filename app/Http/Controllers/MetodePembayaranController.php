<?php

namespace App\Http\Controllers;

use App\Models\MetodePembayaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class MetodePembayaranController extends Controller
{
    protected $metode_pembayaran;
    public function __construct(MetodePembayaran $metode_pembayaran)
    {
        $this->metode_pembayaran = $metode_pembayaran;
    }

    public function index(Request $request)
    {
        $data = $this->metode_pembayaran->all();
        // echo json_encode($data);
        // return response()->json($user);die;
        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('action', function ($row) {
                    $btn = '<a href="javascript:void(0)" class="btn btn-warning btn-sm edit-pembayaran"  data-id="' . $row->id . '"><i class="fa fa-solid fa-edit"></i></a>';
                    $btn .= ' <a href="javascript:void(0)" class="btn btn-danger btn-sm hapus-pembayaran"  data-id="' . $row->id . '"><i class="fa fa-solid fa-trash"></i></a>';
                    if ($row->status == 'on') {
                        $btn .= ' <a href="javascript:void(0)" class="btn btn-primary btn-sm on-pembayaran" title="non aktifkan" data-status="off"  data-id="' . $row->id . '"><i class="fa fa-power-off" aria-hidden="true"></i></a>';
                    } else {
                        $btn .= ' <a href="javascript:void(0)" class="btn btn-danger btn-sm on-pembayaran" title="aktifkan" data-status="on" data-id="' . $row->id . '"><i class="fa fa-power-off" aria-hidden="true"></i></a>';
                    }
                    return $btn;
                })
                ->addColumn('nama', function ($row) {
                    $nama = '<div class="col-md-4">
                                <img src="' . asset($row->nama) . '" style="width:100px;"/>
                            </div>';
                    return $nama;
                })

                ->rawColumns(['action', 'nama'])
                ->make(true);
        }
        return view('layout_admin.master_metode_pembayaran');
    }

    public function destroy($id)
    {
        $check = $this->metode_pembayaran->find($id);

        if ($check) {
            $check->delete();
            return response()->json(['status' => true, 'msg' => 'Berhasil Hapus Data']);
        }
        return response()->json(['status' => false, 'msg' => 'Gagal Hapus Data']);
    }

    public function store(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'nama_pembayaran' => 'required|not_in:0',
            // 'kode_pembayaran' => 'required',
            'norek_pembayaran' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Simpan Metode Pembayaran.', 'data' => $validate->errors()]);
        }

        $data = [
            'nama' => $request->nama_pembayaran,
            'kode' => $request->kode_pembayaran,
            'no_rek' => $request->norek_pembayaran,
            'status' => 'on',
        ];

        $metode_pembayaran = $this->metode_pembayaran->create($data);
        return response()->json([
            'status' => true,
            'msg' => 'Alamat Berhasil disimpan.',
            'data' =>  $metode_pembayaran
        ]);
    }

    public function edit($id)
    {
        $check = $this->metode_pembayaran->find($id);
        if ($check) {
            $check->first();
            return response()->json(['status' => true, 'msg' => 'data ada', 'data' => $check]);
        }
        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }

    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(), [
            'nama_pembayaran' => 'required|not_in:0',
            // 'kode_pembayaran' => 'required',
            'norek_pembayaran' => 'required',
        ]);

        if ($validate->fails()) {
            return response()->json(['status' => false, 'msg' => 'Gagal, Terjadi Kesalaha Saat Ubah Metode Pembayaran.', 'data' => $validate->errors()]);
        }

        $data = [
            'nama' => $request->nama_pembayaran,
            'kode' => $request->kode_pembayaran,
            'no_rek' => $request->norek_pembayaran,
            'status' => 'on',
        ];

        $check = $this->metode_pembayaran->find($id);
        if ($check) {
            $check->fill($data)->save();
            return response()->json([
                'status' => true,
                'msg' => 'Metode Pembayaran Berhasil diubah.',
                'data' => $check
            ]);
        }

        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }
    public function update_status(Request $request, $id)
    {
        $data = [];
        $msg = '';
        if ($request->status == 'off') {
            $data = [
                'status' => 'off',
            ];
            $msg = 'Metode Pembayaran Berhasil dinonaktifkan.';
        } else {
            $data = [
                'status' => 'on',
            ];
            $msg = 'Metode Pembayaran Berhasil diaktifkan.';
        }


        $check = $this->metode_pembayaran->find($id);
        if ($check) {
            $check->fill($data)->save();
            return response()->json(
                [
                    'status' => true,
                    'msg' => $msg,
                    'data' => $check
                ]
            );
        }

        return response()->json(['status' => false, 'msg' => 'data tidak ada']);
    }
}
