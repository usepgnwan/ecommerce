@extends('template_admin.admin')

@section('title', 'Master Product')
@section('container')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">{{ $title }}</h1>

    </div>

    <!-- Row -->
    <div class="row">
        <!-- Datatables -->
        <div class="col-lg-12">
            <div class="from-group mb-2">
                <a href="javascript:void(0)" class="btn btn-primary" value="new" onclick="tambah_review()" href="{{ route('product.create') }}"><i class="fa fa-plus-circle"></i> Tambah Review</a>
            </div>
            <div class="card mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-primary">Produk : {{ $data->nama_produk }} - Stok :
                        {{ $data->stok_sum_qty }}</h6>
                </div>
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable">
                        <thead class="thead-light">
                            <tr>
                                <th>No</th>
                                <th>ID Pemesanan</th>
                                <th>Nama</th>
                                <th>Review</th>
                                <th style="width: 20%">Rating</th>
                                <th>Tanggal</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th colspan='3'></th>
                                <th colspan='1'>Rata-Rata Rating</th>
                                <th>Total</th>
                                <th colspan='2'></th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal_review" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <form enctype="multipart/form-data" id="review-form">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Tambah Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label for="">Status Review</label>
                        @csrf
                        @method('POST')
                        <div data-toggle="buttons">
                            <input type="hidden" name="id" id="id" value="{{request()->penilain}}">
                            <label class="btn btn-outline-danger " id="btn-post">
                                <input type="radio" name="status" id="post" value="aktif" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-power-off"></i> Aktif
                            </label>
                            <label class="btn btn-outline-success" id="btn-post1">
                                <input type="radio" name="status" id="post1" value="nonaktif" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-check"></i> Non Aktif
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off">
                    </div>
                    <div class="form-group">
                        <label><strong>Foto </strong></label>
                        <div class="custom-file">
                            <input type="file" name="image[]" class="  form-control" id="customFile" autocomplete="off" multiple>
                            {{-- <label class="custom-file-label" for="customFile">Choose file</label> --}}
                        </div>
                    </div>
                    <div class="form-group">
                        <label><strong>Rating </strong></label>
                        <div data-toggle="buttons">
                            <label class="btn btn-outline-warning  mx-1" id="btn-rate1">
                                <input type="radio" name="rating" id="rate1" value="1" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-star"></i> 1
                            </label>
                            <label class="btn btn-outline-warning  mx-1" id="btn-rate2">
                                <input type="radio" name="rating" id="rate2" value="2" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-star"></i> 2
                            </label>
                            <label class="btn btn-outline-warning  mx-1" id="btn-rate3">
                                <input type="radio" name="rating" id="rate3" value="3" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-star"></i> 3
                            </label>
                            <label class="btn btn-outline-warning  mx-1" id="btn-rate4">
                                <input type="radio" name="rating" id="rate4" value="4" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-star"></i> 4
                            </label>
                            <label class="btn btn-outline-warning  mx-1" id="btn-rate5">
                                <input type="radio" name="rating" id="rate5" value="5" autocomplete="off"
                                    style="display: none">
                                <i class="fa fa-star"></i> 5
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="review">Review</label>
                        <textarea class="form-control" name="review" id="review" autocomplete="off"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-save-rev">Save</button>
                </div>
        </form>
    </div>
</div>
    <script>
        let rev = {!! json_encode($review) !!};
        let id = "{{ request()->penilain }}";
        // datatabel
        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: "{{ route('penilain.index') }}",
                data: function(d) {
                    d.id = id;
                }
            },
            // footerCallback: function(tfoot, data, start, end, display) {
            //     var api = this.api();
            //     // console.log(api.column(3));
            //     // var p = api.column(3).data().reduce(function (a, b) {
            //     //     return a + b;
            //     // }, 0)
            //     $(api.column(4).footer()).html(`
        //         <div id="bintang-footer">${rev}</div>
        //     `);
            //     $(api.column(6).footer()).html('Jumlah Review Aktif = ' + `
        //         <span id="aktif-rev">{{ $all_item_prev }}</span>
        //     `);
            // },
            initComplete: function() {
                dtFilter(this.api())
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'id_pemesanan',
                    name: 'id_pemesanan',
                },
                {
                    data: 'nama',
                    name: 'nama'
                },
                {
                    data: 'review',
                    name: 'review'
                },
                {
                    data: 'star',
                    name: 'star'
                },
                {
                    data: 'tanggal',
                    name: 'tanggal'
                },
                {
                    data: 'status',
                    name: 'status'
                },
                {
                    data: 'action',
                    name: 'action',
                    orderable: true,
                    searchable: true
                },
            ]
        });


        function dtFilter(table) {
            $(table.column(4).footer()).html(`
                    <div id="bintang-footer"> ${rev}</div>
                `);
            $(table.column(6).footer()).html('Jumlah Review Aktif = ' + `
                    <span id="aktif-rev">{{ $all_item_prev }}</span>
                `);
        }



        function tambah_review() {
            $('.btn').removeClass('active');
            $('.btn-save-rev').html('save');
                    $('.btn-save-rev').prop('disabled', false);
            $('#modal_review').modal('show')
        }
        $('#review-form').submit(function(e) {
            e.preventDefault();
            $('.btn-save-rev').html(
                    '<span><i class="fa  fa-spinner fa-spin"></i> &nbsp; Proses</span>');
                    $('.btn-save-rev').prop('disabled', true);
            console.log(e);
            let formData = new FormData(this);
            console.log(formData)
            var link = "{{ route('penilain.store') }}";
            $.ajax({
                type: 'POST',
                url: link,
                data: formData,
                contentType: false,
                processData: false,
                success: (response) => {
                    console.log(response)
                    if (response.status == true) {
                        this.reset();
                        Swal.fire(
                            'Berhasil!',
                            'backround berhasil diperbaharui!',
                            'success');
                        table.ajax.reload(dtFilter(table), false);
                        $('#bintang-footer').html(response.bintang);
                        $('#aktif-rev').html(response.all_item);
                        $('#modal_review').modal('hide')
                    } else {
                        $('.btn-save-rev').html('save');
                        $('.btn-save-rev').prop('disabled', false);
                        Swal.fire(
                            'Gagal!',
                            response.msg,
                            'error')
                        $('#image_preview').html('')
                    }
                },
            });
        });

        function edit(id, status) {

            let url = "{{ route('penilain.update', ['penilain' => ':id']) }}"
            url = url.replace(':id', id);
            $.ajax({
                method: 'POST',
                url: url,
                data: {
                    status: status,
                    _method: 'PUT'
                },
                success: function(data) {
                    if (data.status) {
                        // console.log(data.bintang);
                        // console.log(data.all_item);
                        Swal.fire(
                            'Berhasil!',
                            'Ubah Status',
                            'success'
                        )
                        table.ajax.reload(dtFilter(table), false);
                        $('#bintang-footer').html(data.bintang);
                        $('#aktif-rev').html(data.all_item);
                    }
                }
            })
        }
    </script>
@endsection
