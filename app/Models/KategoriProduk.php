<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    use HasFactory;
    protected $table = 'produk_kategori';
    protected $fillable = ['produk_id', 'master_category_id'];
    public $timestamps = false;
}