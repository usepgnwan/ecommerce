@extends('template')

@section('judul', $title)

@section('content')
    @if (session('success'))
        {{ session()->get('success') }}
    @endif
    <div class="row">
        <div class="col-md-12 mt-3">
            {{ var_dump(count($data) == 0) }}
            @if (!is_null($data))


                <div class="table-responsive">
                    <table class="table table-hover" id="tblTest">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Product</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php($i = 1)
                            @foreach ($data as $id => $co)
                                <tr>
                                    <th scope="row">{{ $i++ }}</th>
                                    <td>{{ $co['title'] }}</td>
                                    <td>{{ $co['quantity'] }}</td>
                                    <td><input type="button" class="btn btn-success" name="" id="" value="Delete"></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                hehaha
            @endif
        </div>
    </div>
@endsection
