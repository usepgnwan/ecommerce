@extends('frontend.home_user')

@section('content_user')
    <div class="row">
        <div class="container-fluid">
            <div>
                <h4>Edit Profil</h4>
                <hr>
            </div>
            <div class="row edit-profile">
                <form action="">
                    <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-12">
                        <div class="card h-100">
                            <div class="card-body  ">
                                <div class="account-settings">
                                    <div class="user-profile">
                                        <div class="user-avatar">
                                            <img src="{{ asset('assets_frontend/images/user') . '/' . auth()->user()->foto }}"
                                                alt="default" {{-- <img src="https://bootdey.com/img/Content/avatar/avatar7.png" alt="default" --}} class="foto-profile">
                                            <div class="file-input">
                                                <input type="file" name="foto" id="file-input"
                                                    value="{{ asset('assets_frontend/images/user') . '/' . auth()->user()->foto }}"
                                                    class="file-input__input" />
                                                <label class="file-input__label" for="file-input">
                                                    <svg aria-hidden="true" focusable="false" data-prefix="fas"
                                                        data-icon="upload" class="svg-inline--fa fa-upload fa-w-16"
                                                        role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512">
                                                        <path fill="currentColor"
                                                            d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z">
                                                        </path>
                                                    </svg>
                                                    <span>Pilih Gambar</span></label>
                                            </div>
                                            <h6 class="user-email">Ukuran gambar: maks. 1 MB
                                                Format gambar: .JPEG, .PNG</h6>
                                        </div>
                                        <h5 class="user-name auth-name">{{ auth()->user()->name }}</h5>
                                        <h6 class="user-email auth-email">{{ auth()->user()->email }}</h6>
                                    </div>
                                    <div class="about">
                                        <h5>Alamat <a href="javascript:void(0)" id="mdl-address"><sup><span
                                                        class="fa fa-edit"></span></sup></a> <input type="hidden" value="{{auth()->user()->default_address}}" name="id_defualt"> </h5>
                                        @if (!is_null($address_user))
                                            <p class="text-address">{{ $address_user['alamat'] }}.
                                                {{ $address_user['kota'] }}, {{ $address_user['provinsi'] }},
                                                {{ $address_user['no_pos'] }}</p>
                                        @else
                                            <p class="text-address">Belum Ada alamat</p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-12">
                        <div class="card h-100">
                            <div class="card-body ">
                                <div class="row gutters" style="padding: 20px;">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <h6 class="mb-2 text-primary">Personal Details</h6>
                                    </div>
                                    <div class="row" style="padding: 10px">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="fullName">Nama</label>
                                                <input type="text" class="form-control" id="fullName"
                                                    placeholder="Masukan name" value="{{ auth()->user()->name }}"
                                                    name="name">
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="eMail">Email</label>
                                                <input type="email" class="form-control" id="eMail"
                                                    placeholder="Masukan Email" value="{{ auth()->user()->email }}"
                                                    readonly>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row" style="padding: 10px">
                                        <div class="col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="phone">Tanggal Lahir</label>
                                                <input type="date" class="form-control" id="tanggal_lahir"
                                                    placeholder="Masukan Tanggal Lahir" name="tanggal_lahir"
                                                    value="{{ auth()->user()->tanggal_lahir }}">
                                            </div>
                                        </div>
                                        <div class=" col-md-6 col-sm-12">
                                            <div class="form-group">
                                                <label for="phone">Nomer Telp/Wa</label>
                                                <input type="text" class="form-control" id="no_telp" name="no_telp"
                                                    placeholder="Masukan Nomer telepon"
                                                    value="{{ auth()->user()->no_telp }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-md-6 col-sm-12">
                                        <div class="row gutters">
                                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                <h6 class="mt-3 mb-2 text-primary">Jenis Kelamin</h6>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="flexRadioDefault1"
                                                        name="jenis_kelamin" value="laki-laki"
                                                        {{ auth()->user()->jenis_kelamin == 'laki-laki' ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        Laki-laki
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" id="flexRadioDefault1"
                                                        name="jenis_kelamin" value="perempuan"
                                                        {{ auth()->user()->jenis_kelamin == 'perempuan' ? 'checked' : '' }}>
                                                    <label class="form-check-label" for="flexRadioDefault1">
                                                        Perempuan
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row gutters">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                        <div class="text-right" style="padding:15px;">
                                            <button type="submit" id="submit" name="submit"
                                                class="btn btn-primary btn-save-profile">Simpan
                                                Profile</button>
                                            <button type="button" class="btn btn-secondary" id="mdl">Ubah
                                                Password</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Password -->
    <div id="modal_password" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content change-password">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Ubah Password</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button> --}}
                </div>
                <form action="">
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Password Saat Ini</label>
                                    <input type="password" class="form-control" name="old_password"
                                        placeholder="Password Saat Ini">
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Password Baru</label>
                                    <input type="password" class="form-control" name="new_password"
                                        placeholder="Password Baru">
                                </div>
                            </div>
                            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="form-group">
                                    <label for="fullName">Ulangi Password Baru</label>
                                    <input type="password" class="form-control" name="repeat_password"
                                        placeholder="Ulangi Password Baru">
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-password">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan Password</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Modal Alamat -->
    <div id="modal_addres" class="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
        aria-hidden="true" style="overflow: auto !important;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content change-address">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalCenterTitle">Pilih Alamat</h5>
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button> --}}
                </div>
                <form action="">
                    <div class=" all-address">

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary close-address">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('js_extended')
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                }
            });
            $('#mdl').click(function() {
                $('.change-password form')[0].reset();
                $('#modal_password').show();
            });
            $('.close-password').click(function() {
                $('.change-password form')[0].reset();
                $('#modal_password').hide();
            });

            $('#mdl-address').click(function() {
                let id = $('[name="id_defualt"]').val();
                $('#modal_addres').show();
                $('.all-address').html(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class="card h-100">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b><span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                    `);
                    let myid = "{{$id_user}}"
                    let url = '{{ route('get.myaddress', ['id'=>':id']) }}';
                    url = url.replace(':id', myid);
                $.ajax({
                    url: url,
                    method: 'GET',
                    dataType: 'JSON',
                    success: function(data) {
                        if (data.status) {
                            toastr.success(data.msg);
                            $('.all-address').html('');
                            let i = 1;
                            $.each(data.data, function(i, res) {
                                $('.all-address').append(`
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address" data-id_adress="">
                                        <div class="card h-100">
                                            <div class="card-body " style="padding: 20px;">
                                                <div class="row gutters">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                        <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                            <div class="row">
                                                                <div class="col-md-6 col-sm-4">
                                                                    <span class="text-success title-address"
                                                                        style="font-size: 1.3em; color: #c99069">
                                                                        <b> Alamat ${i+1} </b> </span>
                                                                </div>
                                                                <div class="col-md-6" style="text-align: end">
                                                                    <span class="text-primary">
                                                                        <input class="form-check-input" type="radio" id="flexRadioDefault1"
                                                                            name="id_address" value="${res.id}" ${ res.id == id ?'checked' : ''} >
                                                                        <label class="form-check-label" for="flexRadioDefault1">
                                                                            ${ res.id == id ? 'Utama' : 'Jadikan Utama'}
                                                                        </label>
                                                                     </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row" style="padding:0 20px 0 30px">
                                                        <div class="col-md-12 col-sm-12">
                                                            <hr>
                                                            <table>
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">Nama
                                                                                Penerima</label></td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.nama_penerima} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">No. Telp</label></td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.no_telp} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Provinsi</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.provinsi}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Kota</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.kota}</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label for="">No.
                                                                                Pos</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.no_pos} </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:150px;" valign="top"><label
                                                                                for="">Alamat</label>
                                                                        </td>
                                                                        <td width="20px" valign="top">:</td>
                                                                        <td valign="top">${res.alamat}</td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                        <hr>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                `);
                            });
                        } else {
                            $('.all-address').html('');
                            $('.all-address').append(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class="card h-100">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b>ALAMAT BELUM TERSEDIA</b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            `);
                        }
                    }
                });
            });
            $('.close-address').click(function() {
                $('#modal_addres').hide();
            });

            $('#file-input').on('change', function() {
                $input = $(this);
                if ($input.val().length > 0) {
                    fileReader = new FileReader();
                    fileReader.onload = function(data) {
                        $('.foto-profile').attr('src', data.target.result);
                    }
                    fileReader.readAsDataURL($input.prop('files')[0]);
                }
            });


            $('.edit-profile form').submit(function(e) {
                e.preventDefault();
                $('.btn-save-profile').html(
                    '<span><i class="fa  fa-spinner fa-spin"></i> &nbsp; Proses</span>');
                let form = new FormData($('.edit-profile form')[0]);
                form.append("_method", "PUT");
                $.ajax({
                    url: '{{ route('update.profile') }}',
                    data: form,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        console.log(data.data.name)
                        if (data.status) {
                            $('.btn-save-profile').html('Simpan Profile');
                            $(".text-error-foto").remove();
                            $(".text-error-name").remove();
                            $(".text-error-tanggal_lahir").remove();
                            $(".text-error-no_telp").remove();
                            $(".text-error-jenis_kelamin").remove();
                            toastr.success('Sukses Ubah Profile.');

                            $('.auth-name').html(data.data.name);
                            $('.auth-email').html(data.data.email);
                        } else {
                            $('.btn-save-profile').html('Simpan Profile');
                            $(".text-error-foto").remove();
                            $(".text-error-name").remove();
                            $(".text-error-tanggal_lahir").remove();
                            $(".text-error-no_telp").remove();
                            $(".text-error-jenis_kelamin").remove();
                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<span class="text-error-' + field_name +
                                    ' danger-text-error ">' + error + '</span>')
                            });
                            toastr.error('Gagal, Terjadi Kesalahan Saat Ubah Profile.');
                        }
                    }
                });
            });


            $('.change-password form').submit(function(e) {
                e.preventDefault();
                let form = new FormData($('.change-password form')[0]);
                form.append("_method", "PUT");
                $.ajax({
                    url: '{{ route('change.password') }}',
                    data: form,
                    method: 'POST',
                    processData: false,
                    contentType: false,
                    success: function(data) {
                        if (data.status) {
                            toastr.success(data.msg);
                            $(".text-error-old_password").remove();
                            $(".text-error-new_password").remove();
                            $(".text-error-repeat_password").remove();
                            $('.change-password form')[0].reset();
                            $('#modal_password').hide();
                        } else {
                            toastr.error(data.msg);
                            $(".text-error-old_password").remove();
                            $(".text-error-new_password").remove();
                            $(".text-error-repeat_password").remove();
                            $.each(data.data, function(field_name, error) {
                                $(document).find('[name=' + field_name + ']').after(
                                    '<span class="text-error-' + field_name +
                                    ' text-danger ">' + error + '</span>')
                            });
                        }
                    }
                });
            });

            $('.change-address form').submit(function(e) {
                e.preventDefault()
                let form = $(this).serializeArray().concat({
                    name: "_method",
                    value: "PUT"
                });
                $.ajax({
                    url: '{{ route('change.default_address') }}',
                    data: form,
                    method: 'POST',
                    dataType: 'JSON',
                    success: function(data) {
                         if(data.status){
                            $('.text-address').html(`
                                ${data.data.alamat}. ${data.data.kota}, ${data.data.provinsi}, ${data.data.no_pos}
                            `);
                            $('[name="id_defualt"]').val(data.data.id);
                            $('#modal_addres').hide();
                            toastr.success(data.msg);
                         }else{
                            toastr.error(data.msg);
                         }
                    }
                });
            });
        });
    </script>
@endsection
