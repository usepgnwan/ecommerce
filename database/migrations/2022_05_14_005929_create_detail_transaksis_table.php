<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailTransaksisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transaksis', function (Blueprint $table) {
            $table->id();
            $table->foreignId('transaksi_id')
            ->references('id')
            ->on('transaksis')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreignId('product_id')
            ->references('id')
            ->on('product')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreignId('stock_id')
            ->references('id')
            ->on('product_stock')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->foreignId('master_size_id')
            ->references('id')
            ->on('master_size')
            ->constrained()
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('nama_produk');
            $table->string('diskon',100)->nullable();
            $table->string('quantity',100);
            $table->string('size',100);
            $table->string('sub_total',100);
            $table->string('harga_diskon',100)->nullable();
            $table->string('total_item',100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transaksis');
    }
}
