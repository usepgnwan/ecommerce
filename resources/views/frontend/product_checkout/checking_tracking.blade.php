@extends('frontend.template_frontend.frontend')
@section('title', 'Product Checkout')
@section('content')
    <style>
        .bs-example {
            margin: 20px;
        }

        a .rotate {
            -webkit-transform: rotate(-180deg);
            /* Chrome, Safari, Opera */
            -moz-transform: rotate(-180deg);
            /* Firefox */
            -ms-transform: rotate(-180deg);
            /* IE 9 */
            transform: rotate(-180deg);
            /* Standard syntax */
        }

        .float-center-div {
            float: none !important;
        }

    </style>
    <div class="container text-center">
        <h3>Cek Pesanan </h3>
        <p>Masukan ID Pesanan dibawah ini. jika anda lupa anda dapat menghubungi langsung kepada kontak admin
            {{ $store_setting->nama_toko }}.</p>
        <div class="col-md-8 float-center-div" style="margin-left: auto; margin-right:auto">
            <style>
                .ps-form--icon > button {
                    position: absolute;
                    top: 50%;
                    -webkit-transform: translateY(-50%);
                    -moz-transform: translateY(-50%);
                    -ms-transform: translateY(-50%);
                    -o-transform: translateY(-50%);
                    transform: translateY(-50%);
                    right: 10px;
                    }
            </style>
            <div class="ps-cart__promotion">
                <form action="" id="form-search">
                    <div class="form-group">
                        <div class="ps-form--icon"> <button class="btn btn-primary" style="background:#d06418 !important; border:none"> Cari <i class="fa fa-angle-right"></i></button>
                            <input class="form-control" type="text" placeholder="ID Pesanan" name="search">
                        </div>
                    </div>
                </form>
                <div class="tracking-card"></div>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <script>
        $('#form-search').submit(function(e){
            e.preventDefault();
            let data = $(this).serialize();
            $('.tracking-card').html(`
                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 address">
                                    <div class=" ">
                                        <div class="card-body " style="padding: 20px;">
                                            <div class="row gutters">
                                                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mb-5">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 text-center">
                                                                <span class="text-success title-address" style="font-size: 0.8em; color: #c99069; text-align:center">
                                                                    <b><span><i class="fa fa-2x fa-spinner fa-spin"></i> &nbsp; Proses</span></b> </span>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                    `);
            $.ajax({
                    type: "post",
                    url: "{{ route('product.post_check_pesanan') }}",
                    data: data,
                    dataType:'JSON',
                    start_time: new Date().getTime(),
                    success: function(data) {
                        $('.btn-save-form').prop('disabled',false);
                        $('.btn-save-form').html('simpan');
                        if(data.status){
                            setTimeout(() => {
                                $('.tracking-card').html(data.html);
                            }, (new Date().getTime() - this.start_time));
                        }else{
                            setTimeout(() => {
                                $('.tracking-card').html('id tidak ditemukan');
                            }, (new Date().getTime() - this.start_time));
                        }
                    },
                });
        });

        $(document).on('click','#btn-save-form',function(e){
            e.preventDefault();
            let data = new FormData($('#form-upload-bukti')[0]);
            $('#btn-save-form').prop('disabled',true);
            $('#btn-save-form').html('<span><i class="fa fa-spinner fa-spin"></i> &nbsp; Proses</span>');
                $.ajax({
                        type: "post",
                        url: "{{ route('product.put_check_pesanan') }}",
                        data: data,
                        processData: false,
                        contentType: false,
                        dataType:'JSON',
                        start_time: new Date().getTime(),
                        success: function(data) {
                            $('#btn-save-form').prop('disabled',false);
                            $('#btn-save-form').html('Upload');
                            if(data.status){
                                // transaksi.ajax.reload();
                                $('#modal-2').modal('hide');
                                Swal.fire(
                                    'Berhasil',
                                    data.msg,
                                    'success'
                                );
                                $('.bukti-pembayaran').html(data.data);
                                // setTimeout(() => {
                                //             if(data.data.status != 'rejected'){
                                //                 $('.checking-btn').html(`  <button class="btn btn-warning mt-2 add-tracking" title="on" type="button" value="on">
                                //                             <i class="fa fa-plus" aria-hidden="true"></i>
                                //                         </button>`);
                                //             }else{
                                //                 $('.checking-btn').html('');
                                //             }
                                //             // $('.text-status').html(data.data.status);
                                //             // $('.tracking-card').html(data.html);
                                // }, (new Date().getTime() - this.start_time));

                            }else{
                                $(".error-text-bukti_pembayaran").remove();
                                Swal.fire(
                                    'Gagal!',
                                    data.msg,
                                    'error'
                                );
                                $.each(data.data, function(field_name, error) {
                                $(document).find('.text-error-form').after(
                                    '<small class="text-danger error-text-' + field_name +
                                    '">' + error + '</small>')
                                });
                            }
                        },
                });

        });
        $(document).on('submit','#form-review-product', function(e){
            e.preventDefault();
            $('#form-review-product button.btn').prop('disabled',true);
            $('#form-review-product button.btn').html('<span><i class="fa fa-spinner fa-spin"></i> &nbsp; Proses</span>');
            // let data = $(this).serializeArray();
            let data = new FormData(this);
            // console.log(data);
            $.ajax({
                type: 'POST',
                url: '{{route("product.post_detail_review")}}',
                data: data,
                contentType: false,
                processData: false,
                success: (response) => {
                    console.log(response)
                    if (response.status == true) {
                        this.reset();
                        Swal.fire(
                            'Berhasil!',
                            response.msg,
                            'success');
                        $('#content').html(response.html);
                    } else {
                        $('#form-review-product button.btn').prop('disabled',false);
                        $('#form-review-product button.btn').html('<i class="fa fa-star"> Simpan Review </i>');
                        Swal.fire(
                            'Warning',
                            response.msg,
                            'warning');
                    }
                },
            });

        });
    </script>
@endsection
